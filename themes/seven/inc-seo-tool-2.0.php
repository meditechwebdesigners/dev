<script type="text/javascript">
  
  // ================================ FUNCTIONS ==============================================
  
  // function to search particular field for existance of chosen keywords (array, string, string, integer, array)...
  function search_content_for_keywords(keyphrase_array, field_data, field_name, main_content_word_count, response_colors){

    var matched_words = new Array();
    var total_matches = 0;
    
    for(var k = 0; k < keyphrase_array.length; k++){
      var keyword_text = keyphrase_array[k]; 
      // create regular expression... 
      var re = new RegExp(keyword_text, "ig");
      // find and count matches for kew phrase...
      var matches = ( field_data.match(re) || [] ).length;
      total_matches = total_matches + matches;
      // add results to array...
      matched_words.push(matches);
    }
    
    if(total_matches > 0){
      
      if(field_name == 'Main Content'){
        // key phrase density max is 2.5% : but I feel more comfortable with 1.5% for our users...
        // especially if we ever have longer articles...
        var key_phrase_density = (total_matches * 100) / main_content_word_count;
        var optimal_number_of_key_phrases = Math.ceil( (main_content_word_count * 1.5) / 100 );
        if( key_phrase_density >= 1.6 ){
          var density_adjustment = total_matches - optimal_number_of_key_phrases;
          document.write('<p class="' + response_colors[2][0] + '"><span class="icon">' + response_colors[2][1] + '</span> The key phrase density is too high <strong>(' + key_phrase_density.toFixed(2) + '%)</strong>. The key phrases are used too many times for the amount of text on the page. <strong>To reach the optimal density of 1.5%, remove ' + density_adjustment + ' of your key phrases from the main content.</strong></p>');
        }
        else if( key_phrase_density > 1.3 && key_phrase_density < 1.6 ){
          var density_adjustment =  optimal_number_of_key_phrases - total_matches;
          document.write('<p class="' + response_colors[0][0] + '"><span class="icon">' + response_colors[0][1] + '</span> The key phrase density is good <strong>(' + key_phrase_density.toFixed(2) + '%)</strong>.</p>');
        }
        else{
          document.write('<p class="' + response_colors[2][0] + '"><span class="icon">' + response_colors[2][1] + '</span> The key phrase density is too low <strong>(' + key_phrase_density.toFixed(2) + '%)</strong>. The key phrases are not used enough for the amount of text on the page. <strong>To reach the optimal density of 1.5%, add ' + density_adjustment + ' more instances of your key phrases to the main content.</strong></p>');
        }
      }
      else if(field_name == '1st paragraph'){
        // 1st Paragraph...
        document.write('<p class="' + response_colors[0][0] + '"><span class="icon">' + response_colors[0][1] + '</span> Your key phrases appear <strong>(' + total_matches + ')</strong> time(s) in the <em>' + field_name + '</em>.</p>');
      }
      else{
        // otherwise good...
        document.write('<p class="' + response_colors[0][0] + '"><span class="icon">' + response_colors[0][1] + '</span> Your key phrases appear <strong>(' + total_matches + ')</strong> time(s) in the <em>' + field_name + '</em> field(s).</p>');
      }
    }
    else{
      // otherwise bad...
      document.write('<p class="' + response_colors[2][0] + '"><span class="icon">' + response_colors[2][1] + '</span> Your key phrases appear <strong>(' + total_matches + ')</strong> time(s) in the <em>' + field_name + '</em> field(s).</p>');
    }
    
    // show found keywords...
    document.write('<p><span class="found-key-phrases">Key Phrases Found:</span><br />');
    if(total_matches > 0){
      for(var m = 0; m < matched_words.length; m++){
        if(matched_words[m] > 0){
          document.write('(' + matched_words[m] + ' : ' + keyphrase_array[m] + ') ');
        }
      }
    }
    else{
      document.write('<span class="' + response_colors[2][0] + '"><span class="icon">' + response_colors[2][1] + '</span> No key phrases found.</span>');
    }
    document.write('</p>');
  }
  
  
  // function to generate progress bars (integer, integer, string)...
  function create_progress_bar(word_count_result, highest_number, progress_color){
    var progress_width = Math.ceil( (word_count_result * 100) / highest_number );
    if(progress_width > 100){
      var progress_div_width = 100;
    }
    else if(progress_width < 0){
      var progress_div_width = 0;
    }
    else{
      var progress_div_width = progress_width;
    }
    document.write('<div class="bar-graph">');
    document.write('<div style="width:' + progress_div_width + '%; background-color:' + progress_color + '; text-align:center;">');
    document.write('<p>' + progress_width + '%</p>');
    document.write('</div>');
    document.write('</div>');
  }
  
  
  // replace HTML tags, returns and tabs in content with spaces (string)...
  function remove_HTML(content_to_strip){
    var rex1 = /(<([^>]+)>)|[\n\t\r]/ig;
    var stripped_content = content_to_strip.replace(rex1 , " ");
    // replace multiple white spaces with single white spaces...
    var rex2 = /\s{2,}/ig;
    var clean_content = stripped_content.replace(rex2, " ");
    return clean_content;
  }
  
 
  // count how many times an item appears in an array OR all items (array, string[optional])...
  function count_array_values(array, classifier){
    return array.reduce(function(counter, item){
      var p = (classifier || String)(item);
      counter[p] = counter.hasOwnProperty(p) ? counter[p] + 1 : 1;
      return counter;
    }, {})
  }
  
  
  // Polyfills for Object.values and Object.entries functions not supported in browsers...
  const reduce = Function.bind.call(Function.call, Array.prototype.reduce);
  const isEnumerable = Function.bind.call(Function.call, Object.prototype.propertyIsEnumerable);
  const concat = Function.bind.call(Function.call, Array.prototype.concat);
  const keys = Reflect.ownKeys;
  if(!Object.values){
    Object.values = function values(O){
      return reduce(keys(O), (v, k) => concat(v, typeof k === 'string' && isEnumerable(O, k) ? [O[k]] : []), []);
    };
  }
  if(!Object.entries){
    Object.entries = function entries(O){
      return reduce(keys(O), (e, k) => concat(e, typeof k === 'string' && isEnumerable(O, k) ? [[k, O[k]]] : []), []);
    };
  }
  
  
  
  
  // ================================ VARIABLES ==============================================
  
  
  var good = new Array('good', '&#10004;', 'green');
  var so_so = new Array('so-so', '&#9873;', 'orange');
  var bad = new Array('bad', '&#10008;', 'red');
  var response_colors = new Array(good, so_so, bad);

  var title_data = document.getElementById('edit-title').value;
  var keyphrase_data = document.getElementById('edit-field-meta-key-phrase-und-0-value').value;
  var meta_desc_data = document.getElementById('edit-field-meta-description-und-0-value').value;
  var page_url = document.getElementById('edit-path-alias').value;
  var website_url = 'https://ehr.meditech.com/';
  
  <?php 
  // get the content type to determine which additional fields to tell the JS to look at...
  $content_type = ($node->type);
  switch($content_type){
    case 'news_article':
  ?>
      var subtitle_data = document.getElementById('edit-field-subtitle-und-0-value').value;
      var summary_data = document.getElementById('edit-field-summary-und-0-value').value;
      var body_data = document.getElementById('edit-field-body-und-0-value').value;
      var body_data_copy = body_data;
      body_data = remove_HTML(body_data); // primarily to strip out hyperlinks
      var main_content_data = title_data + ' ' + subtitle_data + ' ' + body_data;
  <?php
      break;
  }
  ?>  
  

  
  // ================================ STYLES ==============================================
  
  document.write('<style>');
  document.write('#seo-tool { word-wrap: break-word; }');
  document.write('#seo-tool h2 { font-size: 2em; line-height: 1.25em; }');
  document.write('#seo-tool h3 { font-size: 1.65em; }');
  document.write('#seo-tool h4 { font-size: 1.35em; color: #666; }');
  document.write('.found-key-phrases { font-weight: bold; color: #666; }');
  document.write('.small { font-size: .8em; font-weight: normal; }');
  document.write('.good { color: green; }');
  document.write('.bad { color: red; }');
  document.write('.so-so { color: #c67b1f; }');
  document.write('.icon { font-size: 1.5em; }');
  document.write('hr { border-top: 2px solid black; margin: 1em 0; }');
  document.write('.bar-graph { background: #333; color: white; }');
  document.write('#seo-tool .bar-graph { word-wrap: normal; }');
  document.write('.no-bullets { list-style: none; margin-left: 0; }');
  document.write('.small { font-size: .9em; }');
  document.write('</style>');
  
  
  
  // ================================ SEO TOOL STARTS HERE ==============================================

  document.write('<div id="seo-tool">');
  document.write('<h2 style="text-align:center;">SEO Assistant</h2>');
  document.write('<p>This tool is designed to help you improve your content\'s search engine ranking.</p>');
  document.write('<p>This tool will analyze the page content in the form on the left and point out any issues or recommended improvements.</p>');
  document.write('<p>It focuses on the current DRAFT version of the content and NOT on the live content.</p>');
  document.write('<p>These results will update after you save your page edits.</p>');


  
  document.write('<hr /><!-- DIVIDER ============================================= -->');

  

  document.write('<h3>Key Phrases:</h3>');
  document.write('<p>These are the chosen key phrases:</p>');

  // grab key phrases from Meta Key Phrase field and create an array...
  var keyphrase_array_pretrimmed = keyphrase_data.toLowerCase().split(",");
  var keyphrase_array = new Array();
  for(var kPhrase = 0; kPhrase < keyphrase_array_pretrimmed.length; kPhrase++){
    keyphrase_array.push(keyphrase_array_pretrimmed[kPhrase].trim()); // trim white spaces off keywords
  }
  
  document.write('<ul class="no-bullets">'); 
  for(var k = 0; k < keyphrase_array.length; k++){
    var phrase = keyphrase_array[k].split(' ');
    var phrase_length = phrase.length;
    if(phrase_length <= 5){
      document.write('<li>"' + keyphrase_array[k] + '"</li>');
    }
    else{
      document.write('<li class="' + response_colors[2][0] + '">"' + keyphrase_array[k] + '" : <span class="icon">' + response_colors[2][1] + '</span> Yikes! <strong>This key phrase is too long</strong>. Try to keep your phrases to 5 words or less.</li>');
    }
  }
  document.write('</ul>');

  document.write('<h4>Words Used the Most:</h4>');
  
  // break up key phrases into array of words...
  var keywords = new Array();
  for(var kws = 0; kws < keyphrase_array.length; kws++){
    var key_words = keyphrase_array[kws].split(' ');
    for(var kw = 0; kw < key_words.length; kw++){
      if(key_words[kw] != ''){
        keywords.push(key_words[kw]);
      }
    }
  }
  
  // convert main content into an array and remove white spaces, punctuation and make words lowercase...
  var main_content_array = main_content_data.split(' ');
  // remove blanks from array...
  var main_content_array_no_blanks = new Array();
  for(var n = 0; n < main_content_array.length; n++){
    if(main_content_array[n] != '' && main_content_array[n] != ' '){
      // remove apostrophe s and punctuation...
      var punct_regex = /('s)|(’s)|['’,.!?"”()]/ig;
      var content_no_punct = main_content_array[n].replace(punct_regex, "");
      main_content_array_no_blanks.push( content_no_punct.toLowerCase() );
    }
  }
  
  // list of words to ignore during word counting...
  var words_to_ignore = new Array("a", "about", "above", "after", "again", "against", "all", "am", "an", "and", "any", "are", "as", "at", "be", "because", "been", "before", "being", "below", "between", "both", "but", "by", "could", "did", "do", "does", "doing", "down", "during", "each", "few", "for", "from", "further", "had", "has", "have", "having", "he", "he'd", "he'll", "he's", "her", "here", "here's", "hers", "herself", "him", "himself", "his", "how", "how's", "i", "i'd", "i'll", "i'm", "i've", "if", "in", "into", "is", "it", "it's", "its", "itself", "let's", "me", "more", "most", "my", "myself", "nor", "of", "on", "once", "only", "or", "other", "ought", "our", "ours", "ourselves", "out", "over", "own", "same", "she", "she'd", "she'll", "she's", "should", "so", "some", "such", "than", "that", "that's", "the", "their", "theirs", "them", "themselves", "then", "there", "there's", "these", "they", "they'd", "they'll", "they're", "they've", "this", "those", "through", "to", "too", "under", "until", "up", "very", "was", "we", "we'd", "we'll", "we're", "we've", "were", "what", "what's", "when", "when's", "where", "where's", "which", "while", "who", "who's", "whom", "why", "why's", "with", "would", "you", "you'd", "you'll", "you're", "you've", "your", "yours", "yourself", "yourselves", "—");
  
  // strip out ignored words from main content...
  var main_content_minus_ignored_words = new Array();
  for(var i = 0; i < main_content_array_no_blanks.length; i++){
    if( words_to_ignore.indexOf(main_content_array_no_blanks[i]) == -1 ){
      main_content_minus_ignored_words.push(main_content_array_no_blanks[i]);
    }
  }
  
  // count how many times each word occurs and place in an object...
  var word_values_count = count_array_values(main_content_minus_ignored_words);
  
  // convert previous object into an array using polyfill function...
  var word_values_count_array = Object.entries(word_values_count);
    
  // sort array by counts in ascending order...
  word_values_count_array.sort((function(index){
    return function(a, b){
      return (a[index] === b[index] ? 0 : (a[index] < b[index] ? -1 : 1));
    };
  })(1)); // 1 is the index
  
  // get the last 5 items in array, check if they match keywords in key phrases and create a score and responses... 
  document.write('<p>These are the words used most often on this page.</p>');
  document.write('<ul class="no-bullets">');
  var total_num_of_words = word_values_count_array.length;
  var most_used_score = 0;
  for(var num = 1; num < 6; num++){
    var word_location = total_num_of_words - num;
    var word = word_values_count_array[word_location][0];
    var count = word_values_count_array[word_location][1];
    if(keywords.indexOf(word) != -1){
      document.write('<li class="' + response_colors[0][0] + '"><span class="icon">' + response_colors[0][1] + '</span> ' + word + ' (<strong>' + count + '</strong>)</li>');
      most_used_score++;
    }
    else{
      document.write('<li class="' + response_colors[2][0] + '"><span class="icon">' + response_colors[2][1] + '</span> ' + word + ' (<strong>' + count + '</strong>)</li>');
    }
  }
  document.write('</ul>');
  
  if(most_used_score < 5){
    document.write('<p class="' + response_colors[2][0] + '"><span class="icon">' + response_colors[2][1] + '</span> Shucks. It appears that the top 5 words used in this content are not consistant with the words in the key phrases. The content may need to be re-evaluated.</p>');
  }
  else{
    document.write('<p class="' + response_colors[0][0] + '"><span class="icon">' + response_colors[0][1] + '</span> Excellent! The words in the key phrases are among the most used words in the content.</p>');
  }
 

  
  document.write('<hr /><!-- DIVIDER ============================================= -->');

  
  
  document.write('<h3>Meta Description:</h3>');
  search_content_for_keywords(keyphrase_array, meta_desc_data, 'Meta Description', '', response_colors);
  
  var meta_desc_character_count = meta_desc_data.length;
  
  if(meta_desc_character_count < 156){ 
    var meta_desc_class = response_colors[2][0];
    var meta_desc_symbol = response_colors[2][1];
    var meta_desc_progress_color = response_colors[2][2];
    var meta_desc_character_count_response = 'Boo. The recommended minimum for the Meta Description is <strong>156 characters</strong>. See if you can improve this so that search engines have enough content to define this page.';
  }
  else if(meta_desc_character_count < 140){
    var meta_desc_class = response_colors[1][0];
    var meta_desc_symbol = response_colors[1][1];
    var meta_desc_progress_color = response_colors[1][2];
    var meta_desc_character_count_response = 'Darn. This does not meet the <strong>156 character</strong> recommended minimum. It would be more beneficial if you could expand the Meta Description just a little more.';
  }
  else if(meta_desc_character_count >= 156){
    var meta_desc_class = response_colors[0][0];
    var meta_desc_symbol = response_colors[0][1];
    var meta_desc_progress_color = response_colors[0][2];
    var meta_desc_character_count_response = 'Great! This meets the <strong>156 character</strong> recommended minimum.';
  }
  
  
  document.write('<h4>Character Count</h4>');
  document.write('<p class="' + meta_desc_class + '"><span class="icon">' + meta_desc_symbol + '</span> Your Meta Description has a total of <strong>' + meta_desc_character_count + '</strong> characters. ' + meta_desc_character_count_response + '</p>');
  
  create_progress_bar(meta_desc_character_count, 156, meta_desc_progress_color);

  
  
  document.write('<hr /><!-- DIVIDER ============================================= -->');

  

  document.write('<h3>Page URL:</h3>');
  var page_url_no_hyphens = page_url.replace(/-/ig, ' ');
  search_content_for_keywords(keyphrase_array, page_url_no_hyphens, 'Page URL', '', response_colors);
  document.write('<em>These results may be low if you have a hyphenated key phrase.</em>');
  
  
  
  document.write('<hr /><!-- DIVIDER ============================================= -->');

  

  document.write('<h3>Title:</h3>');
  search_content_for_keywords(keyphrase_array, title_data, 'Title', '', response_colors);
  
  document.write('<h4>Character Count:</h4>');
  
  var title_character_count = title_data.length;
  
  if(title_character_count < 40){
    var title_class = response_colors[2][0];
    var title_symbol = response_colors[2][1];
    var title_progress_color = response_colors[2][2];
    var title_character_count_response = 'Boo. The recommended character count for the Page Title is between <strong>40 to 70 characters</strong>. See if you can improve this so that search engines have enough content to define this page.';
  }
  else if(title_character_count > 39 && title_character_count < 71){
    var title_class = response_colors[0][0];
    var title_symbol = response_colors[0][1];
    var title_progress_color = response_colors[0][2];
    var title_character_count_response = 'Great! This meets the Page Title\'s recommended character count of <strong>40 to 70 characters</strong>.';
  }
  else if(title_character_count > 70 && title_character_count < 90){
    var title_class = response_colors[1][0];
    var title_symbol = response_colors[1][1];
    var title_progress_color = response_colors[1][2];
    var title_character_count_response = 'Yikes! The Page Title is over the <strong>40 to 70 characters</strong> recommended character count. Make sure your key phrases are within the first 70 characters.';
  }
  else{
    var title_class = response_colors[2][0];
    var title_symbol = response_colors[2][1];
    var title_progress_color = response_colors[2][2];
    var title_character_count_response = 'Woah!! The Page Title is way over the <strong>40 to 70 character</strong> recommended character count. Your Page Title should really be shorter. Not all the text will be visible in search results.';
  }

  document.write('<p class="' + title_class + '"><span class="icon">' + title_symbol + '</span> Your Page Title has a total of <strong>' + title_character_count + '</strong> characters. ' + title_character_count_response + '</p>');
  
  create_progress_bar(title_character_count, 70, title_progress_color);
  
  
  document.write('<h4>Key Phrase Placement:</h4>');
  document.write('<ul class="no-bullets">');
  for(var kp = 0; kp < keyphrase_array.length; kp++){
    var title_data_lower_case = title_data.toLowerCase();
    // if key phrase exists in title...
    if( title_data_lower_case.indexOf(keyphrase_array[kp]) >=0 ){
      // find out how long the key phrase is...
      var kp_length = keyphrase_array[kp].length;
      // find out how far from the beginning of the text the key phrase can start...
      var available_character_space = 70 - kp_length;
      // is the starting point of the key phrase within the available_character_space...
      if( title_data_lower_case.indexOf(keyphrase_array[kp]) <= available_character_space){
        document.write('<li class="' + response_colors[0][0] + '"><span class="icon">' + response_colors[0][1] + '</span> Cool! The key phrase "<strong>' + keyphrase_array[kp] + '</strong>" is close to the beginning of the text and therefore it will be <strong>visible</strong> in a search result\'s page title snippet.</li>');
      }
      else if(title_data_lower_case.indexOf(keyphrase_array[kp]) <= 70){
        document.write('<li class="' + response_colors[1][0] + '"><span class="icon">' + response_colors[1][1] + '</span> Not quite there. The key phrase "<strong>' + keyphrase_array[kp] + '</strong>" is too far from the beginning of the text and therefore it is only <strong>slightly visible</strong> in a search result\'s page title snippet. Try re-working the page Title so that the key phrases appear closer to the beginning.</li>');
      }
      else{
        document.write('<li class="' + response_colors[2][0] + '"><span class="icon">' + response_colors[2][1] + '</span> Oops! The key phrase "<strong>' + keyphrase_array[kp] + '</strong>" is too far from the beginning of the text and therefore it is <strong>NOT visible</strong> in a search result\'s page title snippet. Try re-working the page Title so that the key phrases appear closer to the beginning.</li>');
      }
    }
  }
  document.write('</ul>');
  

  
  document.write('<hr /><!-- DIVIDER ============================================= -->');
  

  
  document.write('<h3>Google Search Result Simulation:</h3>');
  /*
  for(var g = 0; g < keyphrase_array.length; g++){
    var keyword_text = keyphrase_array[g];
    // create regular expression... 
    var re = new RegExp(keyword_text, "ig");
    // find matches for kew phrases and add to array...
    var matched_terms = title_data.match(re);
console.log(matched_terms);
    for(let term of matched_terms){
console.log(title_data.indexOf(term));
    }    
  }
  */


  
  document.write('<hr /><!-- DIVIDER ============================================= -->');

  

  document.write('<h3>Subtitle:</h3>');
  if(subtitle_data.length > 0){
    search_content_for_keywords(keyphrase_array, subtitle_data, 'Subtitle', '', response_colors);
  }
  else{
    document.write('<p>The <em>Subtitle</em> field is empty.');
  }


  
  document.write('<hr /><!-- DIVIDER ============================================= -->');


  
  document.write('<h3>Summary:</h3>');
  search_content_for_keywords(keyphrase_array, summary_data, 'Summary', '', response_colors);

  

  document.write('<hr /><!-- DIVIDER ============================================= -->');

  

  document.write('<h3>Main Content <br /><span class="small">(Title, Subtitle & Body)</span>:</h3>');
  
  var main_content_word_count = main_content_array_no_blanks.length;
  if(main_content_word_count < 280){
    var main_content_class = response_colors[2][0];
    var main_content_symbol = response_colors[2][1];
    var main_content_progress_color = response_colors[2][2];
    var main_content_word_count_response = 'Boo. The recommended minimum for page content is <strong>300 words</strong>. See if you can improve this so that search engines have enough content to define what it is relative to.';
  }
  else if(main_content_word_count < 300){
    var main_content_class = response_colors[1][0];
    var main_content_symbol = response_colors[1][1];
    var main_content_progress_color = response_colors[1][2];
    var main_content_word_count_response = 'Darn. This does not meet the <strong>300 word</strong> recommended minimum. It would be more beneficial if you could expand the content just a little more.';
  }
  else if(main_content_word_count >= 300){
    var main_content_class = response_colors[0][0];
    var main_content_symbol = response_colors[0][1];
    var main_content_progress_color = response_colors[0][2];
    var main_content_word_count_response = 'Great! This meets the <strong>300 word</strong> recommended minimum.';
  }
  
  document.write('<h4>Key Phrase Density</h4>');
  search_content_for_keywords(keyphrase_array, main_content_data, 'Main Content', main_content_word_count, response_colors);
  
  
  document.write('<h4>First Paragraph</h4>');
  // convert 1st paragraph content into an array and remove white spaces, punctuation and make words lowercase...
  var body_data_array = body_data_copy.split('</p>');
  var body_data_first_paragraph = remove_HTML(body_data_array[0]);
  search_content_for_keywords(keyphrase_array, body_data_first_paragraph, '1st paragraph', '', response_colors);
  
  
  document.write('<h4>Content Size</h4>');
  document.write('<p class="' + main_content_class + '"><span class="icon">' + main_content_symbol + '</span> Your main content has a total of <strong>' + main_content_word_count + '</strong> words. ' + main_content_word_count_response + '</p>');
  create_progress_bar(main_content_word_count, 300, main_content_progress_color);
  
  
  document.write('<h4>Headers</h4>');
    
  document.write('<p>This section checks to see if there are any headers in the page text. Headers with key phrases can help improve the page\'s rank. Headers are not necessary for shorter pages of content. They help the readability of larger pages.</p>');
  
  var reg_headers = /(<h[2-6]>)/igm; // find open header tags in content...
  var header_matches = body_data_copy.match(reg_headers);
  
  // find out how many chunks of 500 words are in the content...
  var content_chunks = Math.floor(main_content_word_count / 500);

  // report how many headers to use based on content quantity...
  if( content_chunks < header_matches.length ){
    document.write('<p class="' + response_colors[1][0] + '"><span class="icon">' + response_colors[1][1] + '</span> This page uses more headers than are necessary. Based on the amount of content on this page, the recommendation is to use no more than <strong>' + content_chunks + '</strong> headers.</p>');
  }
  else if( content_chunks == header_matches.length ){
    document.write('<p class="' + response_colors[0][0] + '"><span class="icon">' + response_colors[0][1] + '</span> This page uses just enough headers, based on the amount of content on this page.</p>');
  }
  else{
    document.write('<p class="' + response_colors[1][0] + '"><span class="icon">' + response_colors[1][1] + '</span> This page uses less headers than what is optimal. Based on the amount of content on this page, the recommendation is to use at least <strong>' + content_chunks + '</strong> headers.</p>');
  }
  
 //
  //
  //
  // check if headers contain key phases
  //
  //
  //

    
  document.write('<h4>Link Quantity</h4>');  
  // grab all existing links in body content...
  var reg_links = /<a [^>]+>([^<]+)<\/a>/ig;
  var links = body_data_copy.match(reg_links);
  
  if(links != null){

    document.write('<p class="' + response_colors[0][0] + '"><span class="icon">' + response_colors[0][1] + '</span> Awesome! The main content has links in it. This helps search engines find other content and form relationships between differnet pages of content.</p>');

    document.write('<h4>Link Text Quality</h4>'); 

    var link_text_matches_array = new Array();

    var links_text_array = new Array();
    var links_url_array = new Array();

    // process each link...
    for(var l = 0; l < links.length; l++){  
      // strip out URL and anchor text...
      var reg_url = /"([^< ]+)"/; 
      var link_url = links[l].match(reg_url);
      var reg_text = />([^<]+)</;
      var link_text = links[l].match(reg_text);

      // add text from anchor tag to array for later...
      links_text_array.push(link_text);

      // separate text words...
      var link_text_array = link_text[1].split(' ');

      if(link_text_array.length < 2){
        document.write('<p class="' + response_colors[2][0] + '"><span class="icon">' + response_colors[2][1] + '</span> Flag down on link #' + [l+1] + '. <strong>The text used to link to another page or website needs to be more than 1 word</strong>. Try to use key phrases important to the destination page as the link text.</p>');
        var link_text_quality = 'no';
      }

      // create new array and add URL to it...
      var link_text_match_array = new Array(link_url[1]);
      // loop through words to see if they appear in URL...
      for(var lt = 0; lt < link_text_array.length; lt++){
        var link_text_in_url_array = new Array();
        var reg_hyphen = /-/g; // remove hyphens from URL...
        var link_url_no_hyphens = link_url[1].replace(reg_hyphen, ' ');

        if( link_url_no_hyphens.match( link_text_array[lt].toLowerCase() ) ){
          // add matched word to array... 
          link_text_in_url_array.push(link_text_array[lt]);
          // add array to link array...
          link_text_match_array.push(link_text_in_url_array);
        }
      }
      // add un-hyphenated URL to array for later...
      links_url_array.push(link_url_no_hyphens);

      // add link array to group array...
      link_text_matches_array.push(link_text_match_array);
    }

    if(link_text_quality != 'no'){
      document.write('<p class="' + response_colors[0][0] + '"><span class="icon">' + response_colors[0][1] + '</span> Good. Each of the links in the main content use more than 1 word to link with. This improves a search engine\'s ability to form relationships between pages of content.</p>');
    }

    document.write('<h4>Link Text Content</h4>');

    document.write('<p>The next section checks to see if the link text in each link matches up with the words in the URL of its link. <strong>The closer the text matches the URL, the more important the destination page appears to search engines</strong>.</p><p>It\s also important that key phrases on the destination page be used in this page\'s link text.</p><p><em>This is less important for external links unless those external sites are important to your company. Also we have less control over external URLs, resulting in some unavoidable negative results below.</em></p>');

    // see if text as a whole phrase is in URL...
    var whole_text_answers = new Array();
    // see if text as a whole phrase without spaces (WU like) is in URL...
    var whole_text_answers_no_spaces = new Array();
    // see if text contains keyphrases...
    var keyphrase_in_link_text_answers = new Array();

    for(var whole_text = 0; whole_text < links_text_array.length; whole_text++){

      var link_phrase = links_text_array[whole_text][1].toLowerCase();
      var url_from_link = links_url_array[whole_text].toLowerCase();
      // if whole phrase appear in link exactly...
      if( url_from_link.indexOf(link_phrase) >= 0 ){
        whole_text_answers.push('yes');
      }
      else{
        whole_text_answers.push('no');
      }

      var reg_spaces = /\s/g; // remove spaces from URL
      var link_phrase_no_spaces = link_phrase.replace(reg_spaces, ''); 
      // if whole phrase without spaces appear in link exactly...
      if( url_from_link.indexOf(link_phrase_no_spaces) >= 0 ){
        whole_text_answers_no_spaces.push('yes');
      }
      else{
        whole_text_answers_no_spaces.push('no');
      }
      // check to see if this page's keyphrase are in the link text...
      for(var keyp = 0; keyp < keyphrase_array_pretrimmed.length; keyp++){
        var trimmed_keyphrase = keyphrase_array_pretrimmed[keyp].trim();
        if( link_phrase.indexOf(trimmed_keyphrase) >= 0 ){
          keyphrase_in_link_text_answers.push([keyp, 'yes']);
        }
        else{
          keyphrase_in_link_text_answers.push([keyp, 'no']);
        }
      }

    } 

    // loop through results of word matches and create responses...
    for(var group_of_links = 0; group_of_links < link_text_matches_array.length; group_of_links++){
      // if link has text matches...
      if(link_text_matches_array[group_of_links].length > 1){

        document.write('<h4>Link #' + (group_of_links + 1) + '</h4>');

        if(keyphrase_in_link_text_answers[group_of_links][1] == 'yes'){
          document.write('<p class="' + response_colors[2][0] + '"><span class="icon">' + response_colors[2][1] + '</span> Oh dear. At least 1 of the key phrases for this page is in this link\'s text. Unless the destination page uses the same key phrase, this will weaken this page\'s ranking with search engines. Try to avoid this.</p>');
        }

        // if the whole phrase with or without spaces doesn't match what is in the URL, move on..
        if(whole_text_answers[group_of_links] == 'no' && whole_text_answers_no_spaces[group_of_links] == 'no'){

          var total_matched_words = link_text_matches_array[group_of_links].length - 1;
          // figure out responses...
          if(total_matched_words > 3){ // 4 or more = good
            var link_class = response_colors[0][0];
            var link_symbol = response_colors[0][1];
            var link_response = 'Great! The';
          }
          else if(total_matched_words < 4 && total_matched_words > 1){ // 2-3 = so-so
            var link_class = response_colors[1][0];
            var link_symbol = response_colors[1][1];
            var link_response = 'Not great. Only the';
          }
          else{ // less than 2 = bad
            var link_class = response_colors[2][0];
            var link_symbol = response_colors[2][1]; 
            var link_response = 'Not good. Only the';
          }

          document.write('<p class="' + response_colors[1][0] + '"><span class="icon">' + response_colors[1][1] + '</span> Unfortunately, the link text as a whole phrase does not appear to be in the link\'s URL. This weakens the link\'s authority which weakens the destination page\'s ranking.</p>');

          document.write('<p class="' + link_class + '"><span class="icon">' + link_symbol + '</span> ' + link_response + ' word(s): ');

          // loop through words and list them... 
          for(var mWords = 1; mWords < total_matched_words+1; mWords++){
            document.write('<strong>' + link_text_matches_array[group_of_links][mWords] + '</strong>, ');
          }
          document.write(' from the link\'s text appear in its URL: <em class="small">' + link_text_matches_array[group_of_links][0] + '</em></p>');

        }
        else{
          document.write('<p class="' + response_colors[0][0] + '"><span class="icon">' + response_colors[0][1] + '</span> Awesome!! The text in the link appears word-for-word in the URL. This increases the link\'s authority which strengthens the destination page\'s ranking.</p>');
        }

      }
      else{
        // no text matches...
        document.write('<p class="' + response_colors[2][0] + '"><span class="icon">' + response_colors[2][1] + '</span> Not Good. The text in the link does not appear to match the text in the link\'s URL. Try to change the wording so the text and the URL are more similar.</p>');
      }
    }
  }
  else{
    document.write('<p class="' + response_colors[1][0] + '"><span class="icon">' + response_colors[1][1] + '</span> Meh! The main content does not appear to have any links in it. Although not necessary, links help encourage visitors to continue browsing our site or other helpful sites. It also helps search engines form relationships between differnet pages of content.</p>');
  }
  
  

  
  document.write('<hr /><!-- DIVIDER ============================================= -->');


  
  document.write('<h2>Want to learn more about SEO?</h2>');
  document.write('<p>Take a look at the <a href="https://staff.meditech.com/mktwebdocumentation/SEO-Best-Practices/" target="_blank">SEO Best Practices Documentation</a>.</p>');
  
  
  
  document.write('</div>');

</script>