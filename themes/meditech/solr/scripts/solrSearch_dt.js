$(document).ready(function () {
	
    //use to debug popstate event
    window.onpopstate = function(event) {
        //alert("location: " + document.location + ", state: " + JSON.stringify(event.state));
    };
    
    $('#loading-image').hide();
	var form = document.getElementById('searchform');
    
	//.attachEvent() is deprecated in IE9+, and has been removed in IE11.
	if (typeof window.addEventListener === 'function') {
		// Check for addEventListener first, since IE9/10 have both
		form.addEventListener("submit", runsearch);
		//push into history
		window.addEventListener("submit", function (e) {
            //if there is sessionStorage and a solrSearchStamp push the search into history
            if (('sessionStorage' in window) && window.sessionStorage !== null) {
                //ATWEB-6091 - Add Navigation between searches
                //Only push the state into history when we actually ran a simple search (when we have the timestamp)
                if (sessionStorage.getItem("solrSearchStamp")) {                    
                    history.pushState({
                            solrSearchString: sessionStorage.getItem("solrSearchString"),
                            solrSearchDocStatus: sessionStorage.getItem("solrSearchDocStatus"),
                            solrSearchServer: sessionStorage.getItem("solrSearchServer"),
                            solrSearchWuSite: sessionStorage.getItem("solrSearchWuSite"),
                            solrSearchFrom: sessionStorage.getItem("solrSearchFrom"),
                            solrSearchThru: sessionStorage.getItem("solrSearchThru"),
                            solrSearchStamp: sessionStorage.getItem("solrSearchStamp")
                        },null,'#' + document.searchform.SearchString.value
                    );
                }
            }
		});

		//Add Event Listener to back and forward on popstate
		window.addEventListener("popstate", function (e) {
			if ('state' in window.history && window.history.state !== null) {
                //Only restore history state when we previously ran a search (when we have the timestamp)
                //ATWEB-6091 - Add Navigation between searches
				if (history.state['solrSearchStamp']) {
                    //Repopulate Advanced option from history 
					document.searchform.SearchString.value = history.state['solrSearchString'];
                    var serverArrObj = JSON.parse(history.state['solrSearchServer']),
                        statusArrObj = JSON.parse(history.state['solrSearchDocStatus']),
                        wuSiteArrObj   = JSON.parse(history.state['solrSearchWuSite']);                                                          
                    // if server value was blank empty server select
		    if(serverArrObj.length == 0) {
                        $('#server').val('').trigger('change');                        
                    }
                    else {
                        var serverList = [];
                        $.each(serverArrObj,function( key, obj ) {    
                            serverList.push(obj["id"]);
                        });
                        $("#server").val(serverList).trigger('change');
                    }
                    
                    // if status value was blank empty status select
		    if(statusArrObj.length == 0) {
                        $('#docStatus').val('').trigger('change');  
                    }
                    else {
                        var docStatusList = [];
                        $.each(statusArrObj,function( key, obj ) {    
                            docStatusList.push(obj["id"]);
                        });
                        $("#docStatus").val(docStatusList).trigger('change');                       
                    }                   
  
                    // if wusite value was blank empty wusite select
                    if(wuSiteArrObj.length == 0) { 
                        $('#wuSite').val('').trigger('change');  
                    }
                    // else look up su site from history and populate select2 field from history value
                    else {
                        var wuSiteList = [];
                        $.each(wuSiteArrObj,function( key, obj ) {    
                            wuSiteList.push(obj["id"]);
                        });                  
                        // Fetch the pre-selected item from history, and add to the control
                        var wuSiteSelect = $('#wuSite');
                        $.ajax({
                            type: 'POST',
                            url: 'scripts/lookupWuSite.php',
                            dataType: 'json',
                            cache: false,
                            async: false,
                            // Disable global Ajax event handlers
                            global: false,
                            data: {
                                term: wuSiteList,
                                type: 'list_site'
                            }
                        }).then(function (data) {
                            // create the option and append to Select2
                            var w =[];
                            $.each(data.results,function( key, obj ) {    
                                w[key] = new Option(obj["text"],obj["id"],true,true);
                            });
                            // clear all user first
                            $('#wuSite').val('').trigger('change'); 
                            // append option and trigger change
                            wuSiteSelect.append(w).trigger('change');
                            // manually trigger the `select2:select` event
                            wuSiteSelect.trigger({
                                type: 'select2:select',
                                params: {
                                    data: data.results
                                }
                            });
                        });                    
                    }
                    // populate range value from history
                    $('#fromID').val(history.state['solrSearchFrom']);
                    $('#thruID').val(history.state['solrSearchThru']);
                    
                    runsearch(e);
				}
			}
		});
	} else if (typeof window.attachEvent === 'object') {
		form.attachEvent("submit", runsearch);
	}

	//Starting jQuery 1.9, AJAX events should be attached to document only.
	//Show loading before ajax
	$(document).ajaxStart(function () {
		$("#loading-image").show();
	});
	//hide loading after ajax
	$(document).ajaxStop(function () {
		$("#loading-image").hide();
        //ATWEB-6482 by WLUO if searchform is hidden from externalCall show it
        document.getElementById('searchform').style.visibility = 'visible';
	});
 
});

function runsearch(e) {
	if (e.preventDefault) e.preventDefault();

	var payload = {},
		status = [],
		wuSite = [],
		server = [],
		serverSelection = [],
		statusSelection = [],
        wuSiteSelection = [],
		dateSelection = [],
		sForm;

	$('.status').each(function () {
		var entry = $(this),
			id = entry.attr('id'),
            name = entry.text();
		if (id) {
			status.push({
				id: id,
				name: name
			});
			statusSelection.push(id);
		}
	});
    
	$('.server').each(function () {      
		var entry = $(this),
			id = entry.attr('id'),
            name = entry.text();
		if (id) {
			server.push({
				id: id,
				name: name
			});
			serverSelection.push(id);
		}
	});

	$('.wuSite').each(function () {
		var entry = $(this),
			id = entry.attr('id'),
            name = entry.text();
		if (id) {
			wuSite.push({
				id: id,
				name: name
			});
			wuSiteSelection.push({
				id: id,
				name: name
			});
		}
	});    
	payload.status = status;
	payload.server = server;
    payload.wuSite = wuSite;
	from = $('#fromID').val() ? moment($('#fromID').val()).format("YYYY-MM-DD") : ""
	thru = $('#thruID').val() ? moment($('#thruID').val()).format("YYYY-MM-DD") : ""
	dateSelection = [from, thru];
    
    //if thru is empty set thru to today if there is a from
    if (from && !thru) {
        thru = moment().format("YYYY-MM-DD");
    }
    
    //if there is date range and no type (published or edited) set the type to both
    if ((from || thru) && status.length == 0) {        
        status.push({
            id: "wuPublish",
            name: "Published"
		});
        
        status.push({
            id: "wuEdit",
            name: "Edited"
		});        
    }

    //if same search just return without doing anything and clear the search timestamp    
    if (sameSearch(document.searchform.SearchString.value, status, server, wuSite, from, thru)) {
        if (('sessionStorage' in window) && window.sessionStorage !== null) {
            sessionStorage.setItem("solrSearchStamp", '');
        }
        return;
    }

	if (('sessionStorage' in window) && window.sessionStorage !== null) {
		sessionStorage.setItem("solrSearchString", document.searchform.SearchString.value);
		sessionStorage.setItem("solrSearchDocStatus", JSON.stringify(payload.status));
		sessionStorage.setItem("solrSearchServer", JSON.stringify(payload.server));
        sessionStorage.setItem("solrSearchWuSite", JSON.stringify(payload.wuSite));
		sessionStorage.setItem("solrSearchFrom", from);
		sessionStorage.setItem("solrSearchThru", thru);
        sessionStorage.setItem("solrSearchStamp", Date.now());
	}

	$.ajax({
			type: 'POST',
			url: "scripts/getSearchResult_json.php",
			cache: false,
			data: {
				SearchString: document.searchform.SearchString.value,
				Status: payload.status,
                WuSite: payload.wuSite,
				Server: payload.server,
				From: from,
				Thru: thru
			},
			success: function (json_data) {
				if (json_data.length == 0) {
					console.log("search return NO DATA!");
				} else {
					$("main").empty();
					buildContainer(serverSelection, statusSelection, dateSelection,wuSiteSelection);

					var result = [],
						json_result = {};

					$.each(json_data.response.docs, function (i, item) {
						var path = "",
                            json_item = {};
                            
						// Replace with web path
                        // Match Home 
                        if (item.id.search(/\\\\www96\\live_en_d/) == 0) {
							path = item.id.replace("\\\\www96\\live_en_d", 'https://home.meditech.com/en/d');
                            json_item["strFileType"] = item.id.toLowerCase().substr(item.id.lastIndexOf('.') + 1);
                            json_item["strTitle"] = ((typeof item.title === "undefined") || (item.title === "")) ? item.id.replace(/\\/g, '/').replace(/.*\//, '') : item.title;
                            json_item["strHighlight"] = json_data.highlighting[item.id]["content"].map(function (x) {
                                return x.replace(/^\s+|\s+$/g, '');
                            });
                            json_item["strServer"] = item.id.split("\\")[2];
                            json_item["strPublishDate"] = item.wuPublishDate  ? new Date(item.wuPublishDate.replace(' ','T')).toISOString() : item.timestamp;
                            json_item["strUserTitle"] = item.wuUserTitle;
                        // Match Customer 
						}else if (item.id.search(/\\\\www98\\live_en_d/) == 0) {
							path = item.id.replace("\\\\www98\\live_en_d", 'https://customer.meditech.com/en/d');
                            json_item["strFileType"] = item.id.toLowerCase().substr(item.id.lastIndexOf('.') + 1);
                            json_item["strTitle"] = ((typeof item.title === "undefined") || (item.title === "")) ? item.id.replace(/\\/g, '/').replace(/.*\//, '') : item.title;
                            json_item["strHighlight"] = json_data.highlighting[item.id]["content"].map(function (x) {
                                return x.replace(/^\s+|\s+$/g, '');
                            });
                            json_item["strServer"] = item.id.split("\\")[2];
                            json_item["strPublishDate"] = item.wuPublishDate  ? new Date(item.wuPublishDate.replace(' ','T')).toISOString() : item.timestamp;
                            json_item["strUserTitle"] = item.wuUserTitle;
                        // Match http or https on blog.meditech
						}else if (item.id.search(/^https?:\/\/blog.meditech.com/) == 0) {                            
                            path = item.id;
                            json_item["strFileType"] = "html";
                            json_item["strTitle"] = item.page_title;
                            json_item["strHighlight"] = json_data.highlighting[item.id]["post_body"].map(function (x) {
                                return x.replace(/^\s+|\s+$/g, '');
                            });                        
                            json_item["strServer"] = "blog";
                            json_item["strPublishDate"] = moment(item.publish_date).toISOString();
                        }
                        // for ehr.meditech
                        else {
                            path = item.url;
                            json_item["strFileType"] = "html";
                            json_item["strTitle"] = item.label;
                            json_item["strHighlight"] = json_data.highlighting[item.id]["content"].map(function (x) {
                                return x.replace(/^\s+|\s+$/g, '');
                            });
                            json_item["strServer"] = "ehr";
							json_item["strPublishDate"] = item.ds_changed; 
						}                            
						//replace backward slash with forward slash
						path = path.replace(/\\/g, "/");
                        // path and score
						json_item["strThePath"] = path;						
						json_item["strScore"] = item.score
                        // Push row data into result
						result.push(json_item);
					});
					
					destoryDatatable('searchResultTable');
					$("#searchResultTable").show();

					var oTable = $('#searchResultTable').dataTable({
						"processing": true,
						"searching": true,
						"pagingType": "simple_numbers",
						"order": [
							[4, 'dsc']
						],
						"autoWidth": false,
						"dt-responsive": true,
						"lengthChange": false,
						"info": true,
						"data": result,
						//defer creating row until needed
						"deferRender": true,
						"orderClasses": false,
						//callback for row created, use to debug how many row created at render                        
						"createdRow": function (row, data, dataIndex) {
							//row = Td row element that has just been created
							//data = Raw data source (array or object) for this row
							//dataIndex = The index of the row in DataTables' internal storage.
						},
						"language": {
							"search": "_INPUT_",
							"searchPlaceholder": "Filter Results..."
						},
						"columns": [{
								"Data": "strFileType"
							},
							{
								"Data": "strThePath"
							},
							{
								"Data": "strHighlight"
							},
							{
								"Data": "strTitle"
							},
							{
								"Data": "strScore"
							},
							{
								"Data": "strServer"
							},
							{
								"Data": "strPublishDate"
							}							
						],
						"columnDefs": [{
								"data": "strFileType",
								"targets": 0,
								"orderable": false,
								"visible": false
							},
							{
								"data": "strThePath",
								"targets": 1,
								"orderable": false,
								"render": function (data, type, full, meta) {
									var singleItem = "";
									
									singleItem += '<article class="search-results-item">';
									singleItem += '<h5> <a href="' + full.strThePath + '" target="_blank">' + (full.strUserTitle ? full.strUserTitle : full.strTitle) + '</a></h5>';
									singleItem += '<p class="search-results-item-description">';
									singleItem += full.strHighlight.join("<br>");
									singleItem += '</p>';
									if (full.strPublishDate) {
										singleItem += '<p class="search-results-item-description">'										
										singleItem += 'Published on ' + moment(full.strPublishDate).format("MMMM Do, YYYY");
										singleItem += '</p>';
									}
									if (full.strServer == 'www98') {
										singleItem += '<span class="fa fa-lock" title="Customer Access Only"></span> ';
									}
									singleItem += '<span class="search-results-item-url"><a href="' + full.strThePath + '" target="_blank">';
									singleItem += full.strThePath;
									singleItem += '</a></span>';
									
									// Display ranking score if no port (dev)
									if (location.port) {									
										singleItem += '<p class="search-results-item-description">'	
										singleItem += 'score: ' + full.strScore;
										singleItem += '</p>';
									}
									singleItem += '</article>';

									return singleItem;
								}
							},
							{
								"data": "strHighlight",
								"targets": 2,
								"orderable": false,
								"visible": false
							},
							{
								"data": "strTitle",
								"targets": 3,
								"orderable": false,
								"visible": false
							},
							{
								"data": "strScore",
								"targets": 4,
								"orderable": true,
								"visible": false
							},
							{
								"data": "strServer",
								"targets": 5,
								"orderable": false,
								"visible": false
							},
							{
								"data": "strPublishDate",
								"targets": 6,
								"orderable": false,
								"visible": false
							}							
						],
					});
				}
			}
		})
		.done(function (json_data) {
			//ATWEB-5915 by WLUO trigger current filter
			$('#' + sessionStorage.getItem("solrSearchFilterId")).click();
			//ATWEB-5912 by WLUO bind scroll event and setup scroll when clicking on page
			paginateScroll();
		})
		.fail(function (jqxhr_success, textStatus, error) {
			var err = textStatus + ', ' + error;
			console.log("Request Failed: " + err);
		});
}

//sort array by key value
//in : array,sortByKeyName
//out: array
function sortByKeysortByKey(array, key) {
	return array.sort(function (a, b) {
		var x = a[key];
		var y = b[key];
		return ((x < y) ? -1 : ((x > y) ? 1 : 0));
		//to reverse sort (desc)
		//return ((x < y) ? -1 : ((x > y) ? 1 : 0)) * -1;
	});
}

//in : coreUser OID
//out: name Value
//*** this is a sync function ***
function getUserName(oid) {
	var jqXHR =
		$.ajax({
			url: 'scripts/lookupStaff.php',
			dataType: 'json',
			cache: false,
			async: false,
            // Disable global Ajax event handlers
            global: false,
			data: {
				term: oid,
				type: 'oid'
			}
		})
	return jqXHR.responseJSON.results.nameValue;
}

function buildContainer(serverSelection, statusSelection, dateSelection,wuSiteSelection) {
	if (!serverSelection) {
		serverSelection = [];
	}
	if (!statusSelection) {
		statusSelection = [];
	}
	if (!dateSelection) {
		dateSelection = [];
	}

	var statusList = [{
				"id": "wuPublish",
				"name": "Published"
			},
			{
				"id": "wuEdit",
				"name": "Edited"
			}
		],
		serverList = [{
				"id": "ehr",
				"name": "ehr.meditech.com"
			},
			{
				"id": "customer",
				"name": "customer.meditech.com"
			},            
			{
				"id": "home",
				"name": "home.meditech.com"
			},              
			{
				"id": "blog",
				"name": "blog.meditech.com"
			}        
		],
		part1 = [
			'<section class="row">',
			'<img style="display:none;" src="assets/img/iconAjaxLoader.gif" id="loading-image" class="float-center" alt="Loading" title="Loading" border=0>',
			'<div class="small-12 columns filter-types">',
			'<nav class="search-top-nav" id="filter-bar">',
			'<ul class="menu" data-disable-hover="true" data-click-open="true" data-close-on-click="false">',
			'<li><a id="f-all" href="#all" onclick="filterRow(\'all\',this);" title="All">All</a></li>',
			'<li class="active"><a id="f-articles" href="#articles" onclick="filterRow(\'articles\',this);" title="Articles">Articles</a></li>',
			'<li><a id="f-files" href="#files" onclick="filterRow(\'files\',this);" title="Files">Files</a></li>',
			'<li><a id="f-images" href="#images" onclick="filterRow(\'images\',this);" title="Images">Images</a></li>',
			'<li><a id="f-videos" href="#videos" onclick="filterRow(\'videos\',this);" title="Videos">Videos</a></li>',
			// Disabled Advanced Search
			//'<li><a data-toggle="advDropdown">Advanced Search</a>',
			'<li><a id="f-videos" data-open="searchengineModal">Help <span class="fa fa-question-circle"></span></a>',						
			'<div class="f-adv-search bottom dropdown-pane shadow-2dp" id="advDropdown" data-dropdown data-auto-focus data-close-on-click="true">',
			'<label for="server"><span class="fa fa-server"></span> Server</label>',
			'<select id="server" multiple="multiple" style="width:100%;">' + buildSelectField(serverSelection, serverList) + '</select>',
			'<label for="status"><span class="fa fa-flag-o"></span> Status</label>',
			'<select id="docStatus" multiple="multiple" style="width:100%;">' + buildSelectField(statusSelection, statusList) + '</select>',
			'<label for="wuSite"><span class="fa fa-home "></span> Web Utility Site</label>',
			'<select id="wuSite" multiple="multiple" style="width:100%;">' + buildWuSite(wuSiteSelection) + '</select>',            
			'<div class="f-adv-range">',
			'<label for="range"><span class="fa fa-calendar "></span> Range</label>',
			'<label>From</label><input type="text" size="30" id="fromID" name="fromdate" value=' + (dateSelection[0] ? dateSelection[0] : '""') + '></td><td>',
			'<label>Thru</label><input type="text" size="30" id="thruID" name="thrudate" value=' + (dateSelection[1] ? dateSelection[1] : '""') + ' max = ' + moment().format("YYYY-MM-DD") + ' >',
			'</div>',
			'<button class="button secondary tiny float-right" id="adv-search-go" >Go</span>',
			'</div>',
			'</li>',
			'</ul>',
			'</nav>',
			'</div>',
			'<div class="small-12 large-9 columns datatables-container">',
			'<table cellspacing="0" class="display unstriped" cellspacing="0" id="searchResultTable">',
			'<thead>',
			'<tr>',
			'<th class="search_type">Type</th>',
			'<th class="search_result"></th>',
			'<th class="search_highlight">Highlight</th>',
			'<th class="search_title">Title</th>',
			'<th class="search_score">Score</th>',
			'</tr>',
			'</thead>',
			'<tbody></tbody>',
			'</table>',
			'</div>',
			'</section>'
		].join('\n');
        
	$("main").html(part1);

	// re-initialize all foundation
	$(document).foundation();

	//advanced search dropdown show/open event
	//do nothing now
	$('#advDropdown').on('show.zf.dropdown', function () {
		//DO SOMETHING
	});

	//advanced search dropdown hide/close event
	//run search 
	$('#advDropdown').on('hide.zf.dropdown', function (e) {    
		runsearch(e);
        //if there is sessionStorage and a solrSearchStamp push the search into history
        if (('sessionStorage' in window) && window.sessionStorage !== null) {
            //ATWEB-6091 - Add Navigation between searches
            //Only push the state into history when we actually ran a advanced search (when we have the timestamp)
            if (sessionStorage.getItem("solrSearchStamp")) {
                history.pushState({
                        solrSearchString: sessionStorage.getItem("solrSearchString"),
                        solrSearchDocStatus: sessionStorage.getItem("solrSearchDocStatus"),
                        solrSearchServer: sessionStorage.getItem("solrSearchServer"),
                        solrSearchWuSite: sessionStorage.getItem("solrSearchWuSite"),
                        solrSearchFrom: sessionStorage.getItem("solrSearchFrom"),
                        solrSearchThru: sessionStorage.getItem("solrSearchThru"),
                        solrSearchStamp: sessionStorage.getItem("solrSearchStamp")
                    },null,'#' + document.searchform.SearchString.value
                );
            }
        }        
	});

	//Toggles (closes) AdvDropdown on Click
	$('#adv-search-go').click(function() {
		$('#advDropdown').foundation('toggle'); 
	});
	
	//initialize select2 
	loadMultiSelect();
  
    //ATWEB-5913 calendar plugin
    $("#fromID, #thruID").datetimepicker({
        lazyInit:true,
        timepicker:false,
        format:'Y-m-d',
        maxDate: '0'
        }
    );
    
}

function hideRow() {
	var table = $('#searchResultTable').DataTable();
	table.column(0).visible(false);
}

function filterRow(val, event) {

	var table = $('#searchResultTable').DataTable();

	if (val == 'articles') {
		val = '^htm$|^html$';
	} else if (val == 'files') {
		val = '^doc$|^pdf$|^xls$|^ppt$|^sql$|^xlsx$|^docx$|^pptx$|^zip$';
	} else if (val == 'images') {
		val = '^jpg$|^jpeg$|^gif$|^png$|^svg$';
	} else if (val == 'videos') {
		val = '^flv$|^f4v$|^swf$';
	} else if (val == 'all')
		val = "^htm$|^html$|^doc$|^pdf$|^xls$|^ppt$|^sql$|^xlsx$|^docx$|^pptx$|^zip$|^jpg$|^jpeg$|^gif$|^png$|^svg$|^flv$|^f4v$|^swf$";

	//rewrite filtered table content
	table.column(0).search(val ? val : '', true, false).draw();

	//change active filter tab
	$("#filter-bar>ul>li.active").removeClass("active");
	event.parentElement.className = "active";

	//ATWEB-5915 by WLUO save current filter
	sessionStorage.setItem('solrSearchFilterId', event.id);
	//ATWEB-5912 by WLUO bind scroll event and setup scroll when clicking on page
	paginateScroll();
}

//return nothing
//destory datatable by id
function destoryDatatable(tableId) {
	$(document).ready(function () {
		var oTable = $('#' + tableId).dataTable();
		oTable.fnDestroy();
		$('#' + tableId).empty();
	});
}

// Build html for multiselect fields.
function buildSelectField(selections, list) {
	var html = '';
	for (var i = 0; i < list.length; i++) {
		var entry = list[i],
			id = entry.id,
			name = entry.name,
			selected = '';
		if (selections.indexOf(id) >= 0) {
			selected = 'selected';
		}
		html += '<option value="' + id + '" ' + selected + '>' + name + '</option>';
	}
	return html;
}

// Build html for multiselect Users.
function buildUser(selections) {
	var html = '',
		i;
	for (i = 0; i < selections.length; i++) {
		var selection = selections[i],
			id = selection.id,
			name = selection.name;
		html += '<option value="' + id + '" selected>' + name + '</option>';
	}
	return html;
}

// Build html for multiselect WuSite.
function buildWuSite(selections) {
	var html = '',
		i;
	for (i = 0; i < selections.length; i++) {
		var selection = selections[i],
			id = selection.id,
			name = selection.name;
		html += '<option value="' + id + '" selected>' + name + '</option>';
	}
	return html;
}

function loadMultiSelect() {
    //initialize select2 for status
	$docStatus = $('#docStatus');
	// Multiselect status (publish or edit)
	$docStatus.select2({
		placeholder: 'Select Document Status',
		templateSelection: function (selection) {
			var $selection = $('<span id="' + selection.id + '" class="status">' + selection.text + '</span>');
			return $selection;
		},
		theme: "foundation",
		allowClear: true
	});

	$docStatus.on('select2:unselect', function () {
		function cancelAndRemove(event) {
			event.preventDefault()
			removeEvents()
		}

		function removeEvents() {
			$docStatus.off('select2:opening', cancelAndRemove)
			$docStatus.off('select2:closing', cancelAndRemove)
		}
		$docStatus.on('select2:opening', cancelAndRemove)
		$docStatus.on('select2:closing', cancelAndRemove)
		setTimeout(removeEvents, 0)
	});

    //initialize select2 for server
	$server = $('#server');
	// Multiselect server 
	$server.select2({
		placeholder: 'Select Server',
		templateSelection: function (selection) {
			var $selection = $('<span id="' + selection.id + '" class="server">' + selection.text + '</span>');
			return $selection;
		},
		theme: "foundation",
		allowClear: true
	});

	$server.on('select2:unselect', function () {
		function cancelAndRemove(event) {
			event.preventDefault()
			removeEvents()
		}

		function removeEvents() {
			$server.off('select2:opening', cancelAndRemove)
			$server.off('select2:closing', cancelAndRemove)
		}
		$server.on('select2:opening', cancelAndRemove)
		$server.on('select2:closing', cancelAndRemove)
		setTimeout(removeEvents, 0)
	});
    //Any change to the server value will reset wuSite field
    $server.on('change',function () {
        $('#wuSite').select2("val", "");
    });    

    //initialize select2 for WU site
	$wuSite = $('#wuSite');
	//select WU site from table lookup
	$wuSite.select2({
		language: {
			inputTooShort: function () {
				return 'Enter at least 3 characters of a Web Utility site.';
			},
			searching: function () {
				return 'Searching...';
			}
		},
		minimumInputLength: 3,
		width: '100%',
		ajax: {
			url: 'scripts/lookupWuSite.php',
			dataType: 'json',
			type: 'POST',
			data: function (params) {
				return {
					term: params.term,
                    server: JSON.stringify(getServerSelected()),
					type: 'lookahead'
				};
			},
			processResults: function (data) {
				return {
					results: data.results
				};
			},
            cache: false,
            // Disable global Ajax event handlers
            global: false, 
		},
		allowClear: true,
		theme: "foundation",
		templateSelection: function (selection) {
			return $('<span id="' + selection.id + '" class="wuSite">' + selection.text + '</span>');
		},
		escapeMarkup: function (markup) {
			return markup;
		} // let our custom formatter work        
	});
	$wuSite.on('select2:unselect', function () {               
		function cancelAndRemove(event) {
			event.preventDefault()
			removeEvents()
		}

		function removeEvents() {
			$wuSite.off('select2:opening', cancelAndRemove)
			$wuSite.off('select2:closing', cancelAndRemove)
		}
		$wuSite.on('select2:opening', cancelAndRemove)
		$wuSite.on('select2:closing', cancelAndRemove)
		setTimeout(removeEvents, 0)
	});    
}

function getServerSelected() {
    var wuServer = [];
    $('.server').each(function () {      
        var entry = $(this),
            id = entry.attr('id'),
            name = entry.text();
        if (id) {
            wuServer.push((id=="home" ? "www" : id));
        }
    });
    if (wuServer.length > 0) {
        return wuServer;
    }
    return ["customer","www"];
}

//return true if all the argument is same
//return false if any of the argument is not same
function sameSearch(searchString, status, server, wuSite, from, thru) {

	if (searchString !== sessionStorage.getItem("solrSearchString"))
		return false;
	if (JSON.stringify(status) !== sessionStorage.getItem("solrSearchDocStatus"))
		return false;
	if (JSON.stringify(server) !== sessionStorage.getItem("solrSearchServer"))
		return false;
	if (JSON.stringify(wuSite) !== sessionStorage.getItem("solrSearchWuSite"))
		return false;    
	if (from !== sessionStorage.getItem("solrSearchFrom"))
		return false;
	if (thru !== sessionStorage.getItem("solrSearchThru"))
		return false;

	return true;
}

function disableSelect2(id) {
	$('#' + id).select2('destroy');
	$('#' + id).prop('disabled', true);
	$('#' + id).select2();
}

function enableSelect2(id) {
	$('#' + id).select2('destroy');
	$('#' + id).prop('disabled', false);
	$('#' + id).select2();
}

function loadingAnimation() {
	var loading = ['<div id="searchResultTable" class="row align-center">',
		'<div class="small-12 columns">',
		'<img src="assets/img/iconAjaxLoader.gif" id="loading-image" alt="Loading" title="Loading" border=0></br>',
		'</div>',
		'<div class="row align-center">',
		'Processing...',
		'</div>',
		'</div>'
	].join('\n');
	return loading;
}

//ATWEB-5912 by WLUO scroll function back to the top when paging
function paginateScroll() {
	$('html, body').animate({
		//scrollTop: $(".dataTables_wrapper").offset().top
		scrollTop: top
	}, 100);
	$(".paginate_button").unbind('click', paginateScroll);
	$(".paginate_button").bind('click', paginateScroll);
}