<?php 
//ATWEB-6956 - Add restrict to WU Site to advanced search

// Define required files.
require_once 'autoload.php';
	
// Define variables.
$term = $_POST['term'];
$type = $_POST['type'];
if( isset($_POST['server']) )
    $server = json_decode($_POST['server']);

// Get matches.
$app = new App();

if ($type == 'lookahead') {
    $results = $app->wuSiteLookup($term,$server);
}
else if ($type == 'list_site') {
    $results = $app->wuSiteLookupByOidBatch($term);
}

//gzip content
ob_start("ob_gzhandler");
header('Content-Type: application/json');
echo json_encode(array('results'=>$results));
ob_end_flush();

?>