<!-- START campaign--node-2527.php -->
<?php // This template is set up to control the display of the MEDITECH_EXPANSE_ACUTE_CARE content type

$url = $GLOBALS['base_url']; // grabs the site url
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>

  /******************************************************

    GRID

  ******************************************************/

  .grid-item {
    border-right: 1px solid #ffffff;
    border-bottom: 1px solid #ffffff;
    background: none;
    height: 12em;
    padding: 2em 2em 1em 2em;
  }

  @media (max-width: 480px) {
    .grid-item {
      border: none !important;
    }
  }

  .grid-item--numbers {
    margin-top: 0;
  }

  .no-bullets{
    list-style: none;
  }
  
  .big-bold { font-size: 3em; font-weight: bold; line-height: 1em; margin-top: .25em; }

</style>


<div class="js__seo-tool__body-content">

<!--Block 1-->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/male-nurse-hands-on-tablet--blurred-background.jpg);">
  <div class="container__centered">
    <div class="container__one-third">
      &nbsp;
    </div>
    <div class="container__two-thirds transparent-overlay text--white">
      <h1 class="text--white">
        Expanse Acute Care: Tap and swipe your way to shorter days.
      </h1>
      <p>
        Save time and prioritize your workload with the first web-based, mobile EHR &mdash; built by physicians for physicians. With MEDITECH Expanse, you can quickly review personalized charts, easily prioritize your patient list, and give your patients the safe, face-to-face care they deserve.
      </p>
      <p>
        Don't stay late because of complicated transitions and unfinished documentation. Work smarter with an EHR that gives you the same user-friendly experience, whether you're in the inpatient environment, at the clinic, in your office &mdash; everywhere you make rounds.
      </p>
      
      <div class="center" style="margin-top:2em;">
        <?php hubspot_button($cta_code, "Download The Union Hospital Stage 7 Case Study"); ?>
      </div>
      
    </div>
  </div>
</div>
<!--End of Block 1-->


<!-- Block 2 -->
<div class="content__callout">
    <div class="content__callout__media">
        <div class="content__callout__image-wrapper">
            <div class="video js__video" data-video-id="277667398">
                <figure class="video__overlay">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/PAC_2018_Partnership--Video.jpg" alt="Dr Fletcher Patient Populations Video Covershot">
                </figure>
                <a class="video__play-btn" href="https://vimeo.com/277667398"></a>
                <div class="video__container"></div>
            </div>
        </div>
    </div>
    <div class="content__callout__content">
        <div class="content__callout__body">
            <div class="content__callout__body__text">
                <h2>What does it mean to have an EHR that's built by, and for, physicians?</h2>
            </div>
        </div>
    </div>
</div>
<!-- END Block 2 -->


<!-- Block 2 VIDEO
<div class="content__callout">
  <div class="content__callout__media">
    <h2 class="content__callout__title">Partnership</h2>
    <div class="content__callout__image-wrapper">
      <div class="video js__video" data-video-id="236768236">
        <figure class="video__overlay">
          <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Bryan-Bagdasian-MD-and-Janet-Desroche.jpg" alt="Bryan Bagdasian, MD and Janet Desroche">
        </figure>
        <a class="video__play-btn" href="https://vimeo.com/236768236"></a>
        <div class="video__container">
        </div>
      </div>
    </div>
  </div>
  <div class="content__callout__content">
    <div class="content__callout__body">
      <p>What does it mean to have an EHR that's built by, and for, physicians?</p>
    </div>
  </div>
</div>
<!-- End Block 2 VIDEO -->


<!-- Block 3 STATS-->
<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/green-pattern-01.png);">
  <div class="container__centered">

    <div class="page__title--center text--white">
      <h2>Just a touch or two away from the information you need.</h2>
    </div>

    <div class="container center no-pad">

      <div class="container__one-half text--white">
        <div class="transparent-overlay">
          <p class="big-bold">100+ Widgets and Specialty Options</p>
          <h4>Personalize Your Patient Summary</h4> 
          <p>Tailor your layout with widgets and specialty options.</p>
        </div>
      </div>

      <div class="container__one-half text--white">
        <div class="transparent-overlay">
          <p class="big-bold">80% of Order Sets Entered with 1 Tap</p>
          <h4>Order with One Tap of Your Finger</h4> 
          <p>Enter order sets &mdash; developed in collaboration with Zynx Health, Inc.</p>                
        </div>
      </div>

    </div>

  </div>
</div>
<!-- End Block 3 STATS -->


<!-- Block 4 --> 
<div class="container background--cover text--white" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/documenting.jpg);">
  <div class="container__centered">
    <div class="center" style="margin-bottom:2em;">
      <h2>Document the way you want.</h2>
      <h3>Customize patient summaries, assessments, and plans to fit your work style, so you can give your patients more attention, and <a href="https://blog.meditech.com/how-ehrs-can-give-time-back-to-docs">give yourself more time.</a></h3>
    </div>

    <div class="text--white">

      <div class="container__one-fourth">
        <div class="transparent-overlay center">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon-workflows.png" alt="graphic" style="width:50%;">
          <p>Personalize workflow with expert-based standard content templates.</p>
        </div>
      </div>

      <div class="container__one-fourth">
        <div class="transparent-overlay center">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon-physician-documentation.png" alt="graphic" style="width:50%;">
          <p>Document faster with voice recognition, quick text, and saved typicals/normals.</p>
        </div>
      </div>

      <div class="container__one-fourth">
        <div class="transparent-overlay center">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon-interface-panels.png" alt="graphic" style="width:50%;">
          <p>Use Diagnostics Panels for filtering and viewing across specialties.</p>
        </div>
      </div>

      <div class="container__one-fourth">
        <div class="transparent-overlay center">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon-diagnostic-graph.png" alt="graphic" style="width:50%;">
          <p>Identify patient trends with flowsheets that pull data from across care settings.</p>
        </div>
      </div>

    </div>
  </div>
</div>
 <!-- Block 4 -->
 

<!-- Block 5 Quote -->
<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/blue-boxes-pattern.jpg);">
  <div class="container__centered">
    <figure class="container__one-fourth center">
      <img class="quote__content__img" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/smith_justin.png" alt="Justin Smith MD">
    </figure>
    <div class="container__three-fourths text--white">
      <div class="quote__content__text text--large" style="margin-top:1em;">
        <p class="italic">"Every click matters. One click, two or three seconds here, two or three seconds there, by the end of the day it amounts to a significant amount of time. If you look at that over weeks and months it really adds up."</p>
      </div>
      <p class="text--large no-margin--bottom">Justin W. Smith, MD</p>
      <p>Adena Health System</p>
    </div>
  </div>
</div>
<!-- End Block 5 Quote -->


<!-- Block 6 -->
<div class="container">
  <div class="container__centered">
    <div class="container__two-thirds">
      <h2>Excellence included, with embedded Clinical Decision Support.</h2>
      <p>Advance clinical decision-making from day one. Our evidence- and expert-based Standard Content &mdash; embedded in Expanse for Acute Care &mdash; is coordinated with best practice workflows, for safer patient care from the start. Pre-built, <a href="https://ehr.meditech.com/ehr-solutions/meditech-ehr-excellence-toolkits">physician-related toolkits </a> provide outcomes-focused implementation guides that outline best practice workflow and setup recommendations to detect and treat sepsis, CAUTI, and other high priority conditions. </p>
    </div>
    <div class="container__one-third" style="text-align: center">
      <figure>
        <img width="200" style="margin-top: 50px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/build-guide--toolkit-green.png" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/build-guide--toolkit-green.png';this.onerror=null;" alt="green toolkit icon">
      </figure>
    </div>
  </div>
</div>
<!-- Close Block 6 -->

<!-- Block 7 -->
<div class="container bg--emerald">
    <div class="container__centered">
      <div class="container__one-third">
        <h2 class="text--white no-margin--top">Know  your  patients.</h2>
        <div class="text--white">
          <ul>
            <li>View handoff messages, new results, and consult requests from across the continuum</li>
            <li>Tailor your Rounding List to prioritize workflow in one place</li>
            <li>Check the Activity Log for a comprehensive look at your patient's current state, with abnormal or critical results expanded</li>
            <li>Learn their specific circumstances, with special indicators</li>
            <li>Build rapport with patients through personal notes, reminders, and checklists</li>
          </ul>
        </div>
      </div>
      <div class="container__two-thirds">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/acute-care-01.jpg" alt="MEDITECH Surveillance Desktop screenshot">
      </div>
    </div>
  </div>
<!-- END Block 7 -->

<!-- Block 8-->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Interoperability.jpg);">
  <div class="container"> 
    <div class="container__centered">
      <div class="container__two-thirds transparent-overlay">
        <h2 class="text--white no-margin--top">Connect with outside providers.</h2>
        <div class="text--white">
          <p>MEDITECH Expanse is making <a href="https://blog.meditech.com/a-new-way-to-drive-interoperability-and-improve-patient-experience">clinically integrated networks</a> run like clockwork, which will help you do the same. Connect across all care settings and exchange information with outside providers through C-CDAs, APIs, FHIR, HL7, and Direct Messaging. Our <a href="https://ehr.meditech.com/news/alliance-community-hospital-first-meditech-customer-to-deploy-commonwell-services"> successful deployment of CommonWell </a> interoperability services gives you an uninterrupted view of the patient story, no matter where they last received care.</p>
      </div>
      </div>
    </div>
  </div>
</div>

<!-- End Block 8 -->

<!-- Block 9 --> 

<div class="container bg--emerald">
  <div class="container__centered">
    <h2>Prepare for quick and easy discharges.</h2>
    <p>Start planning your patient's discharge on day one with our multidisciplinary Discharge routine. Coordinate with the care team while requesting follow-ups and diagnostics, ordering durable medical equipment, automatically converting inpatient orders to ambulatory orders, and more. You can also manage prescriptions seamlessly with embedded tools offered in collaboration with DrFirst® &mdash; all without having to log into other systems or leave the patient's record. 
</p>

<div style="width:100%; border-bottom:10px dotted#6ac4a9; height:1px; margin:2em 0;"></div>

    <div>
      <div class="container__one-half">
        <ul class="no-bullets">
          <li><i class="far fa-check-square" aria-hidden="true" style="padding-right:0.5em;" ></i> Use e-Prescribing to electronically submit prescriptions (including controlled substances) to outpatient pharmacies.</li>
          <li><i class="far fa-check-square" aria-hidden="true" style="padding-right:0.5em;" ></i>Review medication claims histories.</li>
        </ul>
      </div>
      <div class="container__one-half">
        <ul class="no-bullets">
          <li><i class="far fa-check-square" aria-hidden="true" style="padding-right:0.5em;" ></i><a href="https://ehr.meditech.com/news/meditech-integrates-drug-monitoring-within-expanse-to-improve-opioid-prescribing">Query state prescription drug management plans.</a></li>
          <li><i class="far fa-check-square" aria-hidden="true" style="padding-right:0.5em;" ></i> Submit prior authorizations to ensure eligibility.</li>
          <li><i class="far fa-check-square" aria-hidden="true" style="padding-right:0.5em;" ></i> Check the insurance formulary for covered medications.</li>
        </ul>
      </div>
    </div>
    
  </div>
</div>

<!--End Block 9 -->



<!-- Block 10-->

<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/em_trans_vehicle.jpg);">
  <div class="container__centered">

    <div class="page__title--center">
      <h2 class="text--fresh-mint">Ensure smooth care transitions.</h2>
      <h3 class="text--white">Transitioning patients <a href="https://ehr.meditech.com/ehr-solutions/meditechs-care-coordination">between care settings</a> 
      can be complicated.<br/>Simplify the process with Expanse.</h3>

    </div>

    <div class="container center no-pad--bottom">

      <div class="container__one-half text--white">
        <div class="transparent-overlay">
           <h4>Medication Reconciliation</h4>
           <p>Easily reconcile medications for a complete snapshot of the patient's medication history, including home prescriptions, drugs given during surgery, and medications ordered in acute care.</p>              
          </div>
      </div>

      <div class="container__one-half text--white">
        <div class="transparent-overlay">
           <h4>Handover Coordination</h4>
           <p>Coordinate handovers with a Sign Out routine to communicate the patient's status and next steps to new providers &mdash; especially when transitioning a patient from an emergent setting to inpatient care. </p>               
        </div>
      </div>

    </div>

  </div>
</div>



<!-- end js__seo-tool__body-content -->

<!--Block 11-->
<div class="container bg--black-coconut" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/blue-boxes-pattern.jpg);">
  <div class="container__centered center">
    
    <?php cta_text($cta); ?>
    
    <div class="center" style="margin-top:2em;">
      <?php hubspot_button($cta_code, "Download The Union Hospital Stage 7 Case Study"); ?>
    </div>

    <div style="margin-top:1em;">
        <?php print $share_link_buttons; ?>
    </div>
    
  </div>
</div>
<!-- Close Block 11 CTA -->
<!-- END campaign--node-2527.php -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
