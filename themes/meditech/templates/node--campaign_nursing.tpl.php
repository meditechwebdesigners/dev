<?php // This template is set up to control the display of the CAMPAIGN_NURSING content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

// FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM with mulitple instances...
// to use the function simply assign it to a variable
// then pass in the field collection machine name to the function and the number of instances allowed ($quantity)
// then you'll use the variable when calling an array for each group of sub-fields within the template
function multi_field_collection_data($node, $field_name, $quantity){
  // get array specific to a certain field collection item...
  $field_collection = field_get_items('node', $node, $field_name);
  // create array...
  $multi_field_collections = array();
  // loop through '$quantity' times...
  for($x=0; $x<$quantity; $x++){
    // get all content within that field collection...
    $fc_field_data = entity_load('field_collection_item', array($field_collection[$x]['value']));
    // get field collection ID...
    $fc_field_ID = key($fc_field_data);
    // create variable and load it with field collection data...
    $var_with_data = $fc_field_data[$fc_field_ID];
    // add data to array...
    $multi_field_collections[] = $var_with_data;
  }
  // return variable...
  return $multi_field_collections;
}
$quotes = multi_field_collection_data($node, 'field_fc_quote_1', 3);
$block_5 = multi_field_collection_data($node, 'field_fc_head_ltext_2', 4);
$block_9 = multi_field_collection_data($node, 'field_fc_head_ltext_1', 8);

// FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM...
// to use the function simply assign it to a variable
// then pass in the field collection machine name to the function
// then you'll use the variable when calling each sub-field within the template
function field_collection_data($node, $field_name){
  // get array specific to a certain field collection item...
  $field_collection = field_get_items('node', $node, $field_name);
  // get all content within that field collection...
  $fc_field_data = entity_load('field_collection_item', array($field_collection[0]['value']));
  // get field collection ID...
  $fc_field_ID = key($fc_field_data);
  // create variable and load it with field collection data...
  $var_with_data = $fc_field_data[$fc_field_ID];
  // return variable...
  return $var_with_data;
}
$button_1 = field_collection_data($node, 'field_fc_button_1');
?>
<!-- start node--campaign_nursing.tpl.php template -->

<style>
  .button-vertical-adjustment { margin-top: 6em; }
  @media only screen and (max-width: 50em) {
    .button-vertical-adjustment { margin-top: 0; }
    .mobile-smaller-image { text-align: center; }
    .mobile-smaller-image img { width: 200px; }
    .tablet-half {
      float: left;
      display: block;
      margin-right: 2.35765%;
      width: 48.82117%;
    }
  }
  @media only screen and (max-width: 35em) {
    .tablet-half {
      float: left;
      display: block;
      margin-right: 2.35765%;
      width: 100%;
    }
    .top-margin { margin-top: 1em; }
  }
  .squish-list li { margin-bottom: .5em; }
</style>


<div class="js__seo-tool__body-content">


  <!-- BLOCK 1 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/female-nurse-male-patient-bedside-smiling.jpg);">
    <div class="container__centered">
      <div class="container__two-thirds transparent-overlay text--white">
        <h1 class="text--white js__seo-tool__title" style="margin-top:0;"><?php print $title; ?></h1>
        <h2 class="text--white"><?php print render($content['field_header_1']); ?></h2>
        <div>
          <?php print render($content['field_long_text_1']); ?>
        </div>
      </div>
      <div class="container__one-third button-vertical-adjustment">
        <div class="btn-holder--content__callout">
          <?php
          // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
          if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
            $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
          ?>
            <div class="button--hubspot">
              <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
            </div>
          <?php }else{ ?>
            <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
  <!-- BLOCK 1 -->


  <!-- BLOCK 2 -->
  <div class="content__callout border-none">
    <div class="content__callout__media">
      <div class="content__callout__image-wrapper">
        <div class="video js__video" data-video-id="<?php print render($content['field_video_url']); ?>">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--6.1-physicians-nurses.jpg" alt="Video Covershot">
          </figure>
          <a class="video__play-btn" href="http://vimeo.com/<?php print render($content['field_video_url']); ?>"></a>
          <div class="video__container"></div>
        </div>
      </div>
    </div>
    <div class="content__callout__content">
      <div class="content__callout__body__text">
       <h2><?php print render($content['field_header_2']); ?></h2>
        <?php print render($content['field_long_text_2']); ?>
      </div>
    </div>
  </div>
  <!-- BLOCK 2 -->


  <!-- BLOCK 3 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/lines-on-light-green-bg.png);">
    <div class="container__centered">
      <div class="container transparent-overlay">
        <h2 class="text--white no-margin--top"><?php print render($content['field_header_3']); ?></h2>
        <div class="text--white">
          <?php print render($content['field_long_text_3']); ?>
        </div>
      </div>
    </div>
  </div>
  <!-- BLOCK 3 -->


  <!-- BLOCK 4 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/male-nurse-male-patient-smiling.jpg);">
    <div class="container__centered">
      <div class="container__two-thirds transparent-overlay">
        <h2 class="text--white no-margin--top"><?php print render($content['field_header_4']); ?></h2>
        <div class="text--white">
          <?php print render($content['field_long_text_4']); ?>
        </div>
      </div>
    </div>
  </div>
  <!-- BLOCK 4 -->


  <!-- BLOCK 5 -->
  <div class="content__callout border-none">

    <div class="content__callout__media">
      <div class="content__callout__image-wrapper">
        <div class="video js__video" data-video-id="<?php print render($content['field_video_url_2']); ?>">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-Point-of-Care.jpg" alt="MEDITECH Point of Care">
          </figure>
          <a class="video__play-btn" href="http://vimeo.com/<?php print render($content['field_video_url_2']); ?>"></a>
          <div class="video__container">
          </div>
        </div>
      </div>
    </div>

    <div class="content__callout__content">
      <div class="content__callout__body">
         <h2><?php print render($content['field_header_5']); ?></h2>
          <?php print render($content['field_long_text_5']); ?>
      </div>
    </div>

  </div>
  <!-- BLOCK 5 -->


  <!-- BLOCK 6 -->
  <div class="container bg--emerald">
    <div class="container__centered">
      <div class="container__one-third">
        <h2 class="text--white no-margin--top"><?php print render($content['field_header_6']); ?></h2>
        <div class="text--white" >
          <?php print render($content['field_long_text_6']); ?>
        </div>
      </div>
      <div class="container__two-thirds">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-Surveillance-Desktop-screenshot.png" alt="MEDITECH Surveillance Desktop screenshot">
      </div>
    </div>
  </div>
  <!-- BLOCK 6 -->


  <!-- BLOCK 7 -->
  <div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg);">
    <div class="container__centered">
    <?php
      // add font awesome icons to array...
      $fa = array('fas fa-briefcase','fas fa-heartbeat','fas fa-users');
      $f=0;
      foreach($quotes as $quote){
    ?>
      <div class="container__one-third">
        <div class="quote__content__icon center text--white">
          <i class="<?php print $fa[$f]; ?> fa-4x"></i>
        </div>
        <div class="quote__content__text">
          <p><?php print $quote->field_quote_1['und'][0]['value']; ?></p>
        </div>
        <p class="no-margin--bottom"><?php print $quote->field_full_name['und'][0]['value']; ?></p>
        <p class="text--small"><?php print $quote->field_company['und'][0]['value']; ?></p>
      </div>
    <?php
      $f++;
      }
    ?>
    </div>
  </div>
  <!-- BLOCK 7 -->


  <!-- BLOCK 8 -->
  <div class="container bg--emerald">
    <div class="container__centered">
      <div class="container__one-third">
        <h2 class="text--white no-margin--top"><?php print render($content['field_header_8']); ?></h2>
        <div class="text--white">
          <?php print render($content['field_long_text_8']); ?>
        </div>
      </div>
      <div class="container__two-thirds">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-PCS-Status-Board-screenshot.png" alt="MEDITECH PCS Status Board screenshot">
      </div>
    </div>
  </div>
  <!-- BLOCK 8 -->


  <!-- BLOCK 9 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/care-team-working-together-darkened.jpg);">
    <div class="container__centered">

      <div class="page__title--center" style="margin-bottom:2em;">
        <h2 class="text--white"><?php print render($content['field_header_9']); ?></h2>
        <div class="page__title__ribbon"></div>
        <div class="text--white">
          <?php print render($content['field_long_text_9']); ?>
        </div>
      </div>

      <div class="container no-pad--top">
        <?php for($b1=0; $b1<4; $b1++){ ?>
        <div class="container__one-fourth">
          <div class="transparent-overlay">
            <div class="center text--fresh-mint" style="padding-bottom: .5em;"><i class="fas fa-certificate fa-4x" style="font-size:2em;"></i></div>
            <h4 class="page__title--center text--white"><?php print $block_9[$b1]->field_header_1['und'][0]['value']; ?></h4>
            <div class="text--white center">
              <?php print $block_9[$b1]->field_long_text_1['und'][0]['value']; ?>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>

 	  <div class="container no-pad">
        <?php for($b2=4; $b2<8; $b2++){ ?>
        <div class="container__one-fourth">
          <div class="transparent-overlay">
            <div class="center text--fresh-mint" style="padding-bottom: .5em;"><i class="fas fa-certificate fa-4x" style="font-size:2em;"></i></div>
            <h4 class="page__title--center text--white"><?php print $block_9[$b2]->field_header_1['und'][0]['value']; ?></h4>
            <div class="text--white center">
              <?php print $block_9[$b2]->field_long_text_1['und'][0]['value']; ?>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>

    </div>
  </div>
  <!-- BLOCK 9 -->


  <!-- BLOCK 10 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/business-people-illustration-green-background.png);">
    <div class="container__centered">
      <div class="container__one-half transparent-overlay">
        <h2 class="text--white no-margin--top"><?php print render($content['field_header_7']); ?></h2>
        <div class="text--white">
          <?php print render($content['field_long_text_7']); ?>
        </div>
      </div>
    </div>
  </div>
  <!-- BLOCK 10 -->


</div><!-- end js__seo-tool__body-content -->


<!-- BLOCK 11 -->
<div class="container bg--black-coconut" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dark-grey-triangle-pattern.png);">
  <div class="container__centered center">
    <h2 class="js__seo-tool__body-content text--white"><?php print render($content['field_header_10']); ?></h2>

    <?php
    // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
    if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
      $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
    ?>
      <div class="button--hubspot">
        <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
      </div>
    <?php }else{ ?>
      <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
    <?php } ?>

      <div style="margin-top:1em;">
        <?php print $share_link_buttons; ?>
      </div>
  </div>
</div>
<!-- BLOCK 11 -->


<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
<!-- end node--campaign_nursing.tpl.php template -->