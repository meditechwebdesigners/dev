<?php // This template is set up to control the display of the CAMPAIGN PATIENT EXPERIENCE content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

// FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM...
// to use the function simply assign it to a variable
// then pass in the field collection machine name to the function
// then you'll use the variable when calling each sub-field within the template
function field_collection_data($node, $field_name){
  // get array specific to a certain field collection item...
  $field_collection = field_get_items('node', $node, $field_name);
  // get all content within that field collection...
  $fc_field_data = entity_load('field_collection_item', array($field_collection[0]['value']));
  // get field collection ID...
  $fc_field_ID = key($fc_field_data);
  // create variable and load it with field collection data...
  $var_with_data = $fc_field_data[$fc_field_ID];
  // return variable...
  return $var_with_data;
}
$button_1 = field_collection_data($node, 'field_fc_button_1');
?>
<!-- start node--campaign_patient_experience.tpl.php template -->


<div class="js__seo-tool__body-content">

  <!-- Block 1 -->
  <div class="container bg--emerald background--cover hide__bg-image--mobile" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/patient-experience-hero-bg.jpg);">
    <div class="container__centered">
      <div class="container__one-half">
        <h1 class="text--white text-shadow--black no-margin"><?php print render($content['field_header_1']); ?></h1>
        <div class="text--white text-shadow--black">
          <?php print render($content['field_long_text_1']); ?>
        </div>
        <?php 
        // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
        if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
          $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
        ?>
          <div class="button--hubspot">
            <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
          </div>
        <?php }else{ ?>
          <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
        <?php } ?>
      </div>
    </div>
  </div>
  <!-- Close Block 1 -->
  
  
  <!-- Block 2 New Addition -->
  <div class="container">
    <div class="container__centered">
      <div class="container no-pad">

        <div class="container__three-fourths">
          <h2><?php print render($content['field_header_9']); ?></h2>
          <p>
            <?php print render($content['field_long_text_8']); ?>
          </p>
          
          <figure class="no-target-icon" style="display: inline-block; margin-right:1em;">
            <a href="https://itunes.apple.com/us/app/meditech-mhealth/id1143209032?mt=8" target="_blank"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/App-Store-Badge.png" alt="App Store Download Badge"></a>
          </figure>
          
          <figure class="no-target-icon" style="display: inline-block;">
            <a href="https://play.google.com/store/apps/details?id=com.meditech.PatientPhm&hl=en" target="_blank"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Google-Play-Badge.png" alt="Google Play Download Badge"></a>
          </figure>
          
        </div>

        <div class="container__one-fourth">
          <figure>
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MHealth-App-Icon.png" alt="MHealth app icon">
          </figure>
        </div>
      </div>
    </div>
  </div>
  <!-- Close Block 2 New Addition -->
  

  <!-- Block 3 -->
  <div class="content__callout border-none">

    <div class="content__callout__media">
      <h2 class="content__callout__title"><?php print render($content['field_header_2']); ?></h2>
      <div class="content__callout__image-wrapper">
        <div style="position:relative; padding-bottom:52%; height: 0; min-height: 400px; overflow:hidden; max-width:84.5%; margin:0 auto;">
          <figure style="text-align: center;">
            <img style="max-width: 400px" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/CoordinatedCare.svg" alt="Coordinated Care Diagram">
          </figure>
        </div>
      </div>
    </div>

    <div class="content__callout__content">
      <div class="content__callout__body">
        <div class="content__callout__body__text--large">
          <?php print render($content['field_long_text_2']); ?>
        </div>
      </div>
    </div>

  </div>
  <!-- Close Block 3 -->


  <!-- Block 4 -->
  <div class="container">
    <div class="container__centered">
      <div class="container no-pad">

        <div class="container__one-third">
          <h2><?php print render($content['field_header_3']); ?></h2>
          <div>
            <?php print render($content['field_long_text_3']); ?>
          </div>
        </div>

        <div class="container__two-thirds">
          <figure>
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/patient-portal.png" alt="MEDITECH Health Portal">
          </figure>
        </div>

      </div>
    </div>
  </div>
  <!-- Close Block 4 -->


  <!-- Video 1 -->
  <div class="content__callout border-none">

    <div class="content__callout__media">
      <div class="content__callout__image-wrapper">
        <div class="video js__video" data-video-id="<?php print render($content['field_video_url_2']); ?>">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/default/files/images/video/MEDITECH-health-wearables-video.jpg" alt="Video Covershot">
          </figure>
          <a class="video__play-btn" href="http://vimeo.com/<?php print render($content['field_video_url_2']); ?>"></a>
          <div class="video__container">
          </div>
        </div>
      </div>
    </div>

    <div class="content__callout__content">
      <div class="content__callout__body">
        <div>
         <h2><?php print render($content['field_header_8']); ?></h2>
          <?php print render($content['field_long_text_7']); ?>
        </div>
      </div>
    </div>

  </div>
  <!-- Close Video 1 -->


  <!-- BLOCK 5 -->
  <div class="container background--cover" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/stethoscope-blood-pressure-pe.jpg);">
    <div class="container__centered">
      <div class="container__two-thirds transparent-overlay text--white">
        <h1 class="no-margin--top"><?php print render($content['field_header_5']); ?></h1>
        <div>
          <?php print render($content['field_long_text_5']); ?>
        </div>
      </div>
    </div>
  </div>
  <!-- Close Block 5 -->


  <!-- Video 2 -->
  <div class="content__callout border-none">

    <div class="content__callout__media">
      <div class="content__callout__image-wrapper">
        <div class="video js__video" data-video-id="<?php print render($content['field_video_url']); ?>">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--patients-at-the-center.jpg" alt="Video Covershot">
          </figure>
          <a class="video__play-btn" href="http://vimeo.com/<?php print render($content['field_video_url']); ?>"></a>
          <div class="video__container">
          </div>
        </div>
      </div>
    </div>

    <div class="content__callout__content">
      <div class="content__callout__body">
        <div>
         <h2><?php print render($content['field_header_4']); ?></h2>
          <?php print render($content['field_long_text_4']); ?>
        </div>
        <div class="btn-holder--content__callout">
          <?php 
          // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
          if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
            $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
          ?>
            <div class="button--hubspot">
              <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
            </div>
          <?php }else{ ?>
            <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
          <?php } ?>
        </div>
      </div>
    </div>

  </div>
  <!-- Close Video 2 -->


  <!-- Block 6 -->
  <?php $block_6 = field_collection_data($node, 'field_fc_quote_1'); ?>
  <div class="container bg--emerald">
    <div class="container__centered">
      <figure class="container__one-fourth center">
        <img class="quote__content__img" src="<?php print $url; ?>/sites/all/themes/meditech/images/quote.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/quote.png';this.onerror=null;" alt="quote bubble graphic">
      </figure>
      <div class="container__three-fourths">
        <div class="quote__content__text text--large">
          <?php print $block_6->field_quote_1['und'][0]['value']; ?>
        </div>
        <p class="text--large no-margin--bottom"><?php print $block_6->field_full_name['und'][0]['value']; ?></p>
        <p><?php print $block_6->field_company['und'][0]['value']; ?></p>
      </div>
    </div>
  </div>
  <!-- Close Block 6 -->


  <!-- Block 7 -->
  <div class="container background--cover" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/meditech-patient-portal-feet-on-table.jpg);">
    <div class="container__centered">
      <div class="container__two-thirds transparent-overlay text--white">
        <h1 class="no-margin--top"><?php print render($content['field_header_6']); ?></h1>
        <div>
          <?php print render($content['field_long_text_6']); ?>
        </div>
      </div>
    </div>
  </div>
  <!-- Close Block 7 -->

</div><!-- end js__seo-tool__body-content -->

<!-- Block 8 -->
<div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg);">
  <div class="container__centered center">
    <h2 class="js__seo-tool__body-content"><?php print render($content['field_header_7']); ?></h2>
    <div class="btn-holder--content__callout">
      <?php 
        // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
        if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
          $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
        ?>
          <div class="button--hubspot">
            <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
          </div>
        <?php }else{ ?>
          <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
        <?php } ?>
    </div>


      <div style="margin-top:1em;">
        <?php print $share_link_buttons; ?>
      </div>
  </div>
</div>
<!-- Block 8 -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
<!-- end node--campaign_patient_experience.tpl.php template -->