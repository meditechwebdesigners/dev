<!-- START campaign--node-2077.php -->
<?php // This template is set up to control the display of the CAMPAIGN_MEDITECH_SURVEILLANCE content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<div class="js__seo-tool__body-content">

  <!-- Hero -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/nurse-with-ipad-speaking-with-patient.jpg);">
    <div class="container__centered">

      <div class="container__two-thirds transparent-overlay text--white">
        <h1 class="js__seo-tool__title">Predictive analytics: anticipate the future of patient care</h1>
        <p>Catch patients before they fall through the cracks, help keep them well, and intervene when they’re trending poorly. MEDITECH Surveillance analyzes your data in <strong>real time</strong> and automatically identifies patients who need attention, whether they qualify for a potential HAC or a clinical quality measure. Our integrated toolset fits smoothly into clinical workflows, helping care teams to prioritize care, take action sooner, and improve outcomes.</p>
        
        <div class="center" style="margin-top:2em;">
          <?php hubspot_button($cta_code, "Download Hilo Medical Center's Surveillance Case Study"); ?>
        </div>
      </div>

      <div class="container__one-third">

      </div>

    </div>
  </div>
  <!-- End of Hero -->

  <!-- Block 2 -->
  <div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg">

    <div class="container__centered">
      <div class="page__title--center">
        <h2>Prevent sepsis and other hospital-acquired conditions</h2>
      </div>

      <br>

      <div class="container__centered center">
        <figure>
          <img class="hide--mobile hide--tablet" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/sepsis-surveillance.png" alt="Sepsis surveillance graphic">

          <img class="hide--desktop" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/sepsis-surveillance--vertical.png" alt="Sepsis surveillance graphic">
        </figure>
        <br>
        <p>Quality and Surveillance works in the background of MEDITECH Expanse, pushing information to you as an early warning system.</p>
      </div>
    </div>
  </div>
  <!-- End Block 2 -->

  <!-- Block 3 -->
  <div class="container" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/green-abstract-triangular-shapes.png">
    <div class="container__centered">
      <figure class="container__one-fourth center">
        <img class="quote__content__img" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dr-bagdasian.png" alt="Dr Bagdasian Headshot">
      </figure>
      <div class="container__three-fourths text--white">
        <div class="quote__content__text text--large ">
          <p>"Think of MEDITECH’s Surveillance solution as a combination of the US Air Traffic Control system and the US Geological Survey: it’s that powerful. Surveillance improves communication amongst healthcare teams across all transitions of care. Now, patient safety will take a leap forward with this communication solution."</p>
        </div>
        <p class="text--large no-margin--bottom">Bryan Bagdasian, MD</p>
        <p>Physician Informaticist, MEDITECH</p>
      </div>
    </div>
  </div>
  <!-- End Block 3 -->

  <!-- Block 4 -->
  <div class="container bg--black-coconut" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dark-grey-triangle-pattern.png);">
    <div class="container__centered text--white auto-margins">
      <h2>Stop emerging issues in their tracks.</h2>
      <p><a href="https://ehr.meditech.com/ehr-solutions/meditech-nursing">Your nurses</a> and physicians will know as soon as a patient meets the criteria for sepsis, CAUTI, and other risks.</p>

      <p>Evidence-based rules search the clinical and demographic data in MEDITECH’s EHR, 24/7. Patients who meet profile criteria automatically populate tracking boards that indicate if the patient is showing signs of a potential HAC. Clinicians can order, document, or <a href="https://ehr.meditech.com/ehr-solutions/meditechs-care-coordination">message the care team</a> from these actionable boards, right on the spot.</p>

      <p>Our integrated solution broadcasts alerts to status boards and trackers throughout MEDITECH Expanse, reducing communication delays and expediting interventions.</p>
    </div>
    <br>
    <div class="container__centered center">
      <figure>
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/pcs-status-board.jpg" alt="PCS Status Board">
      </figure>
    </div>
  </div>
  <!-- End Block 4 -->

  <!-- Block 5 -->
  <div class="container" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg">
    <div class="container__centered">
      <figure class="container__one-fourth center">
        <img class="quote__content__img" src="<?php print $url; ?>/sites/all/themes/meditech/images/people/CathyTurner.png" alt="Cathy Turner Headshot">
      </figure>
      <div class="container__three-fourths">
        <div class="quote__content__text text--large">
          <p>"Being able to have crucial information sent to you versus you looking for it, you’re able to save time and intervene earlier in your patient’s care."</p>
        </div>
        <p class="text--large no-margin--bottom">Cathy Turner, BSN, MBA, RN-BC</p>
        <p>Associate Vice President, MEDITECH</p>
        <p><a href="https://blog.meditech.com/how-surveillance-tools-help-nurses-to-focus-on-their-patients">Explore the concept of surveillance</a> in Cathy’s blog post.</p>
      </div>
    </div>
  </div>
  <!-- End Block 5 -->

  <!-- Block 6 -->
  <div class="content__callout border-none">
    <div class="content__callout__media">
      <div class="content__callout__image-wrapper">
        <div class="video js__video" data-video-id="229751178">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--valley-surveillance.jpg" alt="Valley Surveillance Video Covershot">
          </figure>
          <a class="video__play-btn" href="https://vimeo.com/229751178"></a>
          <div class="video__container"></div>
        </div>
      </div>
    </div>
    <div class="content__callout__content">
      <div class="content__callout__body">
        <div class="content__callout__body__text">
          <h2>Early awareness means early intervention</h2>
          <p>Lauren Piech, RN, an informatics nurse at The Valley Hospital (Ridgewood, NJ), describes how this real-time alerting system complements practitioners’ workflow while raising their awareness of HACs and quality metrics.</p>

          <p>Learn more about Valley’s success with Quality and Surveillance in the <a href="https://info.meditech.com/meditechs-surveillance-identifies-and-prevents-infections-at-valley-case-study">case study.</a></p>
        </div>
      </div>
    </div>
  </div>
  <!-- End Block 6 -->

  <!-- Block 7 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/busy-hospital-hallway.jpg);">
    <div class="container__centered auto-margins">
      <h2 class="center text--white">
        Hospital-wide problem-solving.
      </h2>
      <br>
      <div class="container__one-half text--white">
        <p>This clinical solution is so versatile, you can use it in any department.</p>
        <ul>
          <li>Identify at-risk patients earlier</li>
          <li>Keep healthy patients well </li>
          <li>Comply with quality measures</li>
          <li>Prioritize care</li>
          <li>Manage patient populations</li>
        </ul>
      </div>

      <div class="container__one-half text--white transparent-overlay">
        <p>The Valley Hospital created an ED copayment surveillance profile to help track whether registrars requested patients’ copayments. The profile helps staff to be more efficient in their collections and curbs lost revenue.</p>
      </div>

    </div>
  </div>
  <!-- End Block 7 -->

  <!-- Block 8 -->
  <div class="container bg--black-coconut" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dark-grey-triangle-pattern.png);">
    <div class="container__centered text--white auto-margins">
      <h2>Stay vigilant with a real-time global watchlist.</h2>
      <p>Tired of reports that are outdated as soon as they’re run? Put the power of Quality and Surveillance in the hands of everyone who needs it, when and where they need it. Identify your most vulnerable patients across the enterprise with our actionable Watchlist. This real-time, high-level view itemizes every profile your patients meet. Simply click on the indicators to view patients’ qualifying criteria.</p>
    </div>
    <br>
    <div class="container__centered center">
      <figure>
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/surveillance-desktop.jpg" alt="Surveillance Desktop Screenshot">
      </figure>
    </div>
  </div>
  <!-- End Block 8 -->

  <!-- End Block 9 -->
  <div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/green-abstract-triangular-shapes.png);">
    <div class="container__centered text--white">
      <div class="page__title--center">
        <h2>Predict early and act fast with expert-based content.</h2>
      </div>
      <div class="container">
        <div class="container__one-fourth">
          <div class="transparent-overlay">
            <h4>Quality Measures</h4>
            <div>
              <ul>
                <li>AMI</li>
                <li>Stroke</li>
                <li>VTE</li>
                <li>Pneumonia</li>
                <li>SCIP</li>
                <li>Newborn</li>
              </ul>
            </div>
          </div>
        </div>
        <div class="container__one-fourth">
          <div class="transparent-overlay">
            <h4>Condition Specific</h4>
            <div>
              <ul>
                <li>CAUTI</li>
                <li>CLABSI</li>
                <li>VAP</li>
                <li>Sepsis</li>
                <li>Pediatric Sepsis</li>
                <li>Pressure Ulcers</li>
                <li>Positive Microbiology Results</li>
                <li>Fall Risk </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="container__one-fourth">
          <div class="transparent-overlay">
            <h4>Consults</h4>
            <div>
              <ul>
                <li>Respiratory Therapy</li>
                <li>Occupational Therapy</li>
                <li>Physical Therapy</li>
                <li>Speech Therapy</li>
                <li>Dietitian Consult</li>
              </ul>
            </div>
          </div>
        </div>
        <div class="container__one-fourth">
          <div class="transparent-overlay">
            <h4>Preventive Measures</h4>
            <div>
              <ul>
                <li>Readmissions</li>
                <li>Restraint Orders</li>
                <li>CYP2C19 Genotype Poor Metabolizer </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Block 9 -->

  <!-- Block 10 -->
  <div class="container" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg">
    <div class="container__centered">
      <h2>Best practice for better outcomes.</h2>
      <p>Exceed benchmarks with MEDITECH’s experience- and outcomes-based EHR Toolkits for <a href="https://ehr.meditech.com/news/meditech-launches-new-cauti-prevention-ehr-toolkit">CAUTI Prevention</a>, <a href="https://ehr.meditech.com/news/meditech-launches-new-sepsis-ehr-toolkit">Sepsis Management</a>, and <a href="https://ehr.meditech.com/news/improve-safety-and-outcomes-with-meditech-s-new-6x-fall-risk-ehr-toolkit">Fall Risk Management</a>. Condition-specific Surveillance Boards, best practice workflows, and other resources combine forces to help your organization improve clinical and operational outcomes.</p>
    </div>
  </div>
  <!-- End Block 10 -->

</div>
<!-- end js__seo-tool__body-content -->

<!-- Block 11 - CTA Block -->
<div class="container bg--black-coconut" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dark-grey-triangle-pattern.png);">
  <div class="container__centered" style="text-align: center;">
   
    <?php cta_text($cta); ?>
    
    <div class="center" style="margin-top:2em;">
      <?php hubspot_button($cta_code, "Download Hilo Medical Center's Surveillance Case Study"); ?>
    </div>
      
    <div style="margin-top:1em;">
      <?php print $share_link_buttons; ?>
    </div>
    
  </div>
</div>
<!-- End Block 11 -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>

<style>
  @media (min-width: 941px) {
    .hide--desktop {
      display: none;
    }
  }
</style>

<!-- END campaign--node-2077.php --