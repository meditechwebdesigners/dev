<!-- START inc--header.php -->

<style>
  form.gsc-search-box {
      font: inherit;
      margin-top: 0;
      margin-right: 0;
      margin-bottom: 0;
      margin-left: 0;
      width: 100%;
      position: relative;
  }

  table.gsc-search-box {
      border-style: none;
      border-width: 0;
      border-spacing: 0 0;
      width: 100%;
      margin-bottom: 0;
      margin-top: 0;
  }
  .gsc-input-box { border: 0; background: #fff; height: auto; border-color: white; }
  table.gsc-search-box { border: 0; margin: 0; }
  table.gsc-search-box tbody,
  table.gsc-search-box tr:hover>td,
  table.gsc-search-box tr:hover>th { background-color: inherit; }
  td.gsc-search-button { width: 20%; padding: 0; }
  table.gsc-search-box td.gsc-input,
  tbody:last-child tr:last-child>td:first-child,
  tbody:last-child tr:last-child>td:last-child { border: 0; border-bottom-left-radius: 0; padding: 0; }
  tbody:last-child tr:last-child>td:first-child { width: 80%; }
  table#gs_id50, table#gs_id51 { border: 1px solid #7b8888; }
  td#gs_tti50, td#gs_tti51 { padding: .7em 3%; }
  table.gsc-search-box td.gsc-search-button { border: 0; }
  table.gsc-search-box td { vertical-align: inherit; }
  table.gsc-search-box td.gsib_b { vertical-align: middle; }
  table.gsc-search-box table#gs_id50,
  table.gsc-search-box table#gs_id51 { margin: 0; }
  .gsc-search-box-tools .gsc-search-box .gsc-input { padding-right: 0; }
  .gsc-input table { border: none; }
  input.gsc-input { font-size: 1em; }
  .gsib_a { padding: 0; width: 80%; }
  .gsib_b { padding: 0; width: 10%; text-align: center; }
  input.gsc-search-button,
  input.gsc-search-button:hover,
  input.gsc-search-button:focus { border-color: #109372; background-color: #109372; background-image: none; filter: none; color: white; }
  input.gsc-search-button-v2 { width: 13px;
      height: 13px;
      padding: 6px 27px;
      min-width: 13px;
      margin-top: 2px;
  }
  input.gsc-search-button {
      font-family: inherit;
      font-size: inherit;
      font-weight: bold;
      color: #fff;
      padding: 1em 1.5em;
      margin: 0;
      height: 3.1em;
      width: 80px;
      border-radius: inherit;
      -moz-border-radius: inherit;
      -webkit-border-radius: inherit;
      border: 0;
  }
  .gcsc-find-more-on-google { display: none; }
  
    .nav__icon__login {
    font-size: 1.9em;
    line-height: 1.1;
    vertical-align: text-top;
  }
  
  /* New Header Styles **************************************** */

  .container__left {
    width: 30%
  }

  .container__right {
    width: 70%
  }

  .header__search {
    float: right;
  }

  input[type="search"] {
    height: 45px;
    min-width: 200px;
    padding: 0 2.2em 0 1.2em;
    border-radius: 25px;
    border: 2px solid #e0e0e0;
    margin-bottom: 0;
  }

  input[type="search"]:focus {
    border: 2px solid #bec4c4;
    transition: border .4s;
    box-shadow: none;
  }

  input[type="search"]:hover {
    box-shadow: none;
  }

  .form__search {
    width: 250px;
    float: right;
  }

  .header__search button {
    width: 30px;
    height: 30px;
    position: absolute;
    right: 15px;
    top: 10px;
    border: none;
    background: transparent;
  }

  .header__search button i {
    font-size: 24px;
    color: #087E68;
  }

  .header__button {
    float: right;
  }

  .nav {
    padding: 0;
    border: none;
  }

  .nav__menu--large {
    font-size: 1em;
  }

  .nav__menu--large ul.menu li {
    line-height: 55px;
    position: relative;
    margin-right: 0;
    padding-right: 1.4em;
  }

  .nav__menu--large ul.menu li a.active:after {
    width: 0;
    height: 0;
    position: absolute;
    content: " ";
    display: block;
    border-left: 15px solid transparent;
    border-right: 15px solid transparent;
    border-top: 15px solid #087e68;
    bottom: -7px;
    left: 51%;
    cursor: default;
    margin-left: -30px;
    transform: rotate(180deg);
  }

  /* New Sticky Nav Styles **************************************** */

  .btn--login {
    margin-left: 1em;
    transition: color .6s ease-in-out;
  }

  .btn--login:hover,
  .btn--login:active {
    color: #FF610A;
    transition: color .6s ease-in-out;
  }

  .nav__logo--small {
    float: left;
    margin-top: 10px;
    width: 0px;
    height: 35px;
  }

  .nav__secondarymenu {
    margin: 0;
  }

  .js-sticky {
    border-bottom: 0;
    box-shadow: 0 0 8px 0 rgba(72, 72, 72, .9);
    background: #fff;
  }

  .nav__menu__item--menu {
    margin-right: 0;
    padding: .75em;
    border-right: 2px solid #e0e0e0;
    line-height: 1.6875em;
    /* Remove in main.css, use ul line-height */
  }

  .btn--menu a:focus {
    color: #3e4545;
  }

  .nav__menu__item--secondary button {
    width: 30px;
    height: 30px;
    position: absolute;
    right: 9px;
    top: 5px;
    border: none;
    background: transparent;
  }

  .nav__menu__item--secondary button i {
    font-size: 20px;
    color: #087E68;
  }

  .nav__menu__item--secondary .form__search {
    width: 205px;
    margin-top: 8px;
    font-size: 18px;
    margin-bottom: -1px;
  }

  .nav__menu__item--secondary .form__search input[type=search] {
    height: 40px;
    padding: 0 1.8em 0 1em;
  }

  @media (min-width: 65em) {
    .dropdown-menu {
      display: none;
    }
  }

  @media (max-width: 65em) {
    .secondary-nav {
      display: none;
    }

    .main {
      margin-top: 57px;
    }

    .nav__menu--small {
      margin-left: -18px;
      line-height: 0;
      display: inline-block;
    }

    .nav__logo--large {
      width: 130px;
      margin-top: .9em;
      margin-left: 1em;
    }
  }

  @media (max-width: 50em) {
    .nav__menu__item--menu {
      font-size: 18px;
    }

    .nav__logo--large {
      margin-top: 1.5em;
    }

    .nav__icon__login {
      font-size: 2.3em;
      line-height: 1.2;
    }
  }

  @media (max-width: 520px) {
    .nav__menu__item--secondary {
      display: none;
    }

    .nav__menu__item--secondary--last {
      margin-top: 0.5em;
    }
  }

</style>

<!-- HEADER ================================================================== -->
<div class="print-header">
  <img src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/MEDITECH-Logo--2020.svg" onerror="this.src='<?php print $websiteURL; ?>/sites/all/themes/meditech/images/MEDITECH-Logo--2020.png'; this.onerror=null;" style="width:200px; margin-bottom: -2.5em;" alt="MEDITECH logo">
</div>

<header class="header">
  <div class="container no-pad" style="margin-top:1em;">
    <div class="container__centered">

      <div class="container__left">
        <a class="logo_desktop_gae" href="<?php print $websiteURL; ?>" title="Home Page">
          <img src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/MEDITECH-Logo--2020.svg" onerror="this.src='<?php print $websiteURL; ?>/sites/all/themes/meditech/images/MEDITECH-Logo--2020.png'; this.onerror=null;" style="width:210px; margin-top: 6px;" alt="MEDITECH logo">
        </a>
      </div>

      <div class="container__right">
        <div class="header__button"><a href="https://home.meditech.com/en/d/customer/" role="button" class="btn--orange-gradient customers_button_gae" title="Customers">Customers</a>
        </div>
        <div class="header__search">
          <form class="form__search" method="get" action="<?php print $websiteURL; ?>/search-results" id="search-block-form" accept-charset="UTF-8">
            <label class="element-invisible" for="edit-search-block-form--2">Site Search</label>
            <input id="searchBox" title="Enter the terms you wish to search for." placeholder="Search" type="search" name="as_q">
            <button><i class="fas fa-search"></i></button>
          </form>
        </div>
      </div>

    </div>
  </div>
</header>
<!-- End of HEADER -->


<!-- STICKY NAVIGATION ============================================================= -->
<nav class="nav js-nav">
  <div class="container__centered">
    <!-- Navigation Menu Button -->
    <ul class="nav__menu--small">
      <a class="btn--menu sb-toggle-left menu_mobile_gae" href="#">
        <li class="nav__menu__item--menu">Menu</li>
      </a>
    </ul>
    <!-- Navigation Logos -->
    <div class="nav__logo--large">
      <a class="logo_mobile_gae" href="<?php print $websiteURL; ?>" title="Home Page">
        <img src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/MEDITECH-Logo--2020.svg" onerror="this.src='<?php print $websiteURL; ?>/sites/all/themes/meditech/images/MEDITECH-Logo--2020.png'; this.onerror=null;" style="width:120px;" alt="MEDITECH mobile logo">
      </a>
    </div>
    <div class="nav__logo--small">
      <a class="logo_M_gae" href="<?php print $websiteURL; ?>" title="Home Page">
        <img src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/M-Logo--2020.svg" onerror="this.src='<?php print $websiteURL; ?>/sites/all/themes/meditech/images/M-Logo--2020.png'; this.onerror=null;" alt="MEDITECH M logo">
      </a>
    </div>
    <!-- Navigation Menu Expanded -->
    <div class="nav__menu--large header_link_gae">
      <?php
          $active_solutions = '';
          $active_news = '';
          $active_events = '';
          $active_about = '';
          $active_contact = '';

          // need to check this since DEV server has a different folder structure...
          if($currentURLarray[3] == 'drupal'){
            $folder_name = $currentURLarray[4];
          }
          else{
            $folder_name = $currentURLarray[3];
          }

          // because contact is getting variables sent to its URL, need to split it...
          $explode_folder_name = explode('?', $folder_name);

          switch($explode_folder_name[0]){
            case 'ehr-solutions':
              $active_solutions = 'active';
              break;
            case 'news':
            case 'news-tags':
              $active_news = 'active';
              break;
            case 'events':
              $active_events = 'active';
              break;
            case 'about-meditech':
            case 'careers':
              $active_about = 'active';
              break;
            case 'contact':
              $active_contact = 'active';
              break;
            case 'global':
              $active_global = 'active';
              break;
          }
        ?>
      <ul class="menu">
        <li class="first"><a class="<?php if( isset($active_solutions) ){ print $active_solutions; } ?>" href="<?php print $websiteURL; ?>/ehr-solutions">EHR Solutions</a></li>
        <li><a class="<?php if( isset($active_news) ){ print $active_news; } ?>" href="<?php print $websiteURL; ?>/news">News</a></li>
        <li><a href="https://blog.meditech.com">Blog</a></li>
        <li><a class="<?php if( isset($active_events) ){ print $active_events; } ?>" href="<?php print $websiteURL; ?>/events">Events</a></li>
        <li><a class="<?php if( isset($active_about) ){ print $active_about; } ?>" href="<?php print $websiteURL; ?>/about-meditech/about-meditech">About</a></li>
        <li><a class="<?php if( isset($active_global) ){ print $active_global; } ?>" href="<?php print $websiteURL; ?>/global">Global</a></li>
        <li class="last"><a class="<?php if( isset($active_contact) ){ print $active_contact; } ?>" href="<?php print $websiteURL; ?>/contact">Contact</a></li>
      </ul>
    </div>

    <ul class="nav__secondarymenu">
      <li class="nav__menu__item--secondary">
        <form class="form__search" method="get" action="<?php print $websiteURL; ?>/search-results" id="search-block-form" accept-charset="UTF-8">
          <label class="element-invisible" for="edit-search-block-form--2">Site Search</label>
          <input title="Enter the terms you wish to search for." placeholder="Search" type="search" name="as_q">
          <button><i class="fas fa-search"></i></button>
        </form>

      </li>
      <li class="nav__menu__item--secondary--last">
        <a class="btn--login customers_icon_gae" href="https://home.meditech.com/en/d/customer/" title="Customers"><i class="nav__icon__login fas fa-user"></i></a>
      </li>
    </ul>
  </div>

  <?php include('inc--secondary-nav.php'); ?>

</nav>
<!-- End of STICKY NAVIGATION -->

<!-- END inc--header.php -->