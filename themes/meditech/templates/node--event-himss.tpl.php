<?php // This template is set up to control the display of the Event HIMSS content type

  $url = $GLOBALS['base_url']; // grabs the site url

?>
<!-- start node--event-himss.php template -->
 
<?php
$node_id = $node->nid;
include('himss--node-'.$node_id.'.php');
?>
        
<!-- end node--event-himss.tpl.php template -->