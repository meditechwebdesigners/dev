<!-- START inc--secondary-nav.php -->
<style>
  .secondary-nav ul { list-style: none; padding: 0em; }
  .secondary-nav ul li {
    border-right: 2px solid #00bc6f;
    display: inline-block;
    padding: 0em 1em;
    line-height: 90%;
    font-size: 16px;
    overflow: visible;
    margin-top: .75em;
    margin-bottom: .75em;
  }
  .secondary-nav ul li:first-child { padding-left: 1px; }
  .secondary-nav ul li:last-child { border-right: 0; }
  .nav__search { float: right; padding: 0.5em 0; }
</style>
<div class="bg--emerald">
  <div class="container__centered">
    <div class="secondary-nav">
      <?php
      // get nav menu name based off content type (function is in template.php)...
      $sec_nav = get_secondary_nav($node->type);
      // generate Drupal menu...
      $menu = module_invoke('menu', 'block_view', $sec_nav);
      print render($menu['content']);
      ?>
    </div>
  </div>
</div>
<!-- END inc--secondary-nav.php -->