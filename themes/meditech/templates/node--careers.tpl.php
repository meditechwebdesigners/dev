<?php // This template is set up to control the display of the 'Careers' content type 

  $url = $GLOBALS['base_url']; // grabs the site url
?>
<!-- start node--careers.tpl.php template -->

    <section class="container__centered">
	  <div class="container__two-thirds">

        <h1 class="page__title js__seo-tool__title"><?php print $title; ?></h1>
        
        <div class="js__seo-tool__body-content">
          <?php print render($content['field_body']); ?>
        </div>


        <?php
        // VIEWS content for the CAREERS page ======================================================================
        if($node->nid == 307){
        ?>
          <!-- tablesorter js script -->
          <script type="text/javascript" src="<?php print $url; ?>/sites/all/themes/meditech/js/jquery.tablesorter.min.js"></script> 
          <script>
            var $jq = jQuery.noConflict();
            
            // ***************************************************************
            // tablesorter - - - https://github.com/christianbach/tablesorter
            // ***************************************************************

            $jq(".tablesorter").tablesorter();
            
            $jq(document).ready(function(){             
              // [1,0] sets first column to sort in ascending order by default
              // [0,0] leaves 2nd column alone
              $jq("#masachusetts").tablesorter( {sortList: [[1,0], [0,0]]} ); 
              $jq("#minnesota").tablesorter( {sortList: [[1,0], [0,0]]} );
              $jq("#georgia").tablesorter( {sortList: [[1,0], [0,0]]} );
            }); 
          </script>
          
          <style>
            #massachusetts tbody:last-child tr:last-child>td:first-child,
            #minnesota tbody:last-child tr:last-child>td:first-child,
            #georgia tbody:last-child tr:last-child>td:first-child,
            #massachusetts tbody:last-child tr:last-child>td:last-child,
            #minnesota tbody:last-child tr:last-child>td:last-child,
            #georgia tbody:last-child tr:last-child>td:last-child
             { padding: .5em; }
          </style>
          
          <h2 id="jobs">Job Listings</h2>
          <?php print views_embed_view('job_listings_by_state', 'block'); // adds 'job listings by state' Views block... ?>

          
        <?php
        }
        ?>


        <?php
        // VIEWS content for the CAREER EVENTS page ======================================================================
        if($node->nid == 314){
        ?>
          <div class="js__seo-tool__body-content">
            <?php print views_embed_view('career_events', 'block'); // adds 'career events' Views block... ?>
          </div>
        <?php
        }
        ?>
        
        
        <?php
        // Instagram photos for Life at MEDITECH page ======================================================================
        if($node->nid == 310){
        ?>
          <script type="text/javascript" src="<?php print $url; ?>/sites/all/themes/meditech/js/instafeed.min.js"></script>
          <script type="text/javascript">
            // source: http://instafeedjs.com
            var feed = new Instafeed({
              get: 'tagged',
              tagName: 'meditech45',
              clientId: 'c8d0a953b0a34ad595d366dac5485485',
              limit: 9,
              resolution: 'low_resolution',
              template: '<a href="{{link}}"><img src="{{image}}" style="width:33%;" /></a>'
            });
            feed.run();
          </script>
          <div id="instafeed"></div>
        <?php
        }
        ?>

      </div><!-- END container__two-thirds -->
      

      <!-- CAREERS SIDEBAR -->
      <aside class="container__one-third panel">
	    <div class="sidebar__nav careers_sidebar_gae">
          <?php
            $massnBlock = module_invoke('menu', 'block_view', 'menu-careers-section-side-nav');
            print render($massnBlock['content']); 
          ?>
        </div>
        
        <?php
          // If CAREERS main page ==========================================
          if($node->nid == 307){
        ?>
            <div class="sidebar__images">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/careers/proudlyhires.png" alt="red, white and blue ribbon graphic">
            </div>
        <?php } ?>
      </aside>
      <!-- END CAREERS SIDEBAR -->
    
    </section>
    <!-- END SECTION container__centered -->
    
    
    <?php // VIDEO SECTION =================================================================
      // get value from field_video to pass to View (if video exists)...
      $video = render($content['field_video']);
      if(!empty($video)){ // if the page has a video...
        print '<!-- VIDEO -->';
        print '<div class="js__seo-tool__body-content">';
        // remove apostrophes from titles to prevent View from breaking...
        $video_filtered = str_replace("&#039;", "'", $video);
        // adds 'video' Views block...
        print views_embed_view('video_boxed', 'block', $video_filtered);
        print '</div>';
        print '<!-- END VIDEO -->';
      }
    ?>

    <?php // SEO tool for internal use...
      if(node_access('update',$node)){
        print '<!-- SEO Tool is added to this div -->';
        print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
      } 
    ?>  
<!-- end node--about.tpl.php template -->