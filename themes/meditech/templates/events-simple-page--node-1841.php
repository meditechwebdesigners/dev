<!-- start events-simple-page--node-1841.php Upcoming Events & Webinars template -->
<?php $url = $GLOBALS['base_url']; // grabs the site url ?>
     
  <h1 style="display:none;">MEDITECH Events</h1>

  <!-- Hero -->
  <?php print views_embed_view('events_next_big_event', 'block'); // adds 'Events - Next Big Event' Views block... ?>
  <!-- End Hero -->     
      
  <section class="container__centered">
    <div class="container__two-thirds">

      <div class="js__seo-tool__body-content">

        <h2>Upcoming Events</h2>

        <?php print views_embed_view('events_upcoming_events', 'block'); // adds 'Upcoming Events' Views block... ?>

         <h2 style="margin-top:2em;">Upcoming Webinars</h2>

        <?php print views_embed_view('events_upcoming_webinars', 'block'); // adds 'Upcoming Webinars' Views block... ?>

      </div>

    </div><!-- END container__two-thirds -->

    <!-- SIDEBAR -->
    <aside class="container__one-third panel">
      <div class="sidebar__nav">
        <?php
          $messnBlock = module_invoke('menu', 'block_view', 'menu-events-section-side-nav');
          print render($messnBlock['content']); 
        ?>
      </div>
    </aside>
    <!-- END SIDEBAR -->    
  </section>
  
<!-- END events-simple-page--node-1841.php Upcoming Events & Webinars template -->