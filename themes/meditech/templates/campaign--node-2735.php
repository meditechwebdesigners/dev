<!-- START campaign--node-2735.php -->

<?php // This template is set up to control the display of the MEDITECH Interoperability content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
  .circle-icon {
    height: 75px;
    width: 75px;
    color: #fff;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 50%;
    margin: 0 auto;
    margin-bottom: 1em;
  }

  .headshot-border {
    max-width: 150px;
    max-height: 150px;
    border: 6px solid #e6e9ee;
    border-radius: 50%;
    margin-left: auto;
    margin-right: auto;
    margin-bottom: 1em;
  }

  .paragraph-margin {
    margin-left: -2em;
    margin-right: 2em;
  }

  @media screen and (min-width: 50em) and (max-width: 56.250em) {
    .gl-container h2 {
      font-size: 1.2em;
      line-height: 1.40625em;
    }
  }

  @media screen and (max-width: 50em) {
    .paragraph-margin {
      margin: .5em 0em 0em 0em;
      text-align: center;
    }
  }

</style>

<!-- Start Block 1 -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/happy-patient-with-physician.jpg); background-position: right;">
  <div class="container__centered">
    <div class="container__one-half">&nbsp;</div>
    <div class="container__one-half transparent-overlay text--white">
      <h1>Interoperability is about people.</h1>
      <p>Interoperability isn’t really about clicks, or C-CDAs, or interfaces, or data. It’s about people, and simplifying healthcare to make their lives better. That means bridging the gaps that can happen during care transitions, making sure a patient’s record goes wherever they go. And ensuring that patient information always reaches the right clinician, when and where it’s supposed to.</p>
      <p>There’s many different routes to achieve interoperability, just as there are many different routes that a patient and their records can travel. But no matter what direction your organization wants to go, MEDITECH can help you get there.</p>

      <div class="btn-holder--content__callout center" style="margin-bottom:1.5em;">
        <div class="center" style="margin-top:2em;">
          <?php hubspot_button($cta_code, "Download The Union Hospital Interoperability Case Study"); ?>
        </div>
      </div>

    </div>
  </div>
</div>
<!-- End Block 1 -->

<!-- Start Block 2 -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blurred-hospital-lobby.jpg);">
  <div class="container__centered">
    <div class="gl-container">
      <div class="container__one-third bg--green-gradient">
        <h2>Interoperability is free-flowing information.</h2>
        <p>Neither geographical boundaries nor hospital affiliation should hinder a clinician’s access to patient records. That’s why, as a <a href="https://ehr.meditech.com/ehr-solutions/commonwell-connected">CommonWell Connected<sup>&trade;</sup> Member</a>, we extend interoperability efforts beyond local affiliations and regional HIEs to all connected practitioners. And to extend this reach even further, <a href="http://www.commonwellalliance.org/" target="_blank">CommonWell Health Alliance<sup>&reg;</sup></a> provides the opportunity to connect with Carequality, supporting the exchange of member records with nearly all major EHR vendors.</p>
      </div>
      <div class="container__two-thirds center">
        <div class="headshot-border">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/brent-headshot.png" alt="Brent head shot">
        </div>
        <h3>Why does this matter? Consider Brent.</h3>
        <p>Brent is playing college hockey more than a thousand miles from home and blacks out after falling and hitting his head on the ice. He is admitted to a local community hospital. Both the local hospital and Brett’s home hospital are connected through the CommonWell network, allowing his physicians to review his medical history.</p>
        <p>For instance, they have access to his current medications and allergies in case they need to prescribe additional medications. Soon, they will be leveraging the Carequality Connection to also access shared information from his concussion specialist at an academic facility. His care team will be able to review past treatment and test results so they immediately know how best to address his symptoms. And when Brett returns home, his complete medical record will follow him.</p>
      </div>
    </div>
  </div>
</div>
<!-- End Block 2 -->

<!-- Start Block 3 -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/CHF-patient-with-cardiologist.jpg); background-position: top;">
  <div class="container__centered">
    <div class="gl-container">
      <div class="container__one-third  bg--blue-gradient">
        <h2>Interoperability improves the patient experience.</h2>
        <p>Patients want the freedom to choose their own specialist, the convenience of an easily accessible medical history, and the ability to engage with providers regularly. But high quality care requires more than just a strong specialist. It requires real-time patient information to make informed care decisions. From local and regional connections to enrollment in nationwide data exchange services, MEDITECH Expanse keeps patients and their records connected.</p>
      </div>
      <div class="container__two-thirds center">
        <div class="headshot-border">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/diane-headshot.png" alt="Diane head shot">
        </div>
        <h3 style="margin-bottom:1em;">Consider Diane, a 55-year-old CHF patient, who requests a referral to a local cardiologist her friend recommended. MEDITECH’s Expanse EHR provides:</h3>
        <div class="container__one-third">
          <h3>Freedom</h3>
          <p>Diane’s PCP and specialist share information through the local health information exchange (HIE), including her medication list, problem list, and visit notes.</p>
        </div>
        <div class="container__one-third">
          <h3>Convenience</h3>
          <p>Diane doesn’t have to recall all her medical details or have tests duplicated.</p>
        </div>
        <div class="container__one-third">
          <h3>Engagement</h3>
          <p>Diane’s PCP can monitor weight and blood pressure readings that Diane enters using the MHealth Mobile app. She’ll intervene if she detects potential risks, such as rapid weight gain attributed to fluid retention.</p>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Block 3 -->

<!-- Start Block 4 -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/patient-on-gurney.jpg);">
  <div class="container__centered">
    <div class="fw-gl-container">
      <div class="container bg--green-gradient">
        <div class="center" style="margin-bottom:1em;">
          <h2>Interoperability is efficient for clinicians.</h2>
          <p>Timely treatment requires timely information, especially in the ED, where it can prove life saving.</p>
        </div>
        <div class="container__one-third">
          <div class="headshot-border">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/carlos-headshot.png" alt="Carlos head shot">
          </div>
        </div>
        <div class="container__two-thirds">
          <p class="paragraph-margin" style="margin-top:.5em;">Take Carlos. Carlos is rendered unconscious following a motor vehicle accident and is life flighted to a Level II trauma center. With only his driver’s license available and no prior record of care, physicians normally would be unaware that Carlos is diabetic and is allergic to iodine. But because they are using an interoperable MEDITECH EHR, they are able to:</p>
        </div>
      </div>
      <div class="container center">
        <div class="container__one-fourth">
          <div class="circle-icon bg--emerald">
            <i class="fas fa-prescription fa-2x"></i>
          </div>
          <p>Select medications, allergies, problems, and immunizations to add to the patient’s record in Expanse.</p>
        </div>
        <div class="container__one-fourth">
          <div class="circle-icon bg--emerald">
            <i class="far fa-hand-point-up fa-2x"></i>
          </div>
          <p>Review Carlos’ information in other systems through MEDITECH’s secure One Touch EHR-to-EHR connection embedded in their workflow; no need to search for the patient or sign onto another system.</p>
        </div>
        <div class="container__one-fourth">
          <div class="circle-icon bg--emerald">
            <i class="fas fa-bolt fa-2x"></i>
          </div>
          <p>Quickly access Carlos’ external medical information through a medical summary widget embedded in the clinician’s workflow.</p>
        </div>
        <div class="container__one-fourth">
          <div class="circle-icon bg--emerald">
            <i class="far fa-address-card fa-2x"></i>
          </div>
          <p>Navigate a personalized view of outside medical summaries.</p>
        </div>
      </div>
      <div class="container center" style="padding: 0em 2em 1em 2em;">
        <h4>Interoperability speaks up for Carlos by sharing his complete story with clinicians, when he is unable to do so himself.</h4>
      </div>
    </div>
  </div>
</div>
<!-- End Block 4 -->

<!-- Start Block 5 -->
<div class="container">
  <div class="container__centered">
    <div class="auto-margins center">
      <h2>Interoperability is happening today.</h2>
      <p>Interoperability isn’t some far-away goal; MEDITECH users are delivering interoperable, efficient, safe care to patients right now. For example, Union Hospital of Cecil County is connecting to both the Maryland (CRISP) and Delaware (DHIN) HIEs, and <a href="https://info.meditech.com/case-study-union-hospital-improves-care-efficiency-with-multiple-health-information-exchange-strategy-0">producing meaningful results</a> for clinicians, including:</p>
    </div>
    <div class="container center" style="padding: 1.5em 0em 1.5em 0em;">
      <div class="container__one-third">
        <div class="circle-icon">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--schedule.png" alt="Schedule icon">
        </div>
        <p>Saving physicians from one hour of unnecessary paperwork per day.</p>
      </div>
      <div class="container__one-third">
        <div class="circle-icon">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--care-alerts.png" alt="Care alerts icon">
        </div>
        <p>Delivering pertinent patient information to users at a glance, through care alerts.</p>
      </div>
      <div class="container__one-third">
        <div class="circle-icon">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--patient-portal.png" alt="Patient Portal icon">
        </div>
        <p>Embedding HIE portal access into clinical workflows, for a real-time view of any patient’s regional data.</p>
      </div>
    </div>
    <div class="container center no-pad">
      <div class="container__one-third">
        <div class="circle-icon">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--communication.png" alt="Communication icon">
        </div>
        <p>More effective clinical communication through secure messaging.</p>
      </div>
      <div class="container__one-third">
        <div class="circle-icon">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--analytics.png" alt="Analytics icon">
        </div>
        <p>Comparing readmission rates with other organizations and setting achievable goals using real-time analytics.</p>
      </div>
      <div class="container__one-third">
        <div class="circle-icon">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/icon--workflow.png" alt="Workflow icon">
        </div>
        <p>Quicker, more informed decision-making on patient transfers.</p>
      </div>
    </div>
  </div>
</div>
<!-- End Block 5 -->

<!-- Start Block 6 -->
<?php 
if( user_is_logged_in() === true ){
  global $user;
  $userID = $user->uid;
  if( $userID == 105 || $userID == 1770 || $userID == 109 || $userID == 108 || $userID == 3473 ){
?>

<div class="container bg--blue-gradient">
  <div class="container__centered">
    <div class="container__one-third center">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-Health-App-on-Iphone.png" alt="FHIR® APIs Expand Consumer Access on iPhone®" style="width: 250px;">
    </div>
    <div class="container__two-thirds">
      <h2>FHIR<sup>&reg;</sup> APIs Expand Consumer Access on iPhone<sup>&reg;</sup></h2>
      <p>MEDITECH is giving consumers access to their health data through Health Records on iPhone.</p>
      <p>Using FHIR standard APIs, the Health app connects to MEDITECH’s patient portal and pulls forward new data as soon as it’s available.</p>
      <p>Information is aggregated across multiple participating organizations and combined with PGHD, to provide consumers with one convenient record that:</p>
      <ul style="margin-top:1.25em;">
        <li>Includes key data elements, such as allergies, medications, vitals, lab results, conditions, procedures, and immunizations.</li>
        <li>Helps fill information gaps for providers who aren’t familiar with the patient, or who aren’t connected through interoperability.</li>
        <li>Supports secure and convenient access using the same credentials as the patient portal.</li>
      </ul>
    </div>
  </div>
</div>

<?php
  }
  else{
    print '<!-- CONTENT BLOCKED -->';
  }
}
?>
<!-- End Block 6 -->

<!-- Start Block 7 -->
<div class="content__callout">
  <div class="content__callout__media">
    <div class="content__callout__image-wrapper">
      <div class="video js__video" data-video-id="288801648">
        <figure class="video__overlay">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--meditech-greenfield-promo.jpg" alt="MEDITECH Greenfield API Promo - Video Covershot">
        </figure>
        <a class="video__play-btn" href="https://vimeo.com/288801648"></a>
        <div class="video__container"></div>
      </div>
    </div>
  </div>
  <div class="content__callout__content">
    <div class="content__callout__body">
      <div class="content__callout__body__text">
        <h2>Interoperability is preparing for the future.</h2>
        <p>As healthcare technologies continue to advance, providers must keep up in order to stay competitive for the future. MEDITECH Greenfield — a new app development environment supported by RESTful APIs, including FHIR — will help them to do that. We invite creators and visionaries to explore Greenfield, and join us in testing, publishing, and acquiring new apps that will add value to the Expanse EHR.</p>
        <p>If you are interested, please <a href="https://info.meditech.com/welcome-to-meditech-greenfield-rfi">submit this form</a> for more information, and <a href="https://info.meditech.com/subscribe-for-greenfield-updates">subscribe</a> to receive the latest updates.</p>
      </div>
    </div>
  </div>
</div>
<!-- End Block 7 -->

<!-- Block 8 -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-boxes-pattern.jpg);">
  <div class="container__centered">
    <figure class="container__one-fourth center">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dr-louis-harris.png" alt="Dr. Louis Harris Head Shot">
    </figure>
    <div class="container__three-fourths text--white">
      <div class="quote__content__text text--large" style="margin-top:1.5em;">
        <p class="italic">
          "Being able to see where patients have been, what their experience was, what their plans were, what tests they were supposed to have...within our own system is really beneficial."</p>
      </div>
      <p class="text--large no-margin--bottom">Dr. Louis Harris, CMIO</p>
      <p>Citizens Memorial</p>
    </div>
  </div>
</div>
<!-- End Block 8 -->

<!-- Start Block 9 -->
<div class="content__callout">
  <div class="content__callout__media">
    <div class="content__callout__image-wrapper">
      <div class="video js__video" data-video-id="311922713">
        <figure class="video__overlay">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--MEDITECH-interoperability.jpg" alt="MEDITECH Interoperability - Video Covershot">
        </figure>
        <a class="video__play-btn" href="http://vimeo.com/311922713"></a>
        <div class="video__container"></div>
      </div>
    </div>
  </div>
  <div class="content__callout__content">
    <div class="content__callout__body">
      <div class="content__callout__body__text">
        <h2>Interoperability is simply better care, made easier. </h2>
        <p>The Valley Hospital, Signature Healthcare, and Citizens Memorial Hospital share how they’re using MEDITECH’s interoperable EHR to improve care delivery.</p>
      </div>
    </div>
  </div>
</div>
<!-- End Block 9 -->

<!-- Start Block 10 -->
<div class="container bg--black-coconut" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/dark-grey-triangle-pattern.png);">
  <div class="container__centered center text--white">

    <?php cta_text($cta); ?>

    <div class="center" style="margin-top:2em;">
      <?php hubspot_button($cta_code, "Download The Union Hospital Interoperability Case Study"); ?>
    </div>

    <div style="margin-top:1em;">
      <?php print $share_link_buttons; ?>
    </div>

  </div>
</div>
<!-- End Block 10 -->

<!-- END campaign--node-2735.php -->
