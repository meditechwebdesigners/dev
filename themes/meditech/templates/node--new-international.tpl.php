<?php // This template is set up to control the display of the International content type

$url = $GLOBALS['base_url']; // grabs the site url

$block_2 = multi_field_collection_data($node, 'field_fc_head_ltext_1', 4); 
$block_4 = multi_field_collection_data($node, 'field_fc_head_text_1', 6);
$quotes = multi_field_collection_data($node, 'field_fc_quote_1', 3);
$block_9 = multi_field_collection_data($node, 'field_fc_head_ltext_2', 4); 

$block_10 = field_collection_data($node, 'field_fc_head_ltext_btn_1');
$button_1 = field_collection_data($node, 'field_fc_button_1');
$button_2 = field_collection_data($node, 'field_fc_button_2');
$button_3 = field_collection_data($node, 'field_fc_button_3');
?>
<!-- start node--new-international.tpl.php template -->

<div class="js__seo-tool__body-content">
  
	<style>
		/* **** VIDEO ************************************** */

		.video--loop-container {
			min-height: 600px;
			max-height: 650px;
		}

		@media (max-width: 800px) {
			.video--loop-container {
				min-height: 480px;
			}
		}

		.video--loop-container video {
			/* Make video to at least 100% wide and tall */
			min-width: 100%;
			min-height: 100%;
			/* Setting width & height to auto prevents the browser from stretching or squishing the video */
			width: auto;
			height: auto;
			/* Center the video */
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%, -50%);
		}

		.video--title-3 {
			width: 100%;
			margin-left: auto;
			margin-right: auto;
			margin-top: 4em;
			margin-bottom: 1em;
			position: relative;
			color: #fff;
			z-index: 3;
		}
      .grid-item {
        background-color: rgba(0, 0, 0, 0.6);
      }

      @media all and (max-width: 30em) {
        .grid-item {
          border: none;
        }
      }
  </style>
  <!--Block 1-->
  <div class="video--loop-container">
		<div class="video--overlay" style="background:none;"></div>
		<video class="video--loop" preload="auto" autoplay loop muted playsinline>
      <source src="https://player.vimeo.com/external/267652677.hd.mp4?s=f6bd1afcfe2c8f517cdb239f9ab8b73220de7642&profile_id=174">
    </video>
		<div class="container__centered video--title-3">
			<div class="container__two-thirds">
				<div class="transparent-overlay text--white" style="background-color: rgba(0, 0, 0, 0.7);">
          <h1><?php print render($content['field_header_1']); ?></h1>
          <h2><?php print render($content['field_header_2']); ?></h2>
          <p><?php print render($content['field_long_text_1']); ?></p>
				</div>
			</div>
			<div class="container__one-third">&nbsp;</div>
		</div>
	</div>
	<!--End of Block 1-->



	<!--Block 2-->
	<div class="container" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg">
		<div class="container__centered">
			<div class="container__centered" style="margin-bottom:1em;">
				<div class="container__one-half">
          <h2><?php print render($content['field_header_3']); ?></h2>
				  <?php print render($content['field_long_text_2']); ?>
				</div>
				<div class="container__one-half" style="margin-bottom:1em;">
          <div>
            <div class="container__one-third center">
              <a href="https://ehr.meditech.com/international/meditech-asia-pacific" class="btn--orange" style="margin-top:2em;">Learn More about MEDITECH Asia Pacific</a>
            </div>
            <div class="container__one-third center">
              <a href="https://ehr.meditech.com/international/meditech-south-africa" class="btn--orange" style="margin-top:2em;">Learn More about MEDITECH South Africa</a>
            </div>
            <div class="container__one-third center">
              <a href="https://ehr.meditech.com/international/meditech-uk-ireland" class="btn--orange" style="margin-top:2em;">Learn More about MEDITECH UK & Ireland</a>
            </div>
				  </div>
				</div>
			</div>
			<div class="container__centered">
				<div class="container__one-fourth">
					<div class="transparent-overlay text--white">
						<h3>
							<?php print $block_2[0]->field_header_1['und'][0]['value']; ?>
						</h3>
						<?php print $block_2[0]->field_long_text_1['und'][0]['value']; ?>
					</div>
				</div>
				<div class="container__one-fourth">
					<div class="transparent-overlay text--white">
						<h3>
							<?php print $block_2[1]->field_header_1['und'][0]['value']; ?>
						</h3>
						<?php print $block_2[1]->field_long_text_1['und'][0]['value']; ?>
					</div>
				</div>
				<div class="container__one-fourth">
					<div class="transparent-overlay text--white">
						<h3>
							<?php print $block_2[2]->field_header_1['und'][0]['value']; ?>
						</h3>
						<?php print $block_2[2]->field_long_text_1['und'][0]['value']; ?>
					</div>
				</div>
				<div class="container__one-fourth">
					<div class="transparent-overlay text--white">
						<h3>
							<?php print $block_2[3]->field_header_1['und'][0]['value']; ?>
						</h3>
						<?php print $block_2[3]->field_long_text_1['und'][0]['value']; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--End of Block 2-->

	<!--Block 3 - Video-->
	<div class="content__callout">
		<div class="content__callout__media">
			
			<div class="content__callout__image-wrapper">
				<div class="video js__video" data-video-id="174224144">
					<figure class="video__overlay">
						<img src="<?php print $url; ?>/sites/all/themes/meditech/images/international/video-overlay-Botswana-The-Power-and-Impact-of-Serving-a-Global-Healthcare-Mission.jpg" alt="Video Covershot">
					</figure>
					<a class="video__play-btn" href="https://vimeo.com/174224144"></a>
					<div class="video__container">
					</div>
				</div>
			</div>
		</div>
		<div class="content__callout__content">
			<div class="content__callout__body">
				<div class="content__callout__body__text">
				<h2>
				<?php print render($content['field_header_4']); ?>
			</h2>
					<?php print render($content['field_long_text_3']); ?>
				</div>
			</div>
		</div>
	</div>
	<!--End of Block 3-->


	<!--Block 4-->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/hikers-looking-over-canadian-wilderness.jpg);">
    <div class="container__centered">
      <div class="container__one-half transparent-overlay--xp text--white">
        <h2>Connecting Canada with MEDITECH’s EHR</h2>
        <p>Across Canada, MEDITECH customers have a rich history of leading the way in delivering the highest quality healthcare. Find out how we’re connecting clinicians, hospitals, and health authorities with more efficient EHR tools and integration that spans all settings.</p>
        <div class="btn-holder--content__callout" style="margin-bottom:1.5em;">

          <div style="text-align:center;">
            <?php $button_2_code = '9dcbc4c4-bfcb-41ab-a4a3-4bfa592ed9b2'; ?>
            <div class="button--hubspot">
              <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_2_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_2_code; ?>" id="hs-cta-<?php print $button_2_code; ?>">
                  <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_2_code; ?>"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_2_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_2_code; ?>.png" alt="Explore MEDITECH Expanse" /></a></span>
                <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
                <script type="text/javascript">
                  hbspt.cta.load(2897117, '<?php print $button_2_code; ?>', {});
                </script>
              </span>
              <!-- end HubSpot Call-to-Action Code -->
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
	<!--End of Block 4-->
	

	<!--Block 5 - Quote-->
	<?php $block_5 = field_collection_data($node, 'field_fc_quote_1'); ?>
	<div class="container" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg">
		<article class="container__centered">
			<figure class="container__one-fourth center">
				<img class="quote__content__img" src="<?php print $url; ?>/sites/all/themes/meditech/images/quote.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/quote.png';this.onerror=null;" alt="quote bubble graphic">
			</figure>
			<div class="container__three-fourths">
				<div class="quote__content__text text--large">
					<p>
						<?php print $block_5->field_quote_1['und'][0]['value']; ?>
					</p>
				</div>
				<p class="text--large no-margin--bottom">
					<?php print $block_5->field_full_name['und'][0]['value']; ?>
				</p>
				<p>
					<?php print $block_5->field_company['und'][0]['value']; ?>
				</p>
			</div>
		</article>
	</div>
	<!--End of Block 5-->

	<!--Block 6 - Video-->
	<div class="content__callout">
		<div class="content__callout__media">
			<div class="content__callout__image-wrapper">
				<div class="video js__video" data-video-id="193753449">
					<figure class="video__overlay">
						<img src="<?php print $url; ?>/sites/default/files/styles/video-overlay-breakpoints_theme_meditech_big-medium_2x/public/images/video/Clatterbridge-Cancer-Centre_video-overlay--v2.jpg?itok=F2Y_5KNp&timestamp=1494621483" alt="Video Covershot">
					</figure>
					<a class="video__play-btn" href="http://vimeo.com/193753449"></a>
					<div class="video__container">
					</div>
				</div>
			</div>
		</div>
		<div class="content__callout__content">
			<div class="content__callout__body">
				<div class="content__callout__body__text">
				<h2>
				<?php print render($content['field_header_6']); ?>
			</h2>
					<?php print render($content['field_long_text_5']); ?>
				</div>
			</div>
		</div>
	</div>
	<!--End of Block 6-->

	<!--Block 7-->
	<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/caribbean-beach.jpg);">
		<div class="container__centered">
			<h2>
				<?php print render($content['field_header_7']); ?>
			</h2>
			<div class="container__two-thirds">
				<?php print render($content['field_long_text_6']); ?>
			</div>
		</div>
	</div>
	<!--End of Block 7-->

	<!--Block 8-->
	<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/map-of-globe-with-lines-connecting-cities.jpg);">
		<div class="container__centered transparent-overlay">
			<h2 class="text--white center">
				<?php print render($content['field_header_8']); ?>
			</h2>
			<div class="container__centered list-style--none">
				<div class="container__one-fourth text--white">
					<?php print render($content['field_long_text_7']); ?>
				</div>
				<div class="container__one-fourth text--white">
					<?php print render($content['field_long_text_8']); ?>
				</div>
				<div class="container__one-fourth text--white">
					<?php print render($content[ 'field_long_text_9']); ?>
				</div>
				<div class="container__one-fourth text--white">
					<?php print render($content['field_long_text_10']); ?>
				</div>
			</div>
			<div class="container__centered no-pad text--white">
				<div class="container__centered">
					<?php print render($content['field_long_text_11']); ?>
				</div>
			</div>
		</div>
	</div>
	<!--End of Block 8-->

	<!--Block 9-->
	<div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/green-abstract-triangular-shapes.png);">
		<div class="container__centered">
			<div class="page__title--center">
				<h2 class="text--white"><?php print render($content['field_header_9']); ?></h2>
				<p class="text--white">
					<?php print render($content['field_long_text_12']); ?>
				</p>
				<div class="page__title__ribbon"></div>
			</div>
			<div class="container">
				<div class="container__one-fourth">
					<div class="transparent-overlay">
						<div class="text--fresh-mint" style="text-align: center; padding-bottom: 1em;"><i class="fa fa-certificate fa-3x"></i></div>
						<h4 class="page__title--center text--white">
							<?php print $block_9[0]->field_header_1['und'][0]['value']; ?>
						</h4>
						<div class="text--white">
							<p>
								<?php print $block_9[0]->field_long_text_1['und'][0]['value']; ?>
							</p>
						</div>
					</div>
				</div>
				<div class="container__one-fourth">
					<div class="transparent-overlay">
						<div class="text--fresh-mint" style="text-align: center; padding-bottom: 1em;"><i class="fa fa-certificate fa-3x"></i></div>
						<div class="text--white">
							<h4 class="page__title--center text--white">
								<?php print $block_9[1]->field_header_1['und'][0]['value']; ?>
							</h4>
							<p>
								<?php print $block_9[1]->field_long_text_1['und'][0]['value']; ?>
							</p>
						</div>
					</div>
				</div>
				<div class="container__one-fourth">
					<div class="transparent-overlay">
						<div class="text--fresh-mint" style="text-align: center; padding-bottom: 1em;"><i class="fa fa-certificate fa-3x"></i></div>
						<h4 class="page__title--center text--white">
							<?php print $block_9[2]->field_header_1['und'][0]['value']; ?>
						</h4>
						<div class="text--white">
							<p>
								<?php print $block_9[2]->field_long_text_1['und'][0]['value']; ?>
							</p>
						</div>
					</div>
				</div>
				<div class="container__one-fourth">
					<div class="transparent-overlay">
						<div class="text--fresh-mint" style="text-align: center; padding-bottom: 1em;"><i class="fa fa-certificate fa-3x"></i></div>
						<h4 class="page__title--center text--white">
							<?php print $block_9[3]->field_header_1['und'][0]['value']; ?>
						</h4>
						<div class="text--white">
							<p>
								<?php print $block_9[3]->field_long_text_1['und'][0]['value']; ?>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--End of Block 9-->

	<!--Block 10 - CTA-->
	<div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg);">
		<div class="container__centered" style="text-align: center;">
			<h2><?php print render($content['field_header_10']); ?></h2>
			
			<?php 
            // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
            if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
              $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
            ?>
              <div class="button--hubspot">
                <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
              </div>
            <?php }else{ ?>
              <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
            <?php } ?>
            

		</div>
	</div>
	<!--End of Block 10-->

</div>
<!-- end js__seo-tool__body-content -->


<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>

<!-- end node--new-international.tpl.php template -->
