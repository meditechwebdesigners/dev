<!-- start customer-materials-page-menu--node-2332.php template -->
<?php
$url = $GLOBALS['base_url']; // grabs the site url

$content_sections = multi_field_collection_data_unrestricted($node, 'field_fc_head_ltext_1');
$buttons = multi_field_collection_data_unrestricted($node, 'field_fc_button_unl_1');
$side_menu = multi_field_collection_data_unrestricted($node, 'field_fc_text_link_unl_1');
?>


<!-- HERO section -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/customer/MEDITECH-expanse-hero-background.jpg);">
  <div class="container__centered">
    <div class="container center">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/expanse-logo.png" style="width:80%; margin-bottom:8em;">
    </div>
  </div>
</div>
<!-- End of HERO section -->

<section class="container__centered">

  <div class="container__two-thirds">
    
    <h1><?php print $title; ?></h1>
    <?php if( !empty($content['field_sub_header_1']) ){ ?>
      <h2><?php print render($content['field_sub_header_1']); ?></h2>
    <?php } ?>
     
    <?php

    if( isset($content_sections) && !empty($content_sections) ){
      $number_of_content_sections = count($content_sections);
      for($cs=0; $cs<$number_of_content_sections; $cs++){
        if($cs == 0){ // first content section...
          print '<div>';
            if( !empty($content_sections[$cs]->field_header_1['und'][0]['value']) ){
              print '<h2>';
              print $content_sections[$cs]->field_header_1['und'][0]['value']; 
              print '</h2>';
            }
            print '<div>';
            print $content_sections[$cs]->field_long_text_1['und'][0]['value']; 
            print '<div class="center"><img src="'.$url.'/sites/all/themes/meditech/images/expanse-logo_color.png" style="width:80%;"></div>';
            print '</div>';
          print '</div>';
        }
        else{ // all other content sections...
          if( !empty($content_sections[$cs]->field_header_1['und'][0]['value']) ){
              print '<h2>';
              print $content_sections[$cs]->field_header_1['und'][0]['value']; 
              print '</h2>';
            }
          print '<div>';
          print $content_sections[$cs]->field_long_text_1['und'][0]['value']; 
          print '</div>';
        }
      }
    }

    if( isset($buttons) && !empty($buttons) ){
      $number_of_buttons = count($buttons);
      for($b=0; $b<$number_of_buttons; $b++){
        if( !empty($buttons[$b]->field_hubspot_embed_code_1['und'][0]['value']) ){
          $button_code = $buttons[$b]->field_hubspot_embed_code_1['und'][0]['value']; 
          print '<div class="center" style="margin-top:2em;">';
          hubspot_button($button_code, "button");
          print '</div>';
        }
        else{
          print '<div class="center"><a href="'.$buttons[$b]->field_button_url_1['und'][0]['value'].'" class="btn--orange">'.$buttons[$b]->field_button_text_1['und'][0]['value'].'</a></div>';
        }
      }
    }

    ?>
    
  </div>

  
  <?php
  if( isset($side_menu) && !empty($side_menu[0]) ){
    print '<aside class="container__one-third panel">';
    print '<div class="sidebar__nav solutions_sidebar_gae">';
    print '<ul class="menu">';
    $number_of_menu_links = count($side_menu);
    for($ml=0; $ml<$number_of_menu_links; $ml++){
      print '<li><a href="';
      print $side_menu[$ml]->field_link_url_1['und'][0]['value']; 
      print '">';
      print $side_menu[$ml]->field_link_text_1['und'][0]['value']; 
      print '</a></li>';
    }
    print '</ul>';
    print '</div>';
    print '</aside>';
  }
  ?>


</section>


<!-- end customer-materials-page-menu--node-2332.php template -->