<!-- start page.tpl.php template -->
<?php $websiteURL = $GLOBALS['base_url']; // grabs the site url ?>

<?php include('inc--header.php'); ?>

<!-- MOBILE NAV WRAPPER -->
<div id="sb-site">


  <!-- MAIN CONTENT ================================================================== -->
  <main class="main" role="main">
    <a name="main-content" id="main-content"></a><!-- accessibility link destination -->
    
    <div id="page">

      <?php if($messages): ?>
      <!-- HTML for status and error messages. -->
      <div class="container__centered">
        <div id="messages">
          <div class="section clearfix">
            <?php print $messages; ?>
          </div>
        </div>
      </div>
      <!-- end container__centered -->
      <?php endif; ?>

   
      <?php 
      /* 
      *
      *
      START CONTENT REGION
      *
      *
      * check page type to determine which layout goes to the current page 
      *
      *
      */
      ?>


      <!-- START CONTENT REGION -->
      <?php 
      // check to see if current page is a NEWS TAXONOMY PAGE (it pulls it's main content from SECTION 2 of node--news-article.tpl.php)...
      $term_heading = $page['content']['system_main']['term_heading'];
      if( isset($term_heading) && $term_heading['term']['#bundle'] == 'news_tags' ){
      ?>

        <div class="container__centered">
          <div class="container__two-thirds">
            
            <h1 class="page__title" style="margin-bottom:1.5em;"><?php 
              // get taxonomy term to see if the word "News" should come after it...
              $term_name = $term_heading['term']['#term']->name;
              print $term_name;
              // if the term is any of the following, then don't add " News" after them...
              $terms_array = array('Expanse Updates', 'Industry Awards', 'MEDITECH in the Press', 'Podcasts', 'Press Releases', 'Quality Outcomes', 'Signings', 'Solutions', 'Success Stories', 'Videos');
              if( !in_array($term_name, $terms_array) ){
                print ' News'; 
              }
            ?></h1>
            
            <div class="taxonomy-results">
              <?php print render($page['content']); // this content is usually controlled at the node template level (node--news-article.tpl.php) ?>
            </div>
          
            <?php // add pagination links (convert to Load More button with Jquery help)...
            print $pager;        
            // Get the total number of node results...
            $totalResults = $GLOBALS['pager_total_items'][0];
            if($totalResults > 10){ // don't show Load More if less than 11 results
            ?>
              <div>
                <ul class="pager-load-more">
                  <li><button class="load-more-ajax">Load more</button></li>
                </ul>
              </div>
            <?php 
            } 
            ?>
            <script type="text/javascript">
              (function($) {
                Drupal.behaviors.loadMoreAjax = {
                  attach: function(context, settings) {
                    $('.load-more-ajax', context).click(function() {
                      var nextPage = $('.pager .pager-next a').attr('href');
                      var lastPage = $('.pager .pager-last a').attr('href');
                      $.get(nextPage, function(data) {
                        $(data).find('.taxonomy-results').insertBefore($('.item-list'));
                        $('.item-list .pager').remove();
                        if (nextPage == lastPage) {
                          $('.load-more-ajax').remove();
                        } else {
                          $(data).find('.item-list .pager').appendTo($('.item-list'));
                          Drupal.attachBehaviors($('.item-list'));
                        }
                      });
                    });
                    $('.item-list .pager').hide();
                  }
                };
              })(jQuery);
            </script>
        
          </div>
          
          <?php include('inc--news-sidebar.php'); ?>
          
        </div>
        
      <?php
      }
      else{
        
        print render($page['content']); // this content is usually controlled at the node template level
        
      }
      ?>
      <!-- END CONTENT REGION -->
      
      
      <?php 
      /* 
      ************** END checking page CONTENT to determine which layout goes to the current page **************** 
      *
      *
      *
      */
      ?>
      

    </div>
    <!-- End of #Page -->
  </main>
  <!-- End of MAIN -->
  
  <!-- Modal Pop-up Overlay -->
  <div id="mask"></div>


  <!-- FOOTER CONTENT ================================================================== -->

  <footer>

    <?php
    // START Customer Footer...
    if($node->type == 'customer_materials_page_menu'){

      include('inc--footer--customer.php');
      
    } // START general footer...
    else{

      include('inc--footer.php');
      
    } // END general footer
    ?>

    <!-- Legal Information =================================  -->
    <div class="container bg--black-coconut no-pad">
      <section class="container__centered">
        <div class="container__one-half">
          <h5 class="no-margin--top">Medical Information Technology, Inc.</h5>
          <p class="text--small">Copyright &copy; 2019 Medical Information Technology, Inc.</p>
          <p class="text--small footer_link_gae"><a href="<?php print $websiteURL; ?>/cookie-policy">Cookie Policy</a> | <a href="<?php print $websiteURL; ?>/privacy-policy">Privacy Policy</a></p>
        </div>
      </section>
    </div>
    <!-- End of Legal Information -->

  </footer>

  <!-- End of FOOTER -->

</div>
<!-- End of SLIDEOUT NAV WRAPPER -->

<?php include('inc--mobile-nav.php'); ?>

<?php if( user_is_logged_in() ){ ?>
<script type="text/javascript" src="<?php print $websiteURL; ?>/sites/all/themes/meditech/js/seo-tool.js"></script>
<?php } ?>

<!-- end page.tpl.php template
