<!-- START inc--mobile-nav.php -->

<style>
  .slideoutnav {
    padding-top: 3.5em;
  }

  .accordion {
    margin-top: 0;
  }

  .slideoutnav ul.menu li a:after,
  .slideoutnav ul.menu li.last a:after {
    display: none;
  }

  .slideoutnav ul.menu {
    padding: 0 1.5em;
  }

  .slideoutnav ul.menu li {
    list-style-type: none;
    margin-bottom: .5em;
  }

  .accordion__dropdown {
    margin-right: 1em;
    padding: 0;
    background-color: #e6e9ee;
    font-size: 1em;
  }

  .accordion__dropdown ul {
    margin: 0 0 1em 0;
  }

  .accordion__list li:last-child {
    border-bottom: none;
  }

  .slideoutnav ul.menu li a,
  .slideoutnav ul.menu li.last a {
    border-bottom: none;
    margin-right: 0;
  }

  .accordion__list__item {
    border-bottom: 0.1em solid #d7dce4;
  }

  .accordion__list__item a:active {
    color: #087e68;
  }

  .accordion__link {
    font-weight: 500;
  }

  .accordion__link--no-drop {
    padding: 1.2em;
    font-family: "montserrat", Verdana, sans-serif;
    display: block;
    font-weight: 500;
  }

  @media (max-width: 50em) {

    .slideoutnav {
      padding-top: 4.5em;
    }

    form.s-search:before {
      font-size: 1.3em;
    }
  }

  @media (min-width: 521px) {
    .slideoutnav--search {
      display: none;
    }
  }

  @media (max-width: 520px) {
    .form__search {
      width: 100%;
    }

    .header__search {
      display: inline-block;
      padding: 1em;
      float: left;
      width: 100%;
      margin: 0 0 .5em 0;
    }

    .header__search button i {
      font-size: 18px;
    }

    .header__search button {
      width: 30px;
      height: 30px;
      right: 10px;
      top: 10px;
    }

    input[type="search"] {
      padding: 0 2.5em 0 1.2em;
      font-size: 16px;
    }
  }

</style>

<div class="slideoutnav sb-slidebar sb-left">
  <div class="slideoutnav--search">
    <div class="header__search">
      <form class="form__search" method="get" action="<?php print $websiteURL; ?>/search-results" id="search-block-form" accept-charset="UTF-8">
        <label class="element-invisible" for="edit-search-block-form--2">Site Search </label>
        <input title="Enter the terms you wish to search for." placeholder="Search" type="search" name="as_q" value="">
        <button><i class="fas fa-search"></i></button>
      </form>
    </div>
  </div>
  <ul class="menu">
    <li>
      <div class="accordion">
        <ul class="accordion__list">

          <li class="accordion__list__item">
            <a class="accordion__link" href="#">EHR Solutions
              <div class="accordion__list__control"></div></a>
            <div class="accordion__dropdown">
              <ul>
                <li><a href="<?php print $websiteURL; ?>/ehr-solutions">All EHR Solutions</a></li>
                <li><a href="<?php print $websiteURL; ?>/ehr-solutions/#fiscal-responsibility">Fiscal Responsibility</a></li>
                <li><a href="<?php print $websiteURL; ?>/ehr-solutions/#interoperability">Interoperability</a></li>
                <li><a href="<?php print $websiteURL; ?>/ehr-solutions/#nurse-specialty-care">Nurse &amp; Specialty Care</a></li>
                <li><a href="<?php print $websiteURL; ?>/ehr-solutions/#physician-efficiency">Physician Efficiency</a></li>
                <li><a href="<?php print $websiteURL; ?>/ehr-solutions/#population-health">Population Health</a></li>
                <li><a href="<?php print $websiteURL; ?>/ehr-solutions/#quality-outcomes">Quality Outcomes</a></li>
              </ul>
            </div>
          </li>

          <li class="accordion__list__item">
            <a class="accordion__link" href="#">News
              <div class="accordion__list__control"></div></a>
            <div class="accordion__dropdown">
              <ul>
                <li><a href="<?php print $websiteURL; ?>/ehr-solutions">MEDITECH News</a></li>
                <li><a href="<?php print $websiteURL; ?>/ehr-solutions/#interoperability">Interoperability</a></li>
                <li><a href="<?php print $websiteURL; ?>/ehr-solutions/#population-health">Population Health</a></li>
                <li><a href="<?php print $websiteURL; ?>/ehr-solutions/meditechs-physician-productivity">Physician Efficiency</a></li>
                <li><a href="<?php print $websiteURL; ?>/ehr-solutions/meditech-nursing">Nursing</a></li>
                <li><a href="<?php print $websiteURL; ?>/ehr-solutions/meditech-as-a-service-maas">MaaS</a></li>
              </ul>
            </div>
          </li>

          <li class="accordion__list__item">
            <a href="https://blog.meditech.com/" class="accordion__link--no-drop">Blog</a>
          </li>

          <li class="accordion__list__item">
            <a class="accordion__link" href="#">Events
              <div class="accordion__list__control"></div></a>
            <div class="accordion__dropdown">
              <ul>
                <li><a href="<?php print $websiteURL; ?>/events">MEDITECH Events</a></li>
                <li><a href="<?php print $websiteURL; ?>/news-tags/events">Event Coverage</a></li>
                <li><a href="https://docs.google.com/forms/d/1LSolc1vI7WVyLfjbqq0Dziel0AgD0-hG6YUVsF6JEYY/viewform">Call for Abstracts</a></li>
                <li><a href="<?php print $websiteURL; ?>/events/trade-shows">Trade Shows</a>
                </li>
                <li><a href="<?php print $websiteURL; ?>/events/meditech-webinars">Webinars - Live</a></li>
                <li><a href="<?php print $websiteURL; ?>/events/meditech-on-demand-webinars">Webinars - On Demand</a></li>
                <li><a href="<?php print $websiteURL; ?>/events/past-events">Past Events</a></li>
              </ul>
            </div>
          </li>

          <li class="accordion__list__item">
            <a class="accordion__link" href="#">About
              <div class="accordion__list__control"></div></a>
            <div class="accordion__dropdown">
              <ul>
                <li><a href="<?php print $websiteURL; ?>/about-meditech/about-meditech">About MEDITECH</a></li>
                <li><a href="<?php print $websiteURL; ?>/about-meditech/executives">Executives</a></li>
                <li><a href="<?php print $websiteURL; ?>/about-meditech/community">Community</a></li>
                <li><a href="<?php print $websiteURL; ?>/about-meditech/directions-to-meditech">Directions</a>
                </li>
                <li><a href="<?php print $websiteURL; ?>/about-meditech/area-hotels">Area Hotels</a></li>
              </ul>
            </div>
          </li>

          <li class="accordion__list__item">
            <a class="accordion__link" href="#">Global
              <div class="accordion__list__control"></div></a>
            <div class="accordion__dropdown">
              <ul>
                <li><a href="<?php print $websiteURL; ?>/global">MEDITECH Global</a></li>
                <li><a href="<?php print $websiteURL; ?>/global/meditech-asia-pacific">Asia Pacific</a></li>
                <li><a href="<?php print $websiteURL; ?>/global/meditech-canada">Canada</a></li>
                <li><a href="<?php print $websiteURL; ?>/global/meditech-south-africa">South Africa</a>
                </li>
                <li><a href="<?php print $websiteURL; ?>/global/meditech-uk-ireland">UK &amp; Ireland</a></li>
              </ul>
            </div>
          </li>

          <li class="accordion__list__item">
            <a href="<?php print $websiteURL; ?>/contact" class="accordion__link--no-drop">Contact</a>
          </li>

        </ul>
      </div>
    </li>
  </ul>
</div>

<!-- END inc--mobile-nav.php -->