<!-- START campaign--node-2464.php -->
<?php
$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

// FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM with multiple instances...
// to use the function simply assign it to a variable
// then pass in the field collection machine name to the function and the number of instances allowed ($quantity)
// then you'll use the variable when calling an array for each group of sub-fields within the template
function multi_field_collection_data_unrestricted($node, $field_name){
  // get array specific to a certain field collection item...
  $field_collection = field_get_items('node', $node, $field_name); 
  // create array...
  $multi_field_collections = array();
  $quantity = count($field_collection);
  // loop through '$quantity' times...
  for($x=0; $x<$quantity; $x++){
    // get all content within that field collection...
    $fc_field_data = entity_load('field_collection_item', array($field_collection[$x]['value']));
    // get field collection ID...
    $fc_field_ID = key($fc_field_data);
    // create variable and load it with field collection data...
    $var_with_data = $fc_field_data[$fc_field_ID];
    // add data to array...
    $multi_field_collections[] = $var_with_data;
  }
  // return variable...
  return $multi_field_collections;
}
$content_sections = multi_field_collection_data_unrestricted($node, 'field_fc_head_ltext_1');
$buttons = multi_field_collection_data_unrestricted($node, 'field_fc_button_unl_1');
$side_menu = multi_field_collection_data_unrestricted($node, 'field_fc_text_link_unl_1');
?>

<style>
.reverse-grid {
    display: block;
    flex-direction: row;
  }
  
  @media screen and (max-width: 800px) {
  .reverse-grid {
    display: flex;
    flex-direction: column-reverse;
  }
    /** IE fix for flex-direction height issue **/
  .reverse-grid-fix {
    min-height: 100px;  
    }  
  }
</style>

<!-- Block 1 -->
<div class="container">
  <div class="container__centered reverse-grid">

    <div class="container__one-third">
    <?php
      if( !empty($content_sections[0]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[0]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      if( !empty($content_sections[0]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[0]->field_long_text_1['und'][0]['value'];   
      }
      ?>
    </div>
    <div class="container__two-thirds reverse-grid-fix"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/dedicated-community-animation.gif" alt=""></div>

  </div>
</div>
<!-- End Block 1 -->

<!-- END campaign--node-2464.php -->