<?php // This template is for each row of the Views block:  EVENTS SOUTH AFRICA view ....................... 
  // get node ID...
  $nid = $fields['nid']->content;
  $node = node_load($nid);
?>
<!-- start views-view-fields--events-south-africa--block.tpl.php template -->
<p><a href="<?php print $fields['path']->content; ?>" target="_blank"><?php print $fields['title']->content; ?></a> (<?php print $fields['field_date_and_time']->content; ?>)</p>
<!-- end views-view-fields--events-south-africa--block.tpl.php template -->