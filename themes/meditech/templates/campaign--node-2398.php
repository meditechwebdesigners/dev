<!-- START campaign--node-2398.php -- AMBULATORY -->
<?php
$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

    <style>
        .transparent-overlay--white {
            padding: 1em 2em;
            background-color: rgba(256, 256, 256, 0.8);
            min-height: 5em
        }

        @media all and (max-width: 30em) {
            .transparent-overlay--white {
                margin-bottom: 1em;
            }
        }

    </style>

    <div class="js__seo-tool__body-content">

        <!-- Block 1 -->
        <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/expanse--girl-with-binoculars-in-mountains.jpg);">
            <div class="container__centered">
                <div class="container__one-third">&nbsp;</div>
                <div class="container__two-thirds text--white">
                    <div style="margin:5em 0;" class="transparent-overlay">
                        <h1 class="js__seo-tool__title" style="display:none;">MEDITECH Ambulatory</h1>
                        <h2>Expand your view</h2>
                        <p>Ambulatory isn't just about seeing the patient in the physician's office. You need to see the patient's complete story &mdash; where they've been and where they are going next &mdash; to deliver the most appropriate care. With MEDITECH's Expanse EHR, you can. Access data from acute, home health, behavioral health, long-term care, and beyond, presented just the way you need it. No more walls, no more limits.</p>

                        <div class="center" style="margin-top:2em;">
                          <?php hubspot_button($cta_code, "Download the Patient Registries White Paper"); ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- End Block 1 -->


        <!-- Block 2 -->
        <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/african-american-physician-with-patient.jpg);">
            <div class="container__centered">
                <div class="container__two-thirds">
                    <h2>Practice medicine (not computer science)</h2>
                    <p>Stop spending more than half your work day interacting with the EHR and put your focus where it belongs — on patients. With its mobile, flexible, and intuitive design, MEDITECH's Ambulatory solution won't slow you down or get between you and your patients. They'll be happier. You'll be happier. It's time to <a href="https://ehr.meditech.com/ehr-solutions/meditechs-physician-productivity">get back to practicing medicine</a>.</p>
                    <div class="center">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ambulatory_home-screen.jpg" alt="MEDITECH Ambulatory home screen">
                    </div>
                </div>
            </div>
        </div>
        <!-- End Block 2 -->


        <!-- Block 3 -->
        <div class="container bg--blue-gradient">
            <div class="container__centered">
                <div class="container__one-third">
                    <h2>The power of one</h2>
                    <p>We bring it all together for your ambulatory practice: One EHR across all care settings, ensuring a complete picture of health for each of your patients. A single schedule, combining your appointments across settings, rounding schedule, meetings, and more — all in one place. And a single patient bill for all care, wherever it occurred. Add to this accelerated eligibility checking, streamlined referral management, and advanced revenue cycle tools, and your practice will be running at peak efficiency.</p>
                </div>
                <div class="container__two-thirds">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ambulatory_scheduling-grid.jpg" alt="MEDITECH Ambulatory scheduling screen">
                </div>
            </div>
        </div>
        <!-- End Block 3 -->


        <!-- Block 4 -->
        <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/female-physician-tablet-office-window.jpg); padding: 6em 0;">
            <div class="container__centered">
                <div class="container__one-third">&nbsp;</div>
                <div class="container__two-thirds">
                    <h2>A new EHR for a new era</h2>
                    <p>Experience what a modern EHR should be: intuitive, fast, and easy to use. Expanse takes full advantage of mobile technology, untethering you from your desktop so you can tap and swipe through your charts more efficiently. Why shouldn't your EHR be as easy to use as your favorite apps?</p>

                </div>
            </div>
        </div>
        <!-- End Block 4 -->


        <!-- Block 5 -->
        <div class="content__callout border-none">
            <div class="content__callout__media">
                <div class="content__callout__image-wrapper">
                    <div class="video js__video" data-video-id="239126049">
                        <figure class="video__overlay">
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/meditech-web-ambulatory-video-still.jpg" alt="Doctors talk about MEDITECH Expanse Ambulatory Video Covershot">
                        </figure>
                        <a class="video__play-btn" href="https://vimeo.com/239126049"></a>
                        <div class="video__container"></div>
                    </div>
                </div>
            </div>
            <div class="content__callout__content">
                <div class="content__callout__body">
                    <div class="content__callout__body__text">
                        <h2>What docs are saying about our Ambulatory product</h2>
                        <p>Hear about the features practicing physicians love in our Ambulatory solution — from personal widgets, to mobility, to the unsurpassed level of integration it offers.</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Block 5 -->


        <!-- Block 6 -->
        <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/clinician-team-group-shot.jpg);">
            <div class="container__centered">
                <div class="container__one-third text--white" style="padding:3em 0;">
                    <h2>Go team!</h2>
                    <p>Whether you're a physician, nurse, PA, NP, or any other member of the care team, you need an ambulatory solution that supports a team approach. Expanse improves communication and care coordination, with secure messaging and texting, multidisciplinary care plans, and dynamic patient registries for tracking patients as they cross settings. A unified patient record with robust tools for care coordination ensures that your team is always working from the same playbook.</p>
                </div>
            </div>
        </div>
        <!-- End Block 6 -->


        <!-- Block 7 -->
        <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/foot-traffic--beige.jpg);">
            <div class="container__centered">

                <div class="container__one-half">
                    <div class="transparent-overlay--white">
                        <h2>A higher level of care</h2>
                        <p>With our Ambulatory solution, you can do more than just serve the patients you see that day. Take your care up a level and positively impact entire populations with dynamic patient registries &mdash; responsive browser-based lists that are updated the moment data in the EHR changes. Sort patients by common characteristics, quickly identify problem areas, and intervene before minor issues become serious. It's the tool you need to keep your healthy patients well and manage your chronically ill patients more effectively.</p>
                    </div>
                </div>

                <div class="container__one-half center">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ambulatory-registries.png" alt="MEDITECH Ambulatory Registry Screenshot">
                    <p style="margin-top:1em;">See the power of Patient Registries in our <a href="https://info.meditech.com/meditechs-building-a-foundation-for-population-health-patient-registries-white-paper">white paper: Building a Foundation for Population Health</a>.</p>
                </div>

            </div>
        </div>
        <!-- End Block 7 -->


        <!-- Block 8 -->
        <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/);">
            <div class="container__centered">
                <h2>Make it yours</h2>
                <p>In healthcare, one size rarely fits all. You need a personalized EHR designed around your unique workflows and practice patterns, with your own widgets, preferences, and shortcuts built in. Our Ambulatory solution includes out-of-the-box content across more than 40 specialities, including documentation templates, protocols, order sets, and much more — all based on the most current clinical evidence. You take it from there and make it your own. The result: lower stress, higher efficiency, <a href="https://blog.meditech.com/how-to-reduce-burnout-with-a-physician-driven-ehr-design">less burnout</a>. That's better for you and your patients.</p>
                <div>
                    <div class="container__one-third center">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ambulatory_widget-patient.jpg" alt="MEDITECH Ambulatory patient widget">
                    </div>
                    <div class="container__one-third center">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ambulatory_widget-cardiovascular-risk.jpg" alt="MEDITECH Ambulatory cardiovascular risk widget">
                    </div>
                    <div class="container__one-third center">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ambulatory_widget-pfsh.jpg" alt="MEDITECH Ambulatory PFSH widget">
                    </div>
                </div>
            </div>
        </div>
        <!-- End Block 8 -->


        <!-- Block 9 -->
        <div class="container bg--blue-gradient">
            <div class="container__centered">
                <div class="container__one-third">
                    <h2>Hit the target</h2>
                    <p>You can't improve what you don't measure, which is why our Ambulatory solution helps you monitor every aspect of clinical and financial performance, to ensure you're hitting your targets. Our interactive, user-friendly Quality Vantage dashboards take the mystery out of MACRA by providing performance snapshots well before the end of your reporting period, to help you identify and correct any measures that are missing the mark.</p>
                </div>
                <div class="container__two-thirds">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ambulatory_quality-vantage.jpg" alt="MEDITECH Ambulatory quality vantage screen shot">
                </div>
            </div>
        </div>
        <!-- End Block 9 -->


        <!-- Block 10 -->
        <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/older-couple-on-couch-with-laptop.jpg);">
            <div class="container__centered">
                <div class="container__two-thirds">
                    <div class="transparent-overlay text--white" style="padding:1em 2em; float:left;">
                        <div class="container__one-half">
                            <h2>Be more engaging</h2>
                            <p>Engaged patients become active partners in their own care. Encourage them with smart tools like our <a href="https://ehr.meditech.com/ehr-solutions/patient-portal">Patient and Consumer Health Portal</a> and <a href="https://ehr.meditech.com/news/mhealth-meditech-in-the-app-store">MHealth app</a>, which enable patients to easily schedule appointments, pay bills, print immunization records, and securely communicate with their providers. They can even connect their fitness devices and apps. You can also offer your patients the ultimate convenience: care wherever they are, via our secure virtual visit technology.</p>
                        </div>
                        <div class="container__one-half center">
                            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-patient-portal-screen-shot--glucose.png" alt="MEDITECH mobile patient portal Screenshot" style="max-height: 450px;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Block 10 -->





    </div>
    <!-- end js__seo-tool__body-content -->

    <!-- Block 8 - CTA Block -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/);">
        <div class="container__centered center">

            <?php cta_text($cta); ?>

            <div class="center" style="margin-top:2em;">
              <?php hubspot_button($cta_code, "Download the Patient Registries White Paper"); ?>
            </div>

            <div style="margin-top:1em;">
                <?php print $share_link_buttons; ?>
            </div>
        </div>
    </div>
    <!-- End Block 8 -->

    <?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>

    <!-- END campaign--node-2398.php -->
