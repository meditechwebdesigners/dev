<?php // This template is set up to control the display of the CAMPAIGN_CARE_COORDINATION content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$quotes = multi_field_collection_data($node, 'field_fc_quote_1', 3);
$button_1 = field_collection_data($node, 'field_fc_button_1');
?>
<!-- start node--campaign_care_coordination.tpl.php template -->
 
  
<div class="js__seo-tool__body-content">
 
  <style>
    .button-vertical-adjustment { margin-top:8em; }
    @media (max-width: 800px) {
      .button-vertical-adjustment { margin-top:1em; }
    }
    .tranparent-with-p p { margin:0; }
    .spacing { margin-bottom:1em; }
    @media (max-width: 800px) {
      .spacing { margin-bottom:0; }
      .transparent-overlay { margin-bottom:1em; }
    }
  </style>
  
  <!-- Block 1 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/hospital-settings-green.jpg);">
    <div class="container__centered">
      <div class="container__two-thirds transparent-overlay text--white">
        <h1 class="text--white js__seo-tool__title" style="margin-top:0;"><?php print $title; ?></h1>
        <h2 class="text--white"><?php print render($content['field_header_1']); ?></h2>
        <?php print render($content['field_long_text_1']); ?>
      </div>
      <div class="container__one-third center">
        <?php 
        // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
        if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
          $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
        ?>
          <div class="button--hubspot button-vertical-adjustment">
            <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
          </div>
        <?php }else{ ?>
          <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
        <?php } ?>
      </div>
    </div>
  </div>
  <!-- End of Block 1 -->

  <!-- Block 2 - Video Block -->
  <div class="content__callout">
    <div class="content__callout__media">
      <div class="content__callout__image-wrapper">
        <div class="video js__video" data-video-id="263515474">
          <figure class="video__overlay">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--care-coordination-2018.jpg" alt="Video Covershot">
          </figure>
          <a class="video__play-btn" href="https://vimeo.com/263515474"></a>
          <div class="video__container"></div>
        </div>
      </div>
    </div>
    <div class="content__callout__content">
      <div class="content__callout__body">
       <h2><?php print render($content['field_header_2']); ?></h2>
        <?php print render($content['field_long_text_2']); ?>
      </div>
    </div>
  </div>
  <!-- End Block 2 -->
  
  <!-- Block 3 -->
   <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/doctor-smiling-with-stethescope.jpg);">
    <div class="container__centered">
      <div class="container__two-thirds transparent-overlay text--white">
        <h2><?php print render($content['field_header_3']); ?></h2>
        <?php print render($content['field_long_text_3']); ?>
      </div>
    </div>
  </div>
  <!-- End Block 3 -->

  <!-- Block 4 -->
  <?php $block_4 = field_get_items('node', $node, 'field_long_text_unl_1'); ?>
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/pharmacy-medicine-shelves-darkened.jpg);">
    <div class="container__centered text--white">
      <h2><?php print render($content['field_header_4']); ?></h2>
      <?php print render($content['field_long_text_4']); ?>
      <div class="container no-pad text--white spacing">
        <div class="container__one-half transparent-overlay tranparent-with-p"><?php print $block_4[0]['value']; ?></div>
        <div class="container__one-half transparent-overlay tranparent-with-p"><?php print $block_4[1]['value']; ?></div>
      </div>
      <div class="container no-pad text--white">
        <div class="container__one-half transparent-overlay tranparent-with-p"><?php print $block_4[2]['value']; ?></div>
        <div class="container__one-half transparent-overlay tranparent-with-p"><?php print $block_4[3]['value']; ?></div>
      </div>
    </div>
  </div>
  <!-- End Block 4 -->

  <!-- Block 5 -->
  <div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg);">
    <div class="container__centered">
      <div class="container__one-third">
        <h2><?php print render($content['field_header_5']); ?></h2>
        <?php print render($content['field_long_text_5']); ?>
      </div>
      <div class="container__two-thirds">
        <figure>
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-Care-Coordination-tablet--admitted-patients.png" alt="MEDITECH Care Coordination tablet screenshot">
        </figure>
      </div>
    </div>
  </div>
  <!-- End Block 5 -->

  <!-- Block 6 - Video Block -->
  <div class="content__callout">
    <div class="content__callout__media">
      <div class="content__callout__image-wrapper">
        <div class="video js__video" data-video-id="155984195">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--care-coordination-campaign-patient-portal.jpg" alt="Video Covershot">
          </figure>
          <a class="video__play-btn" href="http://vimeo.com/155984195"></a>
          <div class="video__container">
          </div>
        </div>      
      </div>
    </div>
    <div class="content__callout__content">
      <div class="content__callout__body">
       <h2><?php print render($content['field_header_6']); ?></h2>
        <?php print render($content['field_long_text_6']); ?>
      </div>
    </div>
  </div>
  <!-- End Block 6 -->

  <!-- Block 7 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/female-patient-female-doctor-tablet.jpg);">
    <div class="container__centered">
      <div class="container__two-thirds transparent-overlay text--white">
        <h2><?php print render($content['field_header_7']); ?></h2>
        <?php print render($content['field_long_text_7']); ?>
      </div>
    </div>
  </div>
  <!-- End Block 7 -->

  <!-- Block 8 -->
  <div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg);">
    <div class="container__centered">
      <h2><?php print render($content['field_header_8']); ?></h2>
      <?php print render($content['field_long_text_8']); ?>
    </div>
  </div>
  <!-- End Block 8 -->

  <!-- Block 9 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/doctor-nurse-patient-darkened.jpg);">
    <div class="container__centered">
      <?php for($n=0; $n<3; $n++){ ?>
        <div class="container__one-third">
          <div class="transparent-overlay">
            <div class="text--fresh-mint" style="text-align:center; padding-bottom:1em;"><i class="far fa-comments fa-3x"></i></div>
            <div class="text--white">
              <p>"<?php print $quotes[$n]->field_quote_1['und'][0]['value']; ?>"</p>
              <p class="text--large no-margin--bottom"><?php print $quotes[$n]->field_full_name['und'][0]['value']; ?></p>
              <p><?php print $quotes[$n]->field_company['und'][0]['value']; ?></p>
            </div>
          </div>
        </div>
      <?php } ?>
    </div>
  </div>
  <!-- End Block 9 -->
  
</div><!-- end js__seo-tool__body-content -->  

  <!-- Block 10 -->
  <div class="container bg--black-coconut" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/dark-grey-triangle-pattern.png);">
    <div class="container__centered" style="text-align: center;">
      <h2 class="text--white js__seo-tool__body-content"><?php print render($content['field_header_9']); ?></h2>
      <div class="text--white">
        <?php print render($content['field_long_text_9']); ?>
      </div>
      <?php 
      // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
      if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
        $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
      ?>
        <div class="button--hubspot">
          <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
        </div>
      <?php }else{ ?>
        <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
      <?php } ?>
      
      <div style="margin-top:1em;">
        <?php print $share_link_buttons; ?>
      </div>
    </div>
  </div>
  <!-- End Block 10 -->


<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>  
        
<!-- end node--campaign_care_coordination.tpl.php template -->