<?php
  /*
  *
  * This template is set up to control the display of the 'about' content type 
  *
  */
  $url = $GLOBALS['base_url']; // grabs the site url

?>
<!-- start node--about.tpl.php template -->
    
  
  <?php
  /* 
  * ***************************************************************************************************
  * check to see which page is the current page, then load the proper Views or content for that page...
  * ***************************************************************************************************
  */
  ?>


  <?php
  // header for the ABOUT MEDITECH page ======================================================================
  if($title == 'About MEDITECH'){
  ?>
  <!-- Hero -->
  <div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/about/about-bg.png);">
    <div class="container__centered text--white text-shadow--black">
      <h1 class="js__seo-tool__title">About MEDITECH</h1>
      <h2 class="js__seo-tool__body-content">MEDITECH was founded on the idea of pushing healthcare forward. Now, here we are, doing it again.</h2>
    </div>
  </div>
  <?php
  } // END IF About
  ?>
    
  <section class="container__centered">
	<div class="container__two-thirds">

    <?php
     // for any About Page other than the ABOUT MEDITECH page ====================================================
    if($title != 'About MEDITECH'){
    ?>
      <h1 class="page__title js__seo-tool__title"><?php print $title; ?></h1>
    <?php
    } // END IF not About
    ?>
    
    <div class="js__seo-tool__body-content">
      <?php print render($content['field_body']); // render BODY field if it exists... ?>
    </div>
    
    <?php // SEO tool for internal use...
      if($title != 'Area Hotels' && $title != 'Directions to MEDITECH' && $title != 'Executives' && node_access('update',$node)){
        print '<!-- SEO Tool is added to this div -->';
        print '<div class="container no-pad--top js__seo-tool"></div>';
      } 
    ?>    
    
    <?php
    // content for the main EXECUTIVES page ======================================================================
    if($title == 'Executives'){
    ?>     
      <div class="container no-pad">
        <?php print views_embed_view('executives', 'block'); // adds 'Executives' Views block... ?>
      </div><!-- END container -->
    <?php
    }
    ?>

    <?php
    // content for the main DIRECTIONS page ======================================================================
    if($title == 'Directions to MEDITECH'){
      print views_embed_view('locations_directions_page', 'block'); // adds 'locations - directions page' Views block... 
    }
    ?>
    
    <?php
    // content for the main AREA HOTELS page ======================================================================
    if($title == 'Area Hotels'){
    ?>
      <?php print views_embed_view('hotels', 'block'); // adds 'Hotels' Views block... ?>
    <?php
    }
    ?>

    </div><!-- END container__two-thirds -->

    <?php
    // add ABOUT SIDE NAV ===========================================================================
    ?>
    <aside class="container__one-third panel">
      <div class="sidebar__nav about_sidebar_gae">
        <?php
          $massnBlock = module_invoke('menu', 'block_view', 'menu-about-section-side-nav');
          print render($massnBlock['content']); 
        ?>
      </div>
    </aside>
  
  </section>
  
  <?php // VIDEO SECTION =================================================================
    // get value from field_video to pass to View (if video exists)...
    $video = render($content['field_video']);
    if(!empty($video)){ // if the page has a video...
      print '<!-- VIDEO -->';
      // remove apostrophes from titles to prevent View from breaking...
      $video_filtered = str_replace("&#039;", "'", $video);
      // adds 'video' Views block...
      print views_embed_view('video_boxed', 'block', $video_filtered);
      print '<!-- END VIDEO -->';
    }
  ?>  
  
<!-- end node--about.tpl.php template -->