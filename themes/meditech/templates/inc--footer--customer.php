  <!-- START inc-footer--customer.php -->
  <style>
      .footer__customer ul {
            list-style: none;
            padding-left: 0;
        }

        .footer__customer ul li {
            line-height: 1.25em;
            margin-bottom: 1.25em;
        }

        #phone {
            background-color: #e6e9ee;
            font-size: 1.6em;
            text-align: center;
            padding-top: 20px;
            padding-bottom: 20px;
        }

        hr {
            margin: 1em 0;
        }

    </style>

    <hr />

    <!-- SITEMAP ================================================================== -->
    <section class="container__centered">
      <div class="footer__customer main_nav_gae">

        <div class="container__one-fourth">
          <h5>Customer Resources</h5>
          <ul>
            <li><a href="https://home.meditech.com/en/d/migrationbenefits/homepage.htm">5.x to Expanse Migration Benefits</a></li>
            <li><a href="https://customer.meditech.com/en/d/optimization/homepage.htm">5.x Optimization Assessments</a></li>
            <li><a href="https://home.meditech.com/en/d/cstrainingresources/pages/parameters.htm">Parameter Documentation</a></li>
            <li><a href="https://customer.meditech.com/en/d/6xservicegroups/pages/optimizationhomepage.htm">6.x Optimization Assessments</a></li>
            <li><a href="https://www.meditech.com/Programs/ams.exe?TYPE=OnLine.html" title="MEDITECH's Customer Support">Customer Support</a></li>
            <li><a href="https://home.meditech.com/en/d/newsroom/pages/clientservicesnewsletters.htm">Client Services Newsletters</a></li>
            <li><a href="https://home.meditech.com/en/d/clickhome/homepage.htm">Click Reduction</a></li>
            <li><a href="https://home.meditech.com/en/d/newmeditech/pages/collaborativesolutions.htm">Collaborative Solutions</a></li>
            <li><a href="https://ehr.meditech.com/">MEDITECH.com</a></li>
            <li><a href="https://customer.meditech.com/rules/ ">MEDITECH R.U.L.E.S.</a></li>
            <li><a href="https://customer.meditech.com/en/d/5xmultientity/homepage.htm">Multi-Entity Capabilities</a></li>
            <li><a href="https://home.meditech.com/en/d/newmeditech/pages/mobilityresourcescenter.htm">Mobile Devices Resource Center </a></li>
            <li><a href="https://www.meditech.com/workflowintegration/wig.htm" title="Workflow Integration Guide (5.x)">Workflow Integration Guide</a></li>
          </ul>
        </div>

        <div class="container__one-fourth">
          <h5>Best Practices</h5>
          <ul>
            <li><a href="https://customer.meditech.com/en/d/bestpracticesexp/homepage.htm">Expanse</a></li>
            <li><a href="https://customer.meditech.com/en/d/6xreadyimpl/homepage.htm">6.x</a></li>
            <li><a href="https://customer.meditech.com/en/d/bestpractices5x/pages/cshomepage.htm">Client/Server</a></li>
            <li><a href="https://customer.meditech.com/en/d/bestpractices5x/pages/magichomepage.htm">MAGIC</a></li>
            <li><a href="https://customer.meditech.com/en/d/protectednews/pages/stage67resources.htm">Stage 6 &amp; 7</a></li>
          </ul>
          <h5>Product Resources</h5>
          <ul>
            <li><a href="https://customer.meditech.com/productresource/menu/index.php?prodln=E">Expanse</a></li>
            <li><a href="https://customer.meditech.com/productresource/menu/index.php?prodln=maas">Expanse MaaS</a></li>
            <li><a href="https://www.meditech.com/productresources/Pages/PRbiProductResourcesIndexSIX.asp">6.x</a></li>
            <li><a href="https://www.meditech.com/productresources/Pages/PRbiProductResourcesIndexCS.asp">Client/Server</a></li>
            <li><a href="https://www.meditech.com/productresources/Pages/PRbiProductResourcesIndexMAGIC.asp">MAGIC</a></li>
          </ul>

          <h5>Application Programming Interface (API) Resources</h5>
          <ul>
            <li><a href="https://home.meditech.com/en/d/restapiresources/pages/apidoc.htm">App Developer Resources</a></li>
            <li><a href="https://home.meditech.com/en/d/restapiresources/homepage.htm">RESTful API Infrastructure & Interoperability Services Resource Center</a></li>
          </ul>
        </div>

        <div class="container__one-fourth">
          <h5>Customer Share</h5>
          <ul>
            <li><a href="https://www.meditech.com/reportbase/">NPR Report Writer Reports/Design</a></li>
          </ul>
          <h5>Learning</h5>
          <ul>
            <li><a href="https://home.meditech.com/en/d/cstrainingresources/homepage.htm">5.x Fast Track Training</a></li>
            <li><a href="https://home.meditech.com/en/d/newsroom/pages/physicianclpp.htm">CLPP</a></li>
            <li><a href="https://home.meditech.com/en/d/newsroom/pages/doctorshours.htm">Doctors' Hours</a></li>
            <li><a href="https://home.meditech.com/en/d/customer/pages/seminars.htm">Seminars</a></li>
            <li><a href="https://www.meditech.com/kb/homepage.htm">Knowledge Base</a></li>
            <li><a href="https://customer.meditech.com/en/d/clientservicesenhresources/pages/issuemanagement.htm">Issue Management Tutorial</a></li>
            <li><a href="https://home.meditech.com/en/d/casestudies/homepage.htm">White Papers &amp; Case Studies</a></li>
            <li><a href="https://customer.meditech.com/en/d/clinicalnotifications/pages/clnabfeallapplications.htm">Clinical Notifications/Notices</a></li>
            <li><a href="https://customer.meditech.com/en/d/esignature/pages/esignabfeallapplicationsnew.htm">Electronic Signature</a></li>
            <li><a href="https://customer.meditech.com/en/d/prwehrwide/pages/abfetutorials.htm">e-Learning Tutorials</a></li>
            <li><a href="https://customer.meditech.com/en/d/meditechuniversity/pages/mt6bfemtuniversityhomepage.htm">MEDITECH On Demand</a></li>
          </ul>
        </div>

        <div class="container__one-fourth">
          <h5>Development Resources</h5>
          <ul>
            <li><a href="https://www.meditech.com/docsearch/">Release Documentation Search</a></li>
            <li><a href="https://customer.meditech.com/testingguidesearch/">Testing Guide Search</a></li>
            <li><a href="https://customer.meditech.com/bloodbankvalidationsearch/">Blood Bank Validation Guides</a></li>
            <li><a href="https://customer.meditech.com/standardcontentsearch/">Standard Content Search</a></li>
          </ul>
          <h5>EHR &amp; Regulatory</h5>
          <ul>
            <li><a href="https://home.meditech.com/en/d/govregulatory/homepage.htm" title="Regulatory">Regulatory</a></li>
            <li><a href="https://customer.meditech.com/en/d/hipaa/homepage.htm" title="HIPAA">HIPAA</a></li>
            <li><a href="https://customer.meditech.com/en/d/informationsecurity/homepage.htm" title="Information Security">Information Security</a></li>
          </ul>
          <h5>Social Media</h5>
          <ul>
            <li><a href="https://www.facebook.com/MeditechEHR?ref=br_tf" target="_blank" rel="noopener noreferrer" title="MEDITECH Facebook">Facebook</a></li>
            <li><a href="https://twitter.com/MEDITECH" target="_blank" rel="noopener noreferrer" title="MEDITECH Twitter">Twitter</a></li>
            <li><a href="https://www.linkedin.com/company/meditech" target="_blank" rel="noopener noreferrer" title="MEDITECH LinkedIn">LinkedIn</a></li>
          </ul>
          <a class="footer_logo_gae" href="<?php print $websiteURL; ?>"><img src="<?php print $websiteURL; ?>/sites/all/themes/meditech/images/MEDITECH-Logo--50th-Anniversary.png" alt="MEDITECH 50th Anniversary Logo" style="margin:1em 0;"></a>
        </div>

      </div>
    </section>
    <!-- End of SITEMAP -->

    <!-- Phone Number Area-->
    <div id="phone" class="cus-footer-phone">
      <div class="row">
        <a href="tel:781-821-3000" class="phone_number_gae">
          <i class="fas fa-phone fa-lg cus-footer-phone-icon"></i>
          <span class="cus-footer-phone-number">781-821-3000</span>
        </a>
      </div>
    </div>
    <!-- #phone -->
    <!-- END inc-footer--customer.php -->