<?php

/**
 *
 *   This template is for EACH TABLE in the Views block: JOB LISTINGS BY STATE .......................
 *
 *
 * @file
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $caption: The caption for this table. May be empty.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 */
 $url = $GLOBALS['base_url']; // grabs the site url
?>
<!-- start views-view-table--job-listings-by-state--block.tpl.php template -->

<?php if (!empty($title) || !empty($caption)) : ?>
   <h3><?php print $caption . $title; ?></h3>
<?php endif; ?>

<?php 
// check to see if the section $title contains any of the following terms to know what to set as the ID for the table...
// these IDs should match up with the JS created in the node--careers.tpl.php file
if( strpos($title, 'Massachusetts') !== FALSE ){ 
  print '<table class="table tablesorter" id="massachusetts">';
}
elseif( strpos($title, 'Minnesota') !== FALSE ){ 
  print '<table class="table tablesorter" id="minnesota">';
}
else{ 
  print '<table class="table tablesorter" id="georgia">';
} 
?>

  <?php if (!empty($header)) : ?>
    <thead>
      <tr>
        <?php foreach ($header as $field => $label): ?>
          <?php if($label == 'Position'){ ?>
            <th class="table__careers__position"><?php print $label; ?></th>
          <?php } else { ?>
            <th class="tables__careers__department"><?php print $label; ?></th>
          <?php } ?>
        <?php endforeach; ?>
      </tr>
    </thead>
  <?php endif; ?>
  <tbody>
    <?php foreach ($rows as $row_count => $row): ?>
      <tr <?php if ($row_classes[$row_count]) { print 'class="' . implode(' ', $row_classes[$row_count]) .'"';  } ?>>
        <?php foreach ($row as $field => $content): ?>
          <td>
            <?php print $content; ?>
          </td>
        <?php endforeach; ?>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<!-- end views-view-table--job-listings-by-state--block.tpl.php template -->