<!-- START MACRA--node-2725.php -->
<?php // This template is set up to control the display of the Nurse content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
    @media all and (max-width: 58.75em) {
        .image-shrink--20 { max-width: 20%; }
        .image-shrink--60 { max-width: 60%; }
    }
</style>


<!-- Block 1 -->
<div class="container background--cover hide__bg-image--mobile" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/nurse-helping-patient-while-holding-tablet.jpg); background-color:#e6e9ee;">
    <div class="container__centered">
        <div class="container__two-thirds transparent-overlay--white" style="background-color: rgba(255, 255, 255, 0.5);">
            <h2>
                MEDITECH Nursing and Therapy Solutions
            </h2>
            <h3>
                It’s the mobile, patient-centered clinical experience you’ve been waiting for.
            </h3>
            <p>
                It’s time for clinicians and therapists to untether from their WOWs with the help of modern, mobile solutions that fit conveniently in their hand or pocket. With MEDITECH Expanse, you’ll be free to stay focused on what you value most — providing compassionate patient care. From advanced tracking boards for early detection and intervention of at-risk conditions, to our mobile Expanse Point of Care software, you’ll have the tools you need to provide the quality, face-to-face care your patients deserve.
            </p>
            
            <div class="center" style="margin-top:2em;">
              <?php hubspot_button($cta_code, "Register For The 2019 Nurse Forum"); ?>
            </div>
        </div>
        <div class="container__one-third">
            &nbsp;
        </div>

    </div>
</div>
<!-- End of Block 1 -->


<!-- Block 2 - Video -->
<div class="content__callout border-none" style="background-color: #E6E9EE !important;">
    <div class="content__callout__media">
        <div class="content__callout__image-wrapper" style="padding:3em !important;">
            <div class="video js__video video--shadow" data-video-id="308431272">
                <figure class="video__overlay">
                    <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Joe-Farr--Point-of-Care--video-overlay.jpg" alt="Joe Farr on Point of Care">
                </figure>
                <a class="video__play-btn" href="https://vimeo.com/308431272"></a>
                <div class="video__container"></div>
            </div>
        </div>
    </div>
    <div class="content__callout__content">
        <div class="content__callout__body">
            <div class="content__callout__body__text">
                <h2>
                    Discover how easy it is to streamline your workflow.
                </h2>
                <p>
                    Hear from Joe Farr, RN, clinical applications coordinator at King's Daughters Medical Center, about how Expanse Point of Care has “knocked it out of the park” by increasing efficiencies and improving satisfaction for their clinicians and therapists.
                </p>
            </div>
        </div>
    </div>
</div>
<!-- End of Block 2 - Video -->


<!-- Block 3 -->
<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/nurses-sharing-bg.png);">
    <div class="container__centered text--white">
        <div class="container center auto-margins no-pad">
            <h2 class="text--white">
                Document without distractions.
            </h2>
            <p>
                Stay focused on your patients, not your device. MEDITECH Expanse Point of Care is a modern, mobile solution that you can use to complete most documentation using a smartphone or handheld mobile device. Save time, simplify your workflow, and increase face-to-face time with your patients — with no additional software required.
            </p>
        </div>
        <h2 class="center">
            MEDITECH’s Expanse Point of Care software
        </h2>
        <div class="container__one-half transparent-overlay">
            <p class="center">
                <i class="fas fa-user-injured fa-4x"></i>
            </p>
            <h4>
                Improves the <a href="https://ehr.meditech.com/ehr-solutions/to-improve-patient-engagement-focus-on-the-patient">patient experience</a> by:
            </h4>
            <ul>
                <li>
                    Bringing chart navigation and documentation to the bedside
                </li>
                <li>
                    Recording vitals and administering medications without disturbing other patients in the room
                </li>
                <li>
                    Allowing patients to have more face-to-face time with their clinicians.
                </li>
            </ul>
        </div>
        <div class="container__one-half transparent-overlay">
            <p class="center">
                <i class="fas fa-user-md fa-4x"></i>
            </p>
            <h4>
                Improves the user experience by:
            </h4>
            <ul>
                <li>
                    Giving clinicians more time back in their day
                </li>
                <li>
                    Reducing reliance on WOWs — because you’re using the right device for the right job
                </li>
                <li>
                    Providing instant access to patient charts with a simple wristband scan.
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- End of Block 3 -->


<!-- Block 4 - Quote  -->
<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/vast-expanse-night-sky.jpg);">
    <div class="container__centered">
        <figure class="container__one-fourth center">
            <img class="quote__content__img" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/joe-farr.png" alt="Joe Farr, RN, Clinical Applications Coordinator head shot">
        </figure>
        <div class="container__three-fourths text--white">
            <div class="quote__content__text text--large" style="margin-top:1em;">
                <p class="italic">"Expanse Point of Care technology has generated more excitement than any nursing product to date! We’re confident it will solve our current logistical challenges and introduce new efficiencies for bedside providers, while still maintaining the highest patient safety standards."</p>
            </div>
            <p class="text--large no-margin--bottom"><span class="bold">Joe Farr, RN, Clinical Applications Coordinator</span></p>
            <p>King’s Daughters Medical Center</p>
        </div>
    </div>
</div>
<!-- End of Block 4 - Quote -->

<!-- Block 5  -->
<div class="container background--cover hide__bg-image--mobile" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/nurse-holding-tablet-in-ER.jpg); background-color:#e6e9ee;">
    <div class="container__centered text--white">
        <div class=" container__two-thirds transparent-overlay">
            <h2 class="no-margin--top">
                Put patient safety first.
            </h2>
            <p>
                Expanse provides the stopgap measures to prevent errors and preserve your peace of mind. You and your patients will benefit from:
            </p>
            <ul>
                <li>
                    <a href="https://ehr.meditech.com/ehr-solutions/meditech-surveillance">Surveillance boards</a> and notifications to flag for patients at risk for sepsis, falls, and other hospital-acquired conditions
                </li>
                <li>
                    <a href="https://ehr.meditech.com/ehr-solutions/meditech-ehr-excellence-toolkits">Evidence- and expert-based content</a>, rules, and workflows, so you’re never alone in your decision making
                </li>
                <li>
                    Bedside medication, transfusion, and phlebotomy verification
                </li>
                <li>
                    Real-time notifications of out-of-range values and overdue interventions.
                </li>
            </ul>
        </div>
        <div class="container__one-third">
            &nbsp;
        </div>
    </div>
</div>
<!-- End of Block 5  -->

<!-- Block 6 -->
<div class="container bg--white ">
    <div class="container__centered auto-margins">
        <div class="container no-pad">
            <h2>
                Use more intelligent solutions, for more meaningful results.
            </h2>
            <p>
                Using MEDITECH's CAUTI Toolkit in conjunction with <a href="https://ehr.meditech.com/ebooks/5-ehr-tools-for-thriving-under-value-based-care">Surveillance features</a>, several locations within RCCH HealthCare Partners saw an immediate and significant decrease in their CAUTI cases as early as the first quarter following go-LIVE.
            </p>
        </div>
        <div class="container no-pad">
            <div class="container__one-half center">
                <h3 class="text--meditech-green">
                    45% decrease in CAUTI cases
                </h3>
                <p>between Q4 2017 and Q1 2018</p>
                <img class="image-shrink--20" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/pie-chart-45.png" alt="Pie chart showing 45%">
            </div>
            <div class="container__one-half center">
                <h3 class="text--meditech-green">
                    35% reduction in foley catheter days
                </h3>
                <p>between Q4 2017 and Q1 2018</p>
                <img class="image-shrink--20" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/pie-chart-37.png" alt="Pie chart showing 35%">
            </div>
        </div>
    </div>
</div>
<!-- End of Block 6  -->


<!-- Block 7 -->
<div class="container bg--green-gradient">
    <div class="container__centered  text--white">
        <div class="center no-margin--top auto-margins">
            <h2>
                Stay in touch with your team, for smoother care transitions.
            </h2>
            <h4>
                Nurses, physicians, therapists, case managers — you’re all on the same team. Communicate more effectively with our:
            </h4>
        </div>
        <div class="container__one-third">
            <ul>
                <li>
                    Enterprise-wide EHR
                </li>
                <li>
                    Multidisciplinary care plans
                </li>
                <li>
                    Real-time status board monitoring of all patient activity, for smoother handoffs
                </li>
                <li>
                    Inherent clinical messaging and integration with <a href="http://www.forwardadvantage.com/solutions/identity-access-management/identity-management-secure-communications/imprivata/imprivata-cortext/" target="_blank">Imprivata Cortext®</a>secure texting
                </li>
                <li>
                    Embedded links to MCG CareWebQI/Indicia for evaluating clinical indications and determining level of care.
                </li>
            </ul>
        </div>
        <div class="container__two-thirds">
            <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/MEDITECH-PCS-Status-Board-screenshot.png" alt="MEDITECH PCS Status Board screenshot">
        </div>
    </div>
</div>
<!-- End of Block 7  -->


<!-- Block 8 -->
<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/group-of-nurses-and-doctors-saving-patient.jpg);">
    <div class="container__centered">
        <div class="page__title--center" style="margin-bottom:2em;">
            <h2 class="text--white">Smart tools for every nursing specialty.</h2>
            <div class="text--white">
                <p>Fix your specialty’s problem areas, with our web-based solutions.</p>
            </div>
        </div>
        <!-- .page__title--center -->

        <div class="container no-pad--top">

            <div class="container__one-fourth">
                <div class="transparent-overlay">
                    <h4 class="page__title--center text--white">ED Nurses</h4>
                    <div class="text--white center">
                        Use integrated trackers to smooth transitions, as in this <a href="https://info.meditech.com/case-study-ehr-steers-avera-mckennan-ed-nurse-navigator-program-to-475000-annual-savings">ED nurse navigator program</a>.
                    </div>
                </div>
            </div>

            <div class="container__one-fourth">
                <div class="transparent-overlay">
                    <h4 class="page__title--center text--white">Home Care Nurses</h4>
                    <div class="text--white center">
                        Review <a href="https://www.meditech.com/productbriefs/flyers/Home_Care_Mobile_Flyer.pdf">real-time schedules</a> from your smartphone.
                    </div>
                </div>
            </div>

            <div class="container__one-fourth">
                <div class="transparent-overlay">
                    <h4 class="page__title--center text--white">Surgical Nurses</h4>
                    <div class="text--white center">
                        Document from pre-surgical interview to PACU with <a href="https://ehr.meditech.com/ehr-solutions/meditech-surgical-services">integrated interoperative documentation</a>.
                    </div>
                </div>
            </div>

            <div class="container__one-fourth">
                <div class="transparent-overlay">
                    <h4 class="page__title--center text--white">Quality Managers</h4>
                    <div class="text--white center">
                        Review <a href="https://ehr.meditech.com/ehr-solutions/meditech-surveillance">actionable watchlists</a> of patients who qualify for surveillance profiles.
                    </div>
                </div>
            </div>

        </div>
        <!-- .container no-pad--top -->

        <!-- ROW 2 -->

        <div class="container no-pad">

            <div class="container__one-fourth">
                <div class="transparent-overlay">
                    <h4 class="page__title--center text--white">Practice Nurses</h4>
                    <div class="text--white center">
                        Share a <a href="https://ehr.meditech.com/ehr-solutions/web-ambulatory">single visit note</a> between nurses and physicians.
                    </div>
                </div>
            </div>

            <div class="container__one-fourth">
                <div class="transparent-overlay">
                    <h4 class="page__title--center text--white">Long Term Care Nurses</h4>
                    <div class="text--white center">
                        Simultaneously <a href="https://ehr.meditech.com/ehr-solutions/long-term-care-post-acute-services">document MDS assessments and trigger CAAs</a>.
                    </div>
                </div>
            </div>

            <div class="container__one-fourth">
                <div class="transparent-overlay">
                    <h4 class="page__title--center text--white">Critical Care Nurses</h4>
                    <div class="text--white center">
                        <a href="https://ehr.meditech.com/ehr-solutions/critical-care">Live in a flowsheet</a> customized for high acuity settings.
                    </div>
                </div>
            </div>

            <div class="container__one-fourth">
                <div class="transparent-overlay">
                    <h4 class="page__title--center text--white">Case Managers</h4>
                    <div class="text--white center">
                        Complete reviews, <a href="https://ehr.meditech.com/ehr-solutions/bridging-the-gaps-in-care">support transitions</a>, and manage follow up.
                    </div>
                </div>
            </div>

        </div>
        <!-- .container no-pad -->

    </div>
    <!-- .container__centered -->
</div>
<!-- .container background--cover -->

<!-- End of block 8 -->


<!-- Block 9  -->
<div class="container bg--green-gradient">
    <div class="container__centered center">
        <h2>
            Support for busy therapists...
        </h2>
        <p>
            Stay “on the go” with a convenient, reliable tool in your back pocket that will complement your workflow. Expanse Point of Care is the perfect fit for:
        </p>
        <div class="container no-pad">
            <div class="container__one-third transparent-overlay">
                <img class="image-shrink--20" style="width:40%;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Physical-Therapists--Icon.png" alt="Physical Therapists Icon">
                <h4>
                    Physical Therapists
                </h4>
            </div>
            <div class="container__one-third transparent-overlay">
                <img class="image-shrink--20" style="width:40%;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Occupational-Therapists--Icon.png" alt="Occupational Therapists Icon">
                <h4>
                    Occupational Therapists
                </h4>
            </div>
            <div class="container__one-third transparent-overlay">
                <img class="image-shrink--20" style="width:40%;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Respiratory-Therapists--Icon.png" alt="Respiratory Therapists Icon">
                <h4>
                    Respiratory Therapists
                </h4>
            </div>
        </div>
        <div class="container no-pad--bottom">
            <div class="container__one-third transparent-overlay">
                <img class="image-shrink--20" style="width:40%;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Speech-Language-Pathologist--icon.png" alt="Speech Language Pathologist Icon">
                <h4>
                    Speech Language Pathologists
                </h4>
            </div>
            <div class="container__one-third transparent-overlay">
                <img class="image-shrink--20" style="width:40%;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Mental-Health--Icon.png" alt="Mental Health Care Icon">
                <h4>
                    Mental Health Care Providers
                </h4>
            </div>
            <div class="container__one-third transparent-overlay">
                <img class="image-shrink--20" style="width:40%;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Patient-Care-Aides-Icon.png" alt="Patient Care Aide Icon">
                <h4>
                    Patient Care Aides
                </h4>
            </div>
        </div>
    </div>
</div>
<!-- End of Block 9  -->

<!-- Block 10 -->
<div class="container bg--blue-gradient">
    <div class="container__centered center">
        <div class="container__one-third">
            <h2>
                ...and their unique workflows.
            </h2>
            <div class="container auto-margins transparent-overlay">
                <ul class="left">
                    <li>
                        Mobile, modern solution that helps you treat patients wherever they are
                    </li>
                    <li>
                        Easy-to-use, flexible documentation tools
                    </li>
                    <li>
                        Frictionless interaction with patients
                    </li>
                    <li>
                        Easily slips in your pocket and out of the way
                    </li>
                    <li>
                        Patient information supports the mobile workflow needs of busy therapists.
                    </li>
                </ul>
            </div>
        </div>
        <div class="container__one-third">
            <img class="image-shrink--60" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/zebra--patient-data.png" alt="Screenshot of MEDITECH software on ZEBRA phone">
        </div>
        <div class="container__one-third">
            <img class="image-shrink--60" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/zebra--worklists.png" alt="Screenshot of MEDITECH software on ZEBRA phone">
        </div>
    </div>
</div>
<!-- End of Block 10  -->

<!-- Call to Action -->
<div class="container" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/blue-boxes-pattern.jpg);">
    <div class="container__centered text--white" style="text-align: center;">
       
        <?php cta_text($cta); ?>

        <div class="center" style="margin-top:2em;">
          <?php hubspot_button($cta_code, "Register For The 2019 Nurse Forum"); ?>
        </div>
        
        <div style="margin-top:1em;">
          <?php print $share_link_buttons; ?>
        </div>  
        
    </div>
</div>
<!-- End Call to Action -->
