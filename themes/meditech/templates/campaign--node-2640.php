<!-- START campaign--node-2640.php HOME CARE campaign -->
<?php // This template is set up to control the display of the Home Care 2018 content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
  .button-vertical-adjustment { margin-top: 6em; }
  @media (max-width: 800px) {
      .button-vertical-adjustment { margin-top: 1em; }
  }
  .auto-margins--left { max-width: 50em; margin-right: auto; }
  .floating-quote-box { margin-bottom: 0em !important; }
  .content__callout__content { background-color: #E6E9EE; padding-left: 0; padding-right: 6%; }
  @media all and (max-width: 50em) {
      .content__callout__content { padding-left: 6%; color: #3e4545; padding-top: 0em; }
      .video { max-width: 100%; }
  }
  .video--shadow { border-radius: 6px; box-shadow: 13px 13px 40px rgba(0, 0, 0, 0.3); }
  .transparent-overlay--white { padding: 1em; background-color: rgba(255, 255, 255, 0.6); min-height: 5em; }
  .transparent-overlay--black { padding: 1em; background-color: rgba(62, 69, 69, 0.6); min-height: 5em; }
  .transparent-overlay--blue { padding: 1em; background-color: rgba(0, 117, 168, 0.6); min-height: 5em; }
</style>

<!-- Block 1 Hero -->

<div class="container background--cover  hide__bg-image--mobile" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Female-dr-with-stethoscope-whitebg.jpg); background-color:#D7D1c4;">
  <div class="container__centered">
    <div class="container__one-half">
      &nbsp;
    </div>
    <div class="container__one-half text--black-coconut">
      <h1 class="text--black-coconut">
        See the value of an integrated Home Care and Hospice solution.
      </h1>
      <p>
        Support patients through every stage of life, using MEDITECH's integrated Home Care solution with the Expanse EHR. You'll be able to keep patients safe and well-cared for outside traditional acute settings &mdash; using home health, telehealth, and hospice features to give patients a personalized experience from the comfort of home.
      </p>

      <div class="center" style="margin-top:2em;">
        <?php hubspot_button($cta_code, "Sign Up For Home Care Optimization Symposium"); ?>
      </div>

    </div>
  </div>
</div>

<!-- Close Block 1 -->

<!-- Block 2 -->
<div class="container background--cover text--white hide__bg-image--mobile" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Female-dr-with-Stethoscope-and-blue-wall.jpg); background-color:#0b4a56;">
  <div class="container__centered">
    <div class="container no-pad">
      <div class="container__one-half">
        <h2 class="text--white">True integration bridges gaps in care.</h2>
        <p class="text--white">Prevent costly readmissions and ED visits with the power of a single, integrated EHR. Expanse helps homecare agencies and hospitals to closely monitor recently-discharged and/or chronically ill patients, and easily exchange information between entities for streamlined <a href="https://ehr.meditech.com/ehr-solutions/bridging-the-gaps-in-care"> transitions </a> and improved care consistency. </p>
      </div>
    </div>
  </div>
</div>
<!--Close Block 2 -->

<!-- Block 3 VIDEO-->

<div class="content__callout border-none" style="background-color: #E6E9EE !important;">
  <div class="content__callout__media">
    <div class="content__callout__image-wrapper" style="padding:3em !important;">
      <div class="video js__video video--shadow" data-video-id="288730125">
        <figure class="video__overlay">
          <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/video-overlay-Home-Care-2018-video1.jpg" alt="Kalispell Regional">
        </figure>
        <a class="video__play-btn" href="https://vimeo.com/288730125"></a>
        <div class="video__container"></div>
      </div>
    </div>
  </div>
  <div class="content__callout__content">
    <div class="content__callout__body">
      <div class="content__callout__body__text">
        <h2>
          Experience a mobile solution that moves with you.
        </h2>
        <p>
          Using MEDITECH's <a href="https://ehr.meditech.com/ehr-solutions/ehr-mobility"> mobile </a> home care solution, aides and field staff can access their schedules and view patient charts on the move. The easy-to-use solution is accessible via standard web browser on any smart device. Aides can generate more accurate plan-of-care documentation using the solution's structured responses, integrity checks, and simplified documentation tasks.
        </p>
      </div>
    </div>
  </div>
</div>

<!-- End Block 3 VIDEO  transparent-overlay--white-->

<!-- Block 4 -->
<div class="container background--cover  hide__bg-image--mobile" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Youngman-taking-blood-pressure.jpg); background-color:#72838a;">
  <div class="container__centered">
    <div class="container__one-half transparent-overlay--black">
      <h2 class="text--white">Stay in touch with telehealth.</h2>
      <p class="text--white">MEDITECH's telehealth technology keeps providers, patients, and caregivers connected, no matter the distance. Integration of patient-captured vital signs through <a href="http://www.ideallife.com">Ideal Life</a>'s smart devices alert staff of abnormal readings in real time. Home care professionals can coordinate care with primary care physicians by remotely monitoring metrics, vital signs, and any ongoing conditions that the patient may have. And patients can use virtual visits to keep appointments, even when they can't travel. </p>
    </div>
  </div>
</div>
<!--Close Block 4 -->

<!-- Block 5 -->
<div class="container background--cover text--white" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/blue-boxes-pattern.jpg);">
  <div class="container__centered">
    <div class="page__title--center">
      <h2 class="text--white">Access the tools you need to ace your next audit.</h2>
    </div>
    <h4 class="text--white" style="text-align:center;">Audits have you stressed?
      <br>With MEDITECH Home Care, agencies have the tools they need to approach each <a href="https://blog.meditech.com/how-to-survive-your-next-home-care-audit">audit</a> confidently.</h4>
    <br>
    <div>
      <div class="container__one-half center">
        <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/audit-balance-scale.png" alt="Scale" style="width:30%;" />
        <p>The clinical and financial integration of MEDITECH Home Care
          <br> creates a checks and balances system to provide accurate financials
          <br> and prevent errors in billing.</p>
      </div>
      <div class="container__one-half center">
        <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/audit-checklist-icon.png" alt="Checks" style="width:30%;" />
        <p>Our mock surveys, checklists, and comprehensive Audit Guide
          <br> are all specially designed to put agencies on the right track
          <br> to receive top marks.</p>
      </div>
    </div>
  </div>
</div>
<!--Close Block 5 -->

<!-- Block 6 -->
<div class="content__callout border-none" style="background-color: #E6E9EE !important;">
  <div class="content__callout__media">
    <div class="content__callout__image-wrapper" style="padding:3em !important;">
      <div class="video js__video video--shadow" data-video-id="287268875">
        <figure class="video__overlay">
          <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/video-overlay-Home-Care-2018-video2.jpg" alt="Dr Fletcher Patient Populations Video Covershot">
        </figure>
        <a class="video__play-btn" href="https://vimeo.com/meditechehr/review/287268875/3fc42ecb0e"></a>
        <div class="video__container"></div>
      </div>
    </div>
  </div>
  <div class="content__callout__content">
    <div class="content__callout__body">
      <div class="content__callout__body__text">
        <h2>
          Use management reports to get a bird's eye view.
        </h2>
        <p>
          See how Med Center Health's Director of Home Care uses MEDITECH Home Care management <a href="https://blog.meditech.com/using-reporting-to-become-a-home-health-top-performer">reports</a> to monitor her organization's performance on a daily basis. Clinicians and field staff are also leveraging MEDITECH's task-driven system to improve documentation and provide more informed patient care.
        </p>
      </div>
    </div>
  </div>
</div>
<!--Close Block 6 -->



<div class="container background--cover hide__bg-image--mobile" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Patient-with-family-member-holding-hands.jpg);  background-color:#72838a;">
  <div class="container__centered">

    <div class="page__title--center">
      <h2 class="text--white">Extend compassionate end-of-life care to patients and families.</h2>
      <p class="text--white">MEDITECH's integrated Hospice solution supports patients, families, and hospice staff throughout a sensitive time. While patient care stays consistent, the solution can be customized to reflect the wishes of each patient and family.</p>

    </div>

    <div class="container center no-pad--bottom">

      <div class="container__one-third text--white" style="margin-bottom:1em;">
        <div class="transparent-overlay">
          <h4>Patients</h4>
          <p>take comfort knowing that their
          <br> wishes will be reflected
          <br> in personalized care plans.</p>
        </div>
      </div>

      <div class="container__one-third text--white" style="margin-bottom:1em;">
        <div class="transparent-overlay">
          <h4>Providers</h4>
          <p>can use the solution to handle complex
          <br> hospice admissions, schedule IDT
          <br> meetings, and create customizable
          <br> care plans.</p>
        </div>
      </div>

      <div class="container__one-third text--white">
        <div class="transparent-overlay">
          <h4>Families</h4>
          <p>feel supported with access to bereavement care plans
          <br> that facilitate activities and mailings.
          </p>
        </div>
      </div>

    </div>

  </div>
</div>

<!-- Block 8 -->
<div class="container" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/blue-boxes-pattern.jpg);">
  <div class="container__centered center text--white">

    <?php cta_text($cta); ?>

    <div class="center" style="margin-top:2em;">
      <?php hubspot_button($cta_code, "Sign Up For Home Care Optimization Symposium"); ?>
    </div>
    
    <div style="margin-top:1em;">
      <?php print $share_link_buttons; ?>
    </div>  
    
  </div>
</div>
<!-- Close Block 8 -->
<!-- END campaign--node-2640.php -->
