<?php 
  // This template is set up to control the display of the 'ehr solutions inner' content type 
  $url = $GLOBALS['base_url']; // grabs the site url
?>
<!-- start node--ehr-solutions-inner.tpl.php template -->

  <?php
    // check node's taxonomy terms...
    $tags = field_view_field('node', $node, 'field_ehr_solutions_section'); 
    // 'field_ehr_solutions_section' is the machine name of the field in the content type that contains the taxonomy
    foreach($tags['#items'] as $tag){
      $term = taxonomy_term_load($tag['tid']);
      $ehr_section_term = $term->name;
    }

    // set variables based off taxonomy term...
    switch($ehr_section_term){
        
      case 'Hospitals & Health Systems':
        $ehr_section = 'Hospitals & Health Systems';
        $background_image = 'green-horizontal-triangles.png';
        $side_nav = 'menu-ehr-solutions-health-system';
        $background_class = '';
        break;
        
      case 'Post-Acute Services':
        $ehr_section = 'Post-Acute Services';
        $background_image = 'green-horizontal-triangles.png';
        $side_nav = 'menu-ehr-solutions-post-acute';
        $background_class = '';
        break;
        
      case 'Organizational Goals':
        $ehr_section = 'Organizational Goals';
        $background_image = 'green-horizontal-triangles.png';
        $side_nav = 'menu-ehr-solutions-goals';
        $background_class = '';
        break;
        
      case 'None of the Above': // main pages
        $ehr_section = 'Other Care Settings';
        $background_class = 'background--cover';
        break;
        
    }
  
    // set background images for specific main (None of the Above) pages...
    switch($node->nid){
      case 1504:
        $background_image = 'nurses-hall-darkened.jpg';
        break;
      case 1505:
        $background_image = 'nurse-pushing-wheelchair-behind-bed.jpg';
        break;
      case 147:
        $background_image = 'patient-in-bed-blurred.jpg';
        break;
      case 1515:
        $background_image = 'surgery-room-darkened.jpg';
        break;
      default:
        $background_image = 'green-horizontal-triangles.png';
        break;
    }

  ?>

  <!-- HERO section -->
  <div class="container <?php print $background_class; ?>" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/ehr-solutions/<?php print $background_image; ?>);">
    <div class="container__centered">
      <h1 class="text--white text-shadow--black js__seo-tool__title"><?php print $title; ?></h1>
      <h2 class="text--white text-shadow--black js__seo-tool__body-content"><?php print render($content['field_subtitle']); ?></h2>
    </div>
  </div>
  <!-- End of HERO section -->

  <section class="container__centered">

    <div class="container__two-thirds">
      <div class="js__seo-tool__body-content">
        <?php print render($content['field_body']); // main content ?>
      </div><!-- End .js__seo-tool__body-content -->
      <?php // SEO tool for internal use...
        if(node_access('update',$node)){
          print '<!-- SEO Tool is added to this div -->';
          print '<div class="container no-pad--top js__seo-tool"></div>';
        } 
      ?>
    </div>

    <aside class="container__one-third panel">

      <?php 
        // show section side nav unless this is a main page...
        if($ehr_section_term != 'None of the Above'){
      ?>
          <h3><?php print $ehr_section; ?></h3>
          <div class="sidebar__nav solutions_sidebar_gae">
            <?php
              // adds 'ehr solutions health system' menu block...
              $ehr2Block = module_invoke('menu', 'block_view', $side_nav);
              print render($ehr2Block['content']); 
            ?>
          </div>
      <?php
        }
      ?>
      
      <h3>Other Care Settings</h3>
      <div class="sidebar__nav solutions_sidebar_gae">
        <?php
          // adds 'ehr solutions care settings' menu block...
          $ehr2Block = module_invoke('menu', 'block_view', 'menu-ehr-solutions-care-settings');
          print render($ehr2Block['content']); 
        ?>
      </div>
         
      <?php 
        // show section side nav unless this is a main page...
        if($ehr_section_term != 'Organizational Goals'){
      ?>
          <h3>Organizational Goals</h3>
          <div class="sidebar__nav solutions_sidebar_gae">
            <ul class="menu">
              <li><a href="<?php print $url; ?>/ehr-solutions/organizational-goals">See what you can achieve with MEDITECH's EHR software.</a></li>
            </ul>
          </div>
      <?php
        }
      ?>         
          
      <?php 
        // show Web Amb ad if this is a main page...
        if($ehr_section_term == 'None of the Above'){
      ?>
          <div class="solutions_sidebar_gae">
            <h3>Want a closer look at MEDITECH Ambulatory?</h3>
            <img src="<?php echo $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-WebAmb--homescreen_small.png" alt="MEDITECH Ambulatory homescreen">
            <a href="<?php echo $url; ?>/ehr-solutions/ambulatory" class="btn--orange full-width">See Our Solution</a>
          </div>
      <?php
        }
      ?>     
      
    </aside>
    
  </section>
  
<!-- end node--ehr-solutions-inner.tpl.php template -->