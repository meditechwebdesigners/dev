<!-- start views-view-fields--news-main-page-us--block.tpl.php template -->
<?php 
// This template is for each row of the Views block: NEWS-MAIN-PAGE-US BLOCK ....................... 
?>
<article>

  <h3><a class="news_main_link_gae" href="<?php print $fields['path']->content; ?>"><?php print $fields['title']->content; ?></a></h3>
  <div class="inline__text__wrapper">
    <?php $pub_date = $fields['published_at']->content; ?>
    <p><span class="snippet__card__text--callout"><?php print $fields['published_at']->content; ?></span>&nbsp;&mdash;&nbsp;</p>
    <?php print $fields['field_summary']->content; ?>
  </div>

  <ul class="news__article__filters tag_link_news_main_gae">
    <?php print $fields['term_node_tid']->content; ?>
  </ul>
  
  <hr>

</article>
<!-- end views-view-fields--news-main-page-us--block.tpl.php template -->