<!-- START campaign--node-2342.php -->
<?php // This template is set up to control the display of the MAAS content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>
<style>
    .bg--overlay { background-color: rgba(0,0,0,0.4); }
    .bg--black-blue-gradient {
    background: #143346;
    background: -webkit-radial-gradient(circle, #143346, #000713);
    background: -o-radial-gradient(circle, #143346, #000713);
    background: -moz-radial-gradient(circle, #143346, #000713);
    background-image: radial-gradient(circle, #143346, #000713);
    color: #fff;
}
</style>

<!-- Hero -->
<div class="container no-pad--bottom bg--black-blue-gradient">
    <div class="container__centered">
        <div class="container__one-half text--white" style="padding: 2em 1em;">
            <h1>With MaaS, any community health system can expand their view of technology.</h1>
            <p>Take your efficiency to the next level with MEDITECH as a Service (MaaS), the cost-effective monthly subscription to Expanse. MaaS empowers community health systems of all sizes to get up and running with an innovative, web-based EHR that allows you to focus on what you do best: caring for your community.</p>
            <div style="text-align:center;">
                <?php 
        // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
        if( $cta->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
          $cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
        ?>
                <div class="button--hubspot">
                    <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $cta_code; ?>"><span class="hs-cta-node hs-cta-<?php print $cta_code; ?>" id="hs-cta-<?php print $cta_code; ?>">
                            <!--[if lte IE 8]>
                            <div id="hs-cta-ie-element"></div>
                            <![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $cta_code; ?>"><img class="hs-cta-img" id="hs-cta-img-<?php print $cta_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $cta_code; ?>.png" alt="button" /></a></span>
                        <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
                        <script type="text/javascript">
                            hbspt.cta.load(2897117, '<?php print $cta_code; ?>', {});

                        </script>
                    </span>
                    <!-- end HubSpot Call-to-Action Code -->
                </div>
                <?php } ?>
            </div>
        </div>
        <div class="container__one-half no-pad--bottom">
            <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/hand-holding-tablet-with-cloud-floating-above.png" alt="Hand holding tablet with cloud icon floating above">
        </div>
    </div>
</div>
<!-- End of Hero -->

<!-- Block 2 -->
<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/skydiver-parachuting-over-moutains.jpg);">
    <div class="container__centered">
        <div class="page__title--center auto-margins">
            <h2>Low stress and no sacrifices: welcome to the <i>full</i> Expanse.</h2>
            <p>
                Experience all the benefits of a powerful enterprise health record, without the challenges of procuring, deploying, and managing a modern healthcare information system. Expanse invites you to see healthcare through a whole new lens with mobile <a href="https://blog.meditech.com/using-the-cloud-to-untether-medicine-from-wired-tech">tools that untether your care</a> and help you navigate the digital frontier with confidence.
            </p>
            <div class="container left transparent-overlay text--white">
                <div class="container__one-half">
                    <p>
                        Designed to support the delivery of quality care across every community and care setting, MaaS is loaded with expert-based content and intuitive mobile solutions for:
                    </p>
                    <ul>
                        <li>Physicians and nurses</li>
                        <li>Interoperability </li>
                        <li>Patient access</li>
                        <li>Revenue cycle management</li>
                        <li>Clinical decision support.</li>
                    </ul>
                </div>
                <div class="container__one-half center">
                    <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/MaaS-circle-graphic-with-icons.png" alt="MEDITECH as a Service graphic">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Block 2 -->

<!-- Block 3 -->
<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/stopwatch-on-dark-rough-background.jpg);">
    <div class="container__centered">
        <div class="container__one-half">&nbsp;</div>
        <div class="container__one-half">
            <div class="page__title transparent-overlay text--white">
                <h2>Get up and running with an expedited go-LIVE.</h2>
                <p>By using evidence-based best practices and streamlined implementation techniques, MEDITECH will help your organization go LIVE with the latest version of MEDITECH Expanse in an expedited time frame. Here’s how we make it happen:</p>
                <ul>
                    <li>MaaS is delivered as a consolidated agreement, including software licenses, implementation, and service for MEDITECH and third-party vendors</li>
                    <li>We build your system and set up your cloud on our time, not yours</li>
                    <li>Data center is hosted remotely, so there’s less strain on your technical resources.</li>
                </ul>
                <div class="container__centered" style="text-align: center;">
                    <?php 
          // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
          if( $cta->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
            $cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
          ?>
                    <div class="button--hubspot">
                        <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $cta_code; ?>"><span class="hs-cta-node hs-cta-<?php print $cta_code; ?>" id="hs-cta-<?php print $cta_code; ?>">
                                <!--[if lte IE 8]>
                            <div id="hs-cta-ie-element"></div>
                            <![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $cta_code; ?>"><img class="hs-cta-img" id="hs-cta-img-<?php print $cta_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $cta_code; ?>.png" alt="button" /></a></span>
                            <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
                            <script type="text/javascript">
                                hbspt.cta.load(2897117, '<?php print $cta_code; ?>', {});

                            </script>
                        </span>
                        <!-- end HubSpot Call-to-Action Code -->
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Block 3 -->

<!-- Block 4 -->
<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/mountain-range-with-sunset.jpg);">
    <div class="container__centered">
        <div class="container__two-thirds">
            <div class="page__title transparent-overlay text--white">
                <h2>Secure, sustainable services at a predictable cost.</h2>
                <p>
                    MaaS is a true <a href="https://ehr.meditech.com/news/meditech-introduces-maas-a-cloud-based-ehr-subscription">Software-as-a-Service (SaaS) model</a>. MEDITECH Expanse is operated in a centrally hosted, private cloud infrastructure with high levels of physical and network security.
                </p>
                <p>
                    We take care of your EHR, so you can take care of your patients.
                </p>
                <ul>
                    <li>Minimal capital outlay</li>
                    <li>Cost-effective monthly subscription</li>
                    <li>Simple contracting for software, appliances, data center operation, content, and services</li>
                    <li>Best practices for performance monitoring, technology refreshes, updates, and backups</li>
                    <li>System maintenance and network security handling by experts</li>
                    <li>High availability and disaster recovery</li>
                    <li>Network security, including intrusion detection.</li>
                </ul>
            </div>
        </div>
        <div class="container__one-third">&nbsp;</div>
    </div>
</div>
<!-- End Block 4 -->

<!-- Block 5 -->
<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/white-x-texture.jpg);">
    <article class="container__centered">
        <figure class="container__one-fourth center">
            <img class="quote__content__img" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Charlie-Hatfield.png" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/quote.svg';this.onerror=null;" alt="quote bubble graphic">
        </figure>
        <div class="container__three-fourths">
            <div class="quote__content__text text--large">
                <p>
                    Williamson Memorial Hospital decided to partner with MEDITECH and use MaaS because it allows all of our hospital portals and systems to be stored in one central location. Every department at Williamson Memorial will have access to each patient and be able to look up the necessary files in any situation. With MaaS being operated by MEDITECH, it will take much of the stress off of our IT Department.
                </p>
            </div>
            <p class="text--large no-margin--bottom">
                Charlie Hatfield, CEO
            </p>
            <p>
                Williamson Memorial Hospital
            </p>
        </div>
    </article>
</div>
<!-- End of Block 5 -->

<!-- Block 6 -->
<div class="container bg--blue-gradient">
    <div class="container__centered">
        <div class="page__title--center text--white">
            <h2>Who else is in the MaaS cloud?</h2>

            <p>
                Serving the technological needs of an enterprise takes more than one solution. MaaS incorporates features from multiple third-party vendors.
            </p>
            <div class="container__centered">
                <div class="container__one-third" style="padding: 1em;"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/MaaS--cloud-logos-Access.png" alt=""></div>
                <div class="container__one-third" style="padding: 1em;"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/MaaS--cloud-logos-First-Databank.png" alt=""></div>
                <div class="container__one-third" style="padding: 1em;"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/MaaS--cloud-logos-Forward-Advantage.png" alt=""></div>
            </div>
            <div class="container__centered">
                <div class="container__one-third" style="padding: 1em;"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/MaaS--cloud-logos-Intelligent-Medical-Orders.png" alt=""></div>
                <div class="container__one-third" style="padding: 1em;"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/MaaS--cloud-logos-Stedmans.png" alt=""></div>
                <div class="container__one-third" style="padding: 1em;"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/MaaS--cloud-logos-Truven-Health-Analytics.png" alt=""></div>
            </div>
            <div class="container__centered center">
                <div style="padding: 1em;"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/MaaS--cloud-logos-Zynx-Health.png" alt=""></div>
            </div>
        </div>
    </div>
</div>
<!-- End Block 6 -->

<!--Block 7 Quote -->
<div class="container background--cover no-pad" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/quote-bg--starry-northern-lights.jpg);">
    <div class="container bg--overlay">
        <article class="container__centered text--white">
            <figure class="container__one-fourth center">
                <img class="quote__content__img" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/HelenWaters.png" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/quote.png';this.onerror=null;">
            </figure>
            <div class="container__three-fourths">
                <div class="quote__content__text text--large">
                    <p>
                        "MaaS represents a <a href="https://blog.meditech.com/obtaining-an-ehr-just-got-easier">critical step</a> in driving EHR access and adoption for care organizations of all sizes. Reducing the complexity and cost of procuring and maintaining an EHR will allow these hospitals to focus their resources on patient care, productivity, and meeting regulations."
                    </p>
                </div>
                <p class="text--large no-margin--bottom">
                    Helen Waters
                </p>
                <p>
                    Executive Vice President, MEDITECH
                </p>
                <p>
                    Read more from our Executive Vice President on the <a href="https://blog.meditech.com/obtaining-an-ehr-just-got-easier">MEDITECH Blog</a>.
                </p>
            </div>
        </article>
    </div>
</div>
<!--End of Block 7 -->

<!-- Block 8 - CTA Block -->
<div class="container bg--black-coconut" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/blue-boxes-pattern.jpg);">
    <div class="container__centered" style="text-align: center;">
        <h2 class="text--white">
            <?php print $cta->field_header_1['und'][0]['value']; ?>
        </h2>
        <?php 
    // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
    if( $cta->field_long_text_1['und'][0]['value'] != '' ){
    ?>
        <div>
            <?php print $cta->field_long_text_1['und'][0]['value']; ?>
        </div>
        <?php } ?>

        <?php 
    // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
    if( $cta->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
      $cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
    ?>
        <div class="button--hubspot">
            <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $cta_code; ?>"><span class="hs-cta-node hs-cta-<?php print $cta_code; ?>" id="hs-cta-<?php print $cta_code; ?>">
                    <!--[if lte IE 8]>
                            <div id="hs-cta-ie-element"></div>
                            <![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $cta_code; ?>"><img class="hs-cta-img" id="hs-cta-img-<?php print $cta_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $cta_code; ?>.png" alt="button" /></a></span>
                <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
                <script type="text/javascript">
                    hbspt.cta.load(2897117, '<?php print $cta_code; ?>', {});

                </script>
            </span>
            <!-- end HubSpot Call-to-Action Code -->
        </div>
        <?php } ?>

        <div style="margin-top:1em;">
            <?php print $share_link_buttons; ?>
        </div>
    </div>
</div>
<!-- End Block 8 -->

<!-- END campaign--node-2342.php -->
