<?php 
if( user_is_logged_in() === true ){
  global $user;
  $userID = $user->uid;
  if( $userID == 105 || $userID == 1770 || $userID == 109 || $userID == 108 || $userID == 3473 ){
?>

<!-- START campaign--node-2894.php -->
<?php // This template is set up to control the display of the campaign APPLE HEALTH

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');
$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>


<style>
  .text--small { font-size: .75em; }
  .big { font-size: 2em; font-weight: bold; }
  .squish-list li { margin-bottom: .5em; }
  .floating-container { padding: 8em 0; position: relative; }
  .floating-header { margin-top: 5em; }
  .floating { position: absolute; top: -70px; width:100%; }
  .floating img { width: 400px; }
  .floating-list { margin: 2em 0 0 20%; }
  @media all and (max-width: 50em){ /* 800px */
    .hide { display: none; }
    .floating-container { padding: 2em 0; position: inherit; }
    .floating { position: inherit; top: auto; width: 100%; }
    .floating-header { margin-top: 0; }
    .floating img { max-width: 300px; position: relative; top: 0; left: 0; }
    .floating-list { margin: 0 0 0 35%; }
    .show-on-mobile { display: block; }
  }
  
  .no-borders, .no-borders th, .no-borders td { border: none; }
  .padding, .padding th, .padding td,
  .padding tbody:last-child tr:last-child>td:first-child, .padding tbody:last-child tr:last-child>td:last-child { padding: 1em; }
  
  table tr:nth-child(even), table tbody, table tr:nth-child(even), table tbody { background-color: inherit; }

</style>


<!-- Block 1 -->
<div class="container no-pad background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/woman-on-train-with-iphone.jpg);">
  <div class="container__centered bg-overlay--white">
    <div class="container__one-half" style="margin:6em 1em;">
      <h1 style="display:none;">MEDITECH health information through Health Records on iPhone<sup>&reg;</sup></h1>
      <h2>Empower Your Community with Health Records on iPhone<sup>&reg;</sup></h2>

      <p>MEDITECH is empowering consumers with access to their health information through Health Records on iPhone. The feature is running iOS 11.3 or later, the Health app is a secure option for consumers to take their health information with them, wherever they go. It's convenient, simple to use, and <a href="https://ehr.meditech.com/ehr-solutions/patient-engagement">helps people better understand their health</a>.</p>
    </div>
  </div>
</div>
<!-- End of Block 1 -->


<!-- Block 2 -->
<div class="container">
  <div class="container__centered">
    <div class="container__one-third">
      <h2>One Record of Care</h2>
      <p>Patients' records are aggregated across participating organizations and EHRs, simplifying access to, and understanding of, their health information.</p>
    </div>
    <div class="container__two-thirds center">
      <div class="container__one-fifth"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/icon-in-circle-on-stick--hospital.png" alt="h icon" /><br><strong>Hospitals</strong></div>
      <div class="container__one-fifth"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/icon-in-circle-on-stick--medical-bag.png" alt="medical bag icon" /><br><strong>Physician Practices</strong></div>
      <div class="container__one-fifth"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/icon-in-circle-on-stick--stethescope.png" alt="stethescope icon" /><br><strong>Clinics</strong></div>
      <div class="container__one-fifth"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/icon-in-circle-on-stick--sign.png" alt="street sign icon" /><br><strong>Out-of-Network Providers</strong></div>
      <div class="container__one-fifth"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/icon-in-circle-on-stick--watch-pulse.png" alt="pulse symbol on a watch" /><br><strong>Patient-Generated Health Data</strong> (Fitness Devices)</div>
    </div>
  </div>
</div>
<!-- End of Block 2 -->


<!-- Block 3 -->
<div class="container bg--emerald" style="padding:6em 0;">
  <div class="container__centered text--white">
   
    <h2>By the Numbers</h2>
    
    <div style="float:left;">
      <div class="container__one-third" style="padding:1em;">
        <p><span class="big">58%</span> of patients have tried to compile their own complete medical history, which is both inefficient and risky <sup>1</sup></p>
      </div>
      <div class="container__one-third" style="padding:1em;">
        <p><span class="big">85%</span> of patients using online medical records deemed them useful for monitoring their health <sup>2</sup></p>
      </div>
      <div class="container__one-third" style="padding:1em;">
        <p><span class="big">90%</span> of patients said access to Apple Health Records enhanced their understanding of their own health and communications with clinicians and family caregivers. <sup>3</sup></p>
      </div>
    </div>

    <div class="container__centered" style="float:left;">
      <ol class="text--small squish-list">
        <li><a href="https://www.slideshare.net/surescripts/2016-connected-care-and-the-patient-experience-70073294" target="_blank">"2016 Connected Care and the Patient Experience survey,"</a> Surescripts, Dec 12, 2016, accessed April 8, 2019.</li>
        <li>Vaishali Patel, MPH PhD & Christian Johnson, MPH, <a href="https://www.healthit.gov/sites/default/files/page/2018-03/HINTS-2017-Consumer-Data-Brief-3.21.18.pdf" target="_blank">"Individuals' use of online medical records and technology for health needs"</a>, ONC Data Brief, No. 40, April, 2018, accessed April 8, 2019.</li>
        <li>Julie Spitzer, <a href="https://www.beckershospitalreview.com/healthcare-information-technology/uc-san-diego-patients-like-apple-health-records-initial-survey-finds.html" target="_blank">"UC San Diego patients like Apple health records, initial survey finds,"</a> Becker's Health IT & CIO Report, January 14, 2019, accessed April 8, 2019.</li>
      </ol>
    </div>
    
  </div>
</div>
<!-- End of Block 3 -->


<!-- Block 4 -->
<div class="container floating-container" style="background-color:#eee;">
  <div class="container__centered">
   
    <div class="container__one-third center" style="padding:1em;">
      <h2 class="floating-header">What's Available</h2>
    </div>
    
    <div class="container__one-third hide" style="padding:1em;">
      &nbsp;
    </div>
    
    <div class="container__one-third" style="padding:1em;">
      <ul class="squish-list floating-list">
        <li>Medications</li>
        <li>Allergies</li>
        <li>Vital Signs</li>
        <li>Procedures</li>
        <li>Lab Results</li>
        <li>Conditions</li>
        <li>Immunizations</li>
      </ul>
    </div>

  </div>
  <div class="floating center">
    <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/iPhoneX_AllRecords_PR-SCREEN.png" alt="iPhone with screen shot of Apple's Apple Health app">
  </div>
</div>
<!-- End of Block 4 -->


<!-- Block 5 -->
<div class="container">

  <div class="container__centered">
   
    <table class="hide--mobile no-hover no-borders padding">
      <thead>
         <tr>
          <th style="vertical-align:bottom;"><h2 class="no-margin">Key Benefits</h2></th>
          <th class="center"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/icon--devices.png" alt="" /><br>Improve Engagement</th>
          <th class="center"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/icon--laptop-arrow-up.png" alt="" /><br>Drive Portal Adoption</th>
          <th class="center"><img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/icon--back-to-back-puzzle-heads.png" alt="" /><br>Fill in the Gaps</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td style="border-top: 1px solid #bbb; border-bottom: 1px solid #bbb;"><h3>Benefit to Consumers</h3></td>
          <td style="border-top: 1px solid #bbb; border-bottom: 1px solid #bbb;"><p>Many people already use a health or fitness app on iPhone.</p></td>
          <td style="border-top: 1px solid #bbb; border-bottom: 1px solid #bbb;"><p>They can enroll using the same credentials as MEDITECH's Patient and Consumer Health Portal.</p></td>
          <td style="border-top: 1px solid #bbb; border-bottom: 1px solid #bbb;"><p>Give them the power to <a href="https://ehr.meditech.com/ehr-solutions/meditech-interoperability">fill in information gaps</a> by presenting providers with a fuller health record, right from iPhone.</p></td>
        </tr>
        <tr>
          <td><h3>Benefit to Your Organization</h3></td>
          <td><p>People are more prone to review their records when they have familiar, convenient <a href="https://ehr.meditech.com/ehr-solutions/ehr-mobility">mobile access</a>.</p></td>
          <td><p>Raise awareness of other services available via the portal, such as online scheduling and bill pay.</p></td>
          <td><p>More comprehensive data gives physicians a fuller health history, especially for patients who visit multiple providers.</p></td>
        </tr>
      </tbody>
    </table>
      
    <div class="show-on-mobile center" style="width: 60%; margin: 1em 20%;">
      <h2 style="margin-bottom:2em;">Key Benefits</h2>
          
      <h3 style="margin-bottom:1em;">Benefit to Consumers</h3>
      <p><strong>Improve Engagement</strong></p>
      <p>Many people already use a health or fitness app on iPhone.</p>
      <p><strong>Drive Portal Adoption</strong></p>
      <p>They can enroll using the same credentials as MEDITECH's Patient and Consumer Health Portal.</p>
      <p><strong>Fill in the Gaps</strong></p>
      <p>Give them the power to fill in information gaps by presenting providers with a fuller health record, right from iPhone.</p>
          
      <h3 style="margin-top: 2em; margin-bottom:1em;">Benefit to Your Organization</h3>
      <p><strong>Improve Engagement</strong></p>
      <p>People are more prone to review their records when they have familiar, convenient <a href="https://ehr.meditech.com/ehr-solutions/ehr-mobility">mobile access</a>.</p>
      <p><strong>Drive Portal Adoption</strong></p>
      <p>Raise awareness of other services available via the portal, such as online scheduling and bill pay.</p>
      <p><strong>Fill in the Gaps</strong></p>
      <p>More comprehensive data gives physicians a fuller health history, especially for patients who visit multiple providers.</p>

    </div>

  </div>

</div>
<!-- End of Block 5 -->


<!-- Block 6 -->
<div class="container background--cover hide__bg-image--mobile" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/girl-in-jean-jacket-on-smart-phone.jpg); background-color: #c7ced4;">
  <div class="container__centered">
    <div class="container__two-thirds">

      <h2>Convenience, Without the Risk</h2>

      <div class="container">
        <div class="container__one-third">
          <h3>Secure</h3>
          <p>Uses the latest industry standard FHIR&reg; APIs</p>
        </div>
        <div class="container__one-third">
          <h3>Easy to Use</h3>
          <p>Supports single sign on, syncs records, and pushes notifications out to consumers</p>
        </div>
        <div class="container__one-third">
          <h3>Cost Effective</h3>
          <p>No additional fees or added implementation expenses to deploy</p>
        </div>
      </div>

      <div class="container no-pad">
        <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Apple_Health_Badge.png" alt="Apple Health logo" style="width:200px;" />
        <p class="text--small">Apple, the Apple logo, and iPhone are trademarks of Apple Inc., registered in the U.S. and other countries.</p>
      </div>

    </div>
  </div>
</div>
<!-- End of Block 6 -->


<!-- LAST Block -->
<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/); padding:5.5em;">
  <div class="container__centered center">

    <?php cta_text($cta); ?>

    <div class="center" style="margin-top:2em;">
      <?php hubspot_button($cta_code, ""); ?>
    </div>

    <div style="margin-top:1em;">
      <?php print $share_link_buttons; ?>
    </div>

  </div>
</div>
<!-- End of LAST Block -->

<!-- END campaign--node-2894.php -->

<?php
  }
  else{
    print '<!-- CONTENT BLOCKED -->';
  }
}
?>
