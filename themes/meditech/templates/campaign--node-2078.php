<!-- START campaign--node-2078.php -->
<?php // This template is set up to control the display of the TOOLKIT / SEPSIS CAMPAIGN

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
  .quote__content__text {
    font-size: 1em;
  }

  .quote__content__name {
    font-size: 1.5em;
  }

  .container__one-third--homepage {
    padding: 1em;
  }

  .container__one-fifth {
    width: 18%;
    margin-right: 2%;
    padding: 1em 1%;
    float: left;
  }

  .container__one-fifth:last-child {
    margin-right: 0;
  }

  @media (max-width: 1000px) {
    .container__one-fifth {
      width: 100%;
      margin-right: 0;
      margin-top: 1em;
      padding: 1em 5%;
      float: left;
    }
  }


  /******************************************************

    GRID

  ******************************************************/

  .grid-item {
    border-right: 1px solid #ffffff;
    border-bottom: 1px solid #ffffff;
    background: none;
    height: 12em;
    padding: 2em 2em 1em 2em;
  }

  @media (max-width: 480px) {
    .grid-item {
      border: none !important;
    }
  }

  .grid-item--numbers {
    margin-top: 0;
  }

</style>


<div class="js__seo-tool__body-content">

  <!--Block 1-->
  <div class="container background--cover hide__bg-image--tablet" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/woman-talking-to-dr-with-tablet.jpg); background-color:#b4c1c7;">
    <div class="container__centered">
      <div class="container__two-thirds transparent-overlay" style="background-color: rgba(180, 193, 199, 0.8);">
        <h1 class="text--white">
          MEDITECH EHR Excellence Toolkits:
        </h1>
        <h2 class="text--white">
          Reduce opioid-related harm, with our Opioid Stewardship Toolkit
        </h2>
        <p class="text--black-coconut">
          Americans are more likely to die of an opioid overdose than they are in a motor vehicle accident, according to a 2019 report by the <a href="https://injuryfacts.nsc.org/all-injuries/preventable-death-overview/odds-of-dying/" target="_blank" rel="noreferrer noopener">National Safety Council.</a> Although opioids have a legitimate role in pain management, <a href="https://blog.meditech.com/thought-leader-podcast-series-addressing-the-opioid-crisis">stewardship is crucial</a> to ensure prescribing practices that minimize misuse. MEDITECH Expanse’s integrated Opioid Stewardship Toolkit supports efforts to identify high-risk patients and reduce opioid-related harm.</p>

        <div class="center" style="margin-top:2em;">
          <?php hubspot_button($cta_code, "Watch The Opioid Webinar Recording"); ?>
        </div>

      </div>
      <div class="container__one-third">
        &nbsp;
      </div>
    </div>
  </div>
  <!--End of Block 1-->


  <!-- Start New Block 2 -->
  <div class="content__callout border-none" style="background-color: #E6E9EE !important;">
    <div class="content__callout__media">
      <div class="content__callout__image-wrapper" style="padding:3em !important;">
        <div class="video js__video video--shadow" data-video-id="313253834">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dr-bagdasian-opioid-stewardship--video-overlay.jpg" alt="Diabetes Toolkit">
          </figure>
          <a class="video__play-btn" href="https://vimeo.com/meditechehr/review/313253834/7af7202cad"></a>
          <div class="video__container"></div>
        </div>
      </div>
    </div>
    <div class="content__callout__content">
      <div class="content__callout__body">
        <div class="content__callout__body__text">
          <h2>
            Aligned with all 12 CDC guidelines.
          </h2>
          <p>
            In this video, Bryan Bagdasian, MD, lead physician advisor for Standard Clinical Content at MEDITECH, explains how our latest toolkit aligns with all 12 CDC guidelines for prescribing opioids and managing chronic pain.
          </p>
        </div>
      </div>
    </div>
  </div>
  <!-- End New Block 2 -->

  <!-- Block 3 -->
  <div class="container bg--emerald">
    <div class="container__centered">
      <div class="page__title--center">
        <h2>Features that set our toolkit apart from &mdash;and above &mdash; the rest.</h2>

        <!-- OS Icon and Title
      <div class="container__centered" style="margin-bottom:2em; margin-top: 2em;">
        <figure style="text-align: center;">
          <img style="width: 85px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Opioid_Stew-icon.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Opioid_Stew-icon.png';this.onerror=null;" alt="Diabetes-icon">
        </figure>
      </div>

    <div class="container__centered">
      <h4 class="text--white center">Opioid Stewardship</h4>
        <div class="text--white center">
          <div style="width:100%; border-bottom:10px dotted#6ac4a9; height:1px; margin:2em 0;"><svg height="5" width="1060"></svg></div>
        </div>
    </div>

  -->

        <div class="container__centered">
          <h4 class="text--white container__centered">Designed using Expanse technology, MEDITECH’s new Opioid Stewardship Toolkit aligns with all
            <a href="https://www.cdc.gov/mmwr/volumes/65/rr/rr6501e1.htm" target="_blank" rel="noreferrer noopener">12 CDC recommendations</a>
            for chronic pain management.</h4>
        </div>
        <br>

        <div class="container__centered">

          <div class="container__one-half text--white centered">

            <div class="container transparent-overlay" style="background-color: rgba(4, 87, 72, 0.8); margin-bottom:15px;">
              <a href="https://ehr.meditech.com/news/meditech-integrates-drug-monitoring-within-expanse-to-improve-opioid-prescribing" target="_blank" rel="noreferrer noopener">Prescription Drug Monitoring Programs</a>
              <br><i>In-workflow access</i>
            </div>


            <div class="container transparent-overlay" style="background-color: rgba(4, 87, 72, 0.8); margin-bottom:15px">
              <a href="https://ehr.meditech.com/news/adding-epcs-to-your-meditech-system-what-you-need-to-know" target="_blank" rel="noreferrer noopener"> Electronic Prescribing for Controlled Substances</a>
              <br><i>Collaborative solution</i>
            </div>


            <div class="container transparent-overlay" style="background-color: rgba(4, 87, 72, 0.8); margin-bottom:15px">
              Opioid Risk Tool (ORT)
              <br><i>Embedded</i>
            </div>


            <div class="container transparent-overlay" style="background-color: rgba(4, 87, 72, 0.8); margin-bottom:15px">
              Chart Review Tools
              <br><i>For longitudinal tracking</i>
            </div>


            <div class="container transparent-overlay" style="background-color: rgba(4, 87, 72, 0.8); margin-bottom:15px">
              Streamlined Order Sets
              <br><i>To guide appropriate therapies</i>
            </div>

          </div>

          <div class="container__one-half text--white centered">

            <div class="container transparent-overlay" style="background-color: rgba(4, 87, 72, 0.8); margin-bottom:15px">
              Physician’s Note
              <br><i>For tracking the patient’s opioid use, side effects,etc.</i>
            </div>


            <div class="container transparent-overlay" style="background-color: rgba(4, 87, 72, 0.8); margin-bottom:15px">
              Patient Registries
              <br><i>To identify patients and monitor compliance</i>
            </div>


            <div class="container transparent-overlay" style="background-color: rgba(4, 87, 72, 0.8); margin-bottom:15px">
              Patient Education
              <br>
              <i>In departure packet, accessible via patient portal</i>
            </div>


            <div class="container transparent-overlay" style="background-color: rgba(4, 87, 72, 0.8); margin-bottom:15px">
              Opioid Treatment Agreements
              <br><i>Accessible via patient portal</i>
            </div>


            <div class="container transparent-overlay" style="background-color: rgba(4, 87, 72, 0.8); margin-bottom:15px">
              Outcomes Monitoring
              <br><i>Available in MEDITECH’s BCA solution</i>
            </div>


          </div>
        </div>

        <!--
        <div class="text--white center">
          <div style="width:100%; border-bottom:10px dotted#6ac4a9; height:1px; margin:2em 0;"><svg height="5" width="1060"></svg></div>
        </div>

      -->

      </div>
    </div>
  </div>
</div>



<!--End of Block 3 -->


<!-- Block 6 -->

<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-boxes-pattern.jpg);">
  <div class="container__centered center text--white">
    <div>

      <h2>Tackle your healthcare priorities with EHR Excellence Toolkits.</h2>
      <p>Why reinvent the wheel? These step-by-step guides help your organization get the most from our EHR.
        <br>
        Embedded in MEDITECH Expanse, toolkits are an extension of our service. They're built and maintained in collaboration
        <br>
        with staff physicians, clinicians, and customers, to get you up and running fast.</p>



      <div style="width:100%; border-bottom:10px dotted#6ac4a9; height:1px; margin:2em 0;"><svg height="5" width="1060"></svg></div>


    </div>

    <div class="page__title--center">
      <h3 style="margin-bottom: 2em;">
        Already a MEDITECH customer? Access our <a href="https://customer.meditech.com/en/d/ehrtoolkits/homepage.htm" target="_blank" rel="noreferrer noopener">Toolkit Library</a> now, including:
      </h3>

      <div class="container__one-fifth" style="margin-bottom:2em;">
        <img style="width: 100px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/opioid--toolkit-icon.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/opioid--toolkit-icon.png';this.onerror=null;" alt="Opioid-icon">
        <h3><a href="https://customer.meditech.com/en/d/ehrtoolkits/pages/expanseopioidstewardgettingstarted.htm" target="_blank" rel="noreferrer noopener">Opioid
            <br>Stewardship</a></h3>
      </div>
      <div class="container__one-fifth" style="margin-bottom:2em;">
        <img style="width: 100px;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/diabetes--toolkit-icon.svg" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/diabetes--toolkit-icon.png';this.onerror=null;" alt="Diabetes-icon">
        <h3><a href="https://customer.meditech.com/en/d/ehrtoolkits/pages/expansediabetesmanagegettingstarted.htm" target="_blank" rel="noreferrer noopener">Diabetes
            <br>Management</a></h3>
      </div>
      <div class="container__one-fifth" style="margin-bottom:2em;">
        <img style="width: 100px;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/fall-risk--toolkit-icon.svg" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/fall-risk--toolkit-icon.png';this.onerror=null;" alt="person falling icon">
        <h3><a href="https://customer.meditech.com/en/d/ehrtoolkits/pages/6xfallriskgettingstarted.htm" target="_blank" rel="noreferrer noopener">Fall Risk
            <br>Management</a></h3>
      </div>
      <div class="container__one-fifth" style="margin-bottom:2em;">
        <img style="width: 100px;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/sepsis--toolkit-icon.svg" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/sepsis--toolkit-icon.png';this.onerror=null;" alt="thermometer icon">
        <h3><a href="https://customer.meditech.com/en/d/ehrtoolkits/pages/sepsisgettingstarted.htm" target="_blank" rel="noreferrer noopener">Sepsis
            <br> Management</a></h3>
      </div>
      <div class="container__one-fifth">
        <img style="width: 100px;" src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/cauti--toolkit-icon.svg" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/cauti--toolkit-icon.png';this.onerror=null;" alt="urinary tract icon">
        <h3><a href="https://customer.meditech.com/en/d/ehrtoolkits/pages/cautigettingstarted.htm" target="_blank" rel="noreferrer noopener">CAUTI
            <br>Prevention</a></h3>
      </div>
    </div>
  </div>
</div>



<!-- Close Block 6 -->


<!-- Block 7 -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Expanse_grid.jpg);">
  <div class="container__centered center">
    <h2 class="text--black-coconut">Get READY to accelerate clinical quality improvement.</h2>
    <p class="text--black-coconut">Inherent in our <a href="https://blog.meditech.com/are-you-ready-reinvent-your-ehr-system">READY implementations</a>, toolkits help your organization accelerate eCQI for CAUTI, sepsis, fall risk, diabetes, and opioid stewardship.</p>
    <h3>Toolkit components include:</h3>
  </div>


  <div class="container__centered">
    <div class="center" style="margin-bottom:2em;">

      <div class="text--white">

        <div class="container__one-fourth">
          <div class="transparent-overlay center">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/integrated-workflow-guide--toolkit.png" alt="graphic" style="width:50%;">
            <p>Integrated Workflow Guide</p>
          </div>
        </div>

        <div class="container__one-fourth">
          <div class="transparent-overlay center">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/webinar-education--toolkit.png" alt="graphic" style="width:50%;">
            <p>On-Demand Video Education</p>
          </div>
        </div>

        <div class="container__one-fourth">
          <div class="transparent-overlay center">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/support-rules--toolkit.png" alt="graphic" style="width:50%;">
            <p>Clinical Decision Support Rules</p>
          </div>
        </div>

        <div class="container__one-fourth">
          <div class="transparent-overlay center">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/decision-log--toolkit.png" alt="graphic" style="width:50%;">
            <p>Decision Log</p>
          </div>
        </div>

      </div>
    </div>
  </div>

  <div class="container__centered">
    <div class="center" style="margin-bottom:2em;">

      <div class="text--white">

        <div class="container__one-fourth">
          <div class="transparent-overlay center">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/project-plan--toolkit.png" alt="graphic" style="width:50%;">
            <p>Setup Guidance</p>
          </div>
        </div>

        <div class="container__one-fourth">
          <div class="transparent-overlay center">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/standard-content--toolkit.png" alt="graphic" style="width:50%;">
            <p>Standard Content Inventory</p>
          </div>
        </div>

        <div class="container__one-fourth">
          <div class="transparent-overlay center">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/change-log--toolkit.png" alt="graphic" style="width:50%;">
            <p>Change Log</p>
          </div>
        </div>

        <div class="container__one-fourth">
          <div class="transparent-overlay center">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/faqs--toolkit.png" alt="graphic" style="width:50%;">
            <p>Reference Materials & FAQs</p>
          </div>
        </div>

      </div>
    </div>

  </div>
</div>
<!-- END BLOCK 7 -->

<!-- end js__seo-tool__body-content -->


<!-- Block 8 -->
<div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/blue-boxes-pattern.jpg);">
  <div class="container__centered center text--white">

    <?php cta_text($cta); ?>
   
    <div class="center" style="margin-top:2em;">
      <?php hubspot_button($cta_code, "Watch The Opioid Webinar Recording"); ?>
    </div>

    <div style="margin-top:1em;">
      <?php print $share_link_buttons; ?>
    </div>

  </div>
</div>
<!-- Close Block 8 -->


<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
<!-- END campaign--node-2078.php -->
