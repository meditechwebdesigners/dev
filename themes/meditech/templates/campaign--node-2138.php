<!-- END campaign--node-2138.php -->
<?php
$url = $GLOBALS['base_url']; // grabs the site url

// FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM...
// to use the function simply assign it to a variable
// then pass in the field collection machine name to the function
// then you'll use the variable when calling each sub-field within the template
function field_collection_data($node, $field_name){
  // get array specific to a certain field collection item...
  $field_collection = field_get_items('node', $node, $field_name);
  // get all content within that field collection...
  $fc_field_data = entity_load('field_collection_item', array($field_collection[0]['value']));
  // get field collection ID...
  $fc_field_ID = key($fc_field_data);
  // create variable and load it with field collection data...
  $var_with_data = $fc_field_data[$fc_field_ID];
  // return variable...
  return $var_with_data;
}
$button_1 = field_collection_data($node, 'field_fc_button_1');
?>

<style>
  .button-vertical-adjustment { margin-top: 10em; }
  @media (max-width: 800px) {
    .button-vertical-adjustment { margin-top: 1em; }
  }
  .auto-margins { max-width: 50em; margin-left: auto; margin-right: auto; }
  .container__with-padding { width: 100%; padding: 2em 0; float:left; }
  .horizontal-bar { width: 100%; float: left; margin-bottom: 1em; }
  .horizontal-bar-label { float: left; text-align: right; width: 15%; margin-right: 3%; font-weight: bold; }
  .horizontal-bar-container { float: left; width: 80%; }
  .horizontal-bar-graph--meditech { float: left;
    background:
      linear-gradient(225deg, transparent 10px, #006652 0) top right,
      linear-gradient(315deg, transparent 10px, #006652 0) bottom right;
    background-size: 100% 50%;
    background-repeat: no-repeat;
  }
  .horizontal-bar-graph--cerner { float: left;
    background:
      linear-gradient(225deg, transparent 10px, #00baff 0) top right,
      linear-gradient(315deg, transparent 10px, #00baff 0) bottom right;
    background-size: 100% 50%;
    background-repeat: no-repeat;
  }
  .horizontal-bar-graph--epic { float: left;
    background:
      linear-gradient(225deg, transparent 10px, #bf2525 0) top right,
      linear-gradient(315deg, transparent 10px, #bf2525 0) bottom right;
    background-size: 100% 50%;
    background-repeat: no-repeat;
  }
  .horizontal-bar-value { float: left; width: 5%; margin-left: 2%; }
  @media all and (max-width: 1050px){
    .container__one-half.half-graph { width: 100%; margin-bottom: 2em; }
  }
  .meditech-value { font-size: 2em; line-height: .8em; font-weight: bold; }
  /* inner-shadow is actually inner highlight */
  .inner-shadow {
    -moz-box-shadow:    inset 0px .8em 0px 0px rgba(255, 255, 255, 0.35);
    -webkit-box-shadow: inset 0px .8em 0px 0px rgba(255, 255, 255, 0.35);
    box-shadow:         inset 0px .8em 0px 0px rgba(255, 255, 255, 0.35);
  }
  .transparent-overlay--white { padding:1em; background-color:rgba(256,256,256,0.8); min-height:5em }
  @media all and (max-width: 940px){
    .container.hide__bg-image--tablet { padding:0; }
    .hide__bg-image--tablet .container__centered .container__one-half { width: 100%; }
  }
  .big-bold { font-size: 3em; font-weight: bold; line-height: 1em; }
  .block-bordered { padding: 1em; border: 1px solid white; min-height: 20em; margin-bottom: 1em; }
  .block-background { color: #006652; background-color: #ffb993; }
  .text--white .block-background a { color: #006652; border-bottom: 1px solid #006652; }
  .text--white .block-background a:hover { color: #333; border-bottom: 1px solid #333; }
  @media all and (max-width: 940px){
    .block-bordered { min-height: auto; }
  }
  .no-bullets { list-style: none; }
  .no-bullets li .fa { font-size: 2em; color: #006652; margin-right:.25em; }
  /* inner-shadow is actually inner highlight */
  .inner-shadow--vertical {
    -moz-box-shadow:    inset 20px 0px 0px 0px rgba(255, 255, 255, 0.35);
    -webkit-box-shadow: inset 20px 0px 0px 0px rgba(255, 255, 255, 0.35);
    box-shadow:         inset 20px 0px 0px 0px rgba(255, 255, 255, 0.35);
  }
</style>

<div class="js__seo-tool__body-content">

<!-- Block 1 -->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/physician-holding-piggy-bank.jpg);">
  <div class="container__centered">

    <div class="container__two-thirds transparent-overlay text--white">
      <h1 class="js__seo-tool__title">Test Campaign B</h1>
      <h2>An EHR for the future: valuable, viable, and sustainable.</h2>
      <p>Your EHR is more than just a one-time purchase. It's a long term commitment. How do you know you have the right system to handle healthcare challenges of both today and tomorrow, when it comes to tackling value-based care, growing your operating margin, staying independent? We've got you covered — from the bedside to the boardroom, and all points in between — and at a fiscally responsible price point.</p>
    </div>

    <div class="container__one-third center">
      <?php
      // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
      if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
        $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
      ?>
        <div class="button--hubspot button-vertical-adjustment">
          <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="Download Our Interoperability White Paper"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
        </div>
      <?php }else{ ?>
        <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange button-vertical-adjustment"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
      <?php } ?>
    </div>

  </div>
</div>
<!-- End of Block 1 -->

<!-- Block 2 -->
<div class="container background--cover hide__bg-image--tablet" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/heartbeat-graph-value-background.png);">
  <div class="container__centered">
    <div class="container__one-half">&nbsp;</div>
    <div class="container__one-half transparent-overlay--white">
       <h2>MEDITECH beats the competition on quality and value.</h2>
      <p>Some people say that to get more you have to pay more. But is that really true? <a href="https://ehr.meditech.com/news/meditech-6x-outperforms-other-ehrs-on-cms-quality-and-value-measures">A recent study by Navin, Haffty & Associates</a> used data from the CMS database as well as Meaningful Use attestation and EHR data to validate different hospital EHRs based on quality metrics, including HAC penalties, readmission penalties, and value-based purchasing outcomes. The results were telling, as MEDITECH's 6.x EHR surpassed Cerner and Epic in all categories.</p>
    </div>
  </div>
</div>

<div class="container__centered" style="margin-bottom:2em;">
  <div class="container__one-half half-graph">

    <h3 class="center" style="margin-bottom:1em;">Percent of Hospitals with Readmission Penalty</h3>

    <div class="horizontal-bar">
      <div class="horizontal-bar-label" style="line-height:.9em;">MEDITECH 6.x</div>
      <div class="horizontal-bar-container">
        <div class="horizontal-bar-graph--meditech inner-shadow corner--remove" style="width:81%;">&nbsp;</div>
        <div class="horizontal-bar-value meditech-value">81%</div>
      </div>
    </div>

    <div class="horizontal-bar">
      <div class="horizontal-bar-label">Cerner</div>
      <div class="horizontal-bar-container">
        <div class="horizontal-bar-graph--cerner inner-shadow corner--remove" style="width:85%;">&nbsp;</div>
        <div class="horizontal-bar-value">85%</div>
      </div>
    </div>

    <div class="horizontal-bar">
      <div class="horizontal-bar-label">Epic</div>
      <div class="horizontal-bar-container">
        <div class="horizontal-bar-graph--epic inner-shadow corner--remove" style="width:83%;">&nbsp;</div>
        <div class="horizontal-bar-value">83%</div>
      </div>
    </div>

  </div>

  <div class="container__one-half half-graph">

    <h3 class="center" style="margin-bottom:1em;">Percent of Hospitals with Hospital Acquired Conditions Penalty</h3>

    <div class="horizontal-bar">
      <div class="horizontal-bar-label" style="line-height:.9em;">MEDITECH 6.x</div>
      <div class="horizontal-bar-container">
        <div class="horizontal-bar-graph--meditech inner-shadow corner--remove" style="width:13%;">&nbsp;</div>
        <div class="horizontal-bar-value meditech-value">13%</div>
      </div>
    </div>

    <div class="horizontal-bar">
      <div class="horizontal-bar-label">Cerner</div>
      <div class="horizontal-bar-container">
        <div class="horizontal-bar-graph--cerner inner-shadow corner--remove" style="width:25%;">&nbsp;</div>
        <div class="horizontal-bar-value">25%</div>
      </div>
    </div>

    <div class="horizontal-bar">
      <div class="horizontal-bar-label">Epic</div>
      <div class="horizontal-bar-container">
        <div class="horizontal-bar-graph--epic inner-shadow corner--remove" style="width:29%;">&nbsp;</div>
        <div class="horizontal-bar-value">29%</div>
      </div>
    </div>

  </div>
</div>

<div class="container__with-padding" style="background-color:#fff4ee;">
  <div class="container__centered">

    <div class="container__one-half half-graph">

      <h3 class="center" style="margin-bottom:1em;">Percent of Hospitals with Positive Value-Based Purchasing Adjustment</h3>

      <div class="horizontal-bar">
        <div class="horizontal-bar-label" style="line-height:.9em;">MEDITECH 6.x</div>
        <div class="horizontal-bar-container">
          <div class="horizontal-bar-graph--meditech inner-shadow corner--remove" style="width:61%;">&nbsp;</div>
          <div class="horizontal-bar-value meditech-value">61%</div>
        </div>
      </div>

      <div class="horizontal-bar">
        <div class="horizontal-bar-label">Cerner</div>
        <div class="horizontal-bar-container">
          <div class="horizontal-bar-graph--cerner inner-shadow corner--remove" style="width:46%;">&nbsp;</div>
          <div class="horizontal-bar-value">46%</div>
        </div>
      </div>

      <div class="horizontal-bar">
        <div class="horizontal-bar-label">Epic</div>
        <div class="horizontal-bar-container">
          <div class="horizontal-bar-graph--epic inner-shadow corner--remove" style="width:60%;">&nbsp;</div>
          <div class="horizontal-bar-value">60%</div>
        </div>
      </div>

    </div>

    <div class="container__one-half half-graph">

      <h3 class="center" style="margin-bottom:1em;">Percent of Hospitals with Negative Value-Based Purchasing Adjustment</h3>

      <div class="horizontal-bar">
        <div class="horizontal-bar-label" style="line-height:.9em;">MEDITECH 6.x</div>
        <div class="horizontal-bar-container">
          <div class="horizontal-bar-graph--meditech inner-shadow corner--remove" style="width:39%;">&nbsp;</div>
          <div class="horizontal-bar-value meditech-value">39%</div>
        </div>
      </div>

      <div class="horizontal-bar">
        <div class="horizontal-bar-label">Cerner</div>
        <div class="horizontal-bar-container">
          <div class="horizontal-bar-graph--cerner inner-shadow corner--remove" style="width:53%;">&nbsp;</div>
          <div class="horizontal-bar-value">53%</div>
        </div>
      </div>

      <div class="horizontal-bar">
        <div class="horizontal-bar-label">Epic</div>
        <div class="horizontal-bar-container">
          <div class="horizontal-bar-graph--epic inner-shadow corner--remove" style="width:40%;">&nbsp;</div>
          <div class="horizontal-bar-value">40%</div>
        </div>
      </div>

    </div>
  </div>
</div>

<div class="container__centered">
  <div style="float:left; margin-top:2em;">
    <p><strong>A similar study assessed the pre-LIVE to post-LIVE impact of implementing Cerner, Epic, or MEDITECH on hospital operating margins.</strong> The study showed that hospitals implementing MEDITECH realized significant operating margin improvements, compared to those that implemented Cerner or Epic across all bed size tiers.</p>
  </div>
</div>

<div class="container__centered">
  <div class="container__one-half half-graph">

    <h3 class="center" style="margin-bottom:1em;">MEDITECH Outperformance of Cerner*</h3>

    <div class="horizontal-bar">
      <div class="horizontal-bar-label" style="width:25%;">&lt; 100 Beds</div>
      <div class="horizontal-bar-container" style="width:60%;">
        <div class="horizontal-bar-graph--meditech inner-shadow corner--remove" style="width:15.5%;">&nbsp;</div>
        <div class="horizontal-bar-value meditech-value">1.55%</div>
      </div>
    </div>

    <div class="horizontal-bar">
      <div class="horizontal-bar-label" style="width:25%;">100-250 Beds</div>
      <div class="horizontal-bar-container" style="width:60%;">
        <div class="horizontal-bar-graph--meditech inner-shadow corner--remove" style="width:19.8%;">&nbsp;</div>
        <div class="horizontal-bar-value meditech-value">1.98%</div>
      </div>
    </div>

    <div class="horizontal-bar">
      <div class="horizontal-bar-label" style="width:25%;">250+ Beds</div>
      <div class="horizontal-bar-container" style="width:60%;">
        <div class="horizontal-bar-graph--meditech inner-shadow corner--remove" style="width:20%;">&nbsp;</div>
        <div class="horizontal-bar-value meditech-value">2.00%</div>
      </div>
    </div>

  </div>

  <div class="container__one-half half-graph">

    <h3 class="center" style="margin-bottom:1em;">MEDITECH Outperformance of Epic*</h3>

    <div class="horizontal-bar">
      <div class="horizontal-bar-label" style="width:25%;">&lt; 100 Beds</div>
      <div class="horizontal-bar-container" style="width:60%;">
        <div class="horizontal-bar-graph--meditech inner-shadow corner--remove" style="width:11.9%;">&nbsp;</div>
        <div class="horizontal-bar-value meditech-value">1.19%</div>
      </div>
    </div>

    <div class="horizontal-bar">
      <div class="horizontal-bar-label" style="width:25%;">100-250 Beds</div>
      <div class="horizontal-bar-container" style="width:60%;">
        <div class="horizontal-bar-graph--meditech inner-shadow corner--remove" style="width:16.0%;">&nbsp;</div>
        <div class="horizontal-bar-value meditech-value">1.60%</div>
      </div>
    </div>

    <div class="horizontal-bar">
      <div class="horizontal-bar-label" style="width:25%;">250+ Beds</div>
      <div class="horizontal-bar-container" style="width:60%;">
        <div class="horizontal-bar-graph--meditech inner-shadow corner--remove" style="width:46.5%;">&nbsp;</div>
        <div class="horizontal-bar-value meditech-value">4.65%</div>
      </div>
    </div>

  </div>
</div>

<div class="container background--cover" style="padding-bottom:0; background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/heartbeat-graph-background-bottom.png);">
  <div class="container__centered">
    <p>* Improvement in operating margin was calculated based on the increase or decrease in operating margin from the year two years prior to the year of implementation to the year two years after the year of implementation.</p>
    <p>* For a 100-250 bed hospital with an NPR of $140m, a difference of 1.98% in operating margin would equate to an improvement of $2.772m in annual operating margin.</p>
  </div>
  <div class="center" style="margin-bottom:2em;">
    <div class="page__title__ribbon"></div>
    <h3 class="center">When it comes to better quality at a lower cost, MEDITECH leads the pack.<br> Pay less, get more.</h3>
  </div>
</div>
<!-- End Block 2 -->


<!-- Block 3 -->
<div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/green-pattern.png);">
  <div class="container__centered text--white">
    <h2 style="margin-bottom:1.5em;">The proof is in the pudding.</h2>
    <div>
      <div class="container__one-third block-bordered block-background">
        <p><span class="big-bold text--white">$10 Million Cost Savings</span><br> and 45% Mortality Rate Reduction through sepsis screening at <a href="https://ehr.meditech.com/news/meditech-helps-avera-health-reduce-sepsis-mortality-by-45-save-10-million-0">Avera Health</a> (Sioux Falls, SD)</p>
      </div>
      <div class="container__one-third block-bordered">
        <p><span class="big-bold">$2.1 million increase in charge revenue</span><br> via integrated documentation and billing at <a href="https://ehr.meditech.com/news/davies-winner-hilo-medical-center-to-adopt-meditech%E2%80%99s-web-ambulatory">Hilo Medical Center</a> (Hilo, HI)</p>
      </div>
      <div class="container__one-third block-bordered block-background">
        <p><span class="big-bold text--white">$500,000 savings on interface costs</span><br> through MEDITECH integration at <a href="https://ehr.meditech.com/news/meditech-cuts-costs-improves-productivity-at-phoebe-putney">Phoebe Putney Memorial Hospital</a> (Albany, GA)</p>
      </div>
    </div>
    <div>
      <div class="container__one-third block-bordered">
        <p><span class="big-bold">20% increase in cash flow</span><br> and 30% reduction in ED wait times at <a href="https://ehr.meditech.com/news/meditech-s-web-ehr-brings-integration-value-to-wilma-n-v-zquez-medical-center">Wilma N. Vázquez Medical Center</a> (Vega Baja, PR)</p>
      </div>
      <div class="container__one-third block-bordered block-background">
        <p><span class="big-bold text--white">Lowered A/R Days by 50%</span><br> and reduced lost revenue by 90% using Revenue Cycle Management at <a href="https://info.meditech.com/case-study-anderson-regional-cuts-ar-days-by-50-using-meditechs-6.1-revenue-cycle-solution">Anderson Regional Medical Center</a> (Meridian, MS)</p>
      </div>
      <div class="container__one-third block-bordered">
        <p><span class="big-bold">$475,000 annual cost savings</span>,<br> 13.7 percent decrease in non-emergent ED visits at <a href="https://ehr.meditech.com/news/meditech-s-ehr-steers-avera-mckennan-ed-nurse-navigator-program-to-475000-annual-savings">Avera McKennan & University Medical Center</a> (Sioux Falls, SD)</p>
      </div>
    </div>
  </div>
</div>
<!-- End Block 3 -->


<!-- Block 4 -->
<div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/diagonal-rectangles-yellow-background.png);">
  <div class="container__centered">
    <div class="container__two-thirds">
      <h2>Maintain a healthy balance for you and your patients.</h2>
      <p>Improve the health of your patients and improve your bottom line to prevent providers from being plagued by HACs, readmissions, and financial penalties. Increase patient compliance by using MEDITECH's EHR to help keep them engaged and on track.</p>
      <ul>
        <li>Reduce penalties and extended lengths of stay by using our surveillance tools to reduce HACs and other high risk conditions.</li>
        <li>Guide patients down the healthy path by proactively following up on due or overdue checkups, immunizations, mammograms, or other health maintenance items.</li>
        <li><a href="https://blog.meditech.com/how-to-keep-patients-healthy-with-your-patient-portal">Help patients stay healthier at home</a> by using telehealth to monitor their progress and prevent readmissions.</li>
      </ul>
    </div>
    <div class="container__onehird center">
      <i class="fa fa-balance-scale" aria-hidden="true" style="font-size:10em; margin-top:10%; color:#006652;"></i>
    </div>
  </div>
</div>
<!-- End Block 4 -->


<!-- Block 5 -->
<div class="container">
  <div class="container__centered">
    <h2>Keep the cost of ownership low, but quality high.</h2>
    <p>Experience a low cost of ownership like no other, and invest your savings into what matters most — the staffing and resources needed to provide the quality care and welcoming environment your patients deserve. How does MEDITECH do it?</p>

    <div>
      <div class="container__one-half">
        <ul class="no-bullets">
          <li><i class="fa fa-check-square-o" aria-hidden="true"></i> <strong>Perpetual license:</strong> Means no re-licensing of products or mandatory update fees</li>
          <li><i class="fa fa-check-square-o" aria-hidden="true"></i> <strong>Inherent integration:</strong> Reduces both the cost and man hours required to manage unnecessary interfaces</li>
          <li><i class="fa fa-check-square-o" aria-hidden="true"></i> <strong>Cost conscious implementation approach:</strong> Standard content minimizes build time; on-site training eliminates costly travel; honest, upfront pricing prevents cost overruns</li>
        </ul>
      </div>
      <div class="container__one-half">
        <ul class="no-bullets">
          <li><i class="fa fa-check-square-o" aria-hidden="true"></i> <strong>Shared patient record and documentation:</strong> Eliminates redundancies, cuts costs, increases productivity, and reduces the learning curve between settings</li>
          <li><i class="fa fa-check-square-o" aria-hidden="true"></i> <strong>All inclusive support fee:</strong> Prevents unexpected costs, with no added fees based on time of day, nature of the call, or tier-level of support team</li>
          <li><i class="fa fa-check-square-o" aria-hidden="true"></i> <strong>Native web infrastructure:</strong> Minimizes costly third-party three-tier connections and highly regimented device requirements</li>
        </ul>
      </div>
    </div>

    <div style="float:left; width:100%; border-bottom:10px dotted #ffb993; height:1px; margin:1em 0;"></div>

    <div class="container__centered center">
      <a href="https://www.beckershospitalreview.com/hospital-management-administration/will-the-healthcare-bubble-burst-19-key-perspectives.html" target="_blank">MEDITECH Executive Vice President Helen Waters weighs in on the exorbitant cost of healthcare IT and its potential impact.</a>
    </div>

  </div>
</div>
<!-- End Block 5 -->


<!-- Block 6 -->
<div class="content__callout">
  <div class="content__callout__media">
    <h2 class="content__callout__title">Better patient management generates bigger dollars.</h2>
    <div class="content__callout__image-wrapper">
      <div class="video js__video" data-video-id="200671200">
        <figure class="video__overlay">
          <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/video-overlay_dr-fletcher.jpg" alt="Video Covershot">
        </figure>
        <a class="video__play-btn" href="http://vimeo.com/200671200"></a>
        <div class="video__container">
        </div>
      </div>
    </div>
  </div>
  <div class="content__callout__content">
    <div class="content__callout__body">
      <p>With the shift to value-based care, a solid plan to manage your patient populations will also help you to maximize your earning potential. Take a proactive approach to disease management and chronic care by using MEDITECH's patient registries to identify at-risk individuals, reduce gaps in care, and reach out to those who are overdue for health maintenance.</p>
      <p><strong>Michael Fletcher, MD, VP and CMO of Hancock Regional Hospital</strong> describes the difference MEDITECH's Patient Registries can make for your clinical and financial outcomes.</p>
    </div>
  </div>
</div>
<!-- End Block 6 -->


<!-- Block 7 -->
<div class="container bg--emerald">
  <div class="container__centered">
    <div class="page__title">
      <h2>Identify new revenue opportunities.</h2>
      <p>Leverage your EHR as a means of identifying and managing new revenue opportunities, such as annual wellness visits, transitional care management, and  chronic care management billing - CPT 99490.</p>
      <p>Our Revenue Cycle solution and new chronic care management registry can <a href="https://blog.meditech.com/blog-how-to-utilize-cpt-99490-to-increase-revenue-opportunities">help you meet the documentation requirements for CPT 99490</a> and automatically drop charges to billing.</p>
    </div>
  </div>
</div>
<!-- End Block 7 -->


<!-- Block 8 -->
<div class="container">
  <div class="container__centered">
    <h2>Your data. Your way. With Business & Clinical Analytics.</h2>
    <p>Have the right information to make smart financial decisions. With MEDITECH's Business and Clinical Analytics (BCA) solution, we'll help you to connect the dots across your service lines.</p>
    <p>View "at a glance" summaries, and drill down to understand root causes of issues, target those issues, and improve outcomes. Take advantage of our more than 70 standard dashboard views, or use our Self Service Analytics to drag and drop metrics into more personalized charts and graphs.</p>
    <p>BCA puts senior management executives in the driver's seat.</p>

    <div>
      <div class="container__one-third">
        <h3>Drive Your Enterprise Success</h3>
        <p>Use executive dashboards to reveal areas for improvement, identify performance drivers, and make empowered decisions.</p>
      </div>
      <div class="container__one-third">
        <h3>Enhance the Patient and Consumer Experience</h3>
        <p>Exceed patient expectations by monitoring patient satisfaction scores, portal enrollment, and patient education. </p>
      </div>
      <div class="container__one-third">
        <h3>Ensure Financial Success and Sustainability</h3>
        <p>Optimize your revenue cycle, identify profit opportunities, and determine what quality improvements can increase reimbursement and prevent costly penalties.</p>
      </div>
    </div>

    <div style="float:left; width:100%; border-bottom:10px dotted #ffb993; height:1px; margin:1em 0;"></div>

    <div>
      <div class="container__one-third">
        <h3>Enlighten Your Providers</h3>
        <p>Use quality dashboards to assess performance at the individual, practice, and group level to identify variations in care and assure adherence to best practices.</p>
      </div>
      <div class="container__one-third">
        <h3>Envision Your Future</h3>
        <p>Identify trends to stay ahead of the curve, such as what service needs are on the rise, or what programs could pave the way for new revenue opportunities.</p>
      </div>
      <div class="container__one-third">
        <h3>Negotiate with Knowledge</h3>
        <p>Bring in third-party industry data or competitor information to see how your organization sizes up, from patient satisfaction scores to keeping patients in network.</p>
      </div>
    </div>

  </div>
</div>
<!-- End Block 8 -->


<!-- Block 9 -->
<div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/green-abstract-triangular-shapes.png);">
  <div class="container__centered text--white">
    <h2>With a healthy revenue cycle, you can thrive in a value-based world.</h2>
    <p>Retain more of your hard earned dollars through smooth revenue cycle automation. Capture all the information you need upfront, from copays to deductibles. Reduce revenue leakage by automating collection streams and charges across your EHR. Use dynamic and personalized worklists to screen receivables, revenue, cash flow, claims, billing tasks, account checks, and to prevent or manage insurance denials. And keep a close eye on your financial and quality performance by establishing benchmarks, comparing across pay periods, and targeting areas of improvement.</p>
    <figure class="center" style="margin-top:2em;">
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-financial-status-desktop--screenshot.png" alt="MEDITECH financial status desktop screenshot">
    </figure>
  </div>
</div>
<!-- End Block 9 -->


<!-- Block 10 -->
<div class="container" style="background-image:url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg">
  <div class="container__centered">
    <figure class="container__one-fourth center">
      <img class="quote__content__img" src="https://ehr.meditech.com/sites/all/themes/meditech/images/quote.svg" onerror="this.src='https://ehr.meditech.com/sites/all/themes/meditech/images/quote.png';this.onerror=null;" alt="quote bubble graphic">
    </figure>
    <div class="container__three-fourths">
      <div class="quote__content__text text--large ">
        <p>"The transparency of the data within the Financial Status Desktop has driven numerous efficiencies in our processes. We are now able to identify and react more quickly to trends — such as insurance denials — that can have a negative impact on our A/R days."</p>
      </div>
      <p class="text--large no-margin--bottom">Kevin Adams, Revenue Cycle Director</p>
      <p>Anderson Regional Medical Center</p>
    </div>
  </div>
</div>
<!-- End Block 10 -->


</div><!-- end js__seo-tool__body-content -->

<!-- Block 11 - CTA Block -->
<div class="container bg--black-coconut" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dark-grey-triangle-pattern.png);">
  <div class="container__centered">

    <div class="center">
      <h2 class="text--white"></h2>
      <p></p>

      <div class="btn-holder--content__callout no-margin--top">
        <?php
        // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
        if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
          $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
        ?>
          <div class="button--hubspot">
            <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="Download Our Interoperability White Paper"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
          </div>
        <?php }else{ ?>
          <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
        <?php } ?>
      </div>

      <div class="sharethis-centered">
        <?php
          $block = block_load('sharethis', 'sharethis_block');
          $render_array = _block_get_renderable_array(_block_render_blocks(array($block)));
          print render($render_array);
        ?>
      </div>
    </div>

  </div>
</div>
<!-- End Block 11 -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>

<!-- END campaign--node-2138.php -->