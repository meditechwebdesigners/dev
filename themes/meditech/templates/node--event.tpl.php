<?php // This template is set up to control the display of the 'Event' content type 

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$accordion_shortcode_fix = '<p style="display:none;">&nbsp;</p>'; // to prevent accordion's close link from breaking (shortcode issue)

// get event date data...
$eventDate = field_get_items('node', $node, 'field_event_date');
// get first date data...
$date1 = date_create($eventDate[0]['value']);
$day1 = date_format($date1, 'jS');
$month1 = date_format($date1, 'F');
// get second date data...
$date2 = date_create($eventDate[0]['value2']);
$day2 = date_format($date2, 'jS');
$month2 = date_format($date2, 'F');

// Get today's date... 
$today = date("m/d/Y");
// Convert dates to strings...
$todayString = strtotime($today);
$dateToCompareTo = strtotime( date_format($date1, 'm/d/Y') );
// Compare the dates (check if today is after start date)...
if($todayString >= $dateToCompareTo){
  $pastEvent = 'Past ';
}
else{
  $pastEvent = '';
}
?>
<!-- start node--event.tpl.php template -->
      

  <!-- Hero -->
  <div class="content__callout">
    <?php 
      if( !empty($content['field_event_image']) ){      
        $image = field_get_items('node', $node, 'field_event_image');
        $imageURI = $image[0]['uri']; // grab image URI
        $imageFileURL = explode('/', $imageURI); // break up URI to get filename
      }
    ?>
    <div class="content__callout__media content__callout__bg__img" style="background-image: url(<?php print $url.'/sites/default/files/images/events/'.$imageFileURL[4]; ?>);">
    </div>
    
    <div class="content__callout__content--green">
      <h1 class="js__seo-tool__title"><?php print $title; ?></h1>
      <?php
        // get event location data...
        $eventLocation = field_get_items('node', $node, 'field_location_name');
        $eventAddress = field_get_items('node', $node, 'field_location_address');
        $eventCity = field_get_items('node', $node, 'field_location_city');

        $stateTerms = field_view_field('node', $node, 'field_location_state'); 
        if(!empty($stateTerms)){
          foreach($stateTerms["#items"] as $sTerm){
            $eventState = ', '.$sTerm["taxonomy_term"]->description;
          }
        }
        else{
          $eventState = '';
        }

        $countryTerms = field_view_field('node', $node, 'field_location_country'); 
        if(!empty($countryTerms)){
          foreach($countryTerms["#items"] as $cTerm){
            $eventCountry = ', '.$cTerm["taxonomy_term"]->name;
            $eventCountryAbbr = ', '.strip_tags($cTerm["taxonomy_term"]->description);
            if($eventCountry == ', United States' || $eventCountry == ', Canada'){
              $eventCountry = '';
              $eventCountryAbbr = '';
            }
          }
        }
        else{
          $eventCountry = '';
          $eventCountryAbbr = '';
        }
      ?>
      <h2 class="js__seo-tool__body-content"><?php print $eventCity[0]['value']; ?><?php print $eventState; ?><?php print $eventCountryAbbr; ?> <br> 
      <?php
        $dateTBD = field_get_items('node', $node, 'field_date_tbd');
        // if Date TBD is not checked...
        if($dateTBD[0]['value'] != 1){
          print $month1.' '.$day1;
          // if start date and end date are not in the same month...
          if($month1 != $month2){
            print " - ".$month2.' '.$day2;
          }
          // else if dates are in the same month...
          elseif($month1 == $month2 && $day1 != $day2){
            print " - ".$day2;
          }
          // otherwise one day event...
          else{
          }
        }
        else{
          print "Date: TBD";
        }
      ?>
      </h2>

      <div class="js__seo-tool__body-content">
        <?php print render($content['field_summary']); ?>
      </div>
      
      <?php 
        // registration button...
        // see code in function at bottom of this template
        $noReg = field_get_items('node', $node, 'field_no_registration'); 
        $regFormURL = $content['field_registration_form_url'];
        $hubspot_field = field_get_items('node', $node, 'field_hubspot_embed_code_2');
        $hubspot = $hubspot_field[0]['value'];
        $customerOnly = field_get_items('node', $node, 'field_customer_only_event');
        registration_button($eventDate, $noReg, $regFormURL, $hubspot, 0); 
      ?>   
    </div>
  </div>
  <!-- End of Hero -->

  <section class="container__centered">
    <div class="container__two-thirds">

      <div class="container no-pad--top">
       
        <div class="js__seo-tool__body-content">
          <?php print render($content['field_event_intro']); ?>
          
          <div style="text-align:center;">
            <?php 
            // check to see if Hubspot field is blank...
            $hubspot_button_code = render($content[field_hubspot_embed_code_1]);
            if( !empty($hubspot_button_code) ){
            ?>
            <div class="button--hubspot">
              <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $hubspot_button_code; ?>"><span class="hs-cta-node hs-cta-<?php print $hubspot_button_code; ?>" id="hs-cta-<?php print $hubspot_button_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $hubspot_button_code; ?>" ><img class="hs-cta-img" id="hs-cta-img-<?php print $hubspot_button_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $hubspot_button_code; ?>.png"  alt="button"/></a></span>
              <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
              <script type="text/javascript">hbspt.cta.load(2897117, '<?php print $hubspot_button_code; ?>', {});</script>
              </span>
              <!-- end HubSpot Call-to-Action Code -->
            </div>
            <?php } ?>
          </div>
        </div>

        <?php // VIDEO SECTION ========================================================
          // get value from field_video to pass to View (if video exists)...
          $video = render($content['field_video']);
          if(!empty($video)){ // if the page has a video...
            print '<!-- VIDEO -->';
            // remove apostrophes from titles to prevent View from breaking...
            $video_filtered = str_replace("&#039;", "'", $video);
            // adds 'video' Views block...
            print '<div style="margin:1em auto; width:80%;">';
            print '<style>.video.video--full-width .video__play-btn{left:45%;}</style>';
            print views_embed_view('video_unboxed', 'block', $video_filtered);
            print '</div>';
            print '<!-- END VIDEO -->';
          }
        ?>         
         
        <div>
          <?php print $share_link_buttons; ?>
        </div>              
          
        <!-- ACCORDIONS :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: -->
        <div class="accordion">
          <ul class="accordion__list">
      
          <?php // TOPICS =========================================================
            if( !empty($content['field_event_topics']) ){
              print '<li class="accordion__list__item">';
              print '<a class="accordion__link event_accordion_topics_gae" href="#">Topics<div class="accordion__list__control"></div></a>';
              print '<div class="accordion__dropdown">';
              print '<div class="js__seo-tool__body-content">';
              print render($content['field_event_topics']);
              print $accordion_shortcode_fix;
              print '</div><!-- End .js__seo-tool__body-content -->';
              print '<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Topics</a>';
              print '</div>';
              print '</li>';
            }
          ?>
          
          <?php // PRE-CONFERENCE =========================================================
            if( !empty($content['field_pre_conference_sessions']) ){
              print '<li class="accordion__list__item">';
              print '<a class="accordion__link event_accordion_pre_conf_gae" href="#">Pre-Conference Sessions<div class="accordion__list__control"></div></a>';
              print '<div class="accordion__dropdown">';
              print '<div class="js__seo-tool__body-content">';
              print render($content['field_pre_conference_sessions']);
              print $accordion_shortcode_fix;
              print '</div><!-- End .js__seo-tool__body-content -->';
              print '<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Pre-Conference Sessions</a>';
              print '</div>';
              print '</li>';
            }
          ?>
           
          <?php // AGENDA =========================================================
            if( !empty($content['field_event_agenda']) ){
              print '<li class="accordion__list__item">';
              print '<a class="accordion__link event_accordion_agenda_gae" href="#">Agenda<div class="accordion__list__control"></div></a>';
              print '<div class="accordion__dropdown">';
              print '<div class="js__seo-tool__body-content">';
              print render($content['field_event_agenda']);
              print $accordion_shortcode_fix;
              print '</div><!-- End .js__seo-tool__body-content -->';
              print '<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Agenda</a>';
              print '</div>';
              print '</li>';
            }
          ?>
          
          <?php // HOME CARE =========================================================
            if( !empty($content['field_home_care_sessions']) ){
              print '<li class="accordion__list__item">';
              print '<a class="accordion__link event_accordion_home_care_gae" href="#">Home Care Sessions<div class="accordion__list__control"></div></a>';
              print '<div class="accordion__dropdown">';
              print '<div class="js__seo-tool__body-content">';
              print render($content['field_home_care_sessions']);
              print $accordion_shortcode_fix;
              print '</div><!-- End .js__seo-tool__body-content -->';
              print '<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Home Care Sessions</a>';
              print '</div>';
              print '</li>';
            }
          ?>
          
          <?php // KEY NOTE =========================================================
            if(!empty($content['field_event_keynote'])){
              print '<li class="accordion__list__item">';
              print '<a class="accordion__link event_accordion_keynote_gae" href="#">Keynote<div class="accordion__list__control"></div></a>';
              print '<div class="accordion__dropdown">';
              print '<div class="js__seo-tool__body-content">';
              print render($content['field_event_keynote']);
              print $accordion_shortcode_fix;
              print '</div><!-- End .js__seo-tool__body-content -->';
              print '<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Keynote</a>';
              print '</div>';
              print '</li>';
            }
          ?>                 
          
          <?php // ED SESSIONS =========================================================
            if(!empty($content['field_event_ed_sessions'])){
              print '<li class="accordion__list__item">';
              print '<a class="accordion__link event_accordion_ed_sessions_gae" href="#">Education Sessions<div class="accordion__list__control"></div></a>';
              print '<div class="accordion__dropdown">';
              print '<div class="js__seo-tool__body-content">';
              print render($content['field_event_ed_sessions']);
              print $accordion_shortcode_fix;
              print '</div><!-- End .js__seo-tool__body-content -->';
              print '<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Education Sessions</a>';
              print '</div>';
              print '</li>';
            }
          ?>
          
          <?php // CLINICAL TRACK =========================================================
            if(!empty($content['field_clinical_track'])){
              print '<li class="accordion__list__item">';
              print '<a class="accordion__link event_accordion_clinical_track_gae" href="#">Clinical Track<div class="accordion__list__control"></div></a>';
              print '<div class="accordion__dropdown">';
              print '<div class="js__seo-tool__body-content">';
              print render($content['field_clinical_track']);
              print $accordion_shortcode_fix;
              print '</div><!-- End .js__seo-tool__body-content -->';
              print '<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Clinical Track</a>';
              print '</div>';
              print '</li>';
            }
          ?>
          
          <?php // FINANCIAL TRACK =========================================================
            if(!empty($content['field_financial_track'])){
              print '<li class="accordion__list__item">';
              print '<a class="accordion__link event_accordion_financial_track_gae" href="#">Financial Track<div class="accordion__list__control"></div></a>';
              print '<div class="accordion__dropdown">';
             print '<div class="js__seo-tool__body-content">';
              print render($content['field_financial_track']);
              print $accordion_shortcode_fix;
              print '</div><!-- End .js__seo-tool__body-content -->';
              print '<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Financial Track</a>';
              print '</div>';
              print '</li>';
            }
          ?>
          
          <?php // GALLERY =========================================================
            if(!empty($content['field_event_gallery'])){
              print '<li class="accordion__list__item">';
              print '<a class="accordion__link event_accordion_gallery_gae" href="#">Gallery<div class="accordion__list__control"></div></a>';
              print '<div class="accordion__dropdown">';
              print '<div class="js__seo-tool__body-content">';
              print render($content['field_event_gallery']);
              print $accordion_shortcode_fix;
              print '</div><!-- End .js__seo-tool__body-content -->';
              print '<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Gallery</a>';
              print '</div>';
              print '</li>';
            }
          ?>
          
          <?php // TECH LAB =========================================================
            if(!empty($content['field_event_tech_lab'])){
              print '<li class="accordion__list__item">';
              print '<a class="accordion__link event_accordion_tech_lab_gae" href="#">Tech Lab<div class="accordion__list__control"></div></a>';
              print '<div class="accordion__dropdown">';
              print '<div class="js__seo-tool__body-content">';
              print render($content['field_event_tech_lab']);
              print $accordion_shortcode_fix;
              print '</div><!-- End .js__seo-tool__body-content -->';
              print '<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Tech Lab</a>';
              print '</div>';
              print '</li>';
            }
          ?>
           
          <?php // VENDORS =========================================================
            if(!empty($content['field_event_vendors'])){
              print '<li class="accordion__list__item">';
              print '<a class="accordion__link event_accordion_vendors_gae" href="#">Vendors<div class="accordion__list__control"></div></a>';
              print '<div class="accordion__dropdown">';
              print '<div class="js__seo-tool__body-content">';
              print render($content['field_event_vendors']);
              
              // add vendor logos and descriptions if exist...
              
              if(!empty($content['field_vendors'])){
                $vendors = field_get_items('node', $node, 'field_vendors');
                foreach($vendors as $vendor){
                  print '<div class="container no-target-icon" style="padding:1em 0;">';
                    $logoFileName = $vendor['entity']->field_logo['und'][0]['filename'];
                    $logoAlt = $vendor['entity']->field_logo['und'][0]['alt'];
                    $vendorURL = $vendor['entity']->field_website_url['und'][0]['value'];
                    $vendor_description = $vendor['entity']->field_description['und'][0]['value'];
                    print '<a class="vendor_link_gae" href="'.$vendorURL.'" target="_blank"><img src="'.$url.'/sites/default/files/vendors/'.$logoFileName.'" alt="'.$logoAlt.'" style="float:none; padding:0;"></a>';
                    print $vendor_description;
                    // add Edit Vendor link...
                    if( user_is_logged_in() ){ 
                      print '<div style="display:block; text-align:center; margin:0 0 .5em 0;"><span style="font-size:12px;">'; print l( t('Edit this Vendor'),'node/'. $vendor['entity']->nid .'/edit'); print "</span></div>"; 
                    }
                  print '</div>';
                }
              }
              
              print $accordion_shortcode_fix;
              print '</div><!-- End .js__seo-tool__body-content -->';
              print '<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Vendors</a>';
              print '</div>';
              print '</li>';
            }
          ?>
          
          <?php // PRESENTATIONS =========================================================
            if(!empty($content['field_event_presentations'])){
              print '<li class="accordion__list__item">';
              print '<a class="accordion__link event_accordion_presentations_gae" href="#">Presentations<div class="accordion__list__control"></div></a>';
              print '<div class="accordion__dropdown">';
              print '<div class="js__seo-tool__body-content">';
              print render($content['field_event_presentations']);
              print $accordion_shortcode_fix;
              print '</div><!-- End .js__seo-tool__body-content -->';
              print '<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Presentations</a>';
              print '</div>';
              print '</li>';
            }
          ?>
          
          <?php // EVALUATION =========================================================
            if(!empty($content['field_event_evaluation'])){
              print '<li class="accordion__list__item">';
              print '<a class="accordion__link event_accordion_eval_gae" href="#">Evaluation<div class="accordion__list__control"></div></a>';
              print '<div class="accordion__dropdown">';
              print '<div class="js__seo-tool__body-content">';
              print render($content['field_event_evaluation']);
              print $accordion_shortcode_fix;
              print '</div><!-- End .js__seo-tool__body-content -->';
              print '<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Evaluation</a>';
              print '</div>';
              print '</li>';
            }
          ?>
          
          <?php // OUR BOOTH =========================================================
            if(!empty($content['field_event_our_booth'])){
              print '<li class="accordion__list__item">';
              print '<a class="accordion__link event_accordion_our_booth_gae" href="#">Our Booth<div class="accordion__list__control"></div></a>';
              print '<div class="accordion__dropdown">';
              print '<div class="js__seo-tool__body-content">';
              print render($content['field_event_our_booth']);
              print $accordion_shortcode_fix;
              print '</div><!-- End .js__seo-tool__body-content -->';
              print '<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Our Booth</a>';
              print '</div>';
              print '</li>';
            }
          ?>
          
          <?php // Featured Sessions =========================================================
            if(!empty($content['field_event_featured_sessions'])){
              print '<li class="accordion__list__item">';
              print '<a class="accordion__link event_accordion_featured_sessions_gae" href="#">Featured Sessions<div class="accordion__list__control"></div></a>';
              print '<div class="accordion__dropdown">';
              print '<div class="js__seo-tool__body-content">';
              print render($content['field_event_featured_sessions']);
              print $accordion_shortcode_fix;
              print '</div><!-- End .js__seo-tool__body-content -->';
              print '<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Featured Sessions</a>';
              print '</div>';
              print '</li>';
            }
          ?>
          
          <?php // MEET AND GREET =========================================================
            if(!empty($content['field_event_meet_and_greet'])){
              print '<li class="accordion__list__item">';
              print '<a class="accordion__link event_accordion_meet_greet_gae" href="#">Meet and Greet<div class="accordion__list__control"></div></a>';
              print '<div class="accordion__dropdown">';
              print '<div class="js__seo-tool__body-content">';
              print render($content['field_event_meet_and_greet']);
              print $accordion_shortcode_fix;
              print '</div><!-- End .js__seo-tool__body-content -->';
              print '<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Meet and Greet</a>';
              print '</div>';
              print '</li>';
            }
          ?>
          
          <?php // CUSTOMER EVENT =========================================================
            if(!empty($content['field_event_customer_event'])){
              print '<li class="accordion__list__item">';
              print '<a class="accordion__link event_accordion_cust_event_gae" href="#">Customer Event<div class="accordion__list__control"></div></a>';
              print '<div class="accordion__dropdown">';
              print '<div class="js__seo-tool__body-content">';
              print render($content['field_event_customer_event']);
              print $accordion_shortcode_fix;
              print '</div><!-- End .js__seo-tool__body-content -->';
              print '<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Customer Event</a>';
              print '</div>';
              print '</li>';
            }
          ?>
          
          <?php // CUSTOMER SPEAKERS =========================================================
            if(!empty($content['field_event_customer_speakers'])){
              print '<li class="accordion__list__item">';
              print '<a class="accordion__link event_accordion_cust_speakers_gae" href="#">Customer Speakers<div class="accordion__list__control"></div></a>';
              print '<div class="accordion__dropdown">';
              print '<div class="js__seo-tool__body-content">';
              print render($content['field_event_customer_speakers']);
              print $accordion_shortcode_fix;
              print '</div><!-- End .js__seo-tool__body-content -->';
              print '<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Customer Speakers</a>';
              print '</div>';
              print '</li>';
            }
          ?>  
          
          <?php // ADVANCED TRACK =========================================================
            if(!empty($content['field_event_advanced_track'])){
              print '<li class="accordion__list__item">';
              print '<a class="accordion__link event_accordion_advanced_track_gae" href="#">Advanced Track<div class="accordion__list__control"></div></a>';
              print '<div class="accordion__dropdown">';
              print '<div class="js__seo-tool__body-content">';
              print render($content['field_event_advanced_track']);
              print $accordion_shortcode_fix;
              print '</div><!-- End .js__seo-tool__body-content -->';
              print '<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Advanced Track</a>';
              print '</div>';
              print '</li>';
            }
          ?>
          
          <?php // RESOURCES =========================================================
            if(!empty($content['field_event_resources'])){
              print '<li class="accordion__list__item">';
              print '<a class="accordion__link event_accordion_resources_gae" href="#">Resources<div class="accordion__list__control"></div></a>';
              print '<div class="accordion__dropdown">';
              print '<div class="js__seo-tool__body-content">';
              print render($content['field_event_resources']);
              print $accordion_shortcode_fix;
              print '</div><!-- End .js__seo-tool__body-content -->';
              print '<a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Resources</a>';
              print '</div>';
              print '</li>';
            }
          ?>  
          
          </ul>
        </div>
        <!-- END Accordions -->      
        
 
        
        <?php // SEO tool for internal use...
          if(node_access('update',$node)){
            print '<!-- SEO Tool is added to this div -->';
            print '<div class="container no-pad--top js__seo-tool"></div>';
          } 
        ?>  

      </div><!-- END container -->
    </div><!-- END container__two-thirds -->

    
    <!-- SIDEBAR =============================================== -->
    <aside class="container__one-third panel" style="margin-right:0;">
      <div class="sidebar--event__title--first">Event Location</div>

      <div class="js__seo-tool__body-content">
        <p>
          <?php 
            if($eventLocation[0]['value'] != ''){
              print $eventLocation[0]['value'].'<br />';
            }
            if($eventAddress[0]['value'] != ''){
              print $eventAddress[0]['value'].'<br />';
            }
            print $eventCity[0]['value'].$eventState.$eventCountry;
          ?>
        </p>
      </div>

      
      <?php if( !empty($content['field_event_accomodations']) ){ ?>
        <div class="sidebar--event__title">Where To Stay</div>
        <div class="js__seo-tool__body-content">
          <?php print render($content['field_event_accomodations']); ?>
        </div>
      <?php } ?>
      

      <div class="sidebar--event__title">Questions?</div>
      <div class="js__seo-tool__body-content">
        <?php print render($content['field_event_questions']); ?>
      </div>
      
      <?php 
        // registration button...
        registration_button($eventDate, $noReg, $regFormURL, $hubspot, $customerOnly); 
      ?>

    </aside>
    <!-- END SIDEBAR -->

  </section><!-- END section -->
 
<!-- end node--event.tpl.php template -->


<?php
  function registration_button($eventDate, $noReg, $regFormURL, $hubspot, $customerOnly){
    $today = strtotime("now");
    $eventDayOne = strtotime($eventDate[0]['value']);
    // if the event hasn't happened yet...
    if($eventDayOne > $today){
      
      // if the 'No Registration' field is checked, show nothing...
      if($noReg[0]['value'] == 1){
        print '<!-- No Registration field checked, show nothing, event has not happened -->';
      }
      
      else{
        
        // if URL has been provided, show button...
        if( !empty($regFormURL) ){
          print '<div class="btn-holder--content__callout"><a class="btn--orange event_reg_gae" href="';
          print render($regFormURL);
          print '">Register Today!</a></div>';
          print '<!-- URL provided, event has not happened -->';
          if($customerOnly[0]['value'] == 1){
            print '<p style="font-size:.8em; line-height:1.15em; margin-top:1em;">* This event is for MEDITECH customers only. You will be asked for your user name and password in order to register for this event.</p>';
          }
        } // end regform url
        elseif( !empty($hubspot) ){
        ?>
          <div style="text-align:center;">
            <div class="button--hubspot">
              <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $hubspot; ?>"><span class="hs-cta-node hs-cta-<?php print $hubspot; ?>" id="hs-cta-<?php print $hubspot; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $hubspot; ?>" ><img class="hs-cta-img" id="hs-cta-img-<?php print $hubspot; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $hubspot; ?>.png"  alt="button"/></a></span>
              <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
              <script type="text/javascript">hbspt.cta.load(2897117, '<?php print $hubspot; ?>', {});</script>
              </span>
              <!-- end HubSpot Call-to-Action Code -->
            </div>
          </div>
        <?php
        } // end hubspot
        else{
          print '<p><strong>Registration Opening Soon</strong></p>';
          print '<!-- no URL provided, event has not happened -->';
        }
        
      }
      
    } // end hasn't happened yet
    // event has gone by...
    else{
      print '<!-- show nothing, event has gone by -->';
    }  
    
  }
?>