<!-- START inc-edit-page.php -->
<?php
if(node_access('update',$node)){ 
  print '<a href="'.$url.'/node/'.$node->nid.'/edit" title="Edit this page" style="border: 1px solid #087E68; padding: .15em; font-size: .4em; margin-left: .15em;     position: relative; top: -4px;">Edit</a>'; 
}
?>
<!-- END inc-edit-page.php -->