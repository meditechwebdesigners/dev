<!-- START about-page--node-2464.tpl.php -->
<?php
$url = $GLOBALS['base_url']; // grabs the site url

$content_sections = multi_field_collection_data_unrestricted($node, 'field_fc_head_ltext_1');
$buttons = multi_field_collection_data_unrestricted($node, 'field_fc_button_unl_1');
$side_menu = multi_field_collection_data_unrestricted($node, 'field_fc_text_link_unl_1');
?>

  <style>
    .reverse-grid {
      display: block;
      flex-direction: row;
    }

    .bg--polar-gray {
      background-color: #f2f8fc;
    }

    @media screen and (max-width: 800px) {
      .reverse-grid {
        display: flex;
        flex-direction: column-reverse;
      }
      /** IE fix for flex-direction height issue **/
      .reverse-grid-fix {
        min-height: 100px;
      }
    }

  </style>

  <!-- Block 1 -->
  <div class="container">
    <div class="container__centered reverse-grid">

      <div class="container__one-third">
        <?php
      if( !empty($content_sections[0]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[0]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      if( !empty($content_sections[0]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[0]->field_long_text_1['und'][0]['value'];   
      }
      ?>
      </div>
      <div class="container__two-thirds reverse-grid-fix"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/dedicated-community-animation.gif" alt="A dedicated MEDITECH community"></div>
    </div>
  </div>
  <!-- End Block 1 -->

  <!-- Block 2 -->
  <div class="container bg--polar-gray">
    <div class="container__centered">

      <div class="container__one-half"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/stem-education-and-innovation-animation.gif" alt="STEM education & innovation"></div>
      <div class="container__one-half">
        <?php
      if( !empty($content_sections[1]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[1]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      if( !empty($content_sections[1]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[1]->field_long_text_1['und'][0]['value'];
      }
      ?>
      </div>
    </div>
  </div>
  <!-- End Block 2 -->

  <!-- Block 3 -->
  <div class="container">
    <div class="container__centered reverse-grid">

      <div class="container__one-half">
        <?php
      if( !empty($content_sections[2]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[2]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      if( !empty($content_sections[2]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[2]->field_long_text_1['und'][0]['value'];
      }
      ?>
      </div>
      <div class="container__one-half reverse-grid-fix"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/charitable-causes-animation.gif" alt="Charitable causes"></div>
    </div>
  </div>
  <!-- End Block 3 -->

  <!-- Block 4 -->
  <div class="container bg--polar-gray">
    <div class="container__centered">

      <div class="container__two-thirds"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/military-animation.gif" alt="MEDITECH Military Employment"></div>
      <div class="container__one-third">
        <?php
      if( !empty($content_sections[3]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[3]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      if( !empty($content_sections[3]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[3]->field_long_text_1['und'][0]['value'];
      }
      ?>
      </div>
    </div>
  </div>
  <!-- End Block 4 -->

  <!-- Block 5 -->
  <div class="container">
    <div class="container__centered reverse-grid">

      <div class="container__one-third">
        <?php
      if( !empty($content_sections[4]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[4]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      if( !empty($content_sections[4]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[4]->field_long_text_1['und'][0]['value'];
      }
      ?>
      </div>
      <div class="container__two-thirds reverse-grid-fix"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/Wellness-Animation.gif" alt="Wellness Works Program"></div>
    </div>
  </div>
  <!-- End Block 5 -->

  <!-- Block 6 -->
  <div class="container bg--polar-gray">
    <div class="container__centered">

      <div class="container__one-half"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/college-career-animation.gif" alt="College & Career"></div>
      <div class="container__one-half">
        <?php
      if( !empty($content_sections[5]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[5]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      if( !empty($content_sections[5]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[5]->field_long_text_1['und'][0]['value'];
      }
      ?>
      </div>
    </div>
  </div>
  <!-- End Block 6 -->

  <!-- Block 7 -->
  <div class="container">
    <div class="container__centered reverse-grid">

      <div class="container__one-half">
        <?php
      if( !empty($content_sections[6]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[6]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      if( !empty($content_sections[6]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[6]->field_long_text_1['und'][0]['value'];
      }
      ?>
      </div>
      <div class="container__one-half reverse-grid-fix"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/water-power-animation.gif" alt="Water & Power"></div>
    </div>
  </div>
  <!-- End Block 7 -->

  <!-- Block 8 -->
  <div class="container bg--polar-gray">
    <div class="container__centered">

      <div class="container__one-half"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/innovative-climate-control-animation.gif" alt="Innovative Climate Control"></div>
      <div class="container__one-half">
        <?php
      if( !empty($content_sections[7]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[7]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      if( !empty($content_sections[7]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[7]->field_long_text_1['und'][0]['value'];
      }
      ?>
      </div>
    </div>
  </div>
  <!-- End Block 8 -->
  
  <!-- Block 9 -->
<div class="container">
  <div class="container__centered reverse-grid">

      <div class="container__one-third">
      <?php
      if( !empty($content_sections[8]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[8]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      if( !empty($content_sections[8]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[8]->field_long_text_1['und'][0]['value'];
      }
      ?>
    </div>
   <div class="container__two-thirds reverse-grid-fix"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/recycle-animation.gif" alt="Recycling"></div>

  </div>
</div>
<!-- End Block 9 -->

<!-- Block 10 -->
<div class="container bg--polar-gray">
  <div class="container__centered">

   <div class="container__two-thirds"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/art-program-animation.gif" alt="Art Program"></div>
    <div class="container__one-third">
      <?php
      if( !empty($content_sections[9]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[9]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      if( !empty($content_sections[9]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[9]->field_long_text_1['und'][0]['value'];
      }
      ?>
    </div>

  </div>
</div>
<!-- End Block 10 -->

<!-- Block 11 -->
<div class="container background--cover">
  <div class="container__centered reverse-grid">

    <div class="container__one-third">
      <?php
      if( !empty($content_sections[10]->field_header_1['und'][0]['value']) ){
        print '<h2>';
        print $content_sections[10]->field_header_1['und'][0]['value']; 
        print '</h2>';
      }
      if( !empty($content_sections[10]->field_long_text_1['und'][0]['value']) ){
        print $content_sections[10]->field_long_text_1['und'][0]['value'];
      }
      ?>
    </div>
    <div class="container__two-thirds reverse-grid-fix"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/company-picnic-animation.gif" alt="Company Picnic"></div>

  </div>
</div>
<!-- End Block 11 -->

<!-- END about-page--node-2464.tpl.php -->