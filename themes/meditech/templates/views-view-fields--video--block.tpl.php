<?php // This template is for each row of the Views block: VIDEO\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ ....................... 
  $url = $GLOBALS['base_url']; // grabs the site url
?>
<!-- start views-view-fields--video--block.tpl.php template -->
    <div class="content__callout">
      <div class="content__callout__media">
        <div class="content__callout__image-wrapper">
         
          <div class="video js__video" data-video-id="<?php print $fields['field_video_url']->content; ?>">
            <figure class="video__overlay">
              <?php // if the overlay image was added by user, then show overlay image, otherwise show default overlay image...
              if( !empty($fields['field_video_overlay_image']->content) ){
                print $fields['field_video_overlay_image']->content; 
              }
              else{
              ?>
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/video-overlay.jpg" alt="Video Covershot">
              <?php } ?>
            </figure>
            <a href="http://vimeo.com/<?php print $fields['field_video_url']->content; ?>?&autoplay=1" class="video__play-btn"></a>
            <div class="video__container"></div>
          </div>       
          
        </div>
      </div>
      <div class="content__callout__content">
        <div class="content__callout__body">
         <h2><?php print $fields['title']->content; ?></h2>
          <p class="content__callout__body__text--large"><?php print $fields['field_video_description']->content; ?></p>
          <?php if( !empty($fields['field_video_button_link']->content) ){ ?>
          <div class="btn-holder--content__callout">
              <a class="btn--orange video_link_gae" href="<?php print $fields['field_video_button_link']->content; ?>"><?php print $fields['field_video_button_text']->content; ?></a>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
<!-- end views-view-fields--video--block.tpl.php template -->