<?php // This template is set up to control the display of the campaign_transformative_techs content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$button_1 = field_collection_data($node, 'field_fc_button_1');
$block_3 = multi_field_collection_data($node, 'field_fc_head_ltext_1', 3); 
$quotes = multi_field_collection_data($node, 'field_fc_quote_1', 3);
?>
<!-- start node--campaign_transformative_tech.tpl.php template -->

<div class="js__seo-tool__body-content">

    <!--Block 1-->
    <div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/doctor-helping-patient-with-tablet-in-waiting-room.jpg">
        <div class="container__centered">
            <div class="container__two-thirds transparent-overlay text--white">
                <h1 class="text--white">
                    <?php print render($content['field_header_1']); ?>
                </h1>
                <p>
                    <?php print render($content['field_long_text_1']); ?>
                </p>
                <div style="text-align:center;">
                    <?php 
                    // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
                    if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
                      $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
                    ?>
                    <div class="button--hubspot">
                        <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span>
                        <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
                        <script type="text/javascript">
                            hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {});

                        </script>
                        </span>
                        <!-- end HubSpot Call-to-Action Code -->
                    </div>
                    <?php }else{ ?>
                    <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <!--End of Block 1-->

    <!--Block 2 Video-->
    <div class="content__callout">
        <div class="content__callout__media">
            <div class="content__callout__image-wrapper">
                <div class="video js__video" data-video-id="217657040">
                    <figure class="video__overlay">
                        <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/video-overlay-Helen-Waters-at-HIMMS-2017.jpg" alt="Video Covershot of Helen Waters at HIMMS 2017">
                    </figure>
                    <a class="video__play-btn" href="https://vimeo.com/217657040"></a>
                    <div class="video__container">
                    </div>
                </div>
            </div>
        </div>
        <div class="content__callout__content">
            <div class="content__callout__body">
                <div class="content__callout__body__text">
                    <h2>
                        <?php print render($content['field_header_2']); ?>
                    </h2>
                    <?php print render($content['field_long_text_2']); ?>
                </div>
            </div>
        </div>
    </div>
    <!--End of Block 2-->

    <!--Block 3-->
    <div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/doctor-helping-patient-at-home-using-tablet.jpg);">
        <div class="container__centered" style="padding-bottom:1em;">
            <div class="page__title--center">
                <h2>
                    <?php print render($content['field_header_3']); ?>
                </h2>
                <p>
                    <?php print render($content['field_long_text_3']); ?>
                </p>
                <div class="page__title__ribbon--meditech-green"></div>
            </div>
        </div>
        <div class="container__centered">
            <div class="container__one-third">
                <h3>
                    <?php print $block_3[0]->field_header_1['und'][0]['value']; ?>
                </h3>
                <?php print $block_3[0]->field_long_text_1['und'][0]['value']; ?>
            </div>
            <div class="container__one-third">
                <h3>
                    <?php print $block_3[1]->field_header_1['und'][0]['value']; ?>
                </h3>
                <?php print $block_3[1]->field_long_text_1['und'][0]['value']; ?>
            </div>
            <div class="container__one-third">
                <h3>
                    <?php print $block_3[2]->field_header_1['und'][0]['value']; ?>
                </h3>
                <p>
                    <?php print $block_3[2]->field_long_text_1['und'][0]['value']; ?>
                </p>
            </div>
        </div>
    </div>
    <!--End of Block 3-->


    <!--Block 4 Quote-->
    <?php $block_4 = field_collection_data($node, 'field_fc_quote_1'); ?>
    <div class="container" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Green-Triangles-2.png">
        <article class="container__centered text--white">
            <figure class="container__one-fourth center">
                <img class="quote__content__img" alt="Ed Ricks Transform EHR Quote" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Ed-Ricks.png" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Ed-Ricks.png';this.onerror=null;">
            </figure>
            <div class="container__three-fourths">
                <div class="quote__content__text text--large">
                    <p>
                        <?php print $block_4->field_quote_1['und'][0]['value']; ?>
                    </p>
                </div>
                <p class="text--large no-margin--bottom">
                    <?php print $block_4->field_full_name['und'][0]['value']; ?>
                </p>
                <p>
                    <?php print $block_4->field_company['und'][0]['value']; ?>
                </p>
            </div>
        </article>
    </div>
    <!--End of Block 4-->


    <!--Block 5 Video-->
    <div class="content__callout">
        <div class="content__callout__media">
            <div class="content__callout__image-wrapper">
                <div class="video js__video" data-video-id="155984195">
                    <figure class="video__overlay">
                        <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/video-overlay-MEDITECH-and-Patient-Generated-Health-Data.jpg" alt="Video Covershot for MEDITECH and Patient Generated Health Data">
                    </figure>
                    <a class="video__play-btn" href="http://vimeo.com/155984195"></a>
                    <div class="video__container">
                    </div>
                </div>
            </div>
        </div>
        <div class="content__callout__content">
            <div class="content__callout__body">
                <div class="content__callout__body__text">
                    <h2>
                        <?php print render($content['field_header_4']); ?>
                    </h2>
                    <?php print render($content['field_long_text_4']); ?>
                </div>
            </div>
        </div>
    </div>
    <!--End of Block 5-->

    <!--Block 6 Screenshot-->
    <div class="container" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/green-pattern.png">
        <div class="container__centered text--white">
            <div class="page__title--center">
                <h2>
                    <?php print render($content['field_header_5']); ?>
                </h2>
                <div class="page__title__ribbon--meditech-green"></div>
                <p>
                    <?php print render($content['field_long_text_5']); ?>
                </p>
            </div>
            <div class="container__one-half">
                <img src="/sites/all/themes/meditech/images/campaigns/screenshot-sepsis-surveillance-board.png" alt="Sepsis Surveillance Board Screenshot">
            </div>
            <div class="container__one-half">
                <h3>
                    <?php print render($content['field_header_6']); ?>
                </h3>
                <p>
                    <?php print render($content['field_long_text_6']); ?>
                </p>
                <h3>
                    <?php print render($content['field_header_7']); ?>
                </h3>
                <p>
                    <?php print render($content['field_long_text_7']); ?>
                </p>
            </div>
        </div>
    </div>
    <!--End of Block 6-->

    <!--Block 7 CTA-->
    <div class="container" style="background-image: url(/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg);">
        <div class="container__centered" style="text-align: center;">
            <h2>
                <?php print render($content['field_header_8']); ?>
            </h2>
            <div>
                <?php print render($content['field_long_text_8']); ?>
            </div>

            <?php 
            // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
            if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
              $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
            ?>
            <div class="button--hubspot">
                <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span>
                <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
                <script type="text/javascript">
                    hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {});

                </script>
                </span>
                <!-- end HubSpot Call-to-Action Code -->
            </div>
            <?php }else{ ?>
            <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
            <?php } ?>

            <div style="margin-top:1em;">
                <?php print $share_link_buttons; ?>
            </div>
        </div>
    </div>
    <!--End of Block 7-->
</div>
<!-- end js__seo-tool__body-content -->


<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>

<!-- end node--campaign_transformative_tech.tpl.php template -->
