<?php // This template is set up to control the display of the 'executive' individual pages... 

  $url = $GLOBALS['base_url']; // grabs the site url
?>
<!-- start node--executive.tpl.php template -->

<!-- HERO section -->
<?php 
if( !empty($content['field_background_block_image']) ){ 
  // get image data...
  $background_image_data = field_get_items('node', $node, 'field_background_block_image');
  // get image uri...
  $background_image_uri = $background_image_data[0]['uri']; // grab image URI
  // remove "public://" portion at the beginning...
  $background_image_filename = str_replace('public://', '', $background_image_uri);
}  

if( !empty($content['field_image']) ){ 
  // get image data...
  $image_data = field_get_items('node', $node, 'field_image');
  // get image uri...
  $image_uri = $image_data[0]['uri']; // grab image URI
  // remove "public://" portion at the beginning...
  $image_filename = str_replace('public://', '', $image_uri);
}  
?>
  
<style>  
  .executive-background { background-image: url(<?php print $url; ?>/sites/default/files/<?php print $background_image_filename; ?>); background-color:#087e68; padding: 10em 0; }
  .diagonal-lines-left { float:left; position:relative; top:-4em; left:-1em; width:30%; }
  .diagonal-lines-right { float:right; position:relative; top:-4em; left:1em; width:30%; }
  .show-on-mobile { display: none; }

  @media all and (max-width: 50em) {
    .executive-background { padding: 4em 0; }
    .executive-titles { position: relative; top: 7em; }
    .hide-on-tablets { display: none; }
  }
  @media all and (max-width: 30em) {
    .executive-background { background-image: url(<?php print $url; ?>/sites/default/files/<?php print $image_filename; ?>); padding: 12em 0; }
    .executive-titles { position: relative; top: 0; }
    .show-on-mobile { display: block; }
    .hide-on-mobile { display: none; }
  }
</style>

<div class="container background--cover executive-background">
  <div class="container executive-titles hide-on-mobile">

    <?php 
    // check first to see if field collection exists...
    $page_location_data = field_get_items('node', $node, 'field_page_location');

    // function to grab page_location setting...
    if( !empty($page_location_data) ){

      $page_location = $page_location_data[0]['taxonomy_term']->name;

      if($page_location == 'Left Side'){
      ?>
        <div class="container__one-half center text--white transparent-overlay" style="background-color: rgba(0, 0, 0, 0.4);">
          <h1 class="executive__bio__title js__seo-tool__title" style="margin-top:.3em;"><?php print $title; ?></h1>
          <h2 class="executive__bio__subtitle js__seo-tool__body-content"><?php print render($content['field_job_title']); ?></h2>
        </div>
        <div class="container__one-half center">&nbsp;</div>
      <?php
      }

      if($page_location == 'Right Side'){
      ?>
        <div class="container__one-half center">&nbsp;</div>
        <div class="container__one-half center text--white transparent-overlay" style="background-color: rgba(0, 0, 0, 0.4);">
          <h1 class="executive__bio__title js__seo-tool__title" style="margin-top:.3em;"><?php print $title; ?></h1>
          <h2 class="executive__bio__subtitle js__seo-tool__body-content"><?php print render($content['field_job_title']); ?></h2>
        </div>
      <?php
      }

    }
    ?>

  </div>
  <?php
  // function to grab page_location setting...
  if($page_location != ''){
    if($page_location == 'Left Side'){
    ?>
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/diagonal-green-lines--transparent.png" alt="diagonal lines" class="diagonal-lines-left hide-on-tablets" />
    <?php
    }
    if($page_location == 'Right Side'){
    ?>
      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/diagonal-green-lines--transparent.png" alt="diagonal lines" class="diagonal-lines-right hide-on-tablets" />
    <?php
    }
  }
  ?>
</div>
<!-- END HERO section -->


<section class="container__centered">

  <div class="container__two-thirds">
      
    <div class="js__seo-tool__body-content">        

      <h1 class="show-on-mobile js__seo-tool__title"><?php print $title; ?></h1>
      <h2 class="show-on-mobile js__seo-tool__body-content"><?php print render($content['field_job_title']); ?></h2>

      <?php print render($content['field_body']); ?>

    </div>  

    <?php // SEO tool for internal use...
      if(node_access('update',$node)){
        print '<!-- SEO Tool is added to this div -->';
        print '<div class="container js__seo-tool"></div>';
      } 
    ?>

  </div>

  <!-- ABOUT SIDE NAV =========================================================================== -->
  <aside class="container__one-third panel">
    <div class="sidebar__nav">
      <?php
        $massnBlock = module_invoke('menu', 'block_view', 'menu-about-section-side-nav');
        print render($massnBlock['content']); 
      ?>
    </div>
  </aside>

</section>    
<!-- end node--executive.tpl.php template -->