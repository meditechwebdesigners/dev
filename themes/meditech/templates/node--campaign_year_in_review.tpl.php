<?php // This template is set up to control the display of the CAMPAIGN YEAR IN REVIEW content type

  $url = $GLOBALS['base_url']; // grabs the site url

  // FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM...
  // to use the function simply assign it to a variable
  // then pass in the field collection machine name to the function
  // then you'll use the variable when calling each sub-field within the template
  function field_collection_data($node, $field_name){
    // get array specific to a certain field collection item...
    $field_collection = field_get_items('node', $node, $field_name); 
    // get all content within that field collection...
    $fc_field_data = entity_load('field_collection_item', array($field_collection[0]['value']));
    // get field collection ID...
    $fc_field_ID = key($fc_field_data);
    // create variable and load it with field collection data...
    $var_with_data = $fc_field_data[$fc_field_ID];
    // return variable...
    return $var_with_data;
  }

  // FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM with mulitple instances...
  // to use the function simply assign it to a variable
  // then pass in the field collection machine name to the function and the number of instances allowed ($quantity)
  // then you'll use the variable when calling an array for each group of sub-fields within the template
  function multi_field_collection_data($node, $field_name, $quantity){
    // get array specific to a certain field collection item...
    $field_collection = field_get_items('node', $node, $field_name); 
    // create array...
    $multi_field_collections = array();
    // loop through '$quantity' times...
    for($x=0; $x<$quantity; $x++){
      // get all content within that field collection...
      $fc_field_data = entity_load('field_collection_item', array($field_collection[$x]['value']));
      // get field collection ID...
      $fc_field_ID = key($fc_field_data);
      // create variable and load it with field collection data...
      $var_with_data = $fc_field_data[$fc_field_ID];
      // add data to array...
      $multi_field_collections[] = $var_with_data;
    }
    // return variable...
    return $multi_field_collections;
  }

  $quotes = multi_field_collection_data($node, 'field_fc_quote_2', 3);
?>
<!-- start node--campaign_year_in_review.tpl.php template -->
    

<div class="js__seo-tool__body-content">

  <!-- Block 1 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/year-in-review-2015-city-skyline-fireworks.jpg);">
    <div class="container__centered text--white text-shadow--black">
      <h1 class="text--white js__seo-tool__title" style="margin-top:0;"><?php print $title; ?></h1>
      <h2><?php print render($content['field_header_1']); ?></h2>
      <div style="font-size:1.6em;">
        <?php print render($content['field_long_text_1']); ?>
      </div>
    </div>
  </div>
  <!-- Block 1 -->


  <!-- Block 2 -->
  <div class="container hide__bg-image--mobile" style="background: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/year-in-review-2015-doctor-tablet.jpg);">
    <div class="container__centered">
      <div class="container__one-half">
        <h2><?php print render($content['field_header_1']); ?></h2>
        <div>
          <?php print render($content['field_long_text_2']); ?>
        </div>
      </div>
    </div>
  </div>
  <!-- Block 2 -->


  <!-- Block 3 -->
  <?php $block_3 = field_collection_data($node, 'field_fc_quote_1'); ?>
  <div class="container bg--black-coconut">
    <article class="container__centered">
      <figure class="container__one-fourth center">
        <img class="quote__content__img"  src="https://ehr.meditech.com/sites/default/files/styles/people-photo_adaptive_breakpoints_theme_meditech_big-medium_2x/public/images/people/HowardMessing.png">
      </figure>
      <div class="container__three-fourths">
        <div class="quote__content__text text--large">
          <?php print $block_3->field_quote_1['und'][0]['value']; ?>
        </div>
        <p class="text--large no-margin--bottom"><?php print $block_3->field_full_name['und'][0]['value']; ?></p>
        <p><?php print $block_3->field_company['und'][0]['value']; ?></p>
      </div>
    </article>
  </div>
  <!-- Block 3 -->


  <!-- Block 4 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/year-in-review-2015-blue-background.jpg);">
    <div class="container__centered text--white">
      <h2><?php print render($content['field_header_3']); ?></h2>
      <div>
        <?php print render($content['field_long_text_3']); ?>
      </div>
    </div>
  </div>
  <!-- Block 4 -->


  <!-- Block 5 -->
  <div class="container bg--black-coconut background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/population-health_background--photo.jpg);">
    <div class="container__centered text--white">
      <h2><?php print render($content['field_header_4']); ?></h2>
      <div>
        <?php print render($content['field_long_text_4']); ?>
      </div>
    </div>
  </div>
  <!-- Block 5 -->


  <!-- Block 6 -->
  <div class="container bg--emerald">
    <div class="container__centered">
      <?php 
        // add font awesome icons to array...
        $fa = array('far fa-chart-bar','fas fa-heart','fas fa-user');
        $f=0;
        foreach($quotes as $quote){    
      ?>

      <div class="container__one-third">
        <div class="quote__content__icon center">
          <i class="<?php print $fa[$f]; ?> fa-4x"></i>
        </div>
        <p class="quote__content__text"><?php print $quote->field_quote_1['und'][0]['value']; ?></p>
        <p class="no-margin--bottom"><?php print $quote->field_full_name['und'][0]['value']; ?></p>
        <p class="text--small"><?php print $quote->field_company['und'][0]['value']; ?></p>
      </div>

      <?php 
        $f++;
        } 
      ?> 
    </div>
  </div>
  <!-- Block 6 -->


  <!-- Block 7 -->
  <div class="container background--cover hide__bg-image--mobile" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/year-in-review-2015-elderly-african-american-couple.jpg);">
    <div class="container__centered">
      <div class="container__one-half">
        <h2><?php print render($content['field_header_5']); ?></h2>
        <div><?php print render($content['field_long_text_5']); ?></div>
      </div>
    </div>
  </div>
  <!-- Block 7 -->


  <!-- Block 8 -->
  <?php $block_web_amb = field_collection_data($node, 'field_fc_head_ltext_btn_1'); ?>
  <div class="container bg--black-coconut" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/green-pattern-01.png);">
    <div class="container__centered">
      <h2 class="text--white"><?php print $block_web_amb->field_header_1['und'][0]['value']; ?></h2>
      <div class="text--white"><?php print $block_web_amb->field_long_text_1['und'][0]['value']; ?></div>
      <div class="container__centered">
        <div class="btn-holder--content__callout">
          <a href="<?php print $block_web_amb->field_button_url_1['und'][0]['value']; ?>" class="btn--orange campaign_button_gae" target=_blank><?php print $block_web_amb->field_button_text_1['und'][0]['value']; ?></a>
        </div>
      </div>
    </div>
  </div>
  <!-- Block 8 -->


  <!-- Block 9 -->
  <div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangles-horizontal.png);">
    <div class="container__centered">
      <div class="container__two-thirds">
        <h2><?php print render($content['field_header_6']); ?></h2>
        <div><?php print render($content['field_long_text_6']); ?></div>
      </div>
      <div class="container__one-third center">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/year-in-review-2015-social-media-graphic.png">
      </div>
    </div>
  </div>
  <!-- Block 9 -->

</div><!-- end js__seo-tool__body-content -->

<!-- Block 10 -->
<div class="container bg--black-coconut background--cover hide__bg-image--mobile" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/himss-2016-hero.jpg);">
  <div class="container__centered center">
   
    <h2 class="text--white js__seo-tool__body-content"><?php print render($content['field_header_7']); ?></h2>
    <div class="text--white js__seo-tool__body-content"><?php print render($content['field_long_text_7']); ?></div>
    <a href="<?php print render($content['field_button_url_1']); ?>" class="btn--orange campaign_button_gae"><?php print render($content['field_button_text_1']); ?></a>
    
    <div class="sharethis-centered">
      <?php
        $block = block_load('sharethis', 'sharethis_block');
        $render_array = _block_get_renderable_array(_block_render_blocks(array($block)));
        print render($render_array);
      ?>
    </div>    
    
  </div>
</div>
<!-- Block 10 -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>  
<!-- end node--campaign_year_in_review.tpl.php template -->