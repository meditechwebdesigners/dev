<?php
// This template is set up to control the display of the 'simple page' content type

$url = $GLOBALS['base_url']; // grabs the site url

// get current URL info to determine which layout to render...
$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
?>

<!-- start node--simple-page.tpl.php template -->

<?php
  if($node->nid == 358){ // =============================== CONTACT PAGE... ============================================
?>
	<!-- start contact page section of the node--simple-page.tpl.php template -->

    <section class="container__centered">

	  <div class="container__two-thirds">

        <div class="js__seo-tool__body-content">
          <h1 class="js__seo-tool__title">Contact</h1>
          <h2 class="page__title">Are you an existing customer?</h2>
          <p>If you’re a customer in need of assistance, please log in to our <a href="https://www.meditech.com/Programs/ams.exe?TYPE=OnLine.html" target="_blank">Customer Service area</a> and enter a task, or call us anytime at <a class="phone-number" href="tel:1-781-821-3000">781-821-3000</a>.</p>

          <h2>Are you from the media?</h2>
          <p>We’re here to help. Media inquiries should be directed to our <a href="<?php print $url; ?>/meditech-communications-team">Communications team</a>.</p>

          <address class="container no-pad">
            <div class="container__one-third">
              <h3><i class="fas fa-map-marker-alt meditech-green"></i> Address</h3>
              <p>MEDITECH Circle
              <br>Westwood, MA 02090</p>
            </div>
            <div class="container__one-third">
              <h3><i class="fas fa-phone meditech-green"></i> Call</h3>
              <p><a class="phone-number" href="tel:1-781-821-3000">781-821-3000</a></p>
            </div>
            <div class="container__one-third">
              <h3><i class="fas fa-fax meditech-green"></i> Fax</h3>
              <p>781-821-2199</p>
            </div>
          </address>
        </div>

        <!-- CONTACT US FORM =============================== -->
        <h3><i class="fas fa-envelope meditech-green"></i>&nbsp; Send us a message</h3>
        <?php
          $contactForm = module_invoke('webform', 'block_view', 'client-block-1071');
          print render($contactForm['content']);
        ?>
        <!-- End of Contact Form -->

      </div>

      <aside class="container__one-third panel">
        <div class="sidebar__nav">
          <h2 class="sidebar__nav__title">Facilities</h2>
          <p>Click a location for directions.</p>
          <?php print views_embed_view('locations_contact_page', 'block'); // adds 'locations contact page' Views block... ?>
          <a class="sidebar__nav__link--special" href="about-meditech/directions-to-meditech" style="border-top:0;">See directions to all our facilities.</a>
        </div>
      </aside>

    </section>


<?php // ============================== CONTACT CONFIRMATION PAGE directed to from the Webform... ==============================
  } else if($node->nid == 1172) { ?>

  <!-- start contact confirmation page section of the node--simple-page.tpl.php template -->

    <section class="container__centered">

      <div class="container__two-thirds">
        <h1><?php print $title; ?></h1>
        <?php print render($content['field_body']); ?>
      </div>

      <aside class="container__one-third panel">
        <div class="sidebar__nav">
          <h2 class="sidebar__nav__title">Facilities</h2>
          <p>Click a location for directions.</p>
          <?php print views_embed_view('locations_contact_page', 'block'); // adds 'locations contact page' Views block... ?>
          <a class="sidebar__nav__link--special" href="about-meditech/directions-to-meditech" style="border-top:0;">See directions to all our facilities.</a>
        </div>
      </aside>

    </section>


<?php
  } else if($node->nid == 1086){ // ========================== MEDIA COMMUNICATIONS PAGE... =====================================
?>

<!-- start communications page section of the node--simple-page.tpl.php template -->

<!-- Hero -->
<div class="content__callout">
  <div class="content__callout__media content__callout__bg__img--green" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-Communications-Team_hero.jpg);">
    <!-- Title -->
    <div class="content__callout__title-wrapper--green">
      <h1 class="content__callout__title js__seo-tool__title">MEDITECH Communications Team</h1>
    </div>
  </div>
  <div class="content__callout__content--green js__seo-tool__body-content">
    <p>You can find helpful background information in <a href="https://www.meditech.com/productbriefs/flyers/corporateprofile.pdf" title="Corporate Profile">Our Corporate Profile</a> and <a href="https://www.meditech.com/productbriefs/flyers/MEDITECH_Highlights.pdf" title="corporate infographic">Corporate Infographic</a>, while our <a href="<?php print $url; ?>/news" title="MEDITECH News">News page</a> contains the latest and greatest.</p>
  </div>
</div>
<!-- Hero -->

<section class="container__centered">
  <div class="container__two-thirds">

    <div class="container no-pad--bottom">

      <div class="container no-pad js__seo-tool__body-content">

      <!-- Paul Berthiaume -->
        <figure class="info__card desaturateToColorRollover">
          <div class="info__card__media__img">
            <a href="#"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/PaulBerthiaume.png"></a>
          </div>
          <figcaption class="center">
            <h3 class="header-five no-margin">Paul Berthiaume</h3>
            <p>Senior Manager, Integrated Communications<br>
              <a href="https://twitter.com/paulberthiaume" target="_blank" title="@PaulBerthiaume">@PaulBerthiaume</a>
            </p>
          </figcaption>
        </figure>
             
      <!-- Liz Carroll -->
        <figure class="info__card desaturateToColorRollover">
          <div class="info__card__media__img">
            <a href="#"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/LizCarroll.png"></a>
          </div>
          <figcaption class="center">
            <h3 class="header-five no-margin">Liz Carroll</h3>
            <p>Manager, Public and Media Relations<br>
              <a href="https://twitter.com/Liz_Carroll24" target="_blank" title="@Liz_Carroll24">@Liz_Carroll24</a>
            </p>
          </figcaption>
        </figure>
        
      <!-- Christina Noel -->
        <figure class="info__card desaturateToColorRollover">
          <div class="info__card__media__img">
            <a href="#"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/ChristinaNoel.png"></a>
          </div>
          <figcaption class="center">
            <h3 class="header-five no-margin">Christina Noel</h3>
            <p>Social Media Coordinator<br>
              <a href="https://twitter.com/Christina_Noel" target="_blank" title="@Christina_Noel">@Christina_Noel</a>
            </p>
          </figcaption>
        </figure>

      <!-- Jared Mitchell -->
        <figure class="info__card desaturateToColorRollover">
          <div class="info__card__media__img">
            <a href="#"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/JaredMitchell.png"></a>
          </div>
          <figcaption class="center">
            <h3 class="header-five no-margin">Jared Mitchell</h3>
            <p>Public and Media Relations Manager<br>
              <a href="https://twitter.com/JaredRMitchell" target="_blank" title="@JaredRMitchell">@JaredRMitchell</a>
            </p>
          </figcaption>
        </figure>
        
      <!-- Amanda Tullos -->
        <figure class="info__card desaturateToColorRollover">
          <div class="info__card__media__img">
            <a href="#"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/AmandaTullos.png"></a>
          </div>
          <figcaption class="center">
            <h3 class="header-five no-margin">Amanda Tullos</h3>
            <p>Public and Media Relations Manager<br>
              <a href="https://twitter.com/MEDITECHAmanda" target="_blank" title="@MEDITECHAmanda">@MEDITECHAmanda</a>
            </p>
          </figcaption>
        </figure>
        
      <!-- Brooke Crowther -->
        <figure class="info__card desaturateToColorRollover">
          <div class="info__card__media__img">
            <a href="#"><img src="<?php print $url; ?>/sites/all/themes/meditech/images/people/BrookeCrowther.png"></a>
          </div>
          <figcaption class="center">
            <h3 class="header-five no-margin">Brooke Crowther</h3>
            <p>Internal Communications Manager<br>
              <a href="https://twitter.com/BrookeCTweets" target="_blank" title="@BrookeCTweets">@BrookeCTweets</a>
            </p>
          </figcaption>
        </figure>

      

      </div><!-- END container -->

      <!-- CONTACT FORM -->

      <h3><i class="fas fa-envelope meditech-green"></i>&nbsp; Send us a message</h3>

      <?php
        $formContent = module_invoke('webform', 'block_view', 'client-block-1087');
        print render($formContent['content']);
      ?>
    </div>

  </div><!-- END container__two-thirds -->


  <!-- SIDEBAR -->
  <aside class="container__one-third panel communications_sidebar_gae">

    <div class="sidebar__nav js__seo-tool__body-content">
      <ul class="menu">
        <li class="first"><a href="https://www.meditech.com/productbriefs/flyers/corporateprofile.pdf">Corporate Profile</a></li>
        <li class=""><a href="https://home.meditech.com/en/d/home/otherfiles/meditechtoday101818.pdf" title="MEDITECH corporate infographic">Corporate Infographic</a></li>
        <li class=""><a href="https://home.meditech.com/en/d/newsroom/otherfiles/meditechbrandguide.pdf">MEDITECH Brand Identity Guide</a></li>
        <li class=""><a href="<?php print $url; ?>/news-tags/press-releases">MEDITECH Press Releases</a></li>
        <li class=""><a href="<?php print $url; ?>/news">MEDITECH News</a></li>
      </ul>
      <br>
      <p class="bold">Boilerplate</p>
      <p>The next digital transformation of healthcare is underway, and once again, MEDITECH is leading the charge. We’ve been unwavering in preparing for the demands of this paradigm shift, and now we’re all-in with Expanse, the only full-scale EHR designed specifically for the post-Meaningful Use era. Our cutting-edge technology is helping organizations all over the world to see healthcare through a new lens, and navigate this virtual landscape with unparalleled vision and clarity. Whether your destination is clinical efficiency, analytical prowess, or financial success, MEDITECH’s bold innovation, passion, and expertise will get you where you want to go.</p>
      <p><em>*Intended for use by media only, unless given permission otherwise.</em></p>
    </div>

    <a class="twitter-timeline" data-lang="en" data-width="360" data-height="500" data-dnt="true" data-theme="light" data-link-color="#ff610a" href="https://twitter.com/MEDITECH">Tweets by MEDITECH</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

  </aside>

</section>




<?php // ============================== MEDITECH Newsletter page (for HIMSS17)... ==============================
  } else if($node->nid == 1767) { ?>

  <!-- start newsletter page section of the node--simple-page.tpl.php template -->


    <div class="container background--cover" style="height:600px; background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/MEDITECH-HIMSS-Orlando-2017.jpg);">
      <div class="container__centered" style="padding:2em 0 2em;">
        <div class="container__one-half transparent-overlay text--white" style="font-size:2em;">
          <?php print render($content['field_body']); ?>
        </div>
      </div>
    </div>
    

    
        
<?php // ============================== Case Studies page... ==============================
  } else if($node->nid == 1863) { ?>


    <style>
      .container__thirds { float: left; display: block; margin: 2% 1.5%; padding: 2%; width: 30%; height: 320px; text-align: center; overflow: hidden;
        -moz-border-radius: 10px;
        -webkit-border-radius: 10px;
        -webkit-font-smoothing: antialiased;
        border-radius: 10px;
        position: relative;
        background-position: bottom left;
        background-repeat: no-repeat;
        background-size: 125px 125px;
      }
      .container__thirds h3 { font-size: 1em; }
      fieldset#topics { padding: 1em; }
      .topic { display: block; float: left; width: 32%; margin-right: 1%; }
      .topic:nth-child(3n) { margin-right: 0; }
      @media all and (max-width: 65em) {
         .container__thirds { float: left; display: block; margin: 2% 1.5%; padding: 2%; width: 30%; height: 400px; text-align: center; overflow: hidden; }
      }
      @media all and (max-width: 50em) {
         .topic { display: block; float: left; width: 100%; margin-right: 0; }
         .container__thirds { float: left; display: block; margin: 2% 1.5%; padding: 2%; width: 94%; height: 200px; text-align: center; overflow: hidden; }
      }

      .container__thirds.edsc { background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/news/case-studies-icon--ed-specialty--colored.png); }
      .container__thirds.frl { background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/news/case-studies-icon--fiscal-leader--colored.png); }
      .container__thirds.iqs { background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/news/case-studies-icon--quality-safety--colored.png); }
      .container__thirds.int { background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/news/case-studies-icon--interoperability--colored.png); }
      .container__thirds.pne { background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/news/case-studies-icon--physician-nurse--colored.png); }
      .container__thirds.pop { background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/news/case-studies-icon--pop-health--colored.png); }

      .container__thirds .bottom-text { text-align: right; font-size: .9em; color: #999; position: absolute; bottom: 10px; right: 20px; width: 50%; line-height: 1.15em; }
    </style>


    <!-- Hero -->
    <div class="container no-pad background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/innovators--case-study-booklet.jpg);">
      <div class="container__centered bg-overlay--black">
        <div class="container__one-third text--white" style="margin-top:3em; margin-bottom:3em;">
          <h2><span style="font-size:2em;">The Innovators:</span><br>MEDITECH Customers<br>In Action</h2>
          <p>Download our case study booklet to see how our customers are improving outcomes with real results.</p>
          <div class="center" style="margin-top:2em;">
            <?php hubspot_button('5230d784-296e-47df-96e2-88aab7a25cab', "Download The Innovators Booklet"); ?>
          </div>
        </div>
      </div>
    </div>
    <!-- Hero -->

    <section class="container__centered">

      <h1 class="js__seo-tool__title"><?php print $title; ?></h1>

      <fieldset id="topics">
      <legend>Filter by Topic:</legend>
        <?php print views_embed_view('case_study_topics', 'block'); // adds 'case study topics' Views block... ?>
      </fieldset>

      <div class="js__seo-tool__body-content">
        <?php print views_embed_view('case_studies_3_column', 'block'); // adds 'Case Studies - 3-Column' Views block... ?>
      </div>

    </section> 



    <script>  
    // table filter search function...
    // When document is ready: this gets fired before body onload...
    var $jq = jQuery.noConflict();

    $jq(document).ready(function(){
      // FILTER FUNCTION...

      // filter by checkbox selections...
      $jq("fieldset#topics input").click(function(){
        // get checked values using functions below...
        var selectedTopics = getTopicValues();

        // start fresh every time by displaying all DIVs first...
        $jq("div.container__thirds").show();

        if(selectedTopics && selectedTopics.length > 0){
          // hide all DIVs...
          $jq("div.container__thirds").hide();

          for(var t = 0; t < selectedTopics.length; t++){

            $jq("div.container__thirds").each(function(){
              var classList = (this).className.split(/\s+/);
              for(var i = 0; i < classList.length; i++){
                if( classList[i] == selectedTopics[t] ){
                  $jq(this).show();
                }
              }
            });

          }

        }
      });

      function getTopicValues(){
        // create an array to hold the values...
        var topics = [];
        // look for all location check boxes that are checked and add value to array...
        $jq("fieldset#topics input:checkbox:checked").each(function() {
          topics.push($jq(this).val());
        });
        return topics;
      }

      $jq("div.container__thirds").each(function(){
        // get height of container (could be different by device)...
        var containerH = $jq(this).height();
        // get height of inner content...
        var contentH = $jq(this).find("div.container__content").height();
        // get height difference...
        var hDiff = containerH - contentH;
        // set top and bottom margins of inner DIV...
        var newMargin = (hDiff / 2).toFixed(2);
        console.log(containerH + ' | ' + contentH + ' | ' + hDiff + ' | ' + newMargin);
        $jq("div.container__content", this).css('margin', newMargin + 'px 0');
      });

    });
    </script>
   
    
<?php // =============================== COOKIE POLICY PAGE... ============================================
  } else if($node->nid == 2402){ 
  ?>
  
  <!-- start Cookie Policy section of node--simple-page.tpl.php template -->
  <section class="container__centered">
    <div class="container__two-thirds">
      <h1 class="page__title js__seo-tool__title"><?php print $title; ?></h1>
      
      <div class="container center" style="background-color:#e6e9ee; padding:2em 1em; margin-bottom:1em;">
        <div class="container__one-half">
          <a href="#" id="doNotTrack" class="btn--orange" style="margin-bottom:1em;">Stop Tracking Me</a>
          <p class="text--small" style="margin:0; line-height:1.25em;">Clicking this button will create a cookie in your browser that will tell Hubspot NOT to track your activity on this website.</p>
        </div>
        <div class="container__one-half">
          <a href="#" id="removeCookies" class="btn--orange" style="margin-bottom:1em;">Delete Hubspot Cookies</a>
          <p class="text--small" style="margin:0; line-height:1.25em;">Clicking this button will erase all Hubspot cookies from your browser. This action will cause the tracking opt-in/opt-out question to reappear at the top of these pages.</p>
        </div>
      </div>
      
      <div class="js__seo-tool__body-content">
        <?php print render($content['field_body']); ?>
      </div>
    </div>
  </section>
          
  <!-- Start of HubSpot Embed Code -->
  <script type='text/javascript'>
    document.getElementById('doNotTrack').onclick = function(){
      _hsq.push(['doNotTrack']);
    };
    document.getElementById('removeCookies').onclick = function(){
      _hsq.push(['revokeCookieConsent']);
    };
  </script>
  <!-- End of HubSpot Embed Code -->

  <!-- End Cookie Policy section of node--simple-page.tpl.php template -->
            



<?php
  } else { // ============================ ERROR PAGES and other SIMPLE PAGES... =============================================
?>

<!-- start error page section of the node--simple-page.tpl.php template -->

    <section class="container__centered">
      <div class="container__two-thirds">
        <h1 class="page__title js__seo-tool__title"><?php print $title; ?></h1>
        <div class="js__seo-tool__body-content">
          <?php print render($content['field_body']); ?>

          <?php
          if($node->nid == 1501){ // ============ TEST VIDEO PAGE... ====================
            if(node_access('update',$node)){
            ?>
              <iframe src="https://player.vimeo.com/video/179470612?color=ffffff" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
              <p>8-17-16 EDITED Care Coordination Closing the Gap from <a href="https://vimeo.com/meditechehr">MEDITECH</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

              <div class="video video--full-width js__video" data-video-id="222391721">
                <figure class="video__overlay">
                  <?php // if the overlay image was added by user, then show overlay image, otherwise show default overlay image...
                    if( !empty($fields['field_video_overlay_image']->content) ){
                      print $fields['field_video_overlay_image']->content; 
                    }
                    else{
                    ?>
                      <img src="<?php print $url; ?>/sites/all/themes/meditech/images/video-overlay.jpg" alt="Video Covershot">
                  <?php } ?>
                </figure>
                <a href="https://vimeo.com/222391721?&autoplay=1" class="video__play-btn"></a>
                <div class="video__container"></div>
              </div>
            <?php
            }  
          }
          ?>
          
          <?php
          if($node->nid == 344){ // ================= PAGE NOT FOUND ======================
            // rather than displaying a 'Page Not Found' message, we redirect to search results page and try to provide helpful links...
            // remove main domain portain of url...
            $page_not_found = str_replace($url.'/', '', $currentURL);
            // replace certain characters with plus signs for searching...
            $items_to_replace = array('-', '_', '/', '.');
            $replace_with_these = array('+', '+', '+', '+');
            $term_to_search = str_replace($items_to_replace, $replace_with_these, $page_not_found);
            // redirect to search results ['pnf' stands for 'page not found']...
            header('Location: https://ehr.meditech.com/search-results?as_q='.$term_to_search.'&action=pnf');
            exit();
          }
          ?>
        </div>
      </div>
    </section>

<?php } ?>





<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    if($node->nid != 1863){ // don't show on case study page...
      print '<!-- SEO Tool is added to this div -->';
      print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
    }
  }
?>

<!-- end node--simple-page.tpl.php template -->