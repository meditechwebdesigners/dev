<?php // This template is set up to control the display of the INTERNATIONAL content type *********
$url = $GLOBALS['base_url']; // grabs the site url
?>
<!-- start node--international.tpl.php template -->
 

<section class="container__centered">
  <div class="container__two-thirds">

    <!-- Block 1 -->   
    <h1 class="page__title js__seo-tool__title"><?php print $title; ?></h1>
    
    <div class="js__seo-tool__body-content">

      <div class="container--clearfix">
        <?php print render($content['field_body']); ?>
      </div>
      <!-- Block 1 -->


      <!-- Block 2 -->
      <h2><?php print render($content['field_header_1']); ?></h2>

      <?php $block_2 = field_get_items('node', $node, 'field_long_text_unl_1'); ?>

      <div class="container no-pad">
        <div class="container__one-third">
          <div style="max-width:200px; height:100%;">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/americas.png" alt="">
          </div>
          <?php print $block_2[0]['value']; ?>
        </div>

        <div class="container__one-third">
          <div style="max-width:200px; height:100%;">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/united-kingdom.png" alt="">
          </div>
          <?php print $block_2[1]['value']; ?>
        </div>

        <div class="container__one-third">
          <div style="max-width:200px; height:100%;">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/about/lower-eastern-hemisphere.png" alt="">
          </div>
          <?php print $block_2[2]['value']; ?>
        </div>
      </div>
      <!-- Block 2 -->

      <hr>

      <!-- Block 3 -->
      <?php $block_3 = field_collection_data($node, 'field_fc_head_ltext_btn_1'); ?>
      <div class="container no-pad">
        <div class="container__two-thirds">
          <h2><?php print $block_3->field_header_1['und'][0]['value']; ?></h2>
          <?php print $block_3->field_long_text_1['und'][0]['value']; ?>
          <a class="btn--orange international_button_gae" href="<?php print $block_3->field_button_url_1['und'][0]['value']; ?>" target="_blank"><?php print $block_3->field_button_text_1['und'][0]['value']; ?></a>
        </div>

        <div class="container__one-third">
          <img style="margin-top: 1.5em;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/international-botswana-meditech.png" alt="">
        </div>
      </div>
      <!-- Block 3 -->

      <hr>

      <!-- Block 4 -->
      <?php 
        // get selected node IDs and put them in an array...
        $newsIDarray = field_get_items('node', $node, 'field_news_article_node_id');
        // create an array for image orientation choices...
        $imageOrientations = array();
        // get each of the image orientation choices...
        $imageCrop1 = field_get_items('node', $node, 'field_image_1_crop_orientation');
        $imageCrop2 = field_get_items('node', $node, 'field_image_2_crop_orientation');
        $imageCrop3 = field_get_items('node', $node, 'field_image_3_crop_orientation');
        // add the choices to the array...
        $imageOrientations[] = $imageCrop1[0]['taxonomy_term']->description;
        $imageOrientations[] = $imageCrop2[0]['taxonomy_term']->description;
        $imageOrientations[] = $imageCrop3[0]['taxonomy_term']->description;

        // loop through the 3 news articles to generate HTML...
        for($n=0; $n<3; $n++){ 
          $article = node_load($newsIDarray[$n]['value']);
      ?> 
          <figure class="container no-pad">

            <div class="container__one-third">
              <div class="square-img-cropper <?php print $imageOrientations[$n]; ?>">
                <img src="<?php print $url; ?>/sites/default/files/styles/main-article-image-breakpoints_theme_meditech_small-small_2x/public/images/news/<?php print $article->field_news_article_main_image['und'][0]['filename']; ?>" alt="">
              </div>
            </div>

            <div class="container__two-thirds">
              <h3 class="header-four no-margin"><a class="international_news_link_gae" href="<?php print $url.'/'.$article->path['alias']; ?>"><?php print $article->title; ?></a></h3>
              <div>
                <div><?php print $article->field_summary['und'][0]['value']; ?></div>
              </div>
            </div>

          </figure>

          <hr>
      <?php } ?>
      <!-- Block 4 -->


      <!-- Block 5 -->
      <div class="container no-pad">

        <h2><?php print render($content['field_header_2']); ?></h2>

        <?php $block_5 = field_get_items('node', $node, 'field_long_text_unl_2'); ?>

        <div>
          <div class="container__one-fourth list-style--none">
            <?php print $block_5[0]['value']; ?>
          </div>

          <div class="container__one-fourth list-style--none">
            <?php print $block_5[1]['value']; ?>
          </div>

          <div class="container__one-fourth list-style--none">
            <?php print $block_5[2]['value']; ?>
          </div>

          <div class="container__one-fourth list-style--none">
            <?php print $block_5[3]['value']; ?>
          </div>
        </div>

        <div>
          <?php print render($content['field_long_text_1']); ?>
        </div>

      </div>
      <!-- Block 5 -->
    
    </div><!-- End .js__seo-tool__body-content -->

    <?php // SEO tool for internal use...
      if(node_access('update',$node)){
        print '<!-- SEO Tool is added to this div -->';
        print '<div class="container js__seo-tool"></div>';
      } 
    ?>    

  </div>    
  
  
  <?php // add ABOUT SIDE NAV =========================================================================== ?>
  <aside class="container__one-third panel">
    <div class="sidebar__nav">
      <?php
        $massnBlock = module_invoke('menu', 'block_view', 'menu-about-section-side-nav');
        print render($massnBlock['content']); 
      ?>
    </div>
  </aside>
    
</section>


<!-- end node--international.tpl.php template -->