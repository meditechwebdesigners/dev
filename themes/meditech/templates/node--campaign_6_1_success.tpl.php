<?php // This template is set up to control the display of the CAMPAIGN_6_1_SUCCESS content type

$url = $GLOBALS['base_url']; // grabs the site url

// FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM with mulitple instances...
// to use the function simply assign it to a variable
// then pass in the field collection machine name to the function and the number of instances allowed ($quantity)
// then you'll use the variable when calling an array for each group of sub-fields within the template
function multi_field_collection_data($node, $field_name, $quantity){
  // get array specific to a certain field collection item...
  $field_collection = field_get_items('node', $node, $field_name); 
  // create array...
  $multi_field_collections = array();
  // loop through '$quantity' times...
  for($x=0; $x<$quantity; $x++){
    // get all content within that field collection...
    $fc_field_data = entity_load('field_collection_item', array($field_collection[$x]['value']));
    // get field collection ID...
    $fc_field_ID = key($fc_field_data);
    // create variable and load it with field collection data...
    $var_with_data = $fc_field_data[$fc_field_ID];
    // add data to array...
    $multi_field_collections[] = $var_with_data;
  }
  // return variable...
  return $multi_field_collections;
}
$quotes = multi_field_collection_data($node, 'field_fc_quote_1', 3);

// FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM...
// to use the function simply assign it to a variable
// then pass in the field collection machine name to the function
// then you'll use the variable when calling each sub-field within the template
function field_collection_data($node, $field_name){
  // get array specific to a certain field collection item...
  $field_collection = field_get_items('node', $node, $field_name); 
  // get all content within that field collection...
  $fc_field_data = entity_load('field_collection_item', array($field_collection[0]['value']));
  // get field collection ID...
  $fc_field_ID = key($fc_field_data);
  // create variable and load it with field collection data...
  $var_with_data = $fc_field_data[$fc_field_ID];
  // return variable...
  return $var_with_data;
}
$button_1 = field_collection_data($node, 'field_fc_button_1');
?>
  <!-- start node--campaign_6_1_success.tpl.php template -->

  <div class="js__seo-tool__body-content">

    <style>
      .button-vertical-adjustment { margin-top: 6em; }
      @media (max-width: 800px) {
        .button-vertical-adjustment { margin-top: 1em; }
      }
      .tranparent-with-p p { margin: 0; }
      .spacing { margin-bottom: 1em; }
      @media (max-width: 800px) {
        .spacing { margin-bottom: 0; }
        .transparent-overlay { margin-bottom: 1em; }
      }
    </style>

    <!-- BLOCK 1 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/physician-using-6.1-software.jpg);">
      <div class="container__centered">
        <div class="container__two-thirds transparent-overlay text--white">
          <h1 style="margin-top:0;"><?php print $title; ?></h1>
          <h2 class="text--white"><?php print render($content['field_header_1']); ?></h2>
          <?php print render($content['field_long_text_1']); ?>
        </div>
        <div class="container__one-third center">
          <?php 
          // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
          if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
            $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
          ?>
            <div class="button--hubspot button-vertical-adjustment">
              <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="Download Our Interoperability White Paper"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
            </div>
          <?php }else{ ?>
            <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
          <?php } ?>
        </div>
      </div>
    </div>
    <!-- BLOCK 1 -->

    <!-- BLOCK 2 -->
    <!-- this is usually a View but campaigns tend to use there own text rather than original text upload with video -->
    <div class="content__callout border-none">
      <div class="content__callout__media">
        <h2 class="content__callout__title"><?php print render($content['field_header_2']); ?></h2>
        <div class="content__callout__image-wrapper">
          <div class="video js__video" data-video-id="<?php print render($content['field_video_url']); ?>">
            <figure class="video__overlay">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--6.1-physicians-nurses.jpg" alt="Video Covershot">
            </figure>
            <a class="video__play-btn video_gae" href="http://vimeo.com/<?php print render($content['field_video_url']); ?>"></a>
            <div class="video__container"></div>
          </div>
        </div>
      </div>
      <div class="content__callout__content">
        <div class="content__callout__body">
          <div class="content__callout__body__text">
            <?php print render($content['field_long_text_2']); ?>
          </div>
        </div>
      </div>
    </div>
    <!-- BLOCK 2 -->


    <!-- BLOCK 3 -->
    <?php $block_3 = field_get_items('node', $node, 'field_long_text_unl_1'); ?>
      <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/6.1-customers-growing-blurred.jpg);">
        <div class="container__centered text--white">
          <h2><?php print render($content['field_header_3']); ?></h2>
          <?php print render($content['field_long_text_3']); ?>

            <div class="container no-pad spacing">
              <div class="container__one-half transparent-overlay tranparent-with-p">
                <?php print $block_3[0]['value']; ?>
              </div>
              <div class="container__one-half transparent-overlay tranparent-with-p">
                <?php print $block_3[1]['value']; ?>
              </div>
            </div>
            <div class="container no-pad">
              <div class="container__one-half transparent-overlay tranparent-with-p">
                <?php print $block_3[2]['value']; ?>
              </div>
              <div class="container__one-half transparent-overlay tranparent-with-p">
                <?php print $block_3[3]['value']; ?>
              </div>
            </div>
        </div>
      </div>
      <!-- BLOCK 3 -->


      <!-- BLOCK 4 -->
      <!-- this is usually a View but campaigns tend to use there own text rather than original text upload with video -->
      <div class="content__callout border-none">
        <div class="content__callout__media">
          <h2 class="content__callout__title"><?php print render($content['field_header_4']); ?></h2>
          <div class="content__callout__image-wrapper">
            <div class="video js__video" data-video-id="<?php print render($content['field_video_url_2']); ?>">
              <figure class="video__overlay">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--6.1-ease-of-use.jpg" alt="Video Covershot">
              </figure>
              <a class="video__play-btn video_gae" href="http://vimeo.com/<?php print render($content['field_video_url_2']); ?>"></a>
              <div class="video__container"></div>
            </div>
          </div>
        </div>
        <div class="content__callout__content">
          <div class="content__callout__body">
            <div class="content__callout__body__text">
              <?php print render($content['field_long_text_4']); ?>
            </div>
          </div>
        </div>
      </div>
      <!-- BLOCK 4 -->


      <!-- BLOCK 5 -->
      <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/6.1-customer-success--quotes.jpg);">
        <div class="container__centered">
          <?php for($n=0; $n<3; $n++){ ?>
            <div class="container__one-third">
              <div class="transparent-overlay">
                <div style="text-align:center; color:#6AC4A9; padding-bottom:1em;"><i class="fa fa-comments-o fa-3x"></i></div>
                <div class="text--white">
                  <p>"
                    <?php print $quotes[$n]->field_quote_1['und'][0]['value']; ?>"</p>
                  <p class="text--large no-margin--bottom">
                    <?php print $quotes[$n]->field_full_name['und'][0]['value']; ?>
                  </p>
                  <p>
                    <?php print $quotes[$n]->field_company['und'][0]['value']; ?>
                  </p>
                </div>
              </div>
            </div>
            <?php } ?>
        </div>
      </div>
      <!-- BLOCK 5 -->


      <!-- BLOCK 6 -->
      <!-- this is usually a View but campaigns tend to use there own text rather than original text upload with video -->
      <div class="content__callout border-none">
        <div class="content__callout__media">
          <h2 class="content__callout__title"><?php print render($content['field_header_5']); ?></h2>
          <div class="content__callout__image-wrapper">
            <div class="video js__video" data-video-id="<?php print render($content['field_video_url_3']); ?>">
              <figure class="video__overlay">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--6.1-functionality.jpg" alt="Video Covershot">
              </figure>
              <a class="video__play-btn video_gae" href="http://vimeo.com/<?php print render($content['field_video_url_3']); ?>"></a>
              <div class="video__container"></div>
            </div>
          </div>
        </div>
        <div class="content__callout__content">
          <div class="content__callout__body">
            <div class="content__callout__body__text">
              <?php print render($content['field_long_text_5']); ?>
            </div>
          </div>
        </div>
      </div>
      <!-- BLOCK 6 -->

  </div>
  <!-- end js__seo-tool__body-content -->

  <!-- BLOCK 7 -->
  <div class="container bg--black-coconut" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/dark-grey-triangle-pattern.png);">
    <div class="container__centered" style="text-align: center;">
      <h2 class="text--white js__seo-tool__body-content"><?php print render($content['field_header_6']); ?></h2>
      <?php 
      // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
      if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
        $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
      ?>
        <div class="button--hubspot">
          <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="Download Our Interoperability White Paper"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
        </div>
      <?php }else{ ?>
        <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
      <?php } ?>

      <div class="sharethis-centered">
        <?php
          $block = block_load('sharethis', 'sharethis_block');
          $render_array = _block_get_renderable_array(_block_render_blocks(array($block)));
          print render($render_array);
        ?>
      </div>
    </div>
  </div>
  <!-- BLOCK 7 -->

  <?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>

    <!-- end node--campaign_6_1_success.tpl.php template -->