<!-- START campaign--node-2225.php -->
<?php
$url = $GLOBALS['base_url']; // grabs the site url

// FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM...
// to use the function simply assign it to a variable
// then pass in the field collection machine name to the function
// then you'll use the variable when calling each sub-field within the template
function field_collection_data($node, $field_name){
  // get array specific to a certain field collection item...
  $field_collection = field_get_items('node', $node, $field_name);
  // get all content within that field collection...
  $fc_field_data = entity_load('field_collection_item', array($field_collection[0]['value']));
  // get field collection ID...
  $fc_field_ID = key($fc_field_data);
  // create variable and load it with field collection data...
  $var_with_data = $fc_field_data[$fc_field_ID];
  // return variable...
  return $var_with_data;
}
$button_1 = field_collection_data($node, 'field_fc_button_1');

$cta = field_collection_data($node, 'field_fc_cta_block');
?>

  <style>
    .header-tagline {
      padding-bottom: 8em;
    }

    @media all and (max-width: 68.750em) {
      .header-tagline {
        padding-bottom: 4em;
      }
    }

  </style>

  <div class="js__seo-tool__body-content">

    <!-- Block 1 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/starry-expanse-mountains.jpg); background-position:bottom;">
      <div class="container__centered">
        <div class="container">
          <img style="padding-left:2em; padding-right:2em;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/expanse-logo.png" alt="MEDITECH Expanse Logo">
        </div>
        <div class="container header-tagline center no-pad--top text--white text-shadow--black">
          <h1 class="js__seo-tool__title">One EHR, no limits. Welcome to the new vision of healthcare.</h1>
        </div>
      </div>
    </div>
    <!-- End Block 1 -->

    <!-- Block 2 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/a-new-level-of-clarity.jpg);">
      <div class="container__centered">
        <div class="container__one-third">
          <div class="transparent-overlay text--white" style="padding:2em;">
            <h2>A new level of clarity.</h2>
            <p><a href="https://ehr.meditech.com/videos/meditech-expanse-experience-the-difference">See healthcare through a whole new lens</a> with tools that help you navigate the digital frontier with confidence. Expanse reaches across the care continuum and pulls in all the data you need to see the big picture and <a href="https://ehr.meditech.com/ehr-solutions/meditechs-care-coordination">treat the whole patient</a>. And with customizable layouts and widgets at your fingertips, you’ll be looking at everything you want to see (and nothing that you don’t).</p>
          </div>
        </div>
      </div>
    </div>
    <!-- End Block 2 -->

    <!-- Block 3 -->
    <div class="content__callout border-none" style="background-color: #E6E9EE !important;">
      <div class="content__callout__media">

        <div class="content__callout__image-wrapper" style="padding:3em !important;">
          <div class="video js__video video--shadow" data-video-id="286876430">
            <figure class="video__overlay">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--expanse-poem.jpg" alt="Putting the Physician First Expanse Video Covershot">
            </figure>
            <a class="video__play-btn" href="http://vimeo.com/286876430"></a>
            <div class="video__container"></div>
          </div>
        </div>
      </div>
      <div class="content__callout__content">
        <div class="content__callout__body">
          <div class="content__callout__body__text">
            <h2>Putting <span class="italic">you</span> first.</h2>
            <p>The best technologies add value to your life. Expanse prioritizes user satisfaction like no other EHR, and delivers a comfortable, personalized experience to make your day easier, and your work more productive. That means more face-to-face time with your patients, and <a href="https://player.vimeo.com/video/283056884" target="_blank">more time for you to recharge</a> outside the clinical setting.</p>
          </div>
        </div>
      </div>
    </div>
    <!-- End Block 3 -->

    <!-- Block 4 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/break-down-the-walls.jpg);">
      <div class="container__centered">
        <div class="container__one-third text--white">
          <div class="transparent-overlay" style="padding:2em;">
            <h2>Break down the walls.</h2>
            <p>Healthcare doesn’t just happen in one place — you need an EHR to connect the dots. With MEDITECH Expanse, a single, <a href="https://ehr.meditech.com/ehr-solutions/meditech-interoperability">fully interoperable</a> system navigates the twists and turns of every care setting — so you can <a href="https://ehr.meditech.com/ehr-solutions/bridging-the-gaps-in-care">bridge gaps in care</a> and effectively treat the whole patient, wherever they may be in their journey. And with Virtual Visits and our <a href="https://ehr.meditech.com/ehr-solutions/to-improve-patient-engagement-focus-on-the-patient">Patient and Consumer Health Portal</a>, patients are never out of reach.</p>
          </div>
        </div>
      </div>
    </div>
    <!-- End Block 4 -->

    <!-- Block 5 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/an-untethered-care-experience.jpg); background-position:top;">
      <div class="container__centered">
        <div class="container__two-thirds">&nbsp;
        </div>
        <div class="container__one-third">
          <div class="transparent-overlay text--white" style="padding:2em;">
            <h2>An untethered care experience.</h2>
            <p>Why shouldn't your EHR be as easy and convenient as your favorite apps? Expanse takes full advantage of <a href="https://ehr.meditech.com/ehr-solutions/ehr-mobility">mobile technology</a>, untethering you from your desktop so you can tap and swipe through your charts more efficiently.</p>
          </div>
        </div>
      </div>
    </div>
    <!-- End Block 5 -->

    <!-- Block 6 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/a-keen-eye-for-quality.jpg);">
      <div class="container__centered">
        <div class="container__one-half">
          <div class="transparent-overlay text--white" style="padding:2em;">
            <h2>A keen eye for quality.</h2>
            <p>Steer clear of problems before they happen with tools that give you an extra set of eyes. Expanse’s <a href="https://info.meditech.com/meditechs-surveillance-identifies-and-prevents-infections-at-valley-case-study">surveillance solutions</a> provide you with meaningful alerts, so you can monitor at-risk patients and guard against HACs and life-threatening conditions. Meanwhile, our <a href="https://www.meditech.com/productbriefs/flyers/Quality_Vantage_Dashboard_Flyer.pdf">Quality Vantage Dashboards</a> assess performance at the individual, practice, and group level to detect costly, unnecessary variations in care.</p>
          </div>
        </div>
      </div>
    </div>
    <!-- End Block 6 -->

    <!-- Block 6 -->
    <div class="container bg--blue-gradient">
      <div class="container__centered">
        <div class="container__one-half">
          <h2>Prove your value.</h2>
          <p>As the healthcare paradigm continues its shift from volume-based to value-based care, you need an EHR to help you make the transition. Expanse provides you with a host of tools like patient registries to <a href="https://ehr.meditech.com/ehr-solutions/meditech-population-health">proactively manage patient populations </a>and keep up with new payment models. In addition, <a href="https://ehr.meditech.com/ehr-solutions/meditech-business-clinical-analytics">Business and Clinical Analytics</a> enables you to visualize data, monitor trends, and make more informed decisions to keep both your patients and your bottom line healthy.</p>
        </div>
        <div class="container__one-half">
          <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-screenshot-bca-quality.jpg" alt="">
        </div>
      </div>
    </div>
    <!-- End Block 6 -->

    <!-- Block 7 Quote -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/vast-expanse-night-sky.jpg); background-position: middle; overflow:hidden;">
      <div class="container__centered">
        <figure class="container__one-fourth center">
          <img class="quote__content__img" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/joe-farr.png" alt="Justin Smith MD">
        </figure>
        <div class="container__three-fourths text--white">
          <div class="quote__content__text text--large" style="margin-top:1em;">
            <p class="italic">"Expanse Point of Care technology has generated more excitement than any nursing product to date! We’re confident it will solve our current logistical challenges and introduce new efficiencies for bedside providers, while still maintaining the highest patient safety standards."</p>
          </div>
          <p class="text--large no-margin--bottom"><span class="bold">Joe Farr — RN, Clinical Applications Coordinator</span></p>
          <p>King’s Daughters Medical Center</p>
        </div>
      </div>
    </div>
    <!-- End Block 7 Quote -->

  </div>
  <!-- end js__seo-tool__body-content -->

  <!-- Block 8 - CTA Block -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/white-x-texture.jpg);">
    <div class="container__centered center">
      <img style="padding: 2em;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/expanse-logo--sm.png" alt="MEDITECH Expanse Logo">

      <h2>Want to see more of Expanse in action?</h2>
      <p>Sign up for a special webinar to see how Expanse’s <a href="https://www.meditech.com/productbriefs/flyers/Web_Point_of_Care.pdf">Nursing Point of Care</a> solution can get your workflow moving.
      </p>
      <div class="btn-holder--content__callout center" style="margin-bottom:1.5em;">
        <a href="https://info.meditech.com/webinar_webpointofcare_092518-0-0" class="btn--orange home_page_gae">Sign up for Expanse Point of Care Webinar</a>
      </div>
    </div>
  </div>
  <!-- End Block 8 -->

  <?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>

  <!-- END campaign--node-2225.php -->
