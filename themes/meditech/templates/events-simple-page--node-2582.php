<!-- start events-simple-page--node-2582.php MGMA template -->
<?php $url = $GLOBALS['base_url']; // grabs the site url ?>
      
<div class="container background--cover js__seo-tool__body-content" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/abstract-x-background-beige.jpg);">
  
  <div class="container__centered">

    <div clas="container no-pad">
      <?php print render($content['field_body']); ?>
    </div>

    <div class="container" style="padding-top:1em;">
      <h2>Demonstrations</h2>

      <div class="container__one-half">
        <h3>Practice Management</h3>
        <h4>October 16, 2018</h4>
        <p>Learn how Expanse Ambulatory simplifies your practice management workflow from front office to back. Scheduling, check-in, eligibility checks, co-payment collection, coding, and revenue cycle management are all integrated into one complete solution to manage all of your practice needs. </p>
        <div class="center"><a class="btn--orange" href="https://info.meditech.com/webinar_ambpracticemanagement_101618_0-0">Sign Up</a></div>
      </div>
       
      <div class="container__one-half">
        <h3>Mastering the Physician Mindset</h3>
        <h4>November 7, 2018</h4>
        <p>See how MEDITECH Expanse offers physicians the modern, personalized workflows they deserve, through mobile web-based navigation.</p>
        <div class="center"><a class="btn--orange" href="https://info.meditech.com/webinar_masteringthephysmindset_110718_0-">Sign Up</a></div>
      </div>
    </div>

    <div class="center">
      <p>To learn more about Expanse Ambulatory and what it can do for you, check out the resources below.</p>
    </div>
  
  </div>

</div>


<div class="container">
  
  <div class="container__centered" id="videos">

    <h2>Videos</h2>  

    <style> 
      .video-iframe { position:relative; padding-bottom:56.25%; padding-top:35px; height:0; overflow:hidden; margin-bottom:1em; background-color:black; } 
      .video-iframe iframe { position:absolute; top:0; left:0; width:100%; height:100%; } 
    </style> 

    <div>
      <div class="container__one-half center" style="margin-bottom:1em;">
        <div class="video-iframe">
          <iframe width="500" height="315" src="https://www.youtube.com/embed/tAABPgpKHGo" frameborder="0" allowfullscreen></iframe>  
        </div>
        <p>What Physicians Love About Expanse</p>
      </div>
      
      <div class="container__one-half center" style="margin-bottom:1em;">
        <div class="video-iframe">
          <iframe width="500" height="315" src="https://www.youtube.com/embed/NT__Z99fPVk" frameborder="0" allowfullscreen></iframe>  
        </div>
        <p>Patient Registries from Dr. Fletcher at Hancock Regional Hospital</p>
      </div>
    </div>

    <div>
      <div class="container__one-half center" style="margin-bottom:1em;">
        <div class="video-iframe">
          <iframe width="500" height="315" src="https://www.youtube.com/embed/r3KjbS1Ch5I" frameborder="0" allowfullscreen></iframe>  
        </div>
        <p>How Physicians Can Get More Work Done in Less Time with MEDITECH</p>
      </div>
      
      <div class="container__one-half center" style="margin-bottom:1em;">
        <div class="video-iframe">
          <iframe width="500" height="315" src="https://www.youtube.com/embed/FIkLnYmzwjQ" frameborder="0" allowfullscreen></iframe>  
        </div>
        <p>MEDITECH Ambulatory Boosts Productivity for Dr. Kanis</p>
      </div>
    </div>
    
    <div>
      <div class="container__one-half center" style="margin-bottom:1em;">
        <div class="video-iframe">
          <iframe width="500" height="315" src="https://www.youtube.com/embed/7se5lG0hn-8" frameborder="0" allowfullscreen></iframe>  
        </div>
        <p>How Do Doctors Want to Spend their Free Time?</p>
      </div>
      
      <div class="container__one-half center" style="margin-bottom:1em;">
        <div class="video-iframe">
          <iframe src="https://player.vimeo.com/video/288754788" width="500" height="315" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
        <p>A Tour Of Palo Pinto General Hospital's Bridge to Health Mobile Van</p>
      </div>
    </div>
    
  </div>
  
</div>
 
  
<div class="container background--cover js__seo-tool__body-content" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Gradient-Background--Blue.jpg); background-color:#0075a8;">
  
  <div class="container__centered text--white">  

    <h2>Blogs</h2>        
    <ul>
      <li><a href="https://blog.meditech.com/how-meditech-fulfills-amas-8-ehr-usability-principles">How MEDITECH fulfills the AMA’s 8 EHR Usability Principles</a></li>
      <li><a href="https://blog.meditech.com/how-ehrs-can-give-time-back-to-docs">How EHRs can give time back to docs</a></li>
      <li><a href="https://blog.meditech.com/best-practices-clinical-communications-rocket-reuben-rubberband">Best practices in clinical communications: The Rocket, The Reuben, and The Rubberband</a></li>
      <li><a href="https://blog.meditech.com/how-to-eliminate-alert-fatigue-0">How to eliminate alert fatigue</a></li>
    </ul>
    
  </div>
    
</div>
   
    
<div class="container background--cover hide__bg-image--mobile" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/african-american-physician-with-patient.jpg);">
  
  <div class="container__centered">     

    <h2>Solution Details</h2>        
    <ul>
      <li><a href="https://ehr.meditech.com/ehr-solutions/meditech-ambulatory">Benefits of Expanse Ambulatory</a></li>
      <li><a href="https://info.meditech.com/meditechs-building-a-foundation-for-population-health-patient-registries-white-paper">Download our Patient Registry White Paper</a></li>
      <li><a href="https://www.meditech.com/productbriefs/flyers/Quality_Vantage_Dashboard_Flyer.pdf">Learn About our Quality Vantage Dashboards</a></li>
      <li>Review our <a href="https://www.meditech.com/productbriefs/pages/Product_Briefs/Ambulatory_Clinical.pdf">Ambulatory</a> and <a href="https://www.meditech.com/productbriefs/pages/Product_Briefs/Ambulatory_Practice_Management.pdf">Practice Management</a> Functionality Briefs</li>
      <li><a href="https://www.meditech.com/productbriefs/flyers/Populationhealth.pdf">See How MEDITECH Supports Population Health</a></li>
    </ul>
    
  </div>
    
</div>
   
    
<div class="container" style="background-color:#01506b;">
  
  <div class="container__centered text--white"> 
    
    <div class="container__one-half background--cover" style="background-image: url(<?php print $url; ?>/sites/default/files/images/events/2018-physician-cio-forum--event-page.jpg); min-height:250px;">&nbsp;</div>    
    
    <div class="container__one-half">
      <div style="margin:5%;">
        <h2 style="text-align:center;">Physician and CIO Forum</h2>
        <h3 style="text-align:center;">October 16-18</h3>
        <p>MEDITECH customers, be sure to sign up for our Physician and CIO Forum and join the discussion on Twitter <a href="https://twitter.com/hashtag/mdcio2018" target="_blank">#MDCIO2018</a>.</p>
        <div class="center">
          <a class="btn--orange event_reg_gae" href="https://info.meditech.com/physician_cio_forum_101718-0">Register Today!</a>
        </div>
      </div>
    </div>
    
  </div>

</div>
<!-- END events-simple-page--node-2582.php MGMA template -->