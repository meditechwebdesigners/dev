<?php // This template is set up to control the display of the CAMPAIGN_ONCOLOGY content type 
  
$url = $GLOBALS['base_url']; // grabs the site url 

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$button_1 = field_collection_data($node, 'field_fc_button_1');
?>
<!-- start node--campaign_oncology.tpl.php template -->


<div class="js__seo-tool__body-content">

  <!-- BLOCK 1 -->
  <div class="container background--cover hide__bg-image--tablet" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/oncology-patient.jpg);">
    <div class="container__centered">
      <div class="container__two-thirds">
        <h1 class="js__seo-tool__title" style="margin-top:0;"><?php print $title; ?></h1>
        <h2><?php print render($content['field_header_1']); ?></h2>
        <div>
          <?php print render($content['field_long_text_1']); ?>
        </div>
        <div class="center">
          <?php 
          // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
          if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
            $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
          ?>
            <div class="button--hubspot">
              <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
            </div>
          <?php }else{ ?>
            <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
  <!-- BLOCK 1 -->


  <!-- BLOCK 2 -->
  <div class="container bg--black-coconut" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/green-pattern-01.png);">
    <div class="container__centered">
      <div class="page__title--center">
        <h2 class="text--white"><?php print render($content['field_header_2']); ?></h2>
      </div>
      <div class="container no-pad">
        <div class="container__two-thirds text--white">
          <?php print render($content['field_long_text_2']); ?>
        </div>
        <div class="container__one-third center">
          <figure>
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-oncology-diagram.png" alt="MEDITECH oncology diagram">
          </figure>
        </div>
      </div>
    </div>
  </div>
  <!-- BLOCK 2 -->


  <!-- BLOCK 3 -->
  <div class="content__callout border-none">
    <div class="content__callout__media">
      <div class="content__callout__image-wrapper">
        <div class="video js__video" data-video-id="193753449">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/default/files/styles/video-overlay-breakpoints_theme_meditech_big-medium_2x/public/images/video/Clatterbridge-Cancer-Centre_viceo-overlay.png" alt="Video Covershot">
          </figure>
          <a class="video__play-btn" href="https://vimeo.com/193753449"></a>
          <div class="video__container">
          </div>
        </div>
      </div>
    </div>
    <div class="content__callout__content">
      <div class="content__callout__body">
         <h2>MEDITECH’s Oncology Management Brings Efficiencies to Clatterbridge Cancer Centre</h2>
          <p>Clatterbridge Cancer Centre NHS Foundation Trust (Wirral, England) is experiencing greater efficiencies in care delivery, thanks to MEDITECH's Oncology Management solution. Download our <a href="https://info.meditech.com/case-study-clatterbridge-cancer-centre-1">case study</a> to learn how CCC oncologists have reduced ePrescribing steps from 15 down to 8 when ordering chemotherapy with multiple cycles.</p>
      </div>
    </div>
  </div>
  <!-- BLOCK 3 -->


  <!-- BLOCK 4 -->
  <div class="container">
    <div class="container__centered">

      <div class="page__title--center" style="margin-bottom:2em;">     
        <h2><?php print render($content['field_header_4']); ?></h2>
        <h3><?php print render($content['field_sub_header_1']); ?></h3>
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-oncology-screenshot.png" alt="MEDITECH Oncology Screenshot">
      </div>

      <?php $block_4_text = field_get_items('node', $node, 'field_long_text_unl_1'); ?>

      <div class="container__one-half">
        <?php print $block_4_text[0]['value']; ?>
      </div>
      <div class="container__one-half">
        <?php print $block_4_text[1]['value']; ?>
      </div>

    </div>
  </div>
  <!-- BLOCK 4 -->
  
  
  <!-- VIDEO 2 -->
  <div class="content__callout border-none">
    <div class="content__callout__media">
      <div class="content__callout__image-wrapper">
        <div class="video js__video" data-video-id="<?php print render($content['field_video_url']); ?>">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--oncology-campaign.jpg" alt="Video Covershot">
          </figure>
          <a class="video__play-btn" href="http://vimeo.com/<?php print render($content['field_video_url']); ?>"></a>
          <div class="video__container">
          </div>
        </div>
      </div>
    </div>
    <div class="content__callout__content">
      <div class="content__callout__body">
         <h2><?php print render($content['field_header_3']); ?></h2>
          <?php print render($content['field_long_text_3']); ?>
      </div>
    </div>
  </div>
  <!-- VIDEO 2 -->


  <!-- BLOCK 5 -->
  <?php $block_5 = field_collection_data($node, 'field_fc_quote_1'); ?>
  <!--
  <div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/dark-grey-squares-pattern.jpg);">
    <div class="container__centered">
      <figure class="container__one-fourth center">
        <img class="quote__content__img" src="<?php print $url; ?>/sites/all/themes/meditech/images/quote.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/quote.png';this.onerror=null;" alt="quote bubble">
      </figure>
      <div class="container__three-fourths text--white">
        <div class="quote__content__text text--large">
          <p><?php print $block_5->field_quote_1['und'][0]['value']; ?></p>
        </div>
        <p class="text--large no-margin--bottom"><?php print $block_5->field_full_name['und'][0]['value']; ?></p>
        <p><?php print $block_5->field_company['und'][0]['value']; ?></p>
      </div>
    </div>
  </div>
  -->
  <!-- BLOCK 5 -->

</div><!-- end js__seo-tool__body-content -->

<!-- BLOCK 6 -->
<div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/light-grey-squares-pattern.jpg);">
  <div class="container__centered center">
    <h2 class="js__seo-tool__body-content"><?php print render($content['field_header_5']); ?></h2>
    <?php 
    // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
    if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
      $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
    ?>
      <div class="button--hubspot">
        <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
      </div>
    <?php }else{ ?>
      <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
    <?php } ?>
    
      <div style="margin-top:1em;">
        <?php print $share_link_buttons; ?>
      </div>
  </div>
</div>
<!-- BLOCK 6 -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>  
<!-- end node--campaign_oncology.tpl.php template -->