<!-- START campaign--node-2538.php -->
<?php // This template is set up to control the display of the MACRA content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>
<style>
    .pie-chart--key-color-square {
        float: left;
        width: 20px;
        height: 20px;
        margin: 5px;
        border: 1px solid rgba(255, 255, 255, 0.2);
    }

    .center-left-align {
        padding-left: 4em;
        text-align: left;
    }

    @media all and (max-width: 65em) {
        .center-left-align {
            padding-left: 0em;
        }
    }

    .center--image {
        display: block;
        margin-left: auto;
        margin-right: auto;
        width: 50%;
    }

    .transparent-overlay--white {
        padding: 1em;
        background-color: rgba(255, 255, 255, 0.8);
        min-height: 5em;
    }

</style>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

<!-- Hero -->
<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/elder-backpacker-hiking-down-trail-in-wooded-mountains.jpg);">
    <div class="container__centered">
        <div class="container__one-half">
            &nbsp;
        </div>
        <div class="container__one-half transparent-overlay text--white">
            <h1>
                Guiding You on Your MACRA Journey.
            </h1>
            <p>
                You've charted your course for MACRA. But with each update to the ruling, the map is being redrawn. Do you stay the course or rethink your route? Do you stay on the MIPS path, or are you ready to give Advanced APMs a try? The answer is not always simple. We're here to help.
            </p>
        </div>
    </div>
</div>
<!-- END Hero -->
<!-- Block 2 -->
<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/forked-road-at-sunset.jpg);">
    <div class="container__centered text--white">
        <div class="page__title--center">
            <div class="container no-pad">
                <h2>
                    Choosing Your Route.
                </h2>
                <p>
                    You begin your journey faced with two options, each with its own set of rules. Choose your adventure:
                </p>
            </div>
            <div class="container no-pad--top">
                <div class="container__one-half transparent-overlay">
                    <div>
                        <img src="/sites/all/themes/meditech/images/campaigns/ribbon-line-icon.png" alt="Ribbon Icon">
                    </div>
                    <h3>
                        Merit-based Incentive Payment System (MIPS) Path
                    </h3>
                    <strong>
                        Proceed if:
                    </strong>
                    <ul class="center-left-align" style="list-style-type: none;">
                        <li>
                            <i class="fas fa-check-square text--meditech-green" style="padding-right: 0.5em;"></i> You are not a qualified participant in an APM
                        </li>
                        <li>
                            <i class="fas fa-check-square text--meditech-green" style="padding-right: 0.5em;"></i> You bill for Medicare B
                        </li>
                        <li>
                            <i class="fas fa-check-square text--meditech-green" style="padding-right: 0.5em;"></i> Your billed charges > $90,000
                        </li>
                        <li>
                            <i class="fas fa-check-square text--meditech-green" style="padding-right: 0.5em;"></i> Your beneficiaries > 200
                        </li>
                        <li>
                            <i class="fas fa-check-square text--meditech-green" style="padding-right: 0.5em;"></i> You hold an eligible role
                        </li>
                    </ul>
                </div>
                <div class="container__one-half transparent-overlay">
                    <div>
                        <img src="/sites/all/themes/meditech/images/campaigns/hospital-line-icon.png" alt="Hospital Icon">
                    </div>
                    <h3>
                        Advanced Payment Models (APM) Options
                    </h3>
                    <ul class="center-left-align" style="list-style-type: none;">
                        <li>
                            <i class="fas fa-genderless text--meditech-green" style="padding-right: 0.5em;"></i> Medicare Shared Savings Program Track 2 & 3
                        </li>
                        <li>
                            <i class="fas fa-genderless text--meditech-green" style="padding-right: 0.5em;"></i> Next Gen ACOs
                        </li>
                        <li>
                            <i class="fas fa-genderless text--meditech-green" style="padding-right: 0.5em;"></i> Oncology Care Model Two-Sided Risk Arrangement
                        </li>
                        <li>
                            <i class="fas fa-genderless text--meditech-green" style="padding-right: 0.5em;"></i> Comprehensive ESRD Care Model (Large Dialysis Organization Arrangement)
                        </li>
                        <li>
                            <i class="fas fa-genderless text--meditech-green" style="padding-right: 0.5em;"></i> Comprehensive Primary Care Plus (CPC+)
                        </li>
                        <li>
                            <i class="fas fa-genderless text--meditech-green" style="padding-right: 0.5em;"></i> Comprehensive Care for Joint Replacement
                        </li>
                        <li>
                            <i class="fas fa-genderless text--meditech-green" style="padding-right: 0.5em;"></i>MSSP Track 1+ BPCI Advanced
                        </li>
                        <p>
                            New in 2019: Medicare Advantage, All Payer Option
                        </p>
                    </ul>
                </div>
            </div>
            <div class="container no-pad--top center">
                <p>
                    It's important to make sure you qualify before you take those first steps.
                </p>
            </div>
        </div>
    </div>
</div>
<!-- End Block 2 -->
<!-- Block 3 -->
<div class="container bg--green-gradient">
    <div class="container__centered text--white">
        <div class="container no-pad page__title--center">
            <h2>
                Your MIPS Compass.
            </h2>
            <div class="page__title__ribbon--white"></div>
            <div class="row">
                <div class="container__one-third">
                    &nbsp;
                </div>
                <div class="container__one-third">
                    <h3>
                        Quality
                    </h3>
                    <p>
                        Submit collected data for at least 6 measures, or a complete specialty measure set, with bonus points available.
                    </p>
                </div>
                <div class="container__one-third">
                    &nbsp;
                </div>
            </div>
            <div class="row">
                <div class="container__one-third">
                    <h3>
                        Promoting Interoperability
                    </h3>
                    <p>
                        Points for attesting to performance category measures organized under 4 objectives (2015 CEHRT), with bonuses available.
                    </p>
                </div>
                <div class="container__one-third">
                    <img src="/sites/all/themes/meditech/images/campaigns/white-compass-icon.png" alt="Compass Icon">
                </div>
                <div class="container__one-third">
                    <h3>
                        Improvement Activities
                    </h3>
                    <p>
                        Choose from high or medium value activities that promote care coordination, beneficiary engagement, and patient safety.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="container__one-third">
                    &nbsp;
                </div>
                <div class="container__one-third">
                    <h3>
                        Cost
                    </h3>
                    <p>
                        Cost is calculated on Medicare Spending Per Beneficiary (MSPB) and total per capita cost measures.
                    </p>
                </div>
                <div class="container__one-third">
                    &nbsp;
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Block 3 -->
<!-- Block 4 -->
<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/two-backpackers-hiking-along-sea-side-mountains.jpg);">
    <div class="container__centered">
        <div class="page__title--center">
            <div class="container no-pad">
                <h2>
                    MEDITECH Supplies for a Successful Trip
                </h2>
                <p class="auto-margins">
                    Every traveler needs supplies along the way; <a href="https://ehr.meditech.com/ebooks/5-ehr-tools-for-thriving-under-value-based-care"> we've got you covered.</a> From embedded predictive analytics and quality reports to seamless connectivity with patients and outside providers, you'll be equipped with the tools you need to make the transition to value-based care models.
                </p>
            </div>
            <div class="container no-pad--top left">
                <div class="container__one-fourth transparent-overlay--white">
                    <h3 class="center">
                        Improvement Activities
                    </h3>
                    <p>
                        Telehealth: integration of remote monitors and activity trackers
                    </p>
                    <p>
                        Population Health/Patient Registries
                    </p>
                    <p>
                        Behavioral Health
                    </p>
                    <p>
                        Patient Portal, including direct booking (beneficiary empowerment)
                    </p>
                    <p>
                        Virtual visits (expanded practice access)
                    </p>
                    <p>
                        Management of social determinants of health (achieving health equity)
                    </p>
                </div>
                <div class="container__one-fourth transparent-overlay--white">
                    <h3 class="center">
                        Quality
                    </h3>
                    <p>
                        Surveillance (predictive analytics)
                    </p>
                    <p>
                        Quality Vantage dashboards
                    </p>
                    <p>
                        Business and Clinical Analytics
                    </p>
                </div>
                <div class="container__one-fourth transparent-overlay--white">
                    <h3 class="center">
                        Promoting Interoperability
                    </h3>
                    <p>
                        Immunization registry
                    </p>
                    <p>
                        Robust security (required)
                    </p>
                    <p>
                        e-Prescribing (required) and medication reconciliation
                    </p>
                    <p>
                        Interoperability with HIEs (required)
                    </p>
                    <p>
                        Transmission of patient summary
                    </p>
                    <p>
                        Patient access to records (required) and education
                    </p>
                    <p>
                        Secure patient messaging
                    </p>
                </div>
                <div class="container__one-fourth transparent-overlay--white">
                    <h3 class="center">
                        Cost
                    </h3>
                    <p>
                        Clinically driven charge capture
                    </p>
                    <p>
                        Integrated supply chain
                    </p>
                    <p>
                        Automated claims processing
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Block 4 -->
<!-- Block 5 -->
<div class="container bg--white">
    <div class="container__centered">
        <h2 class="center">
            Measuring Your Progress: How MIPS Calculates Scores.
        </h2>
        <div class="row center">
            <div class="container__one-third">
                <h3 class="center">2017</h3>
                <img src="/sites/all/themes/meditech/images/campaigns/MACRA--pie-chart-1.png" alt="MIPS Pie Chart">
                <ul class="left">
                    <li style="list-style: none;">
                        <div class="pie-chart--key-color-square" style="background-color:#00bc6f;"></div>
                        <p>60% Quality</p>
                    </li>
                    <li style="list-style: none;">
                        <div class="pie-chart--key-color-square" style="background-color:#00bbb3;"></div>
                        <p>25% Promoting Interoperability (PI)</p>
                    </li>
                    <li style="list-style: none;">
                        <div class="pie-chart--key-color-square" style="background-color:#ff8300;"></div>
                        <p>15% Improvement Activities (IA)</p>
                    </li>
                </ul>
            </div>
            <div class="container__one-third">
                <h3 class="center">2018</h3>
                <img src="/sites/all/themes/meditech/images/campaigns/MACRA--pie-chart-2.png" alt="MIPS Pie Chart">
                <ul class="left">
                    <li style="list-style: none;">
                        <div class="pie-chart--key-color-square" style="background-color:#00bc6f;"></div>
                        <p>50% Quality</p>
                    </li>
                    <li style="list-style: none;">
                        <div class="pie-chart--key-color-square" style="background-color:#00bbb3;"></div>
                        <p>25% Promoting Interoperability (PI)</p>
                    </li>
                    <li style="list-style: none;">
                        <div class="pie-chart--key-color-square" style="background-color:#ff8300;"></div>
                        <p>15% Improvement Activities (IA)</p>
                    </li>
                    <li style="list-style: none;">
                        <div class="pie-chart--key-color-square" style="background-color:#b24ec4;"></div>
                        <p>10% Cost</p>
                    </li>
                </ul>
            </div>
            <div class="container__one-third">
                <h3 class="center">2019</h3>
                <img src="/sites/all/themes/meditech/images/campaigns/MACRA--pie-chart-3.png" alt="MIPS Pie Chart">
                <ul class="left">
                    <li style="list-style: none;">
                        <div class="pie-chart--key-color-square" style="background-color:#00bc6f;"></div>
                        <p>45% Quality</p>
                    </li>
                    <li style="list-style: none;">
                        <div class="pie-chart--key-color-square" style="background-color:#00bbb3;"></div>
                        <p>25% Promoting Interoperability (PI)</p>
                    </li>
                    <li style="list-style: none;">
                        <div class="pie-chart--key-color-square" style="background-color:#ff8300;"></div>
                        <p>15% Improvement Activities (IA)</p>
                    </li>
                    <li style="list-style: none;">
                        <div class="pie-chart--key-color-square" style="background-color:#b24ec4;"></div>
                        <p>15% Cost</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- End Block 5 -->
<!-- Block 6 -->
<div class="container bg--blue-gradient">
    <div class="container__centered text--white">
        <div class="row">
            <div class="container">
                <h2 class="center">
                    It IS a Competition: Tracking Penalties and Percentages.
                </h2>
                <p class="auto-margins">
                    No matter how well you perform, you are always being measured against your peers — which means the bar is always being raised. How do you stay positive?
                </p>
                <div class="center--image">
                    <img src="/sites/all/themes/meditech/images/campaigns/MACRA--timeline.png" alt="MACRA Timeline">
                </div>
            </div>
        </div>
        <div class="container__one-third">
            <p>
                Stay ahead of the competition by keeping a close eye on your measures. MEDITECH's Quality Vantage dashboards can help. Quality Vantage dashboards provide an interactive view of regulatory measure performance, giving you a "point-in-time" view of measures well before the reporting period and attestation. Color coding indicates where performance is not meeting thresholds, so you can easily see the areas that need extra attention. View by practice, group, hospital, or down to the clinician level — even drill down into a patient list to view patients, encounters, medications, and orders that are counted on the provider/measures.
            </p>
        </div>
        <div class="container__two-thirds">
            <img src="/sites/all/themes/meditech/images/campaigns/MEDITECH-ambulatory_quality-vantage.jpg" alt="MEDITECH ambulatory quality vantage Screenshot">
        </div>
    </div>
</div>
<!-- End Block 6 -->
<!--Block 7 -->
<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/red-and-white-lighthouse--dark.jpg);">
    <div class="container__centered text--white">
        <div class="container__one-third">
            &nbsp;
        </div>
        <div class="container__two-thirds">
            <?php cta_text($cta); ?>

            <div class="center" style="margin-top:2em;">
                <?php hubspot_button($cta_code, "Contact Our Regulatory Team"); ?>
            </div>

            <div style="margin-top:1em;" class="center">
                <?php print $share_link_buttons; ?>
            </div>
        </div>
    </div>
</div>
<!--End of Block 7 -->

<!-- END campaign--node-2538.php -->
