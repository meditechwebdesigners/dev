<?php // This template is set up to control the display of the INTERNATIONAL - CTA content type

$url = $GLOBALS['base_url']; // grabs the site url

// list taxonomy news term associated with article...
$tags = field_view_field('node', $node, 'field_landing_page_type'); 
// 'field_landing_page_type' is the machine name of the field in the content type that contains the taxonomy
foreach($tags['#items'] as $tag){
  $term = taxonomy_term_load($tag['tid']);
  $term_name = $term->name;
}
?>
<!-- start node--call-to-action.tpl.php template -->
    
  <section class="container__centered">
	<div class="container__two-thirds">

      <h1><?php print $term_name.': '.$title; ?></h1>
      
      <?php    
      // get date data...
      if( !empty($content['field_date_and_time']) ){
        if($term_name != 'On Demand Webinar' ){
          $webinarDate = field_get_items('node', $node, 'field_date_and_time');
          // get first date data...
          $date1 = date_create($webinarDate[0]['value']);
          $day1 = date_format($date1, 'l, F j, Y');
          $time1 = date_format($date1, 'g:i A');
          ?>
          <p><strong><?php print $day1.', '.$time1.' EST'; ?></strong></p>
        <?php
        }
      }
      ?>
      
      <div><?php print render($content['field_body']); ?></div>
      
      <?php
            
      if($term_name == 'Live Webinar'){
        
        // get date data...
        if( !empty($content['field_date_and_time']) ){
          // Get today's date... 
          $today = date("m/d/Y");
          // Convert dates to strings...
          $todayString = strtotime($today);
          $dateToCompareTo = strtotime( date_format($date1, 'm/d/Y') );
          // Compare the dates (check if today is after start date)...
          if( $todayString > ($dateToCompareTo + 86400) ){ // 86400 = seconds in a day
            print '<p><em>This event has either been canceled or has already taken place. Please visit our <a href="'.$url.'/events"><strong>Events page</strong></a> to check for other upcoming events</em>.</p>';
          }
          else{
            print '<a href="'.$link_url.'" class="btn--orange cta_page_gae">Register today</a>';
          }
        }
        
      }
      else{
       if($term_name != 'On Demand Webinar' ){
         print '<a href="'.$link_url.'" class="btn--orange cta_page_gae">Download this '.$term_name.'</a>';
       }
        
      }
      ?>

    </div><!-- END container__two-thirds -->
  </section>

<!-- end node--call-to-action.tpl.php template -->