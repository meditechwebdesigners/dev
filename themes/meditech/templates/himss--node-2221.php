<!-- END campaign--node-2021.php -->
<?php
$button_1 = field_collection_data($node, 'field_fc_button_1');
?>

<style>
  .button-vertical-adjustment { margin-top: 8em; }
  @media (max-width: 800px) {
    .button-vertical-adjustment { margin-top: 1em; }
  }
  .auto-margins { max-width: 50em; margin-left: auto; margin-right: auto; }
  
  /* **** VIDEO ************************************** */
  .video--loop-container { min-height: 600px; max-height: 650px; }
  @media (max-width: 800px) {
    .video--loop-container { min-height: 330px; }
  }
  @media (max-width: 440px) {
    .video--loop-container { min-height: 700px; }
  }
  .video--loop-container video {
    /* Make video to at least 100% wide and tall */
    min-width: 100%; min-height: 100%;
    /* Setting width & height to auto prevents the browser from stretching or squishing the video */
    width: auto; height: auto;
    /* Center the video */
    position: absolute; top: 50%; left: 50%; transform: translate(-50%,-50%);
  }
  .video--title {
    max-width: 1152px;
    margin-left: auto;
    margin-right: auto;
    padding-left: 1em;
    padding-right: 2em;
    position: absolute;
    top: 10%;
    bottom: 0;
    left: 6%;
    text-align: left;
    color: #fff;
    z-index: 3;
  }
      
  .card-text h3 { font-size:1em; margin-bottom:0; }
  .card-text h3 i { margin-right: .5em; }
  .card-text p { margin-left:1.75em; }
  
  /* GRID */
  .grid-item{ background: none; height: 13em; padding: 2em 2em 1em 2em; }
  @media (max-width: 715px){
    .grid-item { height: 16em; }
  }
  @media (max-width: 480px){
    .grid-item { height: 12em; }
  }
  .grid-item--numbers { margin-top: 0; font-size: 1em; }
  
  .boxers { background-color: #005171; background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/himss2018-boxers.jpg); }
  
  .diagonal-background-aqua { 
    font-size: 3em; line-height: 1em; 
    background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/diagonal-aqua-lines.png); 
    width: 60%; padding: 1em; margin-left: 2em; 
  }
  .push-from-left { width: 80%; margin: 2em 0 2em 4em; }
  
  @media (max-width: 600px){
    .boxers { background-image: none; }
    .diagonal-background-aqua { width: 80%; padding: 1em; margin-left: 10%; }
    .push-from-left { width: 80%; margin: 2em 10%; }
  }
  
  .clinician-text { padding: 1em; margin: 1em 2% 0 0; width: 30%; float: left; min-height: 170px; }
  .clinician-info { font-size:.78em; }
  .female { background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/HIMSS-peers-bg.jpg) }
  .male { background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/HIMSS-peers-bg.jpg) }
  
  @media (max-width: 800px){
    .female { background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/HIMSS-peers-bg.jpg)  }
    .male { background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/HIMSS-peers-bg.jpg)  }
    .clinician-info { font-size:1em; }
  }
  @media (max-width: 600px){
    .clinician-text { width: 100%; margin: .5em 0; }
    .female { background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/HIMSS-peers-bg.jpg) }
    .male { background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/HIMSS-peers-bg.jpg) }
  }
  .customer-event { background-color: black; background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/Drais-001.jpg); }
</style>


<div class="js__seo-tool__body-content">  


  <!-- Vegas -->
  <div class="video--loop-container">
    <div class="video--overlay" style="background:none;"></div>
    <video class="video--loop" preload="auto" autoplay loop muted playsinline>
      <source src="https://player.vimeo.com/external/247346869.hd.mp4?s=e8688b5ff9aedc19f35b54412458cccc862e0240&profile_id=174">
    </video>
    <div class="container__centered video--title">
      <div class="container__two-thirds transparent-overlay" style="background-color: rgba(0,0,0,0.8);">
        <h1 class="text--white text-shadow--black js__seo-tool__title">MEDITECH at HIMSS18:<br>Your winning streak starts here.</h1>
        <p class="text--white">Visit MEDITECH in booth #1360 and learn more about the first full-scale Web-based EHR designed for the post-Meaningful Use era. We know how important your organization’s digital transformation is and we’re laying out all the cards.</p>
      </div>
    </div>
  </div>
  <!-- END Vegas -->

  
  <!-- BLOCK - Demos -->
  <div class="container">
    <div class="container__centered">
      
      <div class="container center no-pad--top">
        <h2>Catch our headlining act! Experience MEDITECH Expanse in action.</h2>
        <p>We’re putting the spotlight on our Web-based EHR, Expanse. What does our personalized and patient-centric solution do for you?</p>
      </div>
      
      <div class="container__one-third">
        <ul>
          <li>Supports team-based care and promotes care coordination.</li>
          <li>Gets you more face time with patients and less screen time with your technology.</li>
          <li>Helps accelerate care delivery and reduce physician burnout.</li>
          <li>Take it with you, wherever you go… office or hospital, tablet or desktop.</li>
          <li>And more!</li>
        </ul>
      </div>
      
      <div class="container__two-thirds">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/MEDITECH-EHR-screenshot-on-tablet.png" alt="MEDITECH EHR screenshot on tablet" style="width:80%; margin:0 10%;">
      </div>

    </div>
  </div>
  <!-- BLOCK - Demos -->  

  
  <!-- BLOCK - Cards -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/events/royal-flush-hearts.jpg); background-color:#92ebd3;">
   
    <div class="container__centered">
      <div class="container__one-half text--white">
       
        <h2>Play your cards right.</h2>
        <p>MEDITECH Expanse has everything you need for a winning hand. Here’s a glimpse of what you’ll learn about in our booth.</p>
     
        <div class="card-text" style="margin-left:2em;">
          <h3><i class="fas fa-heart" aria-hidden="true"></i> Connecting Care</h3>
          <p>Guide your patients down the healthy path with one record.</p>

          <h3><i class="fas fa-heart" aria-hidden="true"></i> Clinical Efficiency</h3>
          <p>Spend more time with the patient than with your EHR.</p>

          <h3><i class="fas fa-heart" aria-hidden="true"></i> Population Health</h3>
          <p>We help you manage the well, the at-risk, the chronically ill, and the critically ill.</p>

          <h3><i class="fas fa-heart" aria-hidden="true"></i> Consumer Health Experience</h3>
          <p>Patients engaged in their own care have better outcomes (for both you and them!).</p>

          <h3><i class="fas fa-heart" aria-hidden="true"></i> MaaS <span style="font-size:.8em;">(MEDITECH as a Service)</span></h3>
          <p>Reduce the complexity and cost of procuring, hosting, and maintaining an EHR system using our cloud solution.</p>
        </div>
        
      </div>
    </div>
    
  </div>
  <!-- BLOCK - Cards -->
  
  
  <!-- Block - Nurses and Physicians -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/);">
      <div class="container__centered">
        <h2 class="no-margin">Hear from peers who have gone all-in with MEDITECH.</h2>
        <p style="margin-top:1em;">Choosing the right EHR for your organization? Not an easy task. Finding colleagues who are successful with MEDITECH? That makes it simple. Discover how MEDITECH Expanse helps you to work smarter, engage with patients easier, and access information faster. Step in and see who will be sharing their success stories.</p>
      </div>

      <div class="container__centered text--white">

          <div class="clinician-text female">
            <h3 class="grid-item--numbers">Alicia Brubaker, RN, MSN, CCRN-K</h3>
            <p class="clinician-info">Informatics Nurse
            <br>Valley Health System
            <br>Ridgewood, NJ</p>
          </div>
           
          <div class="clinician-text male">
            <h3 class="grid-item--numbers">Andy Burchett, DO</h3>
            <p class="clinician-info">Medical Information Officer
            <br>Avera Health
            <br>Sioux Falls, SD</p>
          </div>
           
          <div class="clinician-text male">
            <h3 class="grid-item--numbers">Louis Dandurand, MD</h3>
            <p class="clinician-info">Emergency Medicine	
            <br>Northwestern Medical Center
            <br>St. Albans, VT</p>
          </div>
          
          <div class="clinician-text male">
            <h3 class="grid-item--numbers">Joe Farr, RN</h3>
            <p class="clinician-info">Clinical Applications Coordinator
            <br>King's Daughters Medical Center
            <br>Brookhaven, MS</p>
          </div>

          <div class="clinician-text female">
            <h3 class="grid-item--numbers">Stacey Hammond Johnston, MD</h3>
            <p class="clinician-info">CMIO and Hospitalist
            <br>Beaufort Memorial Hospital
            <br>Beaufort, SC</p>
          </div>
           
          <div class="clinician-text male">
            <h3 class="grid-item--numbers">Doug Kanis, MD</h3>
            <p class="clinician-info">CMIO, Internal Medicine
            <br>Pella Regional Health
            <br>Pella, IA</p>
          </div>

          <div class="clinician-text male">
            <h3 class="grid-item--numbers">Steven McPherson, BSN, RN</h3>
            <p class="clinician-info">Nurse Manager	
            <br>Union Hospital of Cecil County
            <br>Elkton, MD</p>
          </div>

          <div class="clinician-text male">
            <h3 class="grid-item--numbers">Laurence Spector, MD</h3>
            <p class="clinician-info">Physician Informaticist, MEDITECH
            <br>ED Physician, Steward Health
            <br>Boston, MA</p>
          </div>
          
          <div class="clinician-text male">
            <h3 class="grid-item--numbers">Matthew Surburg, MD</h3>
            <p class="clinician-info">Family Medicine
            <br>Hancock Regional Hospital
            <br>Greenfield, IN</p>
          </div>
          
      </div>
      
  </div>
  <!-- END Block - Nurses and Physicians -->
  
    
  
  <!-- BLOCK - Interoperability -->
  <div class="container background--cover boxers">
   
    <div class="container__centered text--white">
      <h2 class="diagonal-background-aqua">Whatever you’re up against, we’re in your corner.</h2>
      <p class="push-from-left">Our one-two punch of integration and <a href="<?php print $url; ?>/ehr-solutions/meditech-interoperability">interoperability</a> gives you the data you need to navigate all the complexities of healthcare. Check us out at the <a href="http://www.interoperabilityshowcase.org/las-vegas/2018" target="_blank">HIMSS Interoperability Showcase</a>, as we participate in the Nationwide Care Transitions use case with Commonwell Health Alliance. Stop by booth # 11955 March 6-8 and discover how MEDITECH not only advocates for interoperability, but also practices it everyday.</p>
    </div>

  </div>
  <!-- BLOCK - Interoperability -->
  

 
  <!-- BLOCK - Customer Event -->
  <div class="container background--cover customer-event">
   
    <div class="container__centered text--white">
      <div class="container__one-half transparent-overlay">
        <h2 style="font-size:2.5em; line-height:1.25em;">Follow us from the exhibit hall floor to the dance floor.</h2>
        <h3 style="margin-bottom:0;">Drai’s Beachclub & Nightclub</h3>
        <p><strong>Wednesday, March 7th | 7-10 p.m.</strong></p>
        <p>What's Vegas without a little nightlife? Join us for our annual customer appreciation event at one of the city's hottest locales, where you'll be the guest of honor!</p>
        <p><strong>Questions?</strong> Contact Event Coordinator <a href="https://home.meditech.com/webforms/contact.asp?rcpt=pcorcoran|meditech|com&rname=pcorcoran">Patti Corcoran</a>.</p>

        <?php 
        // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
        if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
          $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
        ?>
          <div class="button--hubspot center" style="margin-top:2em; margin-bottom:1em;">
            <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="himss_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
          </div>
        <?php }else{ ?>
          <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange button-vertical-adjustment himss_button_gae"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
        <?php } ?>

      </div>
      <div class="container__one-half">
        <img style="border:1px solid white;" src="<?php print $url; ?>/sites/all/themes/meditech/images/events/Drais-002.jpg"><br>
        <img style="border:1px solid white;" src="<?php print $url; ?>/sites/all/themes/meditech/images/events/Drais-003.jpg">
      </div>
    </div>

  </div>
  <!-- BLOCK - Customer Event -->
  



  
  <!-- BLOCK - Vendors -->
  <div class="container__centered">

    <div class="container center">
      <?php print render($content['field_event_vendors']); ?>
    </div>

    <div class="container" style="padding-top:0;">
      <?php 
      $vendors = field_get_items('node', $node, 'field_vendors');
      for($v=0; $v<3; $v++){
        $vendor = $vendors[$v];
        $vendorNID = $vendor['entity']->nid;
        $vendorNode = node_load($vendorNID);
        $logoFileName = $vendorNode->field_logo['und'][0]['filename'];
        $logoAlt = $vendorNode->field_logo['und'][0]['alt'];
        $vendorURL = $vendorNode->field_website_url['und'][0]['safe_value'];
        print '<div class="container__one-third no-target-icon center">';
        print '<a href="'.$vendorURL.'" target="_blank"><img src="'.$url.'/sites/default/files/vendors/'.$logoFileName.'" alt="'.$logoAlt.'" class="vendor_link_gae"></a>';
        print '</div>';
      } 
    ?>
    </div>

    <div class="container no-pad--top">
      <?php 
      for($v=3; $v<6; $v++){
        $vendor = $vendors[$v];
        $vendorNID = $vendor['entity']->nid;
        $vendorNode = node_load($vendorNID);
        $logoFileName = $vendorNode->field_logo['und'][0]['filename'];
        $logoAlt = $vendorNode->field_logo['und'][0]['alt'];
        $vendorURL = $vendorNode->field_website_url['und'][0]['safe_value'];
        print '<div class="container__one-third no-target-icon center">';
        print '<a href="'.$vendorURL.'" target="_blank"><img src="'.$url.'/sites/default/files/vendors/'.$logoFileName.'" alt="'.$logoAlt.'" class="vendor_link_gae"></a>';
        print '</div>';
      } 
    ?>
    </div>
    
    <div class="container no-pad">
      <?php 
      for($v=6; $v<8; $v++){
        $vendor = $vendors[$v];
        $vendorNID = $vendor['entity']->nid;
        $vendorNode = node_load($vendorNID);
        $logoFileName = $vendorNode->field_logo['und'][0]['filename'];
        $logoAlt = $vendorNode->field_logo['und'][0]['alt'];
        $vendorURL = $vendorNode->field_website_url['und'][0]['safe_value'];
        print '<div class="container__one-half no-target-icon center">';
        print '<a href="'.$vendorURL.'" target="_blank"><img src="'.$url.'/sites/default/files/vendors/'.$logoFileName.'" alt="'.$logoAlt.'" class="vendor_link_gae"></a>';
        print '</div>';
      } 
    ?>
    </div>

  </div>
  <!-- BLOCK - Vendors -->
  
  
   
</div><!-- end js__seo-tool__body-content -->   
 
    


<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>  

<!-- END campaign--node-2021.php -->