<?php // This template is for each row of the Views block:  UPCOMING UK IRE TRADE SHOWS view ....................... 
  // get node ID...
  $nid = $fields['nid']->content;
  $node = node_load($nid);
?>
<!-- start views-view-fields--upcoming-uk-ire-trade-shows--block.tpl.php template -->
<p><a href="<?php print $fields['field_website_url']->content; ?>" target="_blank"><?php print $fields['title']->content; ?></a> (<?php print $fields['field_event_date']->content; ?>)</p>
<!-- end views-view-fields--upcoming-uk-ire-trade-shows--block.tpl.php template -->