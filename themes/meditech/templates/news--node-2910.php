<!-- main NEWS page news--node-2910.php -->
<h1 style="display:none;">MEDITECH News</h1>

<!-- Hero -->
<?php print views_embed_view('news_hero', 'block'); // adds 'News Hero' Views block... ?>
<!-- End Hero -->

<section class="container__centered">
  <div class="container__two-thirds">
    <?php print views_embed_view('sticky_us_news', 'block'); // adds 'Sticky - US News' Views block... ?>
    <article class="card--wrapper">
      <!-- start Content REGION -->
      <?php print views_embed_view('news_main_page_us', 'block'); // adds 'News Main Page - US' Views block... ?>
      <!-- end Content REGION -->
    </article>
  </div>
  <!-- END container__two-thirds -->

  <?php include('inc--news-sidebar.php'); ?>

</section>
<!-- main NEWS page news--node-2910.php -->