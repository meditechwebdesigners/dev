<?php // This template is set up to control the display of the CAMPAIGN_PHYSICIAN_PRODUCTIVITY content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$button_1 = field_collection_data($node, 'field_fc_button_1');
?>
<!-- start node--campaign_physician_productivity.tpl.php template -->



<div class="js__seo-tool__body-content">


    <!-- Block 1 -->
    <div class="container background--cover hide__bg-image--mobile" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/physician-nurse-child-patient.jpg);">
        <div class="container__centered">
            <div class="container__two-thirds">
                <h1 class="js__seo-tool__title" style="margin-top:0;">
                    <?php print $title; ?>
                </h1>
                <h2>
                    <?php print render($content['field_header_1']); ?>
                </h2>
                <div>
                    <?php print render($content['field_long_text_1']); ?>
                </div>
                <?php 
        // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
        if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
          $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
        ?>
                <div class="button--hubspot">
                    <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span>
                    <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
                    <script type="text/javascript">
                        hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {});

                    </script>
                    </span>
                    <!-- end HubSpot Call-to-Action Code -->
                </div>
                <?php }else{ ?>
                <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- Close Block 1 -->


    <!-- Block 2 -->
    <div class="container bg--emerald">
        <div class="container__centered">
            <div class="container no-pad">
                <div class="container__two-thirds">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-WebAmb--home-screen.png" alt="MEDITECH Web EHR Patient Screen">
                    </figure>
                </div>
                <div class="container__one-third">
                    <h2 class="text--white">
                        <?php print render($content['field_header_2']); ?>
                    </h2>
                    <div class="text--white">
                        <?php print render($content['field_long_text_2']); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Close Block 2 -->


    <!-- Block 3 -->
    <?php $block_3 = field_collection_data($node, 'field_fc_quote_1'); ?>
    <div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg);">
        <div class="container__centered">
            <figure class="container__one-fourth center">
                <img class="quote__content__img" src="<?php print $url; ?>/sites/all/themes/meditech/images/quote.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/quote.svg';this.onerror=null;" alt="quote bubble graphic">
            </figure>
            <div class="container__three-fourths">
                <div class="quote__content__text text--large">
                    <?php print $block_3->field_quote_1['und'][0]['value']; ?>
                </div>
                <p class="text--large no-margin--bottom">
                    <?php print $block_3->field_full_name['und'][0]['value']; ?>
                </p>
                <p>
                    <?php print $block_3->field_company['und'][0]['value']; ?>
                </p>
            </div>
        </div>
    </div>
    <!-- Close Block 3 -->


    <!-- Block 4 -->
    <div class="container bg--emerald">
        <div class="container__centered">
            <div class="container no-pad">
                <div class="container__one-half text--white ">
                    <h2>
                        <?php print render($content['field_header_3']); ?>
                    </h2>
                    <div>
                        <?php print render($content['field_long_text_3']); ?>
                    </div>
                </div>
                <div class="container__one-half">
                    <figure>
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-ed-tracker-status-board-expanded.png" alt="MEDITECH Web EHR Patient Screen">
                    </figure>
                </div>
            </div>
        </div>
    </div>
    <!-- Close Block 4 -->


    <!-- Block 5 -->
    <div class="content__callout">
        <div class="content__callout__media">
            <div class="content__callout__image-wrapper">
                <div class="video js__video" data-video-id="<?php print render($content['field_video_url']); ?>">
                    <figure class="video__overlay">
                        <img src="<?php print $url; ?>/sites/default/files/images/video/dramatically-increasing-your-speed-and-efficiency.jpg" alt="Video Covershot">
                    </figure>
                    <a class="video__play-btn" href="https://vimeo.com/<?php print render($content['field_video_url']); ?>"></a>
                    <div class="video__container">
                    </div>
                </div>
            </div>
        </div>
        <div class="content__callout__content">
            <div class="content__callout__body">
                <div class="content__callout__body__text">
                    <h2>
                        <?php print render($content['field_header_4']); ?>
                    </h2>
                    <?php print render($content['field_long_text_4']); ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Close Block 5 -->


    <!-- Block 6 -->
    <div class="container bg--white" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg);">
        <div class="container__centered">
            <div class="container no-pad">
                <div class="container__one-half">
                    <figure style="padding-top: 2.5em;">
                        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-acute-patient-chart.png" alt="MEDITECH Web EHR Patient Screen">
                    </figure>
                </div>
                <div class="container__one-half">
                    <h2>
                        <?php print render($content['field_header_5']); ?>
                    </h2>
                    <div>
                        <?php print render($content['field_long_text_5']); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Close Block 6 -->


</div>
<!-- end js__seo-tool__body-content -->


<!-- Block 7 -->
<div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dark-grey-triangle-pattern.png);">
    <div class="container__centered center text--white ">
        <h2 class="js__seo-tool__body-content">
            <?php print render($content['field_header_6']); ?>
        </h2>
        <?php 
    // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
    if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
      $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
    ?>
        <div class="button--hubspot">
            <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span>
            <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
            <script type="text/javascript">
                hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {});

            </script>
            </span>
            <!-- end HubSpot Call-to-Action Code -->
        </div>
        <?php }else{ ?>
        <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
        <?php } ?>

        <div style="margin-top:1em;">
            <?php print $share_link_buttons; ?>
        </div>
    </div>
</div>
<!-- Close Block 7 -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>
<!-- end node--campaign_physician_productivity.tpl.php template -->
