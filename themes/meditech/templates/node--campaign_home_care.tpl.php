<?php // This template is set up to control the display of the CAMPAIGN_HOME_CARE content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

// FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM...
// to use the function simply assign it to a variable
// then pass in the field collection machine name to the function
// then you'll use the variable when calling each sub-field within the template
function field_collection_data($node, $field_name){
  // get array specific to a certain field collection item...
  $field_collection = field_get_items('node', $node, $field_name); 
  // get all content within that field collection...
  $fc_field_data = entity_load('field_collection_item', array($field_collection[0]['value']));
  // get field collection ID...
  $fc_field_ID = key($fc_field_data);
  // create variable and load it with field collection data...
  $var_with_data = $fc_field_data[$fc_field_ID];
  // return variable...
  return $var_with_data;
}
$button_1 = field_collection_data($node, 'field_fc_button_1');

// FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM with mulitple instances...
// to use the function simply assign it to a variable
// then pass in the field collection machine name to the function and the number of instances allowed ($quantity)
// then you'll use the variable when calling an array for each group of sub-fields within the template
function multi_field_collection_data($node, $field_name, $quantity){
  // get array specific to a certain field collection item...
  $field_collection = field_get_items('node', $node, $field_name); 
  // create array...
  $multi_field_collections = array();
  // loop through '$quantity' times...
  for($x=0; $x<$quantity; $x++){
    // get all content within that field collection...
    $fc_field_data = entity_load('field_collection_item', array($field_collection[$x]['value']));
    // get field collection ID...
    $fc_field_ID = key($fc_field_data);
    // create variable and load it with field collection data...
    $var_with_data = $fc_field_data[$fc_field_ID];
    // add data to array...
    $multi_field_collections[] = $var_with_data;
  }
  // return variable...
  return $multi_field_collections;
}

$block_2 = multi_field_collection_data($node, 'field_fc_head_ltext_1', 3); 
$quotes = multi_field_collection_data($node, 'field_fc_quote_1', 3);
?>
<!-- start node--campaign_home_care.tpl.php template -->
 
  
<div class="js__seo-tool__body-content">  
  

<style>
  .transparent-overlay--white { padding:1em; background-color:rgba(256,256,256,0.8); min-height:5em }
  @media all and (max-width: 30em){
    .transparent-overlay--white { margin-bottom:1em; }
  }  
</style>

  <!-- Block 1 -->
  <div class="container hide__bg-image--mobile" style="background: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/nurse-explaining-prescriptions-to-patient-in-home.jpg) no-repeat; padding:1em 0;">
    <div class="container__centered">
      <div class="container__one-third">
        &nbsp;
      </div>
      <div class="container__two-thirds transparent-overlay--white">
        <h1 class="js__seo-tool__title" style="margin:0;"><?php print $title; ?></h1>
        <h2 style="margin-top:0;"><?php print render($content['field_header_1']); ?></h2>
        <div>
          <?php print render($content['field_long_text_1']); ?>
        </div>
        <div class="center">
          <?php 
          // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
          if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
            $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
          ?>
            <div class="button--hubspot">
              <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
            </div>
          <?php }else{ ?>
            <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
  <!-- Close Block 1 -->



  <!-- Block 2 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/map-graphic.png);">
    <div class="container__centered">
      <div class="container no-pad page__title--center">
       
        <h2><?php print render($content['field_header_2']); ?></h2>
        
        <div class="page__title__ribbon"></div>
        <div>
          <?php print render($content['field_long_text_2']); ?>
        </div>
        
        <h3><?php print render($content['field_header_3']); ?></h3>
        
        <div class="container__one-half">
          <div class="transparent-overlay">
            <h4 class="text--white"><?php print $block_2[0]->field_header_1['und'][0]['value']; ?></h4>
            <div class="text--white left">
              <?php print $block_2[0]->field_long_text_1['und'][0]['value']; ?>
            </div>
          </div>
        </div>
        
        <div class="container__one-half">
          <div class="transparent-overlay">
            <h4 class="text--white"><?php print $block_2[1]->field_header_1['und'][0]['value']; ?></h4>
            <div class="text--white left">
              <?php print $block_2[1]->field_long_text_1['und'][0]['value']; ?>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>
  <!--Close Block 2 -->

  

  <!-- Block 3 -->
  <div class="content__callout">
    <div class="content__callout__media">
      <div class="content__callout__image-wrapper">
          <div class="video js__video" data-video-id="<?php print render($content['field_video_url']); ?>">
            <figure class="video__overlay">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--healthy-home.jpg" alt="Video Covershot">
            </figure>
            <a class="video__play-btn" href="http://vimeo.com/<?php print render($content['field_video_url']); ?>"></a>
            <div class="video__container">
            </div>
          </div>
      </div>
    </div>
    <div class="content__callout__content">
      <div class="content__callout__body">
        <div class="content__callout__body__text">
          <h2><?php print render($content['field_header_4']); ?></h2>
           <?php print render($content['field_long_text_3']); ?>
        </div>
      </div>
    </div>
  </div>
  <!-- Close Block 3 -->

  

  <!-- Block 4 -->
  <div class="container bg--emerald">
    <div class="container__centered">
      <div class="container no-pad">
        <div class="container__two-thirds">
          <h2 class="text--white"><?php print render($content['field_header_5']); ?></h2>
          <div>
            <?php print render($content['field_long_text_4']); ?>
          </div>
        </div>
        <div class="container__one-third center">
          <figure>
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-access-schedules-smartphone-screenshot.png" alt="MEDITECH On The Go Schedules Screenshot">
          </figure>
        </div>
      </div>
    </div>
  </div>
  <!-- Close Block 4 -->
  

  <!-- Block 5 -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/female-patient-female-doctor-tablet_blurred-darkened.jpg);">
    <div class="container__centered">
      <div class="container no-pad">
        <div class="container__two-thirds">
          <figure>
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-Hospice-Care-Plan-screenshot.png" alt="MEDITECH Bereavement Mailings Screenshot">
          </figure>
        </div>
        <div class="container__one-third">
          <h2 class="text--white text-shadow--black"><?php print render($content['field_header_6']); ?></h2>
          <div class="text--white text-shadow--black">
            <?php print render($content['field_long_text_5']); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Close Block 5 -->


  <!-- Block 6 -->
  <div class="container bg--black-coconut">
    <div class="container__centered">
      <div class="container no-pad">
        <div class="container__one-third">
          <h2 class="text--white"><?php print render($content['field_header_7']); ?></h2>
          <div>
            <?php print render($content['field_long_text_6']); ?>
          </div>
        </div>
        <div class="container__two-thirds">
          <figure>
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-integumentary-overview-assessment-screenshot.png" alt="MEDITECH Staff Immunization Record Screenshot">
          </figure>
        </div>
      </div>
    </div>
  </div>
  <!-- Close Block 6 -->
  
  
  <!-- Block 7 -->
  <div class="container bg--emerald">
    <div class="container__centered">
     
      <?php $icons = array('fas fa-user-md', 'fas fa-database', 'fas fa-home'); ?>
      
      <?php for($n=0; $n<3; $n++){ ?>
        <div class="container__one-third">
          <div class="quote__content__icon">
            <i class="<?php echo $icons[$n]; ?> fa-4x text--white"></i>
          </div>
          <div class="quote__content__text">
            <p class="text--white">"<?php print $quotes[$n]->field_quote_1['und'][0]['value']; ?>"</p>
            <p class="text--white no-margin--bottom"><?php print $quotes[$n]->field_full_name['und'][0]['value']; ?></p>
            <p class="text--white text--small"><?php print $quotes[$n]->field_company['und'][0]['value']; ?></p>
          </div>
        </div>
      <?php } ?>
      
    </div>
  </div>
  <!-- Close Block 7 -->


  
</div><!-- end js__seo-tool__body-content -->  


<!-- Block 8 -->
<div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg);">
  <div class="container__centered center">
    <h2 class="js__seo-tool__body-content"><?php print render($content['field_header_8']); ?></h2>
    <?php 
    // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
    if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
      $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
    ?>
      <div class="button--hubspot">
        <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
      </div>
    <?php }else{ ?>
      <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
    <?php } ?>
     
      <div style="margin-top:1em;">
        <?php print $share_link_buttons; ?>
      </div>
  </div>
</div>
<!-- Close Block 8 -->



<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>  
<!-- end node--campaign_home_care.tpl.php template -->