<!-- start testing--node-2785.tpl.php template -->
<!-- MODAL TEST -->
<style>
  .modal-text {
    padding: 3em;
    background-color: white;
    margin-top: 3em;
    max-width: 70em;
    margin: 0 auto;
  }
  #mask { background-color: rgba(0, 0, 0, 0.25); }
</style>

<section class="container__centered">


  <div class="container no-pad">

      <!-- Start Multiple Modal Pop-ups Demo -->
      <div class="container__one-third">
        <!-- Start Modal 1 -->
        <div id="mask" style="display: none;"></div>
        <div id="modal1" class="modal" style="display: none;">
          <a class="close-modal" href="javascript:void(0)">×</a>
          <div class="modal-content">
            <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/BCA-Ambulatory--Regulatory-Clinical-Quality-Analysis.jpg">
          </div>
        </div>

        <div class="open-modal" data-target="modal1" style="margin-bottom:2em;">
          <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/BCA-Ambulatory--Regulatory-Clinical-Quality-Analysis.jpg">
          <div class="mag-bg">
            <i class="mag-icon fas fa-search-plus"></i>
          </div>
        </div>
        <!-- End Modal 1 -->
      </div>

      <div class="container__one-third">
        <!-- Start Modal 2 -->
        <div id="mask" style="display: none;"></div>
        <div id="modal2" class="modal" style="display: none;">
          <a class="close-modal" href="javascript:void(0)">×</a>
          <div class="modal-content">
            <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/BCA-Ambulatory--CPT-Revenue-Analysis.jpg">
          </div>
        </div>

        <div class="open-modal" data-target="modal2" style="margin-bottom:2em;">
          <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/BCA-Ambulatory--CPT-Revenue-Analysis.jpg">
          <div class="mag-bg">
            <i class="mag-icon fas fa-search-plus"></i>
          </div>
        </div>
        <!-- End Modal 2 -->
      </div>

      <div class="container__one-third">
        <!-- Start Modal 3 -->
        <div id="mask" style="display: none;"></div>
        <div id="modal3" class="modal" style="display: none;">
          <a class="close-modal" href="javascript:void(0)">×</a>
          <div class="modal-content">
            <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/BCA--Quality-Readmissions-Summary.jpg">
          </div>
        </div>

        <div class="open-modal" data-target="modal3" style="margin-bottom:2em;">
          <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/BCA--Quality-Readmissions-Summary.jpg">
          <div class="mag-bg">
            <i class="mag-icon fas fa-search-plus"></i>
          </div>
        </div>
        <!-- End Modal 3 -->
      </div>

    </div>
    
    <div class="container no-pad">

      <!-- Start Multiple Modal Pop-ups Demo -->
      <div class="container__one-third">
        <!-- Start Modal 1 -->
        <div id="mask" style="display: none;"></div>
        <div id="modal4" class="modal" style="display: none;">
          <a class="close-modal" href="javascript:void(0)">×</a>
          <div class="modal-content">
            <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/BCA-Ambulatory--Regulatory-Clinical-Quality-Analysis.jpg">
          </div>
        </div>

        <div class="open-modal" data-target="modal4" style="margin-bottom:2em;">
          <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/BCA-Ambulatory--Regulatory-Clinical-Quality-Analysis.jpg">
          <div class="mag-bg">
            <i class="mag-icon fas fa-search-plus"></i>
          </div>
        </div>
        <!-- End Modal 1 -->
      </div>

      <div class="container__one-third">
        <!-- Start Modal 2 -->
        <div id="mask" style="display: none;"></div>
        <div id="modal5" class="modal" style="display: none;">
          <a class="close-modal" href="javascript:void(0)">×</a>
          <div class="modal-content">
            <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/BCA-Ambulatory--CPT-Revenue-Analysis.jpg">
          </div>
        </div>

        <div class="open-modal" data-target="modal5" style="margin-bottom:2em;">
          <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/BCA-Ambulatory--CPT-Revenue-Analysis.jpg">
          <div class="mag-bg">
            <i class="mag-icon fas fa-search-plus"></i>
          </div>
        </div>
        <!-- End Modal 2 -->
      </div>

      <div class="container__one-third">
        <!-- Start Modal 3 -->
        <div id="mask" style="display: none;"></div>
        <div id="modal6" class="modal" style="display: none;">
          <a class="close-modal" href="javascript:void(0)">×</a>
          <div class="modal-content">
            <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/BCA--Quality-Readmissions-Summary.jpg">
          </div>
        </div>

        <div class="open-modal" data-target="modal6" style="margin-bottom:2em;">
          <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/BCA--Quality-Readmissions-Summary.jpg">
          <div class="mag-bg">
            <i class="mag-icon fas fa-search-plus"></i>
          </div>
        </div>
        <!-- End Modal 3 -->
      </div>

    </div>
    
</section>

<script>
function openModalBox2(){
  $jq('.open-modal').each(function() { // for each .open-modal
    var opener = $jq(this);
    var modal = opener.attr('data-target'); // get the id of the modal it should open
    opener.on('click', function() { // assign an event listener only for the current opener
      //console.log('clicked', modal);
      $jq('#mask,#' + modal).fadeIn(200);
      $jq('body').css('overflow', 'hidden'; 'height', '100%');
    });
    $jq('.close-modal, #mask').on('click', function() {
      $jq('#mask,#' + modal).fadeOut(200);
      $jq('body').css('overflow', 'auto'; 'height', 'auto');
    });
  });
};
openModalBox2();
  
  /*
  function openModalBox2(){
    $jq('.open-modal').each(function() { // for each .open-modal
      var opener = $jq(this);
      var modal = opener.attr('data-target'); // get the id of the modal it should open
      opener.on('click', function() { // assign an event listener only for the current opener
        console.log('clicked', modal);
        $jq('#mask,#' + modal).fadeIn(200);
      });
      $jq('.close-modal, #mask').on('click', function() {
        $jq('#mask,#' + modal).fadeOut(200);
      });
    });
  };
  openModalBox2();
  */
</script>
<!-- end testing--node-2785.tpl.php template -->