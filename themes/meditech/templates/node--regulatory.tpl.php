<?php // This template is set up to control the display of the 'Regulatory' content type 

  $url = $GLOBALS['base_url']; // grabs the site url
?>
<!-- start node--regulatory.tpl.php template -->


  <!-- HERO section -->
  <div class="container" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/ehr-solutions/ehr-ROLES.png);">
    <div class="container__centered text--white text-shadow--black">
      <h1><?php print $title; ?></h1>
      <h2><?php print render($content['field_subtitle']); ?></h2>
    </div>
  </div>
  <!-- End of HERO section -->
  
  <section class="container__centered">
   
    <div class="container__two-thirds">
      
      <p><a href="http://www.jointcommission.org/standards_information/npsgs.aspx" target="_blank">The Joint Commission 2019 National Patient Safety Goals</a> set forth clear guidelines for how healthcare organizations can improve the quality of care. As the following table illustrates, MEDITECH's Electronic Health Record (EHR) provides many tools to help organizations increase patient safety and meet The Joint Commission's goals.</p>

      <div class="container no-pad--top">
           
        <!-- Accordions -->
        <div class="accordion">
          <ul class="accordion__list">

            <li class="accordion__list__item"> 
              <a class="accordion__link" href="#">Goal #1: Identify patients correctly.<div class="accordion__list__control"></div></a>
              <div class="accordion__dropdown">
                <p><strong>NPSG.01.01.01</strong><br>Use at least two ways to identify patients. For example, use the patient’s name and date of birth. This is done to make sure that each patient gets the correct medicine and treatment.</p>

                <p><strong>NPSG.01.03.01</strong><br>Make sure that the correct patient gets the correct blood when they get a blood transfusion.</p>

                <h4>MEDITECH Tools</h4>

                <p>MEDITECH's EHR ensures positive patient identification. In conjunction with swipe technology, caregivers can use filed medical numbers, health insurance numbers, account numbers, names, and birth dates to identify their patients.</p>

                <p>MEDITECH's EHR helps to ensure safe medication administration through the Five Rights of medication management: Right Patient, Right Medication, Right Dosage, Right Route, and Right Time. To ensure that the right patient gets the right medication, MEDITECH's system offers a variety of patient identifiers, including the ability to scan bar codes on patient wristbands and medications to correctly identify the patient and the appropriate medication. Caregivers utilize bar code scanning technology prior to administering medications to confirm patient identity and medication information against data readily available via MEDITECH's on-line Medication Administration Record.</p>

                <p>MEDITECH’s Blood Bank and Patient Care and Patient Safety solutions are fully integrated, fostering the sharing of Blood Bank unit and patient data between the two products. All data relevant to transfusions is updated in real time in the Transfusion Administration Record (TAR), a component of our Patient Care and Patient Safety solution.</p>

                <p>The TAR, which is accessible to both nurses and Blood Bank personnel, is an electronic method for documenting patient vital signs and transfusion reactions before, during, and after blood transfusions. When used in conjunction with MEDITECH's Bedside Verification, staff can use bar code readers to scan and match patients with blood products to verify the right patient, blood product, and transfusion time, as well as donor blood type compatibility and blood product expiration dates.</p>

              <a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Goal #1</a>  
              </div>
            </li>

            <li class="accordion__list__item">
              <a class="accordion__link" href="#">Goal #2: Improve staff communication.<div class="accordion__list__control"></div></a>
              <div class="accordion__dropdown">
                <p><strong>NPSG.02.03.01</strong><br>Get important test results to the right staff person on time.</p>

                <h4>MEDITECH Tools</h4>

                <p>MEDITECH's fully-integrated system facilitates comprehensive, enterprise-wide communication throughout a healthcare organization. Physician desktops and nursing status boards act as global views of caregivers' patients and serve as central points from which to process all aspects of patient care.</p>

                <p>MEDITECH's table-driven system helps your organization meet The Joint Commission's list of "Do Not Use" abbreviations, acronyms, and symbols. Relevant clinical data displays provide readily available, real-time patient information when managing medication therapies at the point of order entry and during medication administration. When ordering or administering medications, clinicians have the right information at the right time for critical decision making.</p>

                <p>Integrated capabilities throughout the MEDITECH EHR ensure coordinated and safe ordering processes. The entire care team, including pharmacists, nurses, laboratory technicians, and radiology technicians, are included in the physician-initiated process. Physicians can also sign verbal orders and view results anywhere throughout the continuum.</p>
                <a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Goal #2</a>
              </div>
            </li>

            <li class="accordion__list__item">
              <a class="accordion__link" href="#">Goal #3: Use medicines safely.<div class="accordion__list__control"></div></a>
              <div class="accordion__dropdown">
                <p><strong>NPSG.03.04.01</strong><br>Before a procedure, label medicines that are not labeled. For example, medicines in syringes, cups and basins. Do this in the area where medicines and supplies are set up.</p>

                <p><strong>NPSG.03.05.01</strong><br>Take extra care with patients who take medicines to thin their blood.</p>

                <p><strong>NPSG.03.06.01</strong><br>Record and pass along correct information about a patient’s medicines. Find out what medicines the patient is taking. Compare those medicines to new medicines given to the patient. Make sure the patient knows which medicines to take when they are at home. Tell the patient it is important to bring their up-to-date list of medicines every time they visit a doctor.</p>

                <h4>MEDITECH Tools</h4>

                <p>MEDITECH supports users in providing the capability of printing labels in supporting applications. Labels can be printed for each drug dose, the medication carts and for e-MAR scanning.</p>

                <p>MEDITECH's clinical applications can provide alerts for patients on blood thinning medications.</p>

                <p>Staff can use MEDITECH's clinical applications to record, edit, and retrieve Active Medication Lists and accurately collect home medication information during triage or the admission process using structured data. The Active Medication List consists of available information for medications prescribed outside of the hospital network or taken over-the-counter. During a patient’s visit, any medication orders placed using CPOE or Pharmacy (PHA) appear on the patient’s Active Medication List. Medication lists, containing both home and inpatient medications, can be viewed within the Medication Reconciliation routine, as well as via the Summary Panel in the patient's electronic record. Full medication information and instructions are provided to patients at discharge.</p>
                <a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Goal #3</a>
              </div>
            </li>

            <li class="accordion__list__item">
              <a class="accordion__link" href="#">Goal #6: Improve the safety of clinical alarm systems.<div class="accordion__list__control"></div></a>
              <div class="accordion__dropdown">
                <p><strong>NSPG.06.01.01</strong><br>Make improvements to ensure that alarms on medical equipment are heard and responded to on time.</p>

                <h4>MEDITECH Tools</h4>

                <p>MEDITECH's Risk Management application creates patient notifications that healthcare professionals can use to track and monitor events which put patients at risk, such as missed alarms on medical equipment. Organizations can use this tracking information to ensure staff members follow correct procedures in the future.</p>
                <a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Goal #6</a>
              </div>
            </li>

            <li class="accordion__list__item">
              <a class="accordion__link" href="#">Goal #7: Prevent infection.<div class="accordion__list__control"></div></a>
              <div class="accordion__dropdown">
                <p><strong>NPSG.07.01.01</strong><br>Use the hand cleaning guidelines from the Centers for Disease Control and Prevention or the World Health Organization. Set goals for improving hand cleaning. Use the goals to improve hand cleaning.</p>

                <p><strong>NPSG.07.03.01</strong><br>Use proven guidelines to prevent infections that are difficult to treat.</p>

                <p><strong>NPSG.07.04.01</strong><br>Use proven guidelines to prevent infection of the blood from central lines.</p>

                <p><strong>NPSG.07.05.01</strong><br>Use proven guidelines to prevent infection after surgery.</p>

                <p><strong>NPSG.07.06.01</strong><br>Use proven guidelines to prevent infections of the urinary tract that are caused by catheters.</p>

                <h4>MEDITECH Tools</h4>

                <p>The infection control functionality of MEDITECH's Laboratory Information System helps providers effectively and efficiently identify healthcare-associated infections. An organization's infection control group can easily track patient infections by automatically flagging organisms with markers or through other Customer-Defined screens. Staff can generate reports based on patient, location, physician, site of the infection, and type of organism–whether sensitive or resistant to certain antibiotics. Depending on the outcome of the reports, the infection control group can automatically receive alerts to help track nosocomial infections.</p>

                <p>Evidence Based Links associated to Interventions and Outcomes provide valuable information to assist care providers in preventing infections. Status Board Condition Lists also provide a method of monitoring patient specific indicators and lab results that can be acted upon as needed to prevent infection from developing.</p>

                <p>Authorized clinicians can also pull data from the MEDITECH EHR and store it in a secure database for robust reporting and benchmarking. Organizations can track and report on a variety of issues such as patient and visitor incidents, adverse drug events, employee health and safety, blood utilization, and infections with complete confidentiality. Staff can effectively analyze their efforts and devise strategies for improving outcomes and regulatory compliance.</p>
                
                <p>See <a href="https://customer.meditech.com/en/d/ehrtoolkits/pages/cautigettingstarted.htm" target="_blank">MEDITECH's Catheter-Associated Urinary Tract Infections (CAUTI)</a> and <a href="https://customer.meditech.com/en/d/ehrtoolkits/pages/sepsisgettingstarted.htm" target="_blank">Sepsis Management Toolkits</a>.</p>
                <a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Goal #7</a>
              </div>
            </li>

            <li class="accordion__list__item">
              <a class="accordion__link" href="#">Goal #9: Prevent residents from falling.<div class="accordion__list__control"></div></a>
              <div class="accordion__dropdown">
                <p><strong>NPSG.09.02.01</strong><br>Find out which patients are most likely to fall. For example, is the patient taking any medicines that might make them weak, dizzy or sleepy? Take action to prevent falls for these patients.</p>

                <h4>MEDITECH Tools</h4>

                <p>Authorized care providers can use MEDITECH's clinical applications to promote proactive fall risk management best practices across the continuum of care. Built-in nursing assessment algorithms — adapted for both the ambulatory and acute care settings — use sophisticated rules and calculations to evaluate the patient’s risk level, requiring care team members to take precautionary measures. Electronic alerts communicate the patient’s increased risk, cueing care team members to take action. Finally, integrated clinical decision support (CDS) embedded in nursing, ordering, and documentation tools streamlines workflow — facilitating effective identification and management to improve outcomes. See <a href="https://customer.meditech.com/en/d/ehrtoolkits/pages/6xfallriskgettingstarted.htm" target="_blank">MEDITECH's 6.x Fall Risk Management Toolkit</a>.</p>
                <a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Goal #9</a>
              </div>
            </li>

            <li class="accordion__list__item">
              <a class="accordion__link" href="#">Goal #14: Prevent bed sores.<div class="accordion__list__control"></div></a>
              <div class="accordion__dropdown">
                <p><strong>NPSG.14.01.01</strong><br>Find out which residents are most likely to have bed sores. Take action to prevent bed sores in these residents. From time to time, re-check residents for bed sores.</p>

                <h4>MEDITECH Tools</h4>

                <p>Clinicians can use MEDITECH's Long Term Care System (Patient Care component) to create and electronically document the multidisciplinary care plans required for patient-focused care delivery. In addition, clinicians can use fixed or mobile devices to document everything from assessments to medication administrations at the point of care. Caregivers can also use provider orders to drive the development of multidisciplinary care plans. Care providers can annotate images to document details about injuries or wounds.</p>

                <p>Staff can use Status Boards to do the following:</p>

                <ul>
                  <li>View pertinent client demographic and clinical data.</li>
                  <li>Define event flags.</li>
                  <li>Receive real-time notifications of abnormal and critical results.</li>
                  <li>Quickly scan due/overdue interventions, orders, and medications.</li>
                  <li>Generate patient lists for group program documentation.</li>
                </ul>

                <a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Goal #14</a>
              </div>
            </li>

            <li class="accordion__list__item">
              <a class="accordion__link" href="#">Goal #15: Identify patient safety risks.<div class="accordion__list__control"></div></a>
              <div class="accordion__dropdown">
                <p><strong>NPSG.15.01.01</strong><br>Find out which individuals served are most likely to try to commit suicide.</p>

                <p><strong>NPSG.15.02.01</strong><br>Find out if there are any risks for patients who are getting oxygen. For example, fires in the patient’s home.</p>

                <h4>MEDITECH Tools</h4>

                <p>Authorized care providers can use MEDITECH's clinical applications to document standard and user-defined assessments which help caregivers identify at-risk patients, as well as tools to track and trend outcomes. In addition, clinicians use fixed or mobile devices to document everything from assessments to medication administrations at the point of care.</p>
                <a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Goal #15</a>
              </div>
            </li>

            <li class="accordion__list__item">
              <a class="accordion__link" href="#">Universal Protocol: Prevent mistakes in surgery.<div class="accordion__list__control"></div></a>
              <div class="accordion__dropdown">
                <p><strong>UP.01.01.01</strong><br>Make sure that the correct surgery is done on the correct patient and at the correct place on the patient’s body.</p>

                <p><strong>UP.01.02.01</strong><br>Mark the correct place on the patient’s body where the surgery is to be done.</p>

                <p><strong>UP.01.03.01</strong><br>Pause before the surgery to make sure that a mistake is not being made.</p>

                <h4>MEDITECH Tools</h4>

                <p>MEDITECH's Operating Room Management solution contains a customizable nursing documentation tool that surgical staff can use to document the surgical "Time Out" and Surgical Safety Checklist.</p>
                <a class="js__accordion__toggle accordion__close" href="#"><i class="fas fa-minus-circle"></i>&nbsp; Close Universal Protocol</a>
              </div>
            </li>
          
          </ul>
        </div>
        <!-- END Accordion -->    
        
      </div><!-- END container -->
      
    </div><!-- END container__two-thirds -->

    
    <!-- SIDEBAR -->
    <aside class="container__one-third panel">
      <h3>Care Settings</h3>
      <div class="sidebar__nav">
        <?php
          // adds 'ehr solutions side nav 2' menu block...
          $ehr2Block = module_invoke('menu', 'block_view', 'menu-ehr-solutions-care-settings');
          print render($ehr2Block['content']); 
        ?>
      </div>
    </aside>
    <!-- END SIDEBAR -->

  </section><!-- END section --> 

<!-- end node--regulatory.tpl.php template -->