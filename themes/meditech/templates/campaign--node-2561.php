<!-- START campaign--node-2561.php -- MEDITECH API CAMPAIGN -->
<?php
$url = $GLOBALS['base_url']; // grabs the site url
  
$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
  .transparent-overlay--white { padding:1em; background-color:rgba(256,256,256,0.8); min-height:5em }
  .large-scale-text { font-size: 2.5em; }
  .medium-scale-text { font-size: 1.5em; }
  .greenfield-logo { width: 50%; }
  @media all and (max-width: 60em) {
    .large-scale-text { font-size: 2em; }
    .medium-scale-text { font-size: 1.25em; }
    .greenfield-logo { width: 60%; }
  }
  @media all and (max-width: 30em) {
    .large-scale-text { font-size: 1.5em; }
    .greenfield-logo { width: 70%; }
  }
</style>

  <div class="js__seo-tool__body-content">

    <!-- Block 1 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/doctors-using-mobile-phone-in-hospital-corridor.jpg);">
      <div class="container__centered">
       
        <div class="container__one-half">&nbsp;</div>
        
        <div class="container__one-half transparent-overlay--white">
          <h1 class="js__seo-tool__title" style="display:none;">MEDITECH API</h1>
          <h2 class="center">Create better apps for better healthcare with...</h2>
          <div class="center" style="margin-bottom:1em;">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/MEDITECH-Greenfield--green-logo.png" style="width:60%;" alt="MEDITECH Greenfield" />
          </div>
          <p>MEDITECH is taking another step forward to broaden EHR usability and interoperability, by unveiling <a href="https://ehr.meditech.com/meditech-greenfield">a new space of untapped potential</a>. Welcome to MEDITECH Greenfield - our new app development environment supported by RESTful APIs, including FHIR. </p>
        </div>
        
      </div>
    </div>
    <!-- End Block 1 -->
     
     
    <!-- Spark -->
    <div class="container background--cover center" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/green-grass-field-with-sky.jpg);">
      <div class="container__centered center text--white">
        <div class="video js__video" data-video-id="288801648">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--meditech-greenfield-promo.jpg" alt="MEDITECH Greenfield API Promo - Video Covershot">
          </figure>
          <a class="video__play-btn" href="https://vimeo.com/288801648"></a>
          <div class="video__container"></div>
        </div>
        <h2>Create, share, and grow with us.</h2>
      </div>
    </div>
    <!-- End Spark -->
      
    
    <!-- Quote -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/interoperability-model-city-connecting-lines--green.jpg);">
      <div class="container__centered">
        <div class="container">
          <div class="container__two-thirds center text--white" style="padding:2em; background:#02683c;">
            <p class="medium-scale-text">"In today's healthcare paradigm, EHRs are not only judged on their inherent functionality, but also for how well they connect with other systems. Greenfield is a natural extension of our open web environment, and represents another important step in MEDITECH's commitment to driving interoperability and innovation forward."</p>
			      <p>— Howard Messing, MEDITECH President and CEO</p>
          </div>
        </div>
      </div>
    </div>
    <!-- End Quote -->
    
    
    <!-- Video -->
    <div class="content__callout">
      <div class="content__callout__media">
        <div class="content__callout__image-wrapper">
          <div class="video js__video" data-video-id="288221143">
            <figure class="video__overlay">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--woman-at-laptop--api-development.jpg" alt="MEDITECH Greenfield API - Video Covershot">
            </figure>
            <a class="video__play-btn" href="http://vimeo.com/288221143"></a>
            <div class="video__container"></div>
          </div>
        </div>
      </div>
      <div class="content__callout__content">
        <div class="content__callout__body">
          <div class="content__callout__body__text">
            <h2>Support innovation and drive future growth. </h2>
            <p>How can MEDITECH Greenfield help to increase the value of your Expanse EHR? Hear from our development leaders on how this new environment will improve the ways providers, patients, and consumers all see and use healthcare data.</p>
          </div>
        </div>
      </div>
    </div>
    <!-- End Video -->
        
        
    <!-- Landing Page Link -->
    <div class="container background--cover hide__bg-image--mobile" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/father-swinging-son-over-grass-field--sunset.jpg); background-color:#a6ecff; padding:6em 0;">
      <div class="container__centered">
        <div class="container__one-half">
          <h2>Let's take the next step forward together.</h2> 

          <p>Lay the groundwork for a more connected, user-friendly future, with MEDITECH Greenfield. Working together, we'll create a more convenient, personal user experience for everyone.</p>

          <div class="center" style="margin-top:2em;">
            <?php hubspot_button('c97f9fbf-d0ea-40e2-abab-e57df05dd1d9', "Learn more about MEDITECH Greenfield"); ?>
          </div>
        </div>
      </div>
    </div>
    <!-- End Landing Page Link -->
    

  </div>
  <!-- end js__seo-tool__body-content -->
  


  <!-- CTA -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/X-background-white-left.png); background-color:#d7d1c4; padding:6em 0;">
    <div class="container__centered center">
     
      <?php cta_text($cta); ?>

      <div class="center" style="margin-top:2em;">
        <?php hubspot_button($cta_code, "Subscribe To Receive Greenfield Updates"); ?>
      </div>
    
      <div style="margin-top:1em;">
        <?php print $share_link_buttons; ?>
      </div>
      
    </div>
  </div>
  <!-- End CTA -->

<!-- END campaign--node-2561.php -->