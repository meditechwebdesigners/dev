<?php // **** This template is set up to control the display of the HOME PAGE content type
$url = $GLOBALS['base_url']; // grabs the site url
?>
<!-- start node--home-page.tpl.php template -->

<!-- Announcements/Alerts
<div class="container--page-title" style="background-color:#087E68; color:white; padding:.5em 0;">
  <div class="container__centered">
    <p style="margin:0; font-size:.85em;">On <strong>July 14 at 11 PM EDT</strong>, MEDITECH will update ehr.meditech.com for a period no longer than 15 minutes. There may be temporary slowness during this time frame.</p>
  </div>
</div>
-->

<div class="js__seo-tool__body-content">

  <style>
    /* **** Contact Sales Button ************************************** */

    #contact-sales-btn {
      position: fixed;
      right: 20px;
      bottom: 20px;
      width: 190px;
      border: 6px solid #00BC6F;
      box-shadow: 3px 3px 20px rgba(0, 0, 0, 0.3);
      z-index: 10;
    }

    .contact-title {
      background: #00BC6F;
      padding: 5px;
      color: #fff;
      font-size: 21px;
      font-weight: bold;
      cursor: pointer;
    }

    .contact-title a {
      color: #fff;
    }

    .contact-title a:hover {
      color: #fff;
    }

    .info-window {
      background-color: #fff;
      padding: 10px;
      font-size: 18px;
      display: none;
      text-align: center;
    }

    .float--x-container {
      float: right;
      z-index: 1001;
      padding-right: 3px;
    }

    .float--x {
      color: #fff;
    }

    /* **** VIDEO ************************************** */

    .video--loop-container {
      min-height: 600px;
      max-height: 650px;
    }

    @media (max-width: 800px) {
      .video--loop-container {
        min-height: 330px;
      }
    }

    @media (max-width: 440px) {
      .video--loop-container {
        min-height: 700px;
      }
    }

    .video--loop-container video {
      /* Make video to at least 100% wide and tall */
      min-width: 100%;
      min-height: 100%;
      /* Setting width & height to auto prevents the browser from stretching or squishing the video */
      width: auto;
      height: auto;
      /* Center the video */
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
    }

    .video--title {
      max-width: 1152px;
      margin-left: auto;
      margin-right: auto;
      padding-left: 1em;
      padding-right: 2em;
      position: absolute;
      top: 20%;
      bottom: 0;
      left: 6%;
      text-align: left;
      color: #fff;
      z-index: 3;
    }

    @media (max-width: 1000px) {
      .video--title {
        position: absolute;
        top: 5%;
      }
    }

    @media all and (max-width: 50em) {
      #contact-sales-btn {
        display: none;
      }
    }

    .container__one-fifth {
      width: 18%;
      margin-right: 2%;
      padding: 1em 1%;
      float: left;
    }

    .container__one-fifth:last-child {
      margin-right: 0;
    }

    @media (max-width: 1000px) {
      .container__one-fifth {
        width: 100%;
        margin-right: 0;
        margin-top: 1em;
        padding: 1em 5%;
        float: left;
      }
    }

    .bg-overlay--white {
      background-color: none;
    }

    @media (max-width: 900px) {
      .bg-overlay--white {
        background-color: rgba(255, 255, 255, 0.8);
      }
    }

  </style>


  <!-- Contact Sales Button -->
  <div id="contact-sales-btn">
    <div class="float--x-container"><a href="#!" class="hide_sales_gae" onclick="hide('contact-sales-btn')"><i class="fas fa-times fa-lg float--x"></i></a></div>
    <div class="contact-title">
      <a href="#!" class="sales_button_gae"><i class="fa fa-phone fa-md fa-flip-horizontal" style="margin-right:5px;"></i> Get Started</a>
    </div>
    <div class="info-window">
      <p style="margin-bottom:0px;">Learn more about Expanse:<br> <span style="font-weight:bold;">781-774-7700</span></p>
    </div>
  </div>

  <script>
    // make sure not to conflict with Drupal's jQuery...
    var $jq = jQuery.noConflict();
    $jq(document).ready(function() {
      $jq(".contact-title").click(function() {
        $jq(".info-window").toggle("fast");
      });
    });

    function hide(target) {
      document.getElementById(target).style.display = "none";
    }

  </script>
  <!-- End Contact Sales Button -->


  <h1 style="display:none;">MEDITECH EHR Software Company</h1>



  <!-- Expanse Block -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/starry-sky-house-on-winter-hills.jpg);">
    <div class="container__centered">
      <div class="container">
        <img style="padding-left:2em; padding-right:2em;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-expanse-logo--green-white.png" alt="MEDITECH Expanse Logo">
      </div>
      <div class="container transparent-overlay center no-pad--top text--white text-shadow--black" style="background-color: rgba(21, 19, 74, 0.5); width: 70%; margin: 0 auto; padding: 1em 2em;">
        <p class="text--large">Our fully interoperable web-based platform navigates the care continuum with unparalleled confidence, so you can see the full picture and treat the whole patient &mdash; no matter where you are.</p>
        <p class="text--large" style="margin-bottom:0;">One EHR, no limits. Welcome to the new vision of healthcare.</p>
      </div>

      <div class="btn-holder--content__callout" style="margin-bottom:1.5em;">
        <div class="center" style="margin-top:2em;">
          <?php hubspot_button('33997285-e326-4093-ac3e-d72779e0a5f1', "Explore MEDITECH Expanse"); ?>
        </div>
      </div>

    </div>
  </div>
  <!-- End Expanse Block -->
   
   
<?php 
if( user_is_logged_in() === true ){
  global $user;
  $userID = $user->uid;
  if( $userID == 105 || $userID == 1770 || $userID == 109 || $userID == 108 || $userID == 3473 ){
?>
   
	<!-- Abigail -->
  <div class="container no-pad background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/girl-in-jean-jacket-on-smart-phone.jpg); background-position: top right;">
    <div class="container__centered bg-overlay--white" style="padding:5em 0;">
      <div class="container__one-half" style="padding:2em;">
        <h2>Empower Your Community with Health Records on iPhone<sup>&reg;</sup></h2>
        <p>We are excited to offer consumers access to their health information through Health Records on iPhone<sup>&reg;</sup>. Learn more about the benefits and how your organization can get connected.</p>
        <div class="center">
          <?php hubspot_button('', ""); ?>
        </div>
      </div>
    </div>
  </div>
  <!-- End Abigail -->   
   
<?php
  }
  else{
    print '<!-- CONTENT BLOCKED -->';
  }
}
?>         
    

	<!-- Case Studies -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/home/medical-presentation.jpg); padding:6em 0; background-position: top left;">
    <div class="container__centered">
      <div class="container__one-half">
        &nbsp;
      </div>
      <div class="container__one-half">
        <div class="transparent-overlay--xp text--white">
          <h2>Innovation, Realized</h2>
          <p>Innovation is more than a promise for the future — it’s right here, ready to be unlocked in your EHR. Read our <a href="https://ehr.meditech.com/case-studies">case studies</a> to see how our customers are improving outcomes with real results.</p>
          <div class="center">
            <?php hubspot_button('5230d784-296e-47df-96e2-88aab7a25cab', "Download The Innovators Booklet"); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Case Studies -->


  <!-- L&D Campaign -->
  <div class="container background--cover no-pad" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/home/labor-and-delivery-campaign--2019.jpg); background-position: top right;">
    <div class="container__centered bg-overlay--white" style="padding-top:3em; padding-bottom:3em;">
      <div class="container__one-half">
        <h2>The Labor and Delivery solution nurses need to welcome their newest patients</h2>
        <p>Give nurses the full perinatal story to guide safe, informed care for new mothers and babies.</p>
        <div class="center">
          <?php hubspot_button('b2772783-0455-4993-a5c7-609a31c4b178', "Sign Up For The Labor and Delivery Webinar"); ?>
        </div>
      </div>
      <div class="container__one-half">
        &nbsp;
      </div>
    </div>
  </div>
  <!-- End L&D Campaign -->


  <!-- Nurse Forum -->
  <div class="content__callout">
    <div class="content__callout__media">
      <div class="content__callout__image-wrapper">
        <div class="video js__video" data-video-id="327293262">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/events/video-overlay--MEDITECH-Nurse-Forum-2019-promo.jpg" alt="MEDITECH Nurse Forum 2019 - Video Covershot">
          </figure>
          <a class="video__play-btn" href="https://vimeo.com/327293262?"></a>
          <div class="video__container"></div>
        </div>
      </div>
    </div>
    <div class="content__callout__content">
      <div class="content__callout__body">
        <div class="content__callout__body__text">
          <h2>Experience <em>Innovative Care Coordination</em> at the 2019 Nurse Forum</h2>
          <p>Nurses, this is your year. We invite you to discover what it means to administer care when traditional barriers have been torn down at our <a href="https://ehr.meditech.com/events/2019-nurse-forum">2019 Nurse Forum: Innovative Care Coordination</a>, taking place June 12-14 in Foxborough, MA.</p>
          <div class="center" style="margin-top:2em;">
            <?php hubspot_button('0018b681-9273-4061-a25b-1d573c911d2f', "Register For the 2019 Nurse Forum"); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Nurse Forum -->



  <!-- MaaS Campaign -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/home/MaaS-Campaign--2019.jpg); padding:6em 0; background-position: top left;">
    <div class="container__centered">
      <div class="container__one-half">
        &nbsp;
      </div>
      <div class="container__one-half">
        <div class="transparent-overlay--xp text--white">
          <h2>Reach new heights with <i>MaaS</i></h2>
          <p>MEDITECH as a Service (MaaS) is a cost-effective monthly subscription to Expanse that will get your organization up and running with <a href="https://ehr.meditech.com/expanse">an innovative, web-based EHR</a>. MaaS handles the technology so that you can focus on what you do best: caring for your community. </p>
          <div class="center">
            <?php hubspot_button('7e89aa4b-8ba6-4c87-8752-30fda7980c6e', "Get More with MaaS"); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End MaaS Campaign -->

   <!-- Pop Health Campaign -->
  <div class="content__callout">
    <div class="content__callout__media">
      <div class="content__callout__image-wrapper">
        <div class="video js__video" data-video-id="288754788">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--PPGH-Bridge-to-Health.jpg" alt="Palo Pinto Mobile Health Initiative Video Covershot">
          </figure>
          <a class="video__play-btn" href="https://vimeo.com/288754788?"></a>
          <div class="video__container"></div>
        </div>
      </div>
    </div>
    <div class="content__callout__content">
      <div class="content__callout__body">
        <div class="content__callout__body__text">
          <h2>See population health through a clear lens</h2>
          <p>Meet patients wherever they are in their wellness journey, with MEDITECH's Expanse Population Health solution.</p>
          <div class="center">
            <?php hubspot_button('6883a74b-f0b2-4d99-9df5-b5f223cd7594', "Find Out More About Expanse Population Health"); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Pop Health Campaign -->



  <!-- Begin Numbers-->
  <div class="container background--cover text--white" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/home/grey-purple-blurry-gradient.jpg); padding-bottom: 0;">
    <div class="container__centered">
      <h2 style="font-size:3em; margin-bottom:.15em; margin-top:0;">Exploring new horizons</h2>
      <p>As healthcare's digital transformation continued in 2018, MEDITECH and <a href="https://ehr.meditech.com/news/more-providers-sign-on-for-meditech-expanse-in-2018">our customers</a> went beyond pushing the envelope: we arrived at our destination, delivering the full web experience to both physicians and nurses. This is more than an evolution — it's a revolution:</p>

      <div style="margin-top: 3em;">
        <div class="container__one-fifth center" style="margin-bottom:1em;">
          <p><span style="font-size:6em; font-weight:bold; color:#ffd200;">65</span><br> <strong>hospitals went LIVE with Expanse</strong></p>
        </div>
        <div class="container__one-fifth center" style="margin-bottom:1em;">
          <p><span style="font-size:6em; font-weight:bold; color:#ffd200;">15<span style="font-size:.5em; position:relative; top:-.6em">%</span></span><br> <strong>increase in new Expanse customers</strong></p>
        </div>
        <div class="container__one-fifth center" style="margin-bottom:1em;">
          <p><span style="font-size:6em; font-weight:bold; color:#ffd200;">83<span style="font-size:.5em; position:relative; top:-.6em">%</span></span><br> <strong>increase in Magic-to-Expanse upgrades</strong></p>
        </div>
        <div class="container__one-fifth center" style="margin-bottom:1em;">
          <p><span style="font-size:6em; font-weight:bold; color:#ffd200;">54<span style="font-size:.5em; position:relative; top:-.6em">%</span></span><br> <strong>of 2018 signed business was net new customers, a 15% increase</strong></p>
        </div>
        <div class="container__one-fifth center" style="margin-bottom:1em;">
          <p><span style="font-size:6em; font-weight:bold; color:#ffd200;">75<span style="font-size:.5em; position:relative; top:-.6em">%</span></span><br> <strong>of Expanse signings &amp; migrations are implementing Expanse Ambulatory</strong></p>
        </div>
      </div>
    </div>

    <div class="container__centered center" style="margin-bottom:3em;">
      <div class="gl-container" style="background-color: rgba(62, 69, 69, 0.5);">

        <div class="container__one-half bg--black-coconut">
          <h3 style="margin-top:1.5em;">Connecting care across the continuum to give our customers the complete and total view:</h3>
          <p class="text--small" style="line-height:1.25em;">
            Ambulatory <span style="margin:0 .15em;">&bull;</span> Home Care <span style="margin:0 .15em;">&bull;</span> Behavioral Health <span style="margin:0 .15em;">&bull;</span> IDNs <span style="margin:0 .15em;">&bull;</span> Critical Access Hospitals <span style="margin:0 .15em;">&bull;</span> Surgical Hospitals <span style="margin:0 .15em;">&bull;</span> Community Hospitals <span style="margin:0 .15em;">&bull;</span> Specialty Hospitals
          </p>
        </div>

        <div class="container__one-half">
          <div class="container__one-half">
            <h2 style="font-size:2em;">Top of the KLAS</h2>
            <div class="center" style="margin-top:2em;">
              <?php hubspot_button('08fe9426-f548-4533-a158-b98661f7c455', "Read About Our KLAS Win"); ?>
            </div>
          </div>
          <div class="container__one-half">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/2019-best-in-klas--MEDITECH--acute-care-emr--canada.png" alt="2019 Best in KLAS -- MEDITECH -- acute care EMR -- Canada award logo" style="width:200px;">
          </div>
        </div>

      </div>
    </div>
  </div>
  <!-- End Numbers-->

  <!-- 50th Campaign - Video -->
  <div class="content__callout">
    <div class="content__callout__media">
      <div class="content__callout__image-wrapper">
        <div class="video js__video" data-video-id="305095475">
          <figure class="video__overlay">
            <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--50th-Neil-Pappalardo.jpg" alt="50th Anniversary Video">
          </figure>
          <a class="video__play-btn" href="https://vimeo.com/305095475"></a>
          <div class="video__container"></div>
        </div>
      </div>
    </div>
    <div class="content__callout__content">
      <div class="content__callout__body">
        <div class="content__callout__body__text">
          <h2>Fifty years bold</h2>
          <p>Five decades. Countless memories. Unlimited potential. MEDITECH's 50th anniversary is a unique opportunity to celebrate our rich history, look to a bright future, and thank those who have contributed to where we are today.</p>
          <div class="center" style="margin-top:1em;">
            <?php hubspot_button('b8aab215-13df-4cb6-ac7b-426418eb6acb', "Celebrate MEDITECH's 50th!"); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End 50th Campaign - Video -->



  <!-- BLOCK - CUSTOMERS -->
  <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/casual-business-meeting-with-laptop.jpg);">
    <div class="container__centered">
      <div class="container__two-thirds transparent-overlay--white" style="background-color: rgba(256, 256, 256, 0.9);">
        <h2>Real relationships, real care, real results</h2>
        <p>Our customers have trusted us for 50 years, and with good reason: <a href="https://ehr.meditech.com/ehr-solutions/long-term-partnerships">we really listen</a>. Our personal stake in helping organizations meet challenges and achieve their goals with cutting-edge technology has resulted in an advanced, innovative EHR that makes a real difference in the way they provide care.</p>
        <ul>
          <?php print views_embed_view('6_1_signings_block_2', 'block'); // adds '6.1 Signings - Block - 2' Views block... ?>
        </ul>

        <div class="center">
          <?php hubspot_button('45c79dfa-f89a-4bd1-81df-b672714fac02', "See All Expanse Signings"); ?>
        </div>

      </div>
    </div>
  </div>
  <!-- END BLOCK - CUSTOMERS -->


</div>


<?php // SEO tool for internal use...
if( user_is_logged_in() ){
  print '<!-- SEO Tool is added to this div -->';
  print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
}
?>

<!-- end node--home-page.tpl.php template -->
