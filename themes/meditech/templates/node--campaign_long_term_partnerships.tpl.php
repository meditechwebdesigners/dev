<?php // This template is set up to control the display of the CAMPAIGN_LONG_TERM_PARTNERSHIPS content type

$url = $GLOBALS['base_url']; // grabs the site url

$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$button_1 = field_collection_data($node, 'field_fc_button_1');
?>
  <!-- start node--campaign_long_term_partnerships.tpl.php template -->

  <div class="js__seo-tool__body-content">

    <style>
      .tranparent-with-p p { margin: 0; }
      .spacing { margin-bottom: 1em; }
      @media (max-width: 800px) {
        .spacing { margin-bottom: 0; }
        .transparent-overlay { margin-bottom: 1em; }
      }
    </style>

    <!-- Block 1 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/customer-partnerships.jpg);">
      <div class="container__centered">
        <div class="container__one-half transparent-overlay text--white">
          <h1 class="text--white js__seo-tool__title" style="margin-top:0;"><?php print render($content['field_header_1']); ?></h1>
          <p>
            <?php print render($content['field_long_text_1']); ?>
          </p>
          <div style="text-align: center;">
            <?php 
            // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
            if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
              $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
            ?>
              <div class="button--hubspot">
                <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
              </div>
            <?php }else{ ?>
              <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
    <!-- End of Block 1 -->

    <!-- Block 2 - Video Block -->
    <div class="content__callout">
      <div class="content__callout__media">
        <div class="content__callout__image-wrapper">
          <div class="video js__video" data-video-id="214024425">
            <figure class="video__overlay">
              <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--oppurtunity-to-grow.jpg" alt="Video Covershot">
            </figure>
            <a class="video__play-btn" href="https://vimeo.com/214024425"></a>
            <div class="video__container"></div>
          </div>
        </div>
      </div>
      <div class="content__callout__content">
        <div class="content__callout__body">
         <h2><?php print render($content['field_header_2']); ?></h2>
          <?php print render($content['field_long_text_2']); ?>
        </div>
      </div>
    </div>
    <!-- End Block 2 -->

    <!-- Block 3 -->
    <?php $block_3 = field_collection_data($node, 'field_fc_quote_1'); ?>
      <div class="container" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg">
        <div class="container__centered">
          <figure class="container__one-fourth center">
            <img class="quote__content__img" src="<?php print $url; ?>/sites/all/themes/meditech/images/quote.svg" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/quote.png';this.onerror=null;" alt="Quote bubble">
          </figure>
          <div class="container__three-fourths">
            <div class="quote__content__text text--large">
              <p>
                <?php print $block_3->field_quote_1['und'][0]['value']; ?>
              </p>
            </div>
            <p class="text--large no-margin--bottom">
              <?php print $block_3->field_full_name['und'][0]['value']; ?>
            </p>
            <p>
              <?php print $block_3->field_company['und'][0]['value']; ?>
            </p>
            <p>
              <?php print render($content['field_long_text_3']); ?>
            </p>
          </div>
        </div>
      </div>
      <!-- End Block 3 -->

      <!-- Block 4 -->
      <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Nurse-Navigator-Program.jpg);">
        <div class="container__centered">
          <div class="container__two-thirds transparent-overlay text--white">
            <h2><?php print render($content['field_header_3']); ?></h2>
            <p>
              <?php print render($content['field_long_text_4']); ?>
            </p>
          </div>
        </div>
      </div>
      <!-- End Block 4 -->

      <!-- Block 5 - Video Block -->
      <div class="content__callout">
        <div class="content__callout__media">
          <div class="content__callout__image-wrapper">
            <div class="video js__video" data-video-id="215063769">
              <figure class="video__overlay">
                <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/video-overlay--at-your-service.jpg" alt="Video Covershot">
              </figure>
              <a class="video__play-btn" href="http://vimeo.com/215063769"></a>
              <div class="video__container">
              </div>
            </div>
          </div>
        </div>
        <div class="content__callout__content">
          <div class="content__callout__body">
           <h2><?php print render($content['field_header_4']); ?></h2>
            <?php print render($content['field_long_text_5']); ?>
          </div>
        </div>
      </div>
      <!-- End Block 5 -->

      <!-- Block 6 -->
      <div class="container" style="background-image:url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/light-grey-triangle-pattern.jpg">
        <div class="container__centered">
          <figure class="container__one-fourth center">
            <img class="quote__content__img" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Alicia-Brubaker.png" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Alicia-Brubaker.png';this.onerror=null;" alt="Partnership Profile - Alicia Brubaker">

          </figure>
          <div class="container__three-fourths">
            <h2><?php print render($content['field_header_5']); ?></h2>
            <h4><?php print render($content['field_header_6']); ?></h4>
            <h4><?php print render($content['field_header_7']); ?></h4>
            <div>
              <p>
                <?php print render($content['field_long_text_6']); ?>
              </p>
            </div>
          </div>
        </div>
      </div>
      <!-- End Block 6 -->

      <!-- Block 7 -->
      <?php $block_7 = field_collection_data($node, 'field_fc_head_ltext_1'); ?>
        <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/first-customer-in-ireland.jpg);">
          <div class="container__centered">

            <div class="page__title--center text--white">
              <h2><?php print $block_7->field_header_1['und'][0]['value']; ?></h2>
              <p>
                <?php print $block_7->field_long_text_1['und'][0]['value']; ?>
              </p>
              <div class="page__title__ribbon"></div>
            </div>

            <div class="container center no-pad--bottom">

              <div class="container__one-fourth text--white">
                <div class="transparent-overlay">
                  <h1 class="text--large text--fresh-mint" style="text-align:center;">2003</h1>
                  <?php print render($content['field_long_text_7']); ?>
                </div>
              </div>

              <div class="container__one-fourth text--white">
                <div class="transparent-overlay">
                  <h1 class="text--large text--fresh-mint" style="text-align:center;">2004</h1>
                  <?php print render($content['field_long_text_8']); ?>
                </div>
              </div>

              <div class="container__one-fourth text--white">
                <div class="transparent-overlay">
                  <h1 class="text--large text--fresh-mint" style="text-align:center;">2014</h1>
                  <?php print render($content['field_long_text_9']); ?>
                </div>
              </div>

              <div class="container__one-fourth text--white">
                <div class="transparent-overlay">
                  <h1 class="text--large text--fresh-mint" style="text-align:center;">2017</h1>
                  <?php print render($content['field_long_text_10']); ?>
                </div>
              </div>

            </div>

            <div class="container center no-pad--top" style="margin-top: 2.35765%;">

              <div class="transparent-overlay text--white">
                <h1 class="text--large text--fresh-mint" style="text-align:center;">Now</h1>
                <?php print render($content['field_long_text_11']); ?>
              </div>

            </div>

          </div>
        </div>
        <!-- End Block 7 -->

        <!-- Block 8 -->
          <div class="container bg--black-coconut" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/dark-grey-triangle-pattern.png);">
            <div class="container__centered" style="text-align: center;">
              <h2 class="js__seo-tool__body-content text--white"><?php print render($content['field_header_8']); ?></h2>
              <?php 
              // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
              if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
                $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
              ?>
                <div class="button--hubspot">
                  <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]--><a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" class="campaign_button_gae"><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="button"/></a></span><script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script><script type="text/javascript"> hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {}); </script></span><!-- end HubSpot Call-to-Action Code -->
                </div>
              <?php }else{ ?>
                <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
              <?php } ?>
              
              <div style="margin-top:1em;">
                <?php print $share_link_buttons; ?>
              </div>
            </div>
          </div>
          <!-- End Block 8 -->


  </div>
  <!-- end js__seo-tool__body-content -->


  <?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>

    <!-- end node--campaign_long_term_partnerships.tpl.php template -->