<!-- START campaign--node-2224.php -->
<?php // This template is set up to control the display of the MEDITECH_EXPANSE_ACUTE_CARE content type

$url = $GLOBALS['base_url']; // grabs the site url

// FUNCTION TO PULL IN CONTENT FOR A FIELD COLLECTION ITEM...
// to use the function simply assign it to a variable
// then pass in the field collection machine name to the function
// then you'll use the variable when calling each sub-field within the template
function field_collection_data($node, $field_name){
  // get array specific to a certain field collection item...
  $field_collection = field_get_items('node', $node, $field_name);
  // get all content within that field collection...
  $fc_field_data = entity_load('field_collection_item', array($field_collection[0]['value']));
  // get field collection ID...
  $fc_field_ID = key($fc_field_data);
  // create variable and load it with field collection data...
  $var_with_data = $fc_field_data[$fc_field_ID];
  // return variable...
  return $var_with_data;
}
$button_1 = field_collection_data($node, 'field_fc_button_1');
?>

<style>
  .quote__content__text {
    font-size: 1em;
  }

  .quote__content__name {
    font-size: 1.5em;
  }

  .container__one-third--homepage {
    padding: 1em;
  }

  /******************************************************

    GRID

  ******************************************************/

  .grid-item {
    border-right: 1px solid #ffffff;
    border-bottom: 1px solid #ffffff;
    background: none;
    height: 12em;
    padding: 2em 2em 1em 2em;
  }

  @media (max-width: 480px) {
    .grid-item {
      border: none !important;
    }
  }

  .grid-item--numbers {
    margin-top: 0;
  }

  .no-bullets{
    list-style: none;
  }

</style>


<div class="js__seo-tool__body-content">

<!--Block 1-->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Acute-nurse.jpg);">
  <div class="container__centered">
    <div class="container__one-half">
      &nbsp;
    </div>
    <div class="container__one-half transparent-overlay text--white">
      <h1 class="text--white">
        Expanse Acute Care: Tap and swipe your way to shorter days.
      </h1>
      <p>
        Save time and prioritize your workload with the first web-based, mobile EHR &mdash; built by physicians for physicians. With MEDITECH Expanse, you can quickly review personalized charts, easily prioritize your patient list, and give your patients the safe, face-to-face care they deserve.
      </p>
      <p>
        Don’t stay late because of complicated transitions and unfinished documentation. Work smarter with an EHR that gives you the same user-friendly experience, whether you’re in the inpatient environment, at the clinic, in your office &mdash; everywhere you make rounds.
      </p>
      <div style="text-align:center;">

        <?php
          // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
          if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
            $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
        ?>
        <div class="button--hubspot">
        <!--HubSpot Call-to-Action Code-->
          <span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]-->

          <a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" ><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="Download Our Interoperability White Paper"/></a></span>
          <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
          <script type="text/javascript">
                        hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {});
          </script>
          </span>
        <!-- end HubSpot Call-to-Action Code--> 
        </div>
        <?php }else{ ?>
          <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<!--End of Block 1-->

<!-- Block 2 -->

<div class="content__callout">
    <div class="content__callout__media">
        <div class="content__callout__image-wrapper">
            <div class="video js__video" data-video-id="200671200">
                <figure class="video__overlay">
                    <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/PAC_2018_Partnership--Video.jpg" alt="Dr Fletcher Patient Populations Video Covershot">
                </figure>
                <a class="video__play-btn" href="http://vimeo.com/200671200?&amp;autoplay=1"></a>
                <div class="video__container"></div>
            </div>
        </div>
    </div>
    <div class="content__callout__content">
        <div class="content__callout__body">
            <div class="content__callout__body__text">
                <h2>
                    What does it mean to have an EHR that's built by, and for, physicians?
                </h2>
                <p>
                    
                </p>
            </div>
        </div>
    </div>
</div>

<!-- END Block 2 -->


<!-- Block 2 VIDEO
<div class="content__callout">
  <div class="content__callout__media">
    <h2 class="content__callout__title">Partnership</h2>
    <div class="content__callout__image-wrapper">
      <div class="video js__video" data-video-id="236768236">
        <figure class="video__overlay">
          <img src="https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/Bryan-Bagdasian-MD-and-Janet-Desroche.jpg" alt="Bryan Bagdasian, MD and Janet Desroche">
        </figure>
        <a class="video__play-btn" href="http://vimeo.com/236768236"></a>
        <div class="video__container">
        </div>
      </div>
    </div>
  </div>
  <div class="content__callout__content">
    <div class="content__callout__body">
      <p>What does it mean to have an EHR that’s built by, and for, physicians?</p>
    </div>
  </div>
</div>
<!-- End Block 2 VIDEO -->

<!-- Block 3 STATS-->

<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/green-pattern-01.png);">
          <div class="container__centered">

            <div class="page__title--center text--white">
              <h2>Just a touch or two away from the information you need.</h2>
              
            </div>

            <div class="container center no-pad--bottom">

              <div class="container__one-half text--white">
                <div class="transparent-overlay">
                  <div class="quote__content__icon center text--white">
         <h1 class="text--large" style="text-align:center; font-size:2em">100+</h1>
        </div>
                  <h1 class="text--large text--fresh-mint" style="text-align:center;"> widgets and specialty options</h1>
                  <h4>Personalize Your Patient Summary</h4> 
                  <p>Define a layout that works best for you with widgets and specialty options.</p>                </div>
              </div>

              <div class="container__one-half text--white">
                <div class="transparent-overlay">
                  <div class="quote__content__icon center text--white">
                  <h1 class="text--large" style="text-align:center; font-size:2em">80%</h1>
                  </div>
                  <h1 class="text--large text--fresh-mint" style="text-align:center;"> of order sets entered with one tap</h1>
                  <h4>Order with One Tap of Your Finger</h4> 
                  <p>Enter order sets &mdash; developed in collaboration with Zynx Health, Inc. &mdash; with one tap.</p>                
                </div>
              </div>

            </div>

          </div>
        </div>


<!-- End Block 3 STATS -->


<!-- Block 4 --> 
  <div class="container background--cover text--white" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/documenting.jpg);">
    <div class="container__centered">
      <div class="page__title--center">
        <h2>Document the way you want.</h2>
      </div>
      <h4 class="" style="text-align:center;">Customize patient summaries, assessments, and plans to fit your work style, so you can give your patients more attention, and <a href="http://blog.meditech.com/how-ehrs-can-give-time-back-to-docs">give yourself more time.</a></h4>
      <br>
      <div>
        <div class="container__one-half">
          <p class="transparent-overlay">Personalize workflow with expert-based standard content templates.</p>
          <p class="transparent-overlay">Document faster with voice recognition, quick text, and saved typicals/normals.</p>
        </div>
        <div class="container__one-half">
          <p class="transparent-overlay">Use Diagnostics Panels for filtering and viewing across specialties.</p>
          <p class="transparent-overlay">Identify patient trends with flowsheets that pull data from across care settings.</p>
        </div>
      </div>
    </div>
  </div>
 <!-- Block 4 -->

<!-- Block 5 Quote -->


<div class="container background--cover" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/blue-boxes-pattern.jpg);">
  <div class="container__centered">
    <figure class="container__one-fourth center">
      <img class="quote__content__img" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/smith_justin.png" alt="Justin Smith MD">
    </figure>
    <div class="container__three-fourths text--white">
      <div class="quote__content__text text--large">
        <p class="italic">
          "Every click matters. One click, two or three seconds here, two or three seconds there, by the end of the day it amounts to a significant amount of time. If you look at that over weeks and months it really adds up."</p>
      </div>
      <p class="text--large no-margin--bottom">Justin Smith, MD</p>
      <p>Adena Health System</p>

    </div>
  </div>
</div>

<!-- End Block 5 Quote -->

<!-- Block 6 -->
<div class="container">
  <div class="container__centered">
    <div class="container__two-thirds">
      <h2>Excellence included, with embedded Clinical Decision Support.</h2>
      <p>Advance clinical decision-making from day one. Our evidence- and expert-based Standard Content &mdash; embedded in Expanse for Acute Care &mdash; is coordinated with best practice workflows, for safer patient care from the start. Pre-built, <a href="http://ehr.meditech.com/ehr-solutions/meditech-ehr-excellence-toolkits">physician-related toolkits </a> provide outcomes-focused implementation guides that outline best practice workflow and setup recommendations to detect and treat sepsis, CAUTI, and other high priority conditions. </p>
    </div>
    <div class="container__one-third" style="text-align: center">
      <figure>
        <img width="200" style="margin-top: 50px;" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/build-guide--toolkit-green.png" onerror="this.src='<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/build-guide--toolkit-green.png';this.onerror=null;" alt="green toolkit icon">
      </figure>
    </div>
  </div>
</div>
<!-- Close Block 6 -->

<!-- Block 7 -->
<div class="container bg--emerald">
    <div class="container__centered">
      <div class="container__one-third">
        <h2 class="text--white no-margin--top">Know  your  patients.</h2>
        <div class="text--white">
          <ul>
  <li>View handoff messages, new results, and consult requests from across the continuum</li>
  <li>Tailor your Rounding List to prioritize workflow in one place</li>
  <li>Check the Activity Log for a comprehensive look at your patient's current state, with abnormal or critical results expanded</li>
  <li>Learn their specific circumstances, with special indicators</li>
  <li>Build rapport with patients through personal notes, reminders, and checklists</li>
</ul>        </div>
      </div>
      <div class="container__two-thirds">
        <img src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/acute-care-01.jpg" alt="MEDITECH Surveillance Desktop screenshot">
      </div>
    </div>
  </div>
<!-- END Block 7 -->

<!-- Block 8-->
<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/Interoperability.jpg);">
    <div class="container__centered">
      <div class="container__two-thirds transparent-overlay">
        <h2 class="text--white no-margin--top">Connect with outside providers</h2>
        <div class="text--white">
          <p>MEDITECH Expanse is making <a href="https://blog.meditech.com/a-new-way-to-drive-interoperability-and-improve-patient-experience">clinically integrated networks</a> run like clockwork, which will help you do the same. Connect across all care settings and exchange information with outside providers through C-CDAs, APIs, FHIR, HL7, and Direct Messaging. Our <a href="https://ehr.meditech.com/news/alliance-community-hospital-first-meditech-customer-to-deploy-commonwell-services"> successful deployment of CommonWell </a> interoperability services gives you an uninterrupted view of the patient story, no matter where they last received care.</p>
      </div>
      </div>
    </div>
  </div>

<!-- End Block 8 -->

<!-- Block 9 --> 

<div class="container bg--emerald">
  <div class="container__centered">
    <h2>Prepare for quick and easy discharges.</h2>
    <p>Start planning your patient’s discharge on day one with our multidisciplinary Discharge Routine. Coordinate with the care team while requesting follow-ups and diagnostics, ordering durable medical equipment, automatically converting inpatient orders to ambulatory orders, and more. You can also manage prescriptions seamlessly with embedded tools offered in collaboration with DrFirst® &mdash; all without having to log into other systems or leave the patient’s record. 
</p>

<div style="float:left; width:100%; border-bottom:10px dotted#6ac4a9; height:1px; margin:1em 0;"></div>
  </br>
  </br>

    <div>
      <div class="container__one-half">
        <ul class="no-bullets">
          <li><i class="fa fa-check-square-o" aria-hidden="true" style="color:#ffffff;" ></i> Use e-prescribing to electronically submit prescriptions 
          </br>(including controlled substances) to outpatient pharmacies.</li>
          <li><i class="fa fa-check-square-o" aria-hidden="true" style="color:#ffffff;" ></i>Review medication claims histories.</li>
        </ul>
      </div>
      <div class="container__one-half">
        <ul class="no-bullets">
          <li><i class="fa fa-check-square-o" aria-hidden="true" style="color:#ffffff;" ></i> Query state prescription drug management plans.</li>
          <li><i class="fa fa-check-square-o" aria-hidden="true" style="color:#ffffff;" ></i> Submit prior authorizations to ensure eligibility.</li>
          <li><i class="fa fa-check-square-o" aria-hidden="true" style="color:#ffffff;" ></i> Check the insurance formulary for covered medications.</li>
        </ul>
      </div>
    </div>
  
    
  </div>
</div>

<!--End Block 9 -->



<!-- Block 10-->

<div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/em_trans_vehicle.jpg);">
          <div class="container__centered">

            <div class="page__title--center">
              <h2 class="text--fresh-mint">Ensure smooth care transitions.</h2>
              <h3 class="text--white">Transitioning patients <a href="http://ehr.meditech.com/ehr-solutions/meditechs-care-coordination">between care settings</a> 
              can be complicated. </br>
              Simplify the process with Expanse.</h3>
              
            </div>

            <div class="container center no-pad--bottom">

              <div class="container__one-half text--white">
                <div class="transparent-overlay">
                    <p><strong>Easily Reconcile Medications</strong> 
                    </br>for a complete snapshot of the patient's medication history, 
                    </br>including home perscriptions, drugs given during surgery, and medications ordered in acute care.</p>              
                  </div>
              </div>

              <div class="container__one-half text--white">
                <div class="transparent-overlay">
                   <p><strong>Coordinate Handovers</strong> 
                  </br>with a Sign Out routine to communicate the patient's status 
                  </br>and next steps to new providers &mdash; especially helpful when
                  </br>transitioning a patient from an emergent setting to inpatient care.
                  </p>               
                </div>
              </div>

            </div>

          </div>
        </div>



<!-- end js__seo-tool__body-content -->

<!--Block 11-->
<div class="container bg--black-coconut" style="background-image: url(https://ehr.meditech.com/sites/all/themes/meditech/images/campaigns/blue-boxes-pattern.jpg);">
  <div class="container__centered center">
    <h2 class="text--white">Want to learn more about how MEDITECH Expanse can save you time at the hospital, and in your practice?</h2>
    
        <?php
          // check to see if Hubspot field is blank -- if not, then create Hubspot button -- else create regular button...
          if( $button_1->field_hubspot_embed_code_1['und'][0]['value'] != '' ){
            $button_1_code = $button_1->field_hubspot_embed_code_1['und'][0]['value'];
        ?>
        <div class="button--hubspot">
        <!--HubSpot Call-to-Action Code-->
          <span class="hs-cta-wrapper" id="hs-cta-wrapper-<?php print $button_1_code; ?>"><span class="hs-cta-node hs-cta-<?php print $button_1_code; ?>" id="hs-cta-<?php print $button_1_code; ?>"><!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]-->

          <a href="https://cta-redirect.hubspot.com/cta/redirect/2897117/<?php print $button_1_code; ?>" ><img class="hs-cta-img" id="hs-cta-img-<?php print $button_1_code; ?>" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/2897117/<?php print $button_1_code; ?>.png"  alt="Download Our Interoperability White Paper"/></a></span>
          <script charset="utf-8" src="https://js.hscta.net/cta/current.js"></script>
          <script type="text/javascript">
                        hbspt.cta.load(2897117, '<?php print $button_1_code; ?>', {});
          </script>
          </span>
        <!-- end HubSpot Call-to-Action Code--> 
        </div>
        <?php }else{ ?>
          <a href="<?php print $button_1->field_button_url_1['und'][0]['value']; ?>" class="btn--orange"><?php print $button_1->field_button_text_1['und'][0]['value']; ?></a>
        <?php } ?>
    
      <div style="margin-top:1em;">
        <style>.share-icons a { font-size:1.25em; border-bottom: 0px; margin-right: .25em; }</style><div class="share-icons no-target-icon"><p><a href="https://twitter.com/intent/tweet?text=EHR%20Mobility&amp;via=MEDITECH&amp;url=https://ehr.meditech.com/ehr-solutions/ehr-mobility" target="_blank" title="Share this link on Twitter" rel="noreferrer noopener"><i class="fa fa-twitter-square"></i></a> <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https%3A%2F%2Fehr.meditech.com%2Fehr-solutions%2Fehr-mobility&amp;title=EHR%20Mobility&amp;summary=Meet%20Expanse%2C%20our%20next-generation%20web%20platform.%20Ex" target="_blank" title="Share this link on LinkedIn" rel="noreferrer noopener"><i class="fa fa-linkedin-square"></i></a> <a href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fehr.meditech.com%2Fehr-solutions%2Fehr-mobility" target="_blank" title="Share this link on Facebook" rel="noreferrer noopener"><i class="fa fa-facebook-square"></i></a> <a href="https://plus.google.com/share?url=https%3A%2F%2Fehr.meditech.com%2Fehr-solutions%2Fehr-mobility" target="_blank" title="Share this link on Google Plus" rel="noreferrer noopener"><i class="fa fa-google-plus-square"></i></a> <a href="mailto:?&amp;subject=EHR%20Mobility&amp;body=https%3A%2F%2Fehr.meditech.com%2Fehr-solutions%2Fehr-mobility%0A%0AMeet%20Expanse%2C%20our%20next-generation%20web%20platform.%20Expanse%20is%20defined%20as%2C%20%22an%20uninterrupted%20space%20or%20area%3B%20a%20great%20reach%20or%20extent%20of%20command.%20The%20complete%20and%20total%20view.%22%20That%27s%20exactly%20what%20we%27re%20bringing%20to%20the%20table%20with%20our%20web-based%20EHR%20platform%20%E2%80%94%20an%20open%20workspace%20spanning%20every%20care%20setting%2C%20where%20clinicians%20can%20work%20unfettered%20by%20technological%20constraints.%20Expanse%20is%20designed%20to%20bring%20care%20to%20new%20places%20%E2%80%94%20where%20will%20it%20take%20you%3F" title="Share this link via email software"><i class="fa fa-envelope-square"></i></a></p></div>      </div>
  </div>
</div>
<!-- Close Block 11 CTA -->
<!-- END campaign--node-2078.php -->

<?php // SEO tool for internal use...
  if(node_access('update',$node)){
    print '<!-- SEO Tool is added to this div -->';
    print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  }
?>
