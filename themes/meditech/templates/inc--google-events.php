      <!-- START inc-google-events.php -->
      <!-- Google Analytics Events script -->
      <script type="text/javascript">
        // GOOGLE ANALYTICS EVENT SCRIPTS *****************************************************
        // Example: _gaq.push(['_trackEvent', 'Navigation', 'Footer', $(this).attr('href')]);
        // In this example, the following "category", "action" and "label" data is being sent:
        // Category = 'Navigation' – this is to distinguish a navigation event from other events we record
        // Action = 'Footer' – this is to distinguish it's the footer navigation
        // Label = the URL of the link. This is where it is pointing at and is obtained using the JQuery function, "$(this).attr('href')" .
        // ************ NEEDS TO GO BEFORE ACTUAL LINKS ***************************************   

        var $jq = jQuery.noConflict();
        $jq(document).ready(function(){  
          // HEADER & FOOTER... 
          $jq('.accessiblity_gae').click(function(){ _gaq.push(['_trackEvent', 'Accessibility', 'Skip to Content', $jq(this).attr('href')]); });
          $jq('.logo_desktop_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Logo - Desktop', $jq(this).attr('href')]); }); 
          $jq('.logo_M_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Logo - M', $jq(this).attr('href')]); });
          $jq('.logo_mobile_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Logo - Mobile', $jq(this).attr('href')]); });
          $jq('.menu_mobile_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Menu - Mobile', $jq(this).attr('href')]); });
          $jq('.header_link_gae a').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Header', $jq(this).attr('href')]); });
          $jq('.breadcrumbs a').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Bread Crumbs', $jq(this).attr('href')]); });
          $jq('.footer_link_gae a').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Footer', $jq(this).attr('href')]); });
          $jq('.footer_logo_gae a').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Footer - Logo', $jq(this).attr('href')]); });
          $jq('.customers_button_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Customers - Button', $jq(this).attr('href')]); });
          $jq('.customers_icon_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Customers - Icon', $jq(this).attr('href')]); });
          $jq('.search_icon_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Search - Icon', $jq(this).attr('href')]); });
          $jq('.newsletter_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Newsletter Sign Up - Button', $jq(this).attr('href')]); });
          // HOME PAGE...
          $jq('.home_page_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Home Page - Blocks', $jq(this).attr('href')]); }); 
          $jq('.home_page_numbers_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Home Page - Numbers', $jq(this).attr('href')]); }); 
          // CAMPAIGNS...
          $jq('.button--hubspot a').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Campaign CTA Button Link', $jq(this).attr('href')]); });
          // SEARCH & TAXONOMY...
          $jq('.search_result_link_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Search Result', $jq(this).attr('href')]); });
          $jq('.taxonomy_result_link_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Taxonomy Result', $jq(this).attr('href')]); });
          $jq('.tag_link_taxonomy_gae a').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'News Tag - Taxonomy Result', $jq(this).attr('href')]); });
          $jq('.tag_link_search_gae a').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'News Tag - Search Result', $jq(this).attr('href')]); });
          // SOLUTIONS...
          $jq('.solutions_sidebar_gae a').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Solutions Sidebar', $jq(this).attr('href')]); });
          $jq('.ehr-solutions-main-gae a').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Solutions Main Links', $jq(this).attr('href')]); });
          // NEWS...
          $jq('.tag_link_news_article_gae a').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'News Tag - News Article', $jq(this).attr('href')]); });
          $jq('.tag_link_news_main_gae a').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'News Tag - News Main', $jq(this).attr('href')]); });
          $jq('.news_main_link_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'News Main Page - Link', $jq(this).attr('href')]); });
          $jq('.news_main_sticky_link_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'News Main Page - Sticky Link', $jq(this).attr('href')]); });
          $jq('.cta_link_case_studies_page_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Case Studies page', $jq(this).attr('href')]); });
          $jq('.news_hero_link_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'News Main Page - Hero', $jq(this).attr('href')]); });
          $jq('.news_featured_article_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'News Main Page - Feature', $jq(this).attr('href')]); });
          $jq('.news_related_article_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'News - Related Articles', $jq(this).attr('href')]); });
          $jq('.news_sidebar_gae a').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'News - Sidebar Links', $jq(this).attr('href')]); });
          $jq('.news_article_cta_gae a').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'News Article CTA', $jq(this).attr('href')]); });
          $jq('.white_papers_link_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'White Paper CTA', $jq(this).attr('href')]); });
          $jq('.ebooks_link_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'eBook CTA', $jq(this).attr('href')]); });
          // SOCIAL MEDIA...
          $jq('.Facebook_share_gae').click(function(){ _gaq.push(['_trackEvent', 'Social Media', 'Share on Facebook', $jq(this).attr('href')]); });
          $jq('.Twitter_share_gae').click(function(){ _gaq.push(['_trackEvent', 'Social Media', 'Share on Twitter', $jq(this).attr('href')]); });
          $jq('.LinkedIn_share_gae').click(function(){ _gaq.push(['_trackEvent', 'Social Media', 'Share on LinkedIn', $jq(this).attr('href')]); });
          $jq('.Email_share_gae').click(function(){ _gaq.push(['_trackEvent', 'Social Media', 'Share via Email', $jq(this).attr('href')]); });
          // EVENTS...
          $jq('.events_main_link_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Events Main Page - Link', $jq(this).attr('href')]); });
          $jq('.events_past_link_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Events Past Page - Link', $jq(this).attr('href')]); });
          $jq('.events_sidebar_gae a').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Events Sidebar - Link', $jq(this).attr('href')]); });
          $jq('#views-exposed-form-events-main-page-filter-page .fieldset-legend a').click(function(){ _gaq.push(['_trackEvent',  'Navigation', 'Events Main Page - Filter', $jq(this).attr('href')]); });
          $jq('.events_hero_link_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Events Main Page - Hero', $jq(this).attr('href')]); });
          $jq('.vendor_link_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Vendor Link', $jq(this).attr('href')]); });
          $jq('.tradeshow_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Trade Show Link', $jq(this).attr('href')]); });
          $jq('.event_reg_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Event Registration', $jq(this).attr('href')]); });
          $jq('.event_accordion_topics_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Event Accord - Topics', $jq(this).attr('href')]); });
          $jq('.event_accordion_pre_conf_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Event Accord - Pre Conf', $jq(this).attr('href')]); });
          $jq('.event_accordion_agenda_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Event Accord - Agenda', $jq(this).attr('href')]); });
          $jq('.event_accordion_home_care_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Event Accord - Home Care', $jq(this).attr('href')]); });
          $jq('.event_accordion_keynote_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Event Accord - Key Note', $jq(this).attr('href')]); });
          $jq('.event_accordion_ed_sessions_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Event Accord - Ed Sessions', $jq(this).attr('href')]); });
          $jq('.event_accordion_clinical_track_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Event Accord - Clinical Track', $jq(this).attr('href')]); });
          $jq('.event_accordion_financial_track_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Event Accord - Financial Track', $jq(this).attr('href')]); });
          $jq('.event_accordion_gallery_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Event Accord - Gallery', $jq(this).attr('href')]); });
          $jq('.event_accordion_tech_lab_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Event Accord - Tech Lab', $jq(this).attr('href')]); });
          $jq('.event_accordion_vendors_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Event Accord - Vendors', $jq(this).attr('href')]); });
          $jq('.event_accordion_presentations_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Event Accord - Presentations', $jq(this).attr('href')]); });
          $jq('.event_accordion_eval_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Event Accord - Evaluation', $jq(this).attr('href')]); });
          $jq('.event_accordion_our_booth_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Event Accord - Our Booth', $jq(this).attr('href')]); });
          $jq('.event_accordion_featured_sessions_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Event Accord - Feat Sessions', $jq(this).attr('href')]); });
          $jq('.event_accordion_meet_greet_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Event Accord - Meet Greet', $jq(this).attr('href')]); });
          $jq('.event_accordion_cust_event_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Event Accord - Customer Event', $jq(this).attr('href')]); });
          $jq('.event_accordion_cust_speakers_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Event Accord - Customer Speakers', $jq(this).attr('href')]); });
          $jq('.event_accordion_advanced_track_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Event Accord - Advanced Track', $jq(this).attr('href')]); });
          $jq('.himss_button_gae a').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Event - HIMSS', $jq(this).attr('href')]); });
          $jq('.webinar_live_link_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Webinar - live - link', $jq(this).attr('href')]); });
          $jq('.webinar_on_demand_link_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Webinar - on-demand - link', $jq(this).attr('href')]); });
          // CTAs
          $jq('.cta_page_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'CTA - individual page', $jq(this).attr('href')]); });
          // ABOUT...
          $jq('.about_sidebar_gae a').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'About Sidebar - Link', $jq(this).attr('href')]); });
          $jq('.about_buttons_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'About Page Buttons', $jq(this).attr('href')]); });
          $jq('.careers_sidebar_gae a').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Careers Sidebar - Link', $jq(this).attr('href')]); });
          $jq('.view-job-listings a').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Careers - Job Listing', $jq(this).attr('href')]); });
          $jq('.global_button_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Global - Button', $jq(this).attr('href')]); });
          $jq('.global_news_link_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Global - News Link', $jq(this).attr('href')]); });
          $jq('.directions_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Google Maps', $jq(this).attr('href')]); });
          $jq('.hotel_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Hotels', $jq(this).attr('href')]); });
          // COMMUNICATIONS...
          $jq('.communications_sidebar_gae a').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Communications Sidebar', $jq(this).attr('href')]); });
          $jq('.communications_news_sidebar_gae a').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Communications News Sidebar', $jq(this).attr('href')]); });
          $jq('.email_link_gae').click(function(){ _gaq.push(['_trackEvent', 'Contact', 'Email Link', $jq(this).attr('href')]); });
          $jq('.phone_gae').click(function(){ _gaq.push(['_trackEvent', 'Contact', 'Phone Link', $jq(this).attr('href')]); });
          $jq('.website_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Website Link', $jq(this).attr('href')]); });
          $jq('.sales_button_gae').click(function(){ _gaq.push(['_trackEvent', 'Contact', 'Sales Button Reveal', $jq(this).attr('class')]); });
          $jq('.hide_sales_gae').click(function(){ _gaq.push(['_trackEvent', 'Contact', 'Hide Sales Button', $jq(this).attr('href')]); });
          // COOKIES...
          $jq('#doNotTrack').click(function(){ _gaq.push(['_trackEvent', 'Cookies', 'Hubspot - Do Not Track', $jq(this).attr('href')]); });
          $jq('#removeCookies').click(function(){ _gaq.push(['_trackEvent', 'Cookies', 'Remove Hubspot Cookies', $jq(this).attr('href')]); });
          // VIDEOS...
          $jq('.video__play-btn').click(function(){ _gaq.push(['_trackEvent', 'Media', 'Video Player', $jq(this).attr('href')]); });
          $jq('.video_link_gae').click(function(){ _gaq.push(['_trackEvent', 'Media', 'Video Side Button', $jq(this).attr('href')]); });
          // DOWNLOADS...
          $jq('a[href$="doc"],a[href$="pdf"]').click(function(){ var filename = this.pathname.replace(/^.*[\\\/]/, ''); _gaq.push(['_trackEvent', 'Media', 'Download', filename]); });
          // external links within content...
          $jq('.container__two-thirds a[target="_blank"]').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'External Link', $jq(this).attr('href')]); });
          // internal links within content...
          $jq('.container__two-thirds a:not([target="_blank"])').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Internal Link', $jq(this).attr('href')]); });
          // GLOBAL...
          $jq('.south_africa_sidebar_gae a').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'South Africa Sidebar', $jq(this).attr('href')]); });
          $jq('.south_africa_customers_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'South Africa Customers', $jq(this).attr('href')]); });
          $jq('.asia_pacific_sidebar_gae a').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Asia Pacific Sidebar', $jq(this).attr('href')]); });
          $jq('.asia_pacific_customers_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'Asia Pacific Customers', $jq(this).attr('href')]); });
          $jq('.uk_ire_sidebar_gae a').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'UK-Ireland Sidebar', $jq(this).attr('href')]); });
          $jq('.uk_ire_customers_gae').click(function(){ _gaq.push(['_trackEvent', 'Navigation', 'UK-Ireland Customers', $jq(this).attr('href')]); });
        });
      </script>
      <!-- END inc-google-events.php -->