<!-- START campaign--node-2401.php -- GREENFIELD LANDING PAGE -->
<?php
$url = $GLOBALS['base_url']; // grabs the site url
  
$currentURL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include('inc-share-buttons.php');

$cta = field_collection_data($node, 'field_fc_cta_block');
$cta_code = $cta->field_hubspot_embed_code_1['und'][0]['value'];
?>

<style>
  .transparent-overlay--white { padding:1em; background-color:rgba(256,256,256,0.8); min-height:5em }
  .large-scale-text { font-size: 2.5em; }
  .medium-scale-text { font-size: 1.5em; }
  .greenfield-logo { width: 50%; }
  @media all and (max-width: 60em) {
    .large-scale-text { font-size: 2em; }
    .medium-scale-text { font-size: 1.25em; }
    .greenfield-logo { width: 60%; }
  }
  @media all and (max-width: 30em) {
    .large-scale-text { font-size: 1.5em; }
    .greenfield-logo { width: 70%; }
  }
</style>

  <div class="js__seo-tool__body-content">

    <!-- Block 1 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/green-field-background-01.jpg);">
      <div class="container__centered">
        <div class="container center">
          <h1 class="js__seo-tool__title" style="display:none;">MEDITECH Greenfield -- MEDITECH API</h1>
          <p class="montserrat text--white text-shadow--black large-scale-text" style="margin-top:0; margin-bottom:.5em;">Create. Share. Grow.</p>
          <img class="greenfield-logo" src="<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/MEDITECH-Greenfield-logo--white.png" alt="MEDITECH Greenfield logo" />
          <!--<p class="montserrat text--white text-shadow--black large-scale-text" style="margin-top:.5em; margin-bottom:0;">Create. Share. Grow.</p>-->
          <div class="container__centered" style="margin: 1em 15% 0;">
            <div class="montserrat text--white medium-scale-text">
              <p style="margin-bottom:0;"><strong>A new app development environment supported by RESTful APIs including FHIR. </strong></p>
              <p style="margin-bottom:0;">Embracing the change that is healthcare.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Block 1 -->
      
  
    
    <!-- Block 3 -->
    <div class="container background--cover" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/app-developers--developing.jpg); padding:8em;">
      <div class="container__centered">
        <div class="container transparent-overlay text--white center" style="padding:2em 0;">
            <!--
            <div class="container__one-half">
              <h2>Get started.</h2>
              <a href="#" class="btn--orange" style="margin-bottom:1em;">Sign In</a>
            </div>
            -->
            <div class="">
              <h2>Not signed up yet?</h2>
              <div class="center" style="margin-top:2em;">
                <?php hubspot_button('739bb2fc-968c-4426-b49e-331a2e65fb60', "Register here for MEDITECH Greenfield"); ?>
              </div>
            </div>
        </div>
      </div>
    </div>
    <!-- End Block 3 -->
     
        
    <!-- Block 4 -->
    <div class="container background--cover hide__bg-image--mobile" style="background-image: url(<?php print $url; ?>/sites/all/themes/meditech/images/campaigns/abstract-x-background-beige.jpg); background-color:#d7d1c4;">
      <div class="container__centered">
       
        <h2>FAQs</h2>
        
        <h3 style="margin-top:1.5em;">What is MEDITECH Greenfield?</h3>
        <p>MEDITECH Greenfield is a testing ground for apps that can integrate with MEDITECH Expanse. Developers have the ability to execute the APIs and test their applications against a real MEDITECH EHR. It also gives customers and application developers access to interactive documentation for the APIs published by MEDITECH.</p> 

        <h3 style="margin-top:1.5em;">What is the value in API integration?</h3>
        <p>Standards-based APIs are game changers for interoperable healthcare. By giving providers, patients, and consumers direct access to specific data points through convenient apps, they will get to see and use information in ways they never could before. Apps give providers more freedom and flexibility to work the way they want to. In addition, consumers will have new vehicles for accessing and interacting with their own health data. The result is healthcare innovation that leads to more satisfying user experiences for all. </p>

        <h3 style="margin-top:1.5em;">For which MEDITECH releases/platforms are these APIs available?</h3>
        <p>Currently, MEDITECH Greenfield is available to MEDITECH Expanse customers.</p>

        <h3 style="margin-top:1.5em;">How can I see what APIs are available?</h3>
        <p>Our testing environment includes a list of available Common Clinical Data Set APIs and associated documentation. <a href="https://info.meditech.com/welcome-to-meditech-greenfield-rfi">Register now</a> to get started.</p>
        
        <hr style="border-bottom-color:white;" />
        
        <p class="center">If you have any additional questions, please <a href="https://home.meditech.com/webforms/contact.asp?rcpt=greenfieldinfo|meditech|com&rname=greenfieldinfo">reach out to our MEDITECH Greenfield support team</a>.</p>
        
      </div>
    </div>
    <!-- End Block 4 -->
    

  </div>
  <!-- end js__seo-tool__body-content -->
  

  <!-- Block 6 -->
  <div class="container" style="padding:6em 0;">
    <div class="container__centered center">
     
      <?php cta_text($cta); ?>

      <div class="center" style="margin-top:2em;">
        <?php hubspot_button($cta_code, "Subscribe To Receive Greenfield Updates"); ?>
      </div>   
    
      <div style="margin-top:1em;">
        <?php print $share_link_buttons; ?>
      </div>
      
    </div>
  </div>
  <!-- End Block 6 -->



  <?php // SEO tool for internal use...
  if(node_access('update',$node)){
    //print '<!-- SEO Tool is added to this div -->';
    //print '<div class="container__centered" style="margin-top:2em;"><div class="container__two-thirds no-pad--top js__seo-tool"></div></div>';
  } 
?>

<!-- END campaign--node-2401.php -->