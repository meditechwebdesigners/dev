<?php

// initialize HOOK_PERMISSION.................................
// adds permissions to the permissions page for this module...
function mtContactForm_permission() {
  return array(
    'submit_mtContactForm' => array(
      'title' => t('Submit Contact Forms'),
      'description' => t('Permission to use the main contact forms'),
    ),
    'access_mtContactForm_submissions' => array(
      'title' => t('Access Contact Form Submissions'),
      'description' => t('Permission to access contact form submissions'),
    ),
  );
}



// initialize HOOK_MENU....................................
// create pages for the module...
function mtContactForm_menu() {
  $items = array();
  // create actual form page (but the form will be called by the theme template instead)...
  $items['mtContactForm'] = array(
    'title' => 'Contact Form',
    'type' => MENU_NORMAL_ITEM,
    'access arguments' => array('submit_mtContactForm'),
    'page callback' => 'drupal_get_form',// call a form
    'page arguments' => array('mtContactForm_form'),// form name
  );
  // create the admin group to appear on the 'config' page...
  $items['admin/config/mtContactForm'] = array(
    'title' => 'Contact Form',
    'access arguments' => array('access_mtContactForm_submissions'),
  );
  // create admin page to view contact form submissions...
  $items['admin/config/mtContactForm/submissions'] = array(
    'title' => 'Contact Form Submissions',
    'description' => 'Contact Form Submissions',
    'access arguments' => array('access_mtContactForm_submissions'),
    'page callback' => 'mtContactForm_submissions',
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}




// create contact form with HOOK_FORM..........................
function mtContactForm_form($form, &$form_state){
  $form['firstName'] = array(
    '#type' => 'textfield',
    '#title' => t('First Name'),
    '#size' => 15,
    '#maxlength' => 15,
    '#prefix' => '<div class="container__one-half">',
    '#suffix' => '</div>',
    '#attributes' => array(
      'placeholder' => t('First Name'),
      'class' => array('form-text'),
    ),
  );
  $form['lastName'] = array(
    '#type' => 'textfield',
    '#title' => t('Last Name'),
    '#size' => 20,
    '#maxlength' => 20,
    '#prefix' => '<div class="container__one-half--last">',
    '#suffix' => '</div>',
    '#attributes' => array(
      'placeholder' => t('Last Name'),
      'class' => array('form-text'),
    ),
  );
  $form['emailAddress'] = array(
    '#type' => 'emailfield',
    '#title' => t('Email Address'),
    '#size' => 30,
    '#maxlength' => 30,
    '#attributes' => array(
      'placeholder' => t('name@email.com'),
      'class' => array('form-text'),
    ),
  );
  $form['phoneNumber'] = array(
    '#type' => 'telfield',
    '#title' => t('Phone Number'),
    '#size' => 10,
    '#maxlength' => 10,
    '#attributes' => array(
      'placeholder' => t('0005550000'),
      'class' => array('form-text'),
    ),
  );
  $form['organization'] = array(
    '#type' => 'textfield',
    '#title' => t('Company'),
    '#size' => 30,
    '#maxlength' => 30,
    '#prefix' => '<div class="container__two-third">',
    '#suffix' => '</div>',
    '#attributes' => array(
      'placeholder' => t('The company you work for.'),
      'class' => array('form-text'),
    ),
  );  
  $form['state'] = array(
    '#type' => 'select',
    '#title' => t('State/Province'),
    '#options' => array(
      'Select' => t('-- Select --'),
      'USA' => array(
        'AL' => t('Alabama'),
        'AK' => t('Alaska'),
        'AZ' => t('Arizona'),
        'AR' => t('Arkansas'),
        'CA' => t('California'),
        'CO' => t('Colorado'),
        'CT' => t('Connecticut'),
        'DE' => t('Delaware'),
        'DC' => t('District Of Columbia'),
        'FL' => t('Florida'),
        'GA' => t('Georgia'),
        'HI' => t('Hawaii'),
        'ID' => t('Idaho'),
        'IL' => t('Illinois'),
        'IN' => t('Indiana'),
        'IA' => t('Iowa'),
        'KS' => t('Kansas'),
        'KY' => t('Kentucky'),
        'LA' => t('Louisiana'),
        'ME' => t('Maine'),
        'MD' => t('Maryland'),
        'MA' => t('Massachusetts'),
        'MI' => t('Michigan'),
        'MN' => t('Minnesota'),
        'MS' => t('Mississippi'),
        'MO' => t('Missouri'),
        'MT' => t('Montana'),
        'NE' => t('Nebraska'),
        'NV' => t('Nevada'),
        'NH' => t('New Hampshire'),
        'NJ' => t('New Jersey'),
        'NM' => t('New Mexico'),
        'NY' => t('New York'),
        'NC' => t('North Carolina'),
        'ND' => t('North Dakota'),
        'OH' => t('Ohio'),
        'OK' => t('Oklahoma'),
        'OR' => t('Oregon'),
        'PA' => t('Pennsylvania'),
        'RI' => t('Rhode Island'),
        'SC' => t('South Carolina'),
        'SD' => t('South Dakota'),
        'TN' => t('Tennessee'),
        'TX' => t('Texas'),
        'UT' => t('Utah'),
        'VT' => t('Vermont'),
        'VA' => t('Virginia'),
        'WA' => t('Washington'),
        'WV' => t('West Virginia'),
        'WI' => t('Wisconsin'),
        'WY' => t('Wyoming'),
      ),
      'CANADA' => array(
        'AB' => t('Alberta'),
        'BC' => t('British Columbia'),
        'MB' => t('Manitoba'),
        'NB' => t('New Brunswick'),
        'NL' => t('Newfoundland and Labrador'),
        'NS' => t('Nova Scotia'),
        'ON' => t('Ontario'),
        'PE' => t('Prince Edward Island'),
        'QC' => t('Quebec'),
        'SK' => t('Saskatchewan'),
      ),
    ),
    '#prefix' => '<div class="container__one-third--last">',
    '#suffix' => '</div>',
    '#attributes' => array(
      'class' => array('form-select'),
    ),
  );
  // get page URL for analytics...
  $url = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
  // very hidden element (doesn't show in source)...
  $form['page_url'] = array(
    '#type' => 'value', // value instead of hidden
    '#value' => $url,
  ); 
  // get current time/date...
  $timeDate = date('Y-m-d H:i:s');
  $form['submissionDate'] = array(
    '#type' => 'value', // value instead of hidden
    '#value' => $timeDate,
  );
  $form['message'] = array(
    '#type' => 'textarea',
    '#title' => t('Message'),
    '#attributes' => array('rows' => 10, 'maxlength' => 1000),
    '#resizable' => false,
    '#attributes' => array(
      'placeholder' => t('Hello, I would like to learn more about your...'),
      'class' => array('form__textarea','form-textarea'),
    ),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#attributes' => array(
      'class' => array('form-submit'),
      'id' => array('contactform-submit'),
      'role' => 'button',
      'aria-label' => 'submit form',
    ),
  );
  $form['#attributes'] = array(
    'name' => 'mtContactForm-form',
    'class' => array('form', 'contactform'),
  );
  return $form;
}



    // VALIDATE contact form (server-side validation)................
    function mtContactForm_form_validate($form, &$form_state){
      
      $errorMessages = array();
      
      // check that 'firstName' is not empty...
      if($form_state['values']['firstName'] == ''){
        $errorMessages[] = array('firstName' => t('Please enter your first name.<br />'));
      }
      // check that 'firstName' only contains case insensitive(i) letters(a-z), white spaces( ), hyphens(-) or apostrophes(')...
      if( !preg_match('/^[a-z \'-]+$/i', $form_state['values']['firstName']) ){
        $errorMessages[] = array('firstName' => t('You may only include letters, hyphens, apostrophes or spaces in your first name.<br />'));
      }
      
      // check that 'lastName' is not empty...
      if($form_state['values']['lastName'] == ''){
        $errorMessages[] = array('lastName' => t('Please enter your last name.<br />'));
      }
      // check that 'lastName' only contains case insensitive(i) letters(a-z), white spaces( ), hyphens(-) or apostrophes(')...
      if( !preg_match('/^[a-z \'-]+$/i', $form_state['values']['lastName']) ){
        $errorMessages[] = array('lastName' => t('You may only include letters, hyphens, apostrophes or spaces in your last name.<br />'));
      }
       
      // check that 'organization' is not empty...
      if($form_state['values']['organization'] == ''){
        $errorMessages[] = array('organization' => t('Please enter the name of your organization.<br />'));
      }
      // check that 'organization' only contains case insensitive(i) letters(a-z), white spaces( ), hyphens(-), apostrophes(') or period(.)...
      if( !preg_match('/^[a-z .\'-]+$/i', $form_state['values']['organization']) ){
        $errorMessages[] = array('organization' => t('You may only include letters, hyphens, apostrophes, spaces or periods in your organization.<br />'));
      }
      
      // check that 'phoneNumber' is not empty...
      if($form_state['values']['phoneNumber'] == ''){
        $errorMessages[] = array('phoneNumber' => t('Please enter your phone number.<br />'));
      }
      // check that 'phoneNumber' is a number and is 10 characters long...
      if(!is_numeric($form_state['values']['phoneNumber']) || strlen($form_state['values']['phoneNumber']) != 10){
        $errorMessages[] = array('phoneNumber' => t('You must enter a valid 10-digit phone number.<br />'));
      }
      
      // check that 'emailAddress' is not empty...
      if($form_state['values']['emailAddress'] == ''){
        $errorMessages[] = array('emailAddress' => t('Please enter your email address.<br />'));
      }
      // check that 'emailAddress' contains letters or numbers before and after the @ symbol and letters or numbers before a period and letters after the period...
      if( !preg_match('/^[a-z0-9\.]+@[a-z0-9]+\.[a-z]+/i', $form_state['values']['emailAddress']) ){
        $errorMessages[] = array('emailAddress' => t('You must enter a valid email address.<br />'));
      }
      
      $states = array('Select','AL','AK','AZ','AR','CA','CO','CT','DE','DC','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY','AB','BC','MB','NB','NL','NS','ON','PE','QC','SK');
      // if value = 'Select'...
      if( in_array($form_state['values']['state'], $states) ){
        if($form_state['values']['state'] == 'Select'){
          $errorMessages[] = array('state' => t('Please select the state or province your organization is located in.<br />'));
        }
      }
      // if value is not in the array...
      else{
        $errorMessages[] = array('state' => t('You must select the state or province your organization is located in.<br />'));
      }
      
      // check that 'message' is not empty...
      if($form_state['values']['message'] == ''){
        $errorMessages[] = array('message' => t('Please enter a message.<br />'));
      }
      // check that 'message' doesn't contain any hyperlinks...
      if( preg_match('/http/i',$form_state['values']['message']) || preg_match('/https/i',$form_state['values']['message']) ){
        $errorMessages[] = array('message' => t('Please do not include hyperlinks in your message.<br />'));
      }

      foreach($errorMessages as $item){
        foreach($item as $element => $message){
          form_set_error($element, $message);
        }
      }
      
      return FALSE;
      
    }


    // hook_mail.........................................
    function mtContactForm_form_mail($key, &$message, $params) {
      $headers = array(
        'MIME-Version' => '1.0',
        'Content-Type' => 'text/html; charset=UTF-8;',
        'Content-Transfer-Encoding' => '8Bit',
        'X-Mailer' => 'Drupal'
      );
      foreach ($headers as $key => $value) {
        $message['headers'][$key] = $value;
      }
      $message['subject'] = $params['subject'];
      $message['body'] = $params['body'];
    }


    // SUBMIT contact form data via email and save to database.......................
  function mtContactForm_form_submit($form, &$form_state) {
      $id = db_insert('mtContactForm') 
        -> fields(array(
          'firstName' => $form_state['values']['firstName'],
          'lastName' => $form_state['values']['lastName'],
          'organization' => $form_state['values']['organization'],
          'phoneNumber' => $form_state['values']['phoneNumber'],
          'emailAddress' => $form_state['values']['emailAddress'],
          'state' => $form_state['values']['state'],
          'message' => $form_state['values']['message'],
          'page_url' => $form_state['values']['page_url'],
          'submissionDate' => $form_state['values']['submissionDate'],
        ))
        -> execute();      
      
      // break up phone number for display purposes...
      $pnum1 = substr($form_state['values']['phoneNumber'], 0, 3);
      $pnum2 = substr($form_state['values']['phoneNumber'], 3, 3);
      $pnum3 = substr($form_state['values']['phoneNumber'], 6, 4);
      $displayPhoneNumber = $pnum1.'-'.$pnum2.'-'.$pnum3;      
      $submitted_email = $form_state['values']['emailAddress'];
    
      // prepare email...
      $from = $submitted_email;
      $body[] = 'Name: '.$form_state['values']['firstName'].' '.$form_state['values']['lastName'].'&lt;br /&gt;'.
        'Email: '.$submitted_email.'&lt;br /&gt;'.
        'Organization: '.$form_state['values']['organization'].'&lt;br /&gt;'.
        'Phone: '.$displayPhoneNumber.'&lt;br /&gt;'.
        'State/Province: '.$form_state['values']['state'].'&lt;br /&gt;&lt;br /&gt;'.
        'Message: '.$form_state['values']['message'].'&lt;br /&gt;&lt;br /&gt;'.
        'Date of submission: '.$form_state['values']['submissionDate'];
      $to = 'info@meditech.com'; //seancollins@meditech.com'; //  emails will be sent to this email address!
      $params = array(
        'body' => $body,
        'subject' => 'MEDITECH Contact Request - ('.$form_state['values']['submissionDate'].')',
      );
    
      // prepare confirmation email...
      $body2[] = 'This is your confirmation email. Thanks again for contacting us. We got your email.';
      $to2 = $from;      
      $params2 = array(
        'body' => $body2,
        'subject' => 'MEDITECH Confirmation',
      );
    
      // send email 1 if all works...
      if( drupal_mail('mtContactForm_form', 'contact', $to, language_default(), $params, $from, TRUE) ){
        // send email 2 if all works...
        /*if( drupal_mail('mtContactForm_form', 'contact', $to2, language_default(), $params2, $to, TRUE) ){
          drupal_set_message('You will receive a confirmation email shortly.');
        }*/
        drupal_set_message('Thank you for contacting us at MEDITECH, we will respond to you soon.');      
      } else {
        drupal_set_message('There was an error with your submission. Please try again later.');
      }
    }


 // FUNCTION to display submissions from Contact Form...
    function mtContactForm_submissions(){
      
      $submissions = db_query('SELECT * FROM (mtContactForm) ORDER BY id DESC');
      
      // display results...
      $header = array( t('ID'), t('First Name'), t('Last Name'), t('Organization'), t('Phone Number'), t('Email Address'), t('State'), t('Message'), t('Submitted On'), t('Page URL') );
      $rows = array();
      foreach($submissions as $submission){    
        // break up phone number for display purposes...
        $pnum1 = substr($submission -> phoneNumber, 0, 3);
        $pnum2 = substr($submission -> phoneNumber, 3, 3);
        $pnum3 = substr($submission -> phoneNumber, 6, 4);
        $displayPhoneNumber = $pnum1.'-'.$pnum2.'-'.$pnum3;
        
        $rows[] = array(
          check_plain($submission -> id),
          check_plain($submission -> firstName),
          check_plain($submission -> lastName),
          check_plain($submission -> organization),
          check_plain($displayPhoneNumber),
          check_plain($submission -> emailAddress),
          check_plain($submission -> state),
          check_plain($submission -> message),
          check_plain($submission -> submissionDate),
          check_plain($submission -> page_url),
        );
      }
      $output = theme('table', array('header' => $header, 'rows' => $rows));
      return $output;
    }
